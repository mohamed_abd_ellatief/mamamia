<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKitchentagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kitchentags', function (Blueprint $table) {
              $table->increments('id');
              $table->string('title');
              $table->string('image');
              $table->timestamps();
        });

        Schema::create('kitchen_kitchentag', function (Blueprint $table) {
          $table->integer('kitchen_id')->unsigned();
          $table->integer('kitchentag_id')->unsigned();
          $table->foreign('kitchentag_id')->references('id')->on('kitchentags')->onUpdate('cascade')->onDelete('cascade');
          $table->foreign('kitchen_id')->references('id')->on('kitchens')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kitchentags');
    }
}
