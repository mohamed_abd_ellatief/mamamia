<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKitchensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kitchens', function (Blueprint $table) {
          $table->increments('id');
          $table->string('ar_title');
          $table->string('en_title');
          $table->longText('ar_description')->nullable();
          $table->longText('en_description')->nullable();
          $table->integer('kitchencategory_id')->unsigned()->nullable();
          $table->integer('user_id')->unsigned();
          $table->string('minicharge')->nullable();
          $table->string('status')->default(0);
          $table->string('openstatus')->default(1);
          $table->string('deliveryflag')->default(1);
          $table->string('deliverypriceperkilo')->nullable();
          $table->string('delivermaxbill')->nullable();
          $table->string('image')->nullable();
          $table->string('phone')->nullable();
          $table->string('phone2')->nullable();
          $table->string('address')->nullable();
          $table->string('lat')->nullable();
          $table->string('lng')->nullable();
          $table->rememberToken();
          $table->timestamps();
          $table->foreign('kitchencategory_id')->references('id')->on('kitchencategories')->onUpdate('cascade')->onDelete('cascade');
          $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kitchen');
    }
}
