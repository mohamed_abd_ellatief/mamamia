<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meals', function (Blueprint $table) {

            $table->increments('id');
            $table->string('ar_title');
            $table->string('en_title');
            $table->longText('ar_description')->nullable();
            $table->longText('en_description')->nullable();
            $table->longText('component')->nullable();
            $table->string('price');
            $table->integer('kitchentag_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('status')->default('0');
            $table->integer('openstatus')->default('1');
            $table->string('neededtime')->nullable();
            $table->string('image')->nullable();



            $table->timestamps();


            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('kitchentag_id')->references('id')->on('kitchentags')->onUpdate('cascade')->onDelete('cascade');

        });
        Schema::create('kitchen_meal', function (Blueprint $table) {
          $table->integer('kitchen_id')->unsigned();
          $table->integer('meal_id')->unsigned();
          $table->foreign('kitchen_id')->references('id')->on('kitchens')->onUpdate('cascade')->onDelete('cascade');
          $table->foreign('meal_id')->references('id')->on('meals')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meals');
    }
}
//many to many with timecargories
//many to many with kitchens
