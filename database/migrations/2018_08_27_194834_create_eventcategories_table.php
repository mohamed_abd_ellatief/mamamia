<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventcategories', function (Blueprint $table) {
          $table->increments('id');
            $table->string('title');
            $table->timestamps();
        });
        Schema::create('meal_eventcategory', function (Blueprint $table) {
          $table->integer('meal_id')->unsigned();
          $table->integer('eventcategory_id')->unsigned();
          $table->foreign('eventcategory_id')->references('id')->on('eventcategories')->onUpdate('cascade')->onDelete('cascade');
          $table->foreign('meal_id')->references('id')->on('meals')->onUpdate('cascade')->onDelete('cascade');
        });
        Schema::table('meals', function (Blueprint $table) {
          $table->string('customer_num')->nullable();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventcategories');
    }
}
