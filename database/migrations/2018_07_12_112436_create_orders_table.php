<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('component');
            $table->integer('user_id')->unsigned();
            $table->integer('vendor_id')->unsigned();
            $table->string('lat');
            $table->string('lng');
            $table->string('userplace');
            $table->string('orderdate');
            $table->string('ordertime');
            $table->string('usernote');
            $table->string('vendornote');
            $table->string('neededtime');
            $table->string('totalprice');
            $table->string('distancetouser');
            $table->string('status')->defualt(0);
            $table->timestamps();







            $table->integer('kitchen_id')->unsigned();
            $table->foreign('kitchen_id')->references('id')->on('kitchens')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('vendor_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

        });

        Schema::create('order_meal', function (Blueprint $table) {
          $table->integer('meal_id')->unsigned();
          $table->integer('order_id')->unsigned();
          $table->foreign('meal_id')->references('id')->on('meals')->onUpdate('cascade')->onDelete('cascade');
          $table->foreign('order_id')->references('id')->on('orders')->onUpdate('cascade')->onDelete('cascade');
          $table->string('amount');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
