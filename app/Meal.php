<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
  public function kitchens(){
   return $this->belongsToMany('App\Kitchen');
}
public function eventcategory(){
 return $this->belongsToMany('App\Eventcategory');
}
public function timecategory(){
 return $this->belongsToMany('App\Timecategory');
}
public function vendor(){
 return $this->belongsTo('App\User');
}

function mealimages(){
  return $this->hasMany('App\Mealimage');
}

public function mealname($id){
  $Order_Meal=DB::table('order_meal')->where('order_id',$id)->first();
  if($Order_Meal){
    $meal_name=Meal::find($Order_Meal->meal_id);
  }else{
    return false;
    die();
  }
  return $meal_name;
  die();
}
public function meals($id){
  $Order_Meal=DB::table('order_meal')
  ->select('order_meal.amount','meals.ar_title','meals.en_title','meals.id','order_meal.note')
  ->join('meals','order_meal.meal_id','meals.id')
  ->where('order_meal.order_id',$id)
  ->get();
  if(count($Order_Meal)== 0){
    return false;
    die();
  }
  return $Order_Meal;
  die();
}
public function Preparation($id){
  $Order_Meal=DB::table('order_meal')
  ->select('meals.neededtime')
  ->join('meals','order_meal.meal_id','meals.id')
  ->where('order_meal.order_id',$id)
  ->max('meals.neededtime');
  // if($Order_Meal){
  //   return false;
  //   die();
  // }
  return $Order_Meal;
  die();
}

// function hours(){
//   return $this->hasMany('App\Availblehours', 'attribute_id');
// }
}
