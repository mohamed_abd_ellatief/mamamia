<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order_driver extends Model
{

    protected $table='order_driver';
    protected $fillable=['order_id','driver_id','status'];
    	public $timestamps = false;

public function orders()
{
    return $this->hasMany('App\Order','id','order_id');
  
}
public function drivers()
{
    return $this->hasMany('App\Drivers','id','driver_id');
  
}
}
