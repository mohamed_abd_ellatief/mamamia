<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eventcategory extends Model
{
    public function getCategories($id)
    {
        $Categories=Eventcategory::join('meal_eventcategory','eventcategories.id','meal_eventcategory.eventcategory_id')
        ->where('meal_eventcategory.meal_id',$id)->get();
        if($Categories){
            return $Categories;
        }
        return false;
    }
}
