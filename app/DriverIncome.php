<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriverIncome extends Model
{
		public $timestamps = false;

    protected $table='driver_income';

     public function drivers()
    {
        return $this->belongsTo('App\Drivers','driver_id');

    }
}
