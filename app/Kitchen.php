<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Kitchen extends Model
{
  use SoftDeletes;
    function kitchencategory(){
      return $this->belongsTo('App\Kitchencategory');
    }
    function vendor(){
      return $this->belongsTo('App\User');
    }
    function kitchenimages(){
      return $this->hasMany('App\Kitchenimage');
    }
    function kitchentags(){
      return $this->belongsToMany('App\Kitchentag');
    }
     function meals(){
     return $this->belongsToMany('App\Meal');
  }
  // function hours(){
  //   return $this->hasMany('App\Availblehours', 'attribute_id');
  // }

  public function getName($id){
    $Kitchen=Kitchen::find($id);
    if($Kitchen){
      return $Kitchen;
    }else{
      return false;
    }
  }

  public function Kitchens(){
    $Kitchen=Kitchen::where('user_id',Auth::user()->id)->get();
    if($Kitchen){
      return $Kitchen;
    }else{
      return false;
    }
  }

  protected $dates = ['deleted_at'];
}
