<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Events\StatusLiked;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notification;
use Auth;
class RegisterController extends Controller
{
    public function store(Request $request){

        $this->validate($request,[
            'name' => 'required|string|max:255',
            'mobilenumber' => 'required|unique:users|regex:/(05)[0-9]{8}/',
            'email' => 'string|email|max:255|nullable',
            'password' => 'required|string|min:4|confirmed',
        ]);
		   $data = new User;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->mobilenumber = $request->mobilenumber;
        $data->category = 'vendor';
        $data->isverified = '1';
        $data->password = Hash::make($request->password);
        $data->save();

        $Notification=new Notification;
        $Notification->ar_description="There is a new account named ".$request->name." waiting for activation";
        $Notification->en_description="هناك حساب جديد بأسم ".$request->name." ينتظر التفعيل";
        $Notification->type="admin";
        $Notification->views=0;
        $Notification->user_id=$data->id;
        $Notification->save();
        $Admins=User::where('category','admin')->get();
        foreach($Admins as $Admin){
            $count=Notification::where('type','admin')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count,$Admin->id));
        }
        Auth::login($data);
        return redirect('/DashBoard');
    }
}
