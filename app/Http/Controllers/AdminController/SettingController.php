<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mainsettings;


class SettingController extends Controller
{
    public function index(){
        $Setting=Mainsettings::find(1);
        return view('admin.setting.index',compact('Setting'));
    }
    public function terms(){
        $Setting=Mainsettings::find(1);
        return response()->json(["code"=>200,'Status'=>'success','terms'=>['ar_title'=>$Setting->ar_title,'en_title'=>$Setting->en_title]]);
    }

    public function update(Request $request){
        $this->validate($request,[
            'title' => 'required|max:191',
            'vat' => 'required|numeric',
            'distance' => 'required|between:0,99.99',
            'image'=>'image|mimes:jpeg,jpg,gif,png',
            'logo'=>'image|mimes:jpeg,jpg,gif,png',
        ]);
        $Setting=Mainsettings::find(1);
        if($request->title){
            $Setting->title=$request->title;
        }
        if($request->vat){
            $Setting->vat=$request->vat;
        }
        if($request->distance){
            $Setting->distance=$request->distance;
        }
        if($request->mobilenumber){
            $Setting->mobilenumber=$request->mobilenumber;
        }

        if($request->profitRate){
            $Setting->profitRate=$request->profitRate;
        }
        if($request->ar_title){
            $Setting->ar_title=$request->ar_title;
        }
        if($request->en_title){
            $Setting->en_title=$request->en_title;
        }
        if($file=$request->file('image'))
        {
            $name=time().'.'.$file->getClientOriginalExtension();
            $file->move('cpanel/upload/mainSetting',$name);
            $Setting->image=$name;
        }
        if($file=$request->file('logo'))
        {
            $name=time().'.'.$file->getClientOriginalExtension();
            $file->move('cpanel/upload/mainSetting',$name);
            $Setting->logo=$name;
        }
        
        try{
            $Setting->save();
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        return redirect()->back()->with('message','Success');
    }
}
