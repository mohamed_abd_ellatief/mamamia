<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\IncomeTax;
use Auth;
class IncomeTaxesController extends Controller
{
    public function index(){

        $Taxes = IncomeTax::OrderBy('created_at','desc')->get();
        return view('admin.IncomeTax.index',compact('Taxes'));
    }

        public function edit(Request $request){
            $Kitchen=$request->id;
            return view('admin.ProfitRate.model',compact('Kitchen'));
        }

    public function store(Request $request){
        // return $request->all();
        $this->validate($request,[
            'price' => 'required',
            'kitchen_id' => 'required',
        ]);
    if($request->price == 0){
                return redirect()->back()->with('message','Failed');
            }
      $data = new IncomeTax;
      $data->price=$request->price;
      $data->kitchen_id=$request->kitchen_id;
      $data->created_by=Auth::user()->id;
      $data->save();

      try {

      } catch (\Exception $e) {

      }
      return back()->with('message','true');
    }
}
