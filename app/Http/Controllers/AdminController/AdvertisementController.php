<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Advertisement;
use Carbon\Carbon;

class AdvertisementController extends Controller
{
    Public function index(){
        $now=new Carbon();
        $first=Advertisement::
        whereDate('startdate','<',$now->format('Y-m-d'))
        ->whereDate('enddate','>',$now->format('Y-m-d'))
        ->orderby('status','desc');
        $Advertisements=Advertisement::
        whereDate('startdate','>',$now->format('Y-m-d'))
        ->orwhereDate('enddate','<',$now->format('Y-m-d'))
        ->union($first)
        ->orderby('status','desc')->get();

        return view('admin.Advertisement.index',compact('Advertisements'));
    }

    Public function store(Request $request){

        $this->validate($request,[
            'start' => 'required|date',
            'end' => 'required|date',
            'image'=>'required|image|mimes:jpeg,jpg,gif,png',
        ]);
        $Advertisement=new Advertisement;
        if($request->en_title){
            $Advertisement->en_title=$request->en_title;
        }
        if($request->ar_title){
            $Advertisement->ar_title=$request->ar_title;
        }
        $Advertisement->status="on";
        $Advertisement->startdate=date('Y-m-d',strtotime($request->start));
        $Advertisement->enddate=date('Y-m-d',strtotime($request->end));
        if($request->hasfile('image'))
        {
            if($file=$request->file('image'))
            {
                $name=time().'.'.$file->getClientOriginalExtension();
                $file->move('cpanel/upload/advertisement',$name);
                $Advertisement->image=$name;
            }
        }
        try{
            $Advertisement->save();
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        return redirect()->back()->with('message','Success');
    }

    Public function UpdateStatusAdvert(Request $request){
        $Advertisement=Advertisement::find($request->id);
        if($Advertisement->status == "off"){
            $Advertisement->status="on";
        }else{
            $Advertisement->status="off";
        }
        $Advertisement->save();
    }

    Public function delete(Request $request){
        try{
            Advertisement::whereIn('id',$request->id)->delete();
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        return redirect()->back()->with('message','Success');
    }

    public function edit(Request $request){
        $Advertisement=Advertisement::find($request->id);
        return view('admin.Advertisement.model',compact('Advertisement'));
    }

    Public function update(Request $request){

        $this->validate($request,[
            'id'   =>'required',
            'start'=>'required|date',
            'end' => 'required|date',
            'image'=>'image|mimes:jpeg,jpg,gif,png',
        ]);
        $Advertisement=Advertisement::find($request->id);
        if($request->en_title){
            $Advertisement->en_title=$request->en_title;
        }
        if($request->ar_title){
            $Advertisement->ar_title=$request->ar_title;
        }
        $Advertisement->startdate=date('Y-m-d',strtotime($request->start));
        $Advertisement->enddate=date('Y-m-d',strtotime($request->end));
        if($request->hasfile('image'))
        {
            if($file=$request->file('image'))
            {
                $name=time().'.'.$file->getClientOriginalExtension();
                $file->move('cpanel/upload/advertisement',$name);
                $Advertisement->image=$name;
            }
        }
        try{
            $Advertisement->save();
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        return redirect()->back()->with('message','Success');
    }
}
