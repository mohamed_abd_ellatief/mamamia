<?php

namespace App\Http\Controllers\AdminController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
use App\Notification;

class VendorController extends Controller
{
    public function index(){
        $first=User::where('category','vendor')
        ->where('status','1')
        ->orderby('created_at','desc');
        $Vendors=User::where('category','vendor')
        ->orderby('created_at','desc')
        ->get();
        return view('admin.vendors.index',compact('Vendors'));
    }

	public function Vendor($id){

		  $first=User::where('category','vendor')
        ->where('status','1')
        ->orderby('created_at','desc');
        $Vendors=User::where('id',$id)->where('category','vendor')
        ->where('status','0')
        ->orderby('created_at','desc')
        ->union($first)->get();
		        return view('admin.vendors.index',compact('Vendors'));

	}

    public function store(Request $request){
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'mobilenumber' => 'required|unique:users|regex:/(05)[0-9]{8}/',
            'password' => 'required|string|min:4',
            'image' => 'mimes:jpeg,jpg,gif,png'
        ]);
        $Vendor=new User;
        $Vendor->name=$request->name;
        $Vendor->mobilenumber=$request->mobilenumber;
        $Vendor->email=$request->email;
        $Vendor->category="vendor";
        $Vendor->password=Hash::make($request->password);
        $Vendor->status=1;
        $Vendor->isverified=1;
        if($file=$request->file('image'))
        {
            $name=time().'.'.$file->getClientOriginalExtension();
            $file->move('cpanel/upload/user',$name);
            $Vendor->image=$name;
        }
        try{
            $Vendor->save();
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        return redirect()->back()->with('message','Success');
    }

    public function UpdateStatusVendor(Request $request){
        $User=User::find($request->id);
        if($User->status == '1'){
            $User->status="0";
        }else{
            $User->status="1";
        }
        $User->save();

		$datas = Notification::where('user_id',$request->id)->get();
		foreach($datas as $a){
		$data =	Notification::find($a->id);
		$data->views=1;
		$data->save();
		}
        return response($b);
    }

    public function edit(Request $request){
        $Vendor=User::find($request->id);
        return view('admin.vendors.model',compact('Vendor'));
    }

    public function update(Request $request){
        $OldVendor=User::find($request->id);
        $NewVendor=User::where('mobilenumber',$request->mobilenumber)->where('category','vendor')->first();
        if($NewVendor){
            if($OldVendor->mobilenumber == $NewVendor->mobilenumber){
                if($request->password){
                    $this->validate($request,[
                        'id'=>'required',
                        'name' => 'required|string|max:255',
                        'password' => 'required|string|min:4',
                        'mimes:jpeg,jpg,gif,png'
                    ]);
                }else{
                    $this->validate($request,[
                        'id'=>'required',
                        'name' => 'required|string|max:255',
                        'mimes:jpeg,jpg,gif,png'
                    ]);
                }
            }else{
                return redirect()->back()->with('message','Founded');
            }
        }else{
            if($request->password){
                $this->validate($request,[
                    'id'=>'required',
                    'name' => 'required|string|max:255',
                    'mobilenumber' => 'regex:/(05)[0-9]{8}/',
                    'password' => 'required|string|min:4',
                    'mimes:jpeg,jpg,gif,png'
                ]);
            }else{
                $this->validate($request,[
                    'id'=>'required',
                    'name' => 'required|string|max:255',
                    'mobilenumber' => 'regex:/(05)[0-9]{8}/',
                    'mimes:jpeg,jpg,gif,png'
                ]);
            }
        }
        $User=User::find($request->id);
        $User->name=$request->name;
        $User->mobilenumber=$request->mobilenumber;
        $User->email=$request->email;
        if($request->password){
            $User->password=Hash::make($request->password);
        }
        if($file=$request->file('image'))
        {
            $name=time().'.'.$file->getClientOriginalExtension();
            $file->move('cpanel/upload/user',$name);
            $User->image=$name;
        }
        try{
            $User->save();
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        return redirect()->back()->with('message','Success');
    }
    public function delete($dataList)
    {
      
        // if()
        // {
            //return "success";
      try {


           $user= User::find($dataList);
       $user->delete();
          return redirect()->back()->with('message', 'Success');
             }
        catch (Exception $e) {

            $erro= 'هذا القسم مرتبط بجدول الماركات ولا يمكن حذفه الا بعد حذف جميع الماركات المرتبطة به';
            return redirect()->back()->with('message' ,'Failed' );
        }
        // return redirect()->back()->with('message','Success');
        // }else{
   // return redirect()->back()->with('message','Failed');


        

    }
}
