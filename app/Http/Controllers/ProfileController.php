<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use Auth;

class ProfileController extends Controller
{
    public function profile(){
        return view('cpanel.profile');
    }

    public function update(Request $request){
    
        $v = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'mobilenumber' => 'required|regex:/(05)[0-9]{8}/',
            'email' => 'email|nullable',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);
        if ($v->fails())
            {
            return redirect()->back()->with('message','Failed');
            }
        if($request->mobilenumber != Auth::user()->mobilenumber){
            $v = Validator::make($request->all(), [
            'mobilenumber' => 'unique:users',
            ]);
            if ($v->fails())
            {
            return redirect()->back()->with('message','Failed');
            }
        }
        
        $User=User::find(Auth::user()->id);
        $User->name=$request->name;
        $User->mobilenumber=$request->mobilenumber;

        if($file=$request->file('image')){
        $name=time().'.'.$file->getClientOriginalExtension();
        $file->move('cpanel/upload/user',$name);
        $User->image=$name;
        }

        try{
        $User->save();
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        return redirect()->back()->with('message','Success');

    }
}
