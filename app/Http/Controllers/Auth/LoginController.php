<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/DashBoard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request){
        $mobilenumber=$request->mobilenumber;
        $password=$request->password;
        if (Auth::attempt(['mobilenumber' => $mobilenumber, 'password' => $password,'category'=>'admin']))
        {
            return redirect('/');
        }
        elseif (Auth::attempt(['mobilenumber' => $mobilenumber, 'password' => $password,'category'=>'vendor','status'=>'1']))
        {
            return redirect('DashBoard');
        }
        elseif (Auth::attempt(['mobilenumber' => $mobilenumber, 'password' => $password,'category'=>'vendor','status'=>'0']))
        {
            return redirect('/');
        }
        elseif (Auth::attempt(['mobilenumber' => $mobilenumber, 'password' => $password,'category'=>'cashier','status'=>'1']))
        {
            return redirect('orders_cashier');
        }
        elseif (Auth::attempt(['mobilenumber' => $mobilenumber, 'password' => $password,'category'=>'cashier','status'=>'0']))
        {
            return redirect('/');
        }
        else{
            return redirect('login');
        }
    }
    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }
}
