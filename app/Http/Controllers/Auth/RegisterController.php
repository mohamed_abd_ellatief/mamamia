<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Notification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Events\StatusLiked;
use DB;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/SaveNotifaction';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'mobilenumber' => 'required|unique:users|regex:/(05)[0-9]{8}/',
            'email' => 'string|email|max:255|nullable',
            'password' => 'required|string|min:4|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

		$Notification=new Notification;
        $Notification->ar_description="There is a new account named ".$data['name']." waiting for activation";
        $Notification->en_description="هناك حساب جديد بأسم ".$data['name']." ينتظر التفعيل";
        $Notification->type="admin";
        $Notification->views=0;
        $Notification->save();
        $Admins=User::where('category','admin')->get();
        foreach($Admins as $Admin){
            $count=Notification::where('type','admin')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count,$Admin->id));
        }
		
		
        return User::create([
            'name' => $data['name'],
            'category' => 'vendor',
			'not_id' => DB::getPdo()->lastInsertId(),
            'isverified' => '1',
            'mobilenumber' => $data['mobilenumber'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
	
	
    }
}
