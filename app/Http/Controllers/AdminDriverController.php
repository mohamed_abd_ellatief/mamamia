<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kitchen;
use App\Drivers;
use App\Order;
use App\order_driver;
use App\DriverVerefication;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Hash;
use DB;
use App\Meal;
use App\Notification;
use App\Additions;
use Carbon\Carbon;
use App\Events\StatusLiked;
use App\User;
use App\Cashier;
use App\Verndorrule;
use App\Userpoint;
use App\DriverIncome;
class AdminDriverController extends Controller
{
  public function index()
      {
        $drivers=Drivers::all();
        return view('admin.driver.drivers',compact('drivers'));
      }    

      public function driver_active(Request $request)
      {

        $user=Drivers::find($request->id);
        $to=$user->phone;
    $msg= str_replace(' ', '%20', 'مرحبا في ماماميا درايفر م تنشيط حسابك سجل دخولك وزود دخلك ');
   
               $url = "http://www.jawalbsms.ws/api.php/sendsms?user=yahyainvetechs&pass=verificationSmS3&to=$to&message=$msg&sender=Mammamia";
   
               $ch = curl_init($url); // init the curl with jawalb API url
               curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
               curl_setopt($ch, CURLOPT_HEADER, 0);
               $data = curl_exec($ch);  //run curl api call, on success curl call, $data will contain the API call response
               curl_close($ch);
        $user->status=1;
        $user->save();
        $order_driver=order_driver::where('driver_id',$request->id)->where('status','R')->delete();
        
        return redirect()->back()->with('message', 'Success');

      }
       public function driver_dis_active(Request $request)
      {
        
            
        $user=Drivers::find($request->id);
        $user->status=0;
        $user->save();
        return redirect()->back()->with('message', 'Success');

      }
        public function DeleteDriver($id)
  {

      try {


          Drivers::find($id)->delete();
          return redirect()->back()->with('message', 'Success');
             }
        catch (Exception $e) {

            $erro= 'هذا القسم مرتبط بجدول الماركات ولا يمكن حذفه الا بعد حذف جميع الماركات المرتبطة به';
            return redirect()->back()->with('message' ,'Failed' );
        }

  }
          public function Delete_multi_Driver(Request $request)
  {
       // return  $request->arr;

      try {
        for ($i=0; $i <count($request->arr) ; $i++) { 
          Drivers::find($request->arr[$i])->delete();
        }

               return redirect()->back()->with('message', 'Success');
             }
        catch (Exception $e) {

            return redirect()->back()->with('message' ,'Failed' );
        }

  }


  public function add_driver_form()
  {
    return view('admin.driver.add_form');
  }


  public function add_driver_submit(Request $request)
  {
   // return $request->all();
      $validator = Validator::make($request->all(), [
            'username' => 'required|max:255',
            'phone' => 'required|unique:drivers,phone',
            'password' => 'required|max:255',
            'lisence_image' => 'required|mimes:jpeg,png,jpg,gif',
        ]);
        
        if ($validator->fails())
        {
          return redirect()->back()->with('errors',$validator->errors());
        }
        else
        {
          $driver=new Drivers;
          $driver->username=$request->username;
          $driver->phone=$request->phone;
          $driver->lisence_number=$request->lisence_number;
          $driver->password=Hash::make($request->password);
           if (request()->hasFile('lisence_image')) {
            $file = request()->file('lisence_image');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('./cpanel/upload/user/', $fileName);    
            $driver->lisence_image = $fileName;
            $driver->save();
          return redirect()->back()->with('message','done');

          }
        }
  }

  public function add_driver_formIncome()
  {
    $drivers=Drivers::all();
    return view('admin.driver.add_form_Income',compact('drivers'));
  }


  public function add_driver_submitIncome(Request $request)
  {
   // return $request->all();
      $validator = Validator::make($request->all(), [
            'username' => 'required|unique:driver_income,driver_id',
        ]);
        
        if ($validator->fails())
        {
          return redirect()->back()->with('error','error');
        }
        else
        {
       if ($request->payed > $request->total) {
      return redirect()->back()->with('warning','warning');
       }
       else
       {
          $driver=new DriverIncome;

          $driver->driver_id=$request->username;
          $driver->total_income =$request->total;
          $driver->paid_amount=$request->payed; 
          $driver->remaining_balance=$request->total-$request->payed;
          $driver->save();
          return redirect()->back()->with('message','done');

       }
        
          }
        
  }

  public function driver_place($lat,$lng)
  {
    $lat=$lat;
    $lng=$lng;
    return view('admin.driver.map',compact('lat','lng'));
  }
  public function edit($id)
  {
    $driver=Drivers::find($id);
    return view('admin.driver.edit',compact('driver'));

  }
  public function update_driver_submit($id,Request $request)
  {
   //= return $request->all();
    $driver=Drivers::find($id);
    //return $driver;
      $validator = Validator::make($request->all(), [
             'phone' => 'unique:drivers,phone,'.$id,
                     ]);
        
        if ($validator->fails())
        {
          return redirect()->back()->with('errors',$validator->errors());
        }
        else
        {
          
          $driver->username=$request->username;
          $driver->phone=$request->phone;
          $driver->lisence_number=$request->lisence_number;
          if (isset($request->password)) {
          $driver->password=Hash::make($request->password);
          }
           if (request()->hasFile('lisence_image')) {
            $file = request()->file('lisence_image');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('./cpanel/upload/user/', $fileName);    
            $driver->lisence_image = $fileName;


          }
        }
                    $driver->update();
          return redirect()->back()->with('message','done');
  }
  public function driver_income()
  {
    $drivers=DriverIncome::with('drivers')->get();
   return view('admin.driver.driver_income',compact('drivers'));
  }

  public function pay(Request $request)
  {
    $driver=DriverIncome::where('driver_id',$request->driver_id)->get();
    //return $driver;
    $driver[0]->paid_amount+=$request->pay;
    $driver[0]->remaining_balance-=$request->pay;
    $driver=DriverIncome::where('driver_id',$request->driver_id)->update([
      'paid_amount'=>$driver[0]->paid_amount,
      'remaining_balance'=>$driver[0]->remaining_balance
    ]);
     return redirect()->back()->with('message','done');


  }

     public function DeleteDriverIncome($id)
  {

      try {


          DriverIncome::find($id)->delete();
          return redirect()->back()->with('message', 'Success');
             }
        catch (Exception $e) {

            $erro= 'هذا القسم مرتبط بجدول الماركات ولا يمكن حذفه الا بعد حذف جميع الماركات المرتبطة به';
            return redirect()->back()->with('message' ,'Failed' );
        }

  }
          public function Delete_multi_DriverIncome(Request $request)
  {
       // return  $request->arr;

      try {
        for ($i=0; $i <count($request->arr) ; $i++) { 
          DriverIncome::find($request->arr[$i])->delete();
        }

               return redirect()->back()->with('message', 'Success');
             }
        catch (Exception $e) {

            return redirect()->back()->with('message' ,'Failed' );
        }

  }
  public function driver_order()
  {
    $order_driver=order_driver::with(['orders','drivers'])->get();
    // for ($i=0; $i < count($order_driver); $i++) { 
    //   echo $order_driver[$i]->orders."<br>";
    //   echo $order_driver[$i]->drivers."<br>";
    // }
   

     return view('admin.driver.driver_order',compact('order_driver'));

  }
  public function all_driver_order()
  {
    $orders=Order::with('drivers')->paginate(10);
    $order_driver=order_driver::with(['orders','drivers'])->get();
    // for ($i=0; $i < count($order_driver); $i++) { 
    //   echo $order_driver[$i]->orders."<br>";
    //   echo $order_driver[$i]->drivers."<br>";
    // }
   
  // return $orders[1]->drivers;
    
     return view('admin.driver.all_driver_order',compact(['orders','order_driver']))->withModel('order_driver');

  }
}
?>