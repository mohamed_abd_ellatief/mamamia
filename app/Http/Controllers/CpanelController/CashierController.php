<?php

namespace App\Http\Controllers\CpanelController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Cashier;
use App\Kitchen;
use App\User;
use Auth;
use DB;


class CashierController extends Controller
{
    public function index($id)
    {
        $Restaurant=Kitchen::find($id);
        $Kitchens=Kitchen::where('user_id',Auth::user()->id)->get();
        $Cashiers=Cashier::select('users.*','cashiers.id as master_id')
        ->join('users','cashiers.cashier_id','users.id')
        ->join('kitchens','cashiers.kitchen_id','kitchens.id')
        ->where('cashiers.vendor_id',Auth::user()->id)
        ->where('cashiers.kitchen_id',$id)
        ->get();
        return view('cpanel.cashiers.index',compact('Cashiers','Restaurant','Kitchens'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'kitchen_id' => 'required',
            'mobilenumber' => 'required|unique:users|regex:/(05)[0-9]{8}/',
            'password' => 'required|string|min:4',
        ]);
        $User=new User;
        $User->name=$request->name;
        $User->mobilenumber=$request->mobilenumber;
        $User->email=$request->email;
        $User->password=Hash::make($request->password);
        $User->status=1;
        $User->isverified=1;
        $User->category="cashier";
        if($file=$request->file('image'))
        {
            $name=time().'.'.$file->getClientOriginalExtension();
            $file->move('cpanel/upload/user',$name);
            $User->image=$name;
        }
        try{
            $User->save();
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        $Cashier=new Cashier;
        $Cashier->kitchen_id=$request->kitchen_id;
        $Cashier->cashier_id=$User->id;
        $Cashier->vendor_id=Auth::user()->id;
        try{
            $Cashier->save();
        } catch (\Exception $e) {
            echo 'hello 2';
            die();
            return redirect()->back()->with('message','Failed');
        }
        return redirect()->back()->with('message','Success');
    }

    public function UpdateStatusCashier(Request $request){
        $User=User::find($request->id);
        if($User->status == '1'){
            $User->status="0";
        }else{
            $User->status="1";
        }
        $User->save();
    }

    public function UpdateKitchenCashier(Request $request){
        $Cashier=Cashier::find($request->id);
        $Cashier->kitchen_id=$request->kitchen_id;
        $Cashier->save();
    }

    public function edit(Request $request){
        $Cashier=User::find($request->id);
        return view('cpanel.cashiers.model',compact('Cashier'));
    }

    public function update(Request $request){
        $OldCashier=User::find($request->id);
        $NewCashier=User::where('mobilenumber',$request->mobilenumber)->where('category','cashier')->first();
        if($NewCashier){
            if($OldCashier->mobilenumber == $NewCashier->mobilenumber){
                if($request->password){
                    $this->validate($request,[
                        'id'=>'required',
                        'name' => 'required|string|max:255',
                        'password' => 'required|string|min:6',
                    ]);    
                }else{
                    $this->validate($request,[
                        'id'=>'required',
                        'name' => 'required|string|max:255',
                    ]);    
                }
            }else{
                return redirect()->back()->with('message','Founded');
            }
        }else{
            if($request->password){
                $this->validate($request,[
                    'id'=>'required',
                    'name' => 'required|string|max:255',
                    'mobilenumber' => 'regex:/(05)[0-9]{8}/',
                    'password' => 'required|string|min:6',
                ]);
            }else{
                $this->validate($request,[
                    'id'=>'required',
                    'name' => 'required|string|max:255',
                    'mobilenumber' => 'regex:/(05)[0-9]{8}/',
                ]);
            }
        }
        $User=User::find($request->id);
        $User->name=$request->name;
        $User->mobilenumber=$request->mobilenumber;
        $User->email=$request->email;
        if($request->password){
            $User->password=Hash::make($request->password);
        }
        if($file=$request->file('image'))
        {
            $name=time().'.'.$file->getClientOriginalExtension();
            $file->move('cpanel/upload/user',$name);
            $User->image=$name;
        }
        try{
            $User->save();
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        return redirect()->back()->with('message','Success');
    }
}
