<?php

namespace App\Http\Controllers\CpanelController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Additions;
use Auth;
class AdditionsController extends Controller
{
    public function index($id){
            $Additions = Additions::where('Meal_id',$id)->get();
        return view('cpanel.Addition.index',compact('Additions','id'));
    }

    public function store(Request $request){


        $this->validate($request,[
            'en_title' => 'required|max:191',
            'ar_title' => 'required|max:191',
            'price'=>'required',
            'Meal_id'=>'required',
        ]);


        $data = new Additions;
        $data->ar_title=$request->ar_title;
        $data->en_title=$request->en_title;
        $data->price=$request->price;
        $data->Meal_id=$request->Meal_id;
        $data->confirm=$request->confirm;
        $data->created_by=Auth::user()->id;
        try {
            $data->save();
          } catch (\Exception $e) {
            return redirect()->back()->with('error_message','هناك خطأ فى عملية الاضافة');
        }
          return redirect()->back()->with('message','تمت عملية الاضافة بنجاح');


    }

    public function delete(Request $request)
    {
      try{
        Additions::whereIn('id',$request->id)->delete();
    } catch (\Exception $e) {
        return response()->json(['message'=>'Failed']);
    }
    return response()->json(['message'=>'Success']);
  
  
        }
  
    public function edit(Request $request){
  
      $Additions = Additions::find($request->id);
      return view('cpanel.Addition.model',compact('Additions'));
  }
  
  public function update(Request  $request){
  
      $this->validate(request(),[
        'en_title' => 'required|max:191',
        'ar_title' => 'required|max:191',
        'price'=>'required',
    ]);
  
      $data =  Additions::find($request->id);
        $data->ar_title=$request->ar_title;
        $data->en_title=$request->en_title;
        $data->price=$request->price;
        $data->confirm=$request->confirm;
     $data->updated_by=Auth::user()->id;
  
  
  try {
    $data->save();

    } catch (\Exception $e) {
      return redirect()->back()->with('error_message','هناك خطأ فى عملية الاضافة');
    }
    return redirect()->back()->with('message','تمت عملية الاضافة بنجاح');
  
  }
  }
