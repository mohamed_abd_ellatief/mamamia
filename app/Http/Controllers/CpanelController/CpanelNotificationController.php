<?php

namespace App\Http\Controllers\CpanelController;

use Request;
use App\Http\Controllers\Controller;
use App\Order;
use Auth;
use App\Notification;
use App\Events\StatusLiked;
use DB;
use App\User;
class CpanelNotificationController extends Controller
{
    public function NotificationCount(){
        return view('cpanel.notification.count');
    }
    public function NotificationData(){
        return view('cpanel.notification.Data');
    }
    public function SaveNotifaction(){
		$User = User::orderBy('id', 'desc')->first();
    $Notification=new Notification;
        $Notification->ar_description="There is a new account named ".$User->name." waiting for activation";
        $Notification->en_description="هناك حساب جديد بأسم ".$User->name." ينتظر التفعيل";
        $Notification->type="admin";
        $Notification->views=0;
		$Notitication->user_id2=$User->id;
        $Notification->save();
        $Admins=User::where('category','admin')->get();
        foreach($Admins as $Admin){
            $count=Notification::where('type','admin')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count,$Admin->id));
        }
		return view('/Dashbourd');
	}
		
    public function index(){
        if(Request::segment(1) == 'ar'){
            $Orders=Order::select('orders.id','totalprice','type','kitchens.ar_title as kitchen_title','orders.en_partyname')
            ->join('kitchens','orders.kitchen_id','kitchens.id')
            ->where('vendor_id',Auth::user()->id)
            ->whereIn('orders.status',[0,2])
            ->get();
            foreach($Orders as $Order){
                if($Order->type == "meal"){
                    $title=DB::table('order_meal')
                    ->join('meals','order_meal.meal_id','meals.id')
                    ->where('order_id',$Order->id)
                    ->select('ar_title')
                    ->first();
					if($title){
                    $Order->title=$title->ar_title;
					}
                }else{
                    $Order->title=$Order->en_partyname;
                }
            }
        }else{
            $Orders=Order::select('orders.id','totalprice','type','kitchens.en_title as kitchen_title','orders.en_partyname')
            ->join('kitchens','orders.kitchen_id','kitchens.id')
            ->where('vendor_id',Auth::user()->id)
            ->whereIn('orders.status',[0,2])
            ->get();
          foreach($Orders as $Order){
               if($Order->type == "meal"){
                 $title=DB::table('order_meal')
                  ->join('meals','order_meal.meal_id','meals.id')
                    ->where('order_id',$Order->id)
                    ->select('en_title')
                   ->first();
				   				   if($title){
                    $Order->title=$title->en_title;		
								   }
                }else{
                    $Order->title=$Order->en_partyname;
                }
             }
        }
        return view('cpanel.notification.index',compact('Orders'));
    }

    
}
