<?php

namespace App\Http\Controllers\CpanelController\CpanelUsersControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class CpanelVendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $allvendors=User::where('category','vendor')->get();
        return view('cpanel.users.vendor.index',compact('allvendors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $usertoshow=User::find($id);
      $usertoshow->kitchen;

      return view('cpanel.users.vendor.show',compact('usertoshow'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $usertoedite=User::find($id);
          return view('cpanel.users.vendor.edit',compact('usertoedite'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $model="App\User";
      $arrayofindexes=array('name','email','mobilenumber');
      $pathtoupload=public_path('cpanel/dist/img/user');
      $flag="1";
      $uploadimageflag="cpanel";
      autoupdate($model,$request,$arrayofindexes,$pathtoupload,$flag,$id,$uploadimageflag);
      return redirect()->back()->with('success','vendor updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      return('destroy');

    }
    public function activatevendor($id){
          $usertoactivate=User::find($id);
          $usertoactivate->status=1;
          $usertoactivate->save();
          return redirect()->back()->with('success','vendor activated');
    }
    public function disactivatevendor($id){
          $usertodisactivate=User::find($id);
        $usertodisactivate->status=0;
        $usertodisactivate->save();
        return redirect()->back()->with('success','vendor disactivated');
    }
}
