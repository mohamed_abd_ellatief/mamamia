<?php

namespace App\Http\Controllers\CpanelController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mealimages;
class CpanelMealsimagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       // return $request->all();
       $imagepath=resource_path('assets/cpanel/dist/img/meal');
         foreach ($request->images as  $oneimage) {
           $newimage= new Mealimages();
           $newimage->image=uploadimage($oneimage,$imagepath);
           $newimage->meal_id=$request->meal_id;
           $newimage->save();
         }
         return redirect()->back()->with('success','image uploaded');

     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         //
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         //
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {   $model="App\Mealimages";
         $imagepath=resource_path('assets/cpanel/dist/img/meal');
         $flag="1";
         autodelete($model,$imagepath,$id,$flag);
         return redirect()->back()->with('success','image deleted');
     }
 }
