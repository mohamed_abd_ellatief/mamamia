<?php

namespace App\Http\Controllers\CpanelController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Order;
use App\Meal;
use App\Kitchen;
use App\Cashier;
use App\Notification;
use App\Userpoint;
use App\Verndorrule;
use App\Events\StatusLiked;
use DB;
use Carbon\Carbon;

class CpanelOrderController extends Controller
{

  public function AllOrders($id){
    $Orders=Order::where('status',$id)
    ->paginate(15);
    $ar_title=" الطلبات ";
    $en_title="Orders";
    $ar_lable="تفاصيل الطلب";
    $en_lable="Order Details";
    return view('cpanel.order.index0',compact('Orders','Restaurant','Meal_Order','ar_title','en_title','ar_lable','en_lable'));

  }
    public function index($id){
        $Orders=Order::where('kitchen_id',$id)
        ->where('status',0)
        ->where('type','meal')
        ->get();
        $Meal_Order=[];
        foreach($Orders as $Order){
            $Meal_Order[]=DB::table('order_meal')
            ->select('order_id','meal_id')
            ->where('order_id',$Order->id)
            ->first();
        }
        $Restaurant=Kitchen::find($id);
        $ar_title="الطلبات الجديدة";
        $en_title="Orders";
        $ar_lable="تفاصيل الطلب";
        $en_lable="Order Details";
        return view('cpanel.order.index',compact('Orders','Restaurant','Meal_Order','ar_title','en_title','ar_lable','en_lable'));
    }

    public function invoices($id){
        $Orders=Order::where('kitchen_id',$id)
        ->where('type','meal')
        ->where('status',1)
        ->get();
        $Meal_Order=[];
        foreach($Orders as $Order){
            $Meal_Order[]=DB::table('order_meal')->select('order_id','meal_id')->where('order_id',$Order->id)->first();
        }
        $Restaurant=Kitchen::find($id);
        $ar_title="فواتير الطلبات";
        $en_title="Invoice Orders";
        $ar_lable="تفاصيل الفاتورة";
        $en_lable="Invoice Details";
        return view('cpanel.order.index',compact('Orders','Restaurant','Meal_Order','ar_title','en_title','ar_lable','en_lable'));
    }

    public function archives($id){
        $Orders=Order::where('kitchen_id',$id)
        ->where('type','meal')
        ->whereIn('status',[1,-1])
        ->get();
        $Meal_Order=[];
        foreach($Orders as $Order){
            $Meal_Order[]=DB::table('order_meal')->select('order_id','meal_id')->where('order_id',$Order->id)->first();
        }
        $Restaurant=Kitchen::find($id);
        $ar_title="أرشيف الطلبات";
        $en_title="Archive Orders";
        $ar_lable="تفاصيل الطلب";
        $en_lable="Order Details";
        return view('cpanel.order.index',compact('Orders','Restaurant','Meal_Order','ar_title','en_title','ar_lable','en_lable'));
    }


    public function acceptOrder(Request $request){
        $Order=Order::find($request->id);
        $Order->vendornote=$request->vendornote;
        $Order->status=1;
        try{
            $Order->save();
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
        $Meal_id=DB::table('order_meal')->where('order_id',$request->id)->first();
        $Meal=Meal::find($Meal_id->meal_id);
        $Notification=Notification::where('order_id',$request->id)->first();
        $Notification->ar_description="تم قبول الطلب ( ".$Meal->ar_title." )";
        $Notification->en_description="Your Order has been accepted ( ".$Meal->en_title." )";
        $Notification->type="user";
        $Notification->views=0;
        try{
            $Notification->save();
            $count=Notification::where('user_id',$Order->user_id)
            ->where('type','user')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count,$Order->user_id));
            $count_vendor=Notification::where('vendor_id',$Order->vendor_id)
            ->where('type','vendor')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count_vendor,$Order->vendor_id));
            $Rule=Verndorrule::where('vendor_id',$Order->vendor_id)
            ->where('kitchen_id',$Order->kitchen_id)
            ->where('rule','<=',$Order->totalprice)
            ->first();
            if($Rule){   // Give Point to User
                $Point=intval(($Order->totalprice*$Rule->points)/$Rule->rule);
                $PointUser=Userpoint::where('user_id',$Order->user_id)
                ->where('vendor_id',$Order->vendor_id)
                ->where('kitchen_id',$Order->kitchen_id)
                ->first();
                if($PointUser){
                    $Userpoint=Userpoint::find($PointUser->id);
                    $Userpoint->points=$Userpoint->points+$Point;
                    $Userpoint->save();
                }else{
                    $Userpoint=new Userpoint;
                    $Userpoint->user_id=$Order->user_id;
                    $Userpoint->vendor_id=$Order->vendor_id;
                    $Userpoint->kitchen_id=$Order->kitchen_id;
                    $Userpoint->points=$Point;
                    $Userpoint->save();
                }
            }

            return back()->with('message','accepted');
        } catch (\Exception $e) {
			            return back()->with('message','Failed');

        }

    }


    public function rejectOrder(Request $request){
        $Order=Order::find($request->id);
        $Order->vendornote=$request->vendornote;
        $Order->status=-1;
        try{
            $Order->save();
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
        $Meal_id=DB::table('order_meal')->where('order_id',$request->id)->first();
        $Meal=Meal::find($Meal_id->meal_id);
        $Notification=Notification::where('order_id',$request->id)->first();
        $Notification->ar_description="تم رفض الطلب ( ".$Meal->ar_title." )";
        $Notification->en_description="Your Order has been rejected ( ".$Meal->en_title." )";
        $Notification->type="user";
        $Notification->views=0;
        try{
            $Notification->save();
            if($Order->usepoint> 0){
                $PointUser=Userpoint::where('user_id',$Order->user_id)
                ->where('vendor_id',$Order->vendor_id)
                ->where('kitchen_id',$Order->kitchen_id)
                ->first();
                if($PointUser){
                    $Userpoint=Userpoint::find($PointUser->id);
                    $Userpoint->points=$Userpoint->points+$Order->usepoint;
                    $Userpoint->save();
                }else{
                    $Userpoint=new Userpoint;
                    $Userpoint->user_id=$Order->user_id;
                    $Userpoint->vendor_id=$Order->vendor_id;
                    $Userpoint->kitchen_id=$Order->kitchen_id;
                    $Userpoint->points=$Order->usepoint;
                    $Userpoint->save();
                }

            }
            $count=Notification::where('user_id',$Order->user_id)
            ->where('type','user')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count,$Order->user_id));
            $count_vendor=Notification::where('vendor_id',$Order->vendor_id)
            ->where('type','vendor')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count_vendor,$Order->vendor_id));
			            return back()->with('message','reject');
        } catch (\Exception $e) {
			            return back()->with('message','Failed');
        }

    }

	
    public function acceptOrder2(Request $request){
        $Order=Order::find($request->id);
        $Order->vendornote=$request->vendornote;
        $Order->status=1;
        try{
            $Order->save();
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
        $Meal_id=DB::table('order_meal')->where('order_id',$request->id)->first();
        $Meal=Meal::find($Meal_id->meal_id);
        $Notification=Notification::where('order_id',$request->id)->first();
        $Notification->ar_description="تم قبول الطلب ( ".$Meal->ar_title." )";
        $Notification->en_description="Your Order has been accepted ( ".$Meal->en_title." )";
        $Notification->type="user";
        $Notification->views=0;
        try{
            $Notification->save();
            $count=Notification::where('user_id',$Order->user_id)
            ->where('type','user')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count,$Order->user_id));
            $count_vendor=Notification::where('vendor_id',$Order->vendor_id)
            ->where('type','vendor')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count_vendor,$Order->vendor_id));
            $Rule=Verndorrule::where('vendor_id',$Order->vendor_id)
            ->where('kitchen_id',$Order->kitchen_id)
            ->where('rule','<=',$Order->totalprice)
            ->first();
            if($Rule){   // Give Point to User
                $Point=intval(($Order->totalprice*$Rule->points)/$Rule->rule);
                $PointUser=Userpoint::where('user_id',$Order->user_id)
                ->where('vendor_id',$Order->vendor_id)
                ->where('kitchen_id',$Order->kitchen_id)
                ->first();
                if($PointUser){
                    $Userpoint=Userpoint::find($PointUser->id);
                    $Userpoint->points=$Userpoint->points+$Point;
                    $Userpoint->save();
                }else{
                    $Userpoint=new Userpoint;
                    $Userpoint->user_id=$Order->user_id;
                    $Userpoint->vendor_id=$Order->vendor_id;
                    $Userpoint->kitchen_id=$Order->kitchen_id;
                    $Userpoint->points=$Point;
                    $Userpoint->save();
                }
            }

            return response()->json(['message'=>'accepted']);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }

    }


    public function rejectOrder2(Request $request){
        $Order=Order::find($request->id);
        $Order->vendornote=$request->vendornote;
        $Order->status=-1;
        try{
            $Order->save();
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
        $Meal_id=DB::table('order_meal')->where('order_id',$request->id)->first();
        $Meal=Meal::find($Meal_id->meal_id);
        $Notification=Notification::where('order_id',$request->id)->first();
        $Notification->ar_description="تم رفض الطلب ( ".$Meal->ar_title." )";
        $Notification->en_description="Your Order has been rejected ( ".$Meal->en_title." )";
        $Notification->type="user";
        $Notification->views=0;
        try{
            $Notification->save();
            if($Order->usepoint> 0){
                $PointUser=Userpoint::where('user_id',$Order->user_id)
                ->where('vendor_id',$Order->vendor_id)
                ->where('kitchen_id',$Order->kitchen_id)
                ->first();
                if($PointUser){
                    $Userpoint=Userpoint::find($PointUser->id);
                    $Userpoint->points=$Userpoint->points+$Order->usepoint;
                    $Userpoint->save();
                }else{
                    $Userpoint=new Userpoint;
                    $Userpoint->user_id=$Order->user_id;
                    $Userpoint->vendor_id=$Order->vendor_id;
                    $Userpoint->kitchen_id=$Order->kitchen_id;
                    $Userpoint->points=$Order->usepoint;
                    $Userpoint->save();
                }

            }
            $count=Notification::where('user_id',$Order->user_id)
            ->where('type','user')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count,$Order->user_id));
            $count_vendor=Notification::where('vendor_id',$Order->vendor_id)
            ->where('type','vendor')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count_vendor,$Order->vendor_id));
            return response()->json(['message'=>'reject']);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }

    }
    public function UpdateOrder(Request $request){
        if($request->status == 1){
            $Order=Order::find($request->id);
            $Order->vendornote=$request->vendornote;
            $Order->status=1;
            try{
                $Order->save();
            } catch (\Exception $e) {
                return redirect()->back()->with('message','Failed');
            }
            $Meal_id=DB::table('order_meal')->where('order_id',$request->id)->first();
            $Meal=Meal::find($Meal_id->meal_id);
            $Notification=Notification::where('order_id',$request->id)->first();
            $Notification->ar_description="تم قبول الطلب ( ".$Meal->ar_title." )";
            $Notification->en_description="Your Order has been accepted ( ".$Meal->en_title." )";
            $Notification->type="user";
            $Notification->views=0;
            try{
                $Notification->save();
                $count=Notification::where('user_id',$Order->user_id)
                ->where('type','user')
                ->where('views','0')
                ->count();
                event(new StatusLiked($count,$Order->user_id));
                $count_vendor=Notification::where('vendor_id',$Order->vendor_id)
                ->where('type','vendor')
                ->where('views','0')
                ->count();
                event(new StatusLiked($count_vendor,$Order->vendor_id));
                    $Rule=Verndorrule::where('vendor_id',$Order->vendor_id)
                    ->where('kitchen_id',$Order->kitchen_id)
                    ->where('rule','<=',$Order->totalprice)
                    ->first();
                    if($Rule){   // Give Point to User
                        $Point=intval(($Order->totalprice*$Rule->points)/$Rule->rule);
                        $PointUser=Userpoint::where('user_id',$Order->user_id)
                        ->where('vendor_id',$Order->vendor_id)
                        ->where('kitchen_id',$Order->kitchen_id)
                        ->first();
                        if($PointUser){
                            $Userpoint=Userpoint::find($PointUser->id);
                            $Userpoint->points=$Userpoint->points+$Point;
                            $Userpoint->save();
                        }else{
                            $Userpoint=new Userpoint;
                            $Userpoint->user_id=$Order->user_id;
                            $Userpoint->vendor_id=$Order->vendor_id;
                            $Userpoint->kitchen_id=$Order->kitchen_id;
                            $Userpoint->points=$Point;
                            $Userpoint->save();
                        }

                    }
                return redirect()->back()->with('message','accepted');
            } catch (\Exception $e) {
                return redirect()->back()->with('message','Failed');
            }
        }else{
            $Order=Order::find($request->id);
            $Order->vendornote=$request->vendornote;
            $Order->status=-1;
            try{
                $Order->save();
            } catch (\Exception $e) {
                return redirect()->back()->with('message','Failed');
            }
            if($Order->usepoint> 0){
                $PointUser=Userpoint::where('user_id',$Order->user_id)
                ->where('vendor_id',$Order->vendor_id)
                ->where('kitchen_id',$Order->kitchen_id)
                ->first();
                if($PointUser){
                    $Userpoint=Userpoint::find($PointUser->id);
                    $Userpoint->points=$Userpoint->points+$Order->usepoint;
                    $Userpoint->save();
                }else{
                    $Userpoint=new Userpoint;
                    $Userpoint->user_id=$Order->user_id;
                    $Userpoint->vendor_id=$Order->vendor_id;
                    $Userpoint->kitchen_id=$Order->kitchen_id;
                    $Userpoint->points=$Order->usepoint;
                    $Userpoint->save();
                }
            }
            $Meal_id=DB::table('order_meal')->where('order_id',$request->id)->first();
            $Meal=Meal::find($Meal_id->meal_id);
            $Notification=Notification::where('order_id',$request->id)->first();
            $Notification->ar_description="تم رفض الطلب ( ".$Meal->ar_title." )";
            $Notification->en_description="Your Order has been rejected ( ".$Meal->en_title." )";
            $Notification->type="user";
            $Notification->views=0;
            try{
                $Notification->save();
                $count=Notification::where('user_id',$Order->user_id)
                ->where('type','user')
                ->where('views','0')
                ->count();
                event(new StatusLiked($count,$Order->user_id));
                $count_vendor=Notification::where('vendor_id',$Order->vendor_id)
                ->where('type','vendor')
                ->where('views','0')
                ->count();
                event(new StatusLiked($count_vendor,$Order->vendor_id));
                return redirect()->back()->with('message','reject');
            } catch (\Exception $e) {
                return redirect()->back()->with('message','Failed');
            }
        }
    }

    public function OrderDetails(Request $request){
        $Order=Order::find($request->id);
        // return $request->id;
        $Notification=Notification::where('order_id',$request->id)->where('views',0)->where('type','vendor')->first();
        if($Notification){
        $Notification->views=1;
        $Notification->save();
        $count=Notification::where('vendor_id',$Notification->vendor_id)
        ->where('type','vendor')
        ->where('views','0')
        ->count();
        event(new StatusLiked($count,$Notification->vendor_id));
        }
        return view('cpanel.order.modal',compact('Order'));
    }


    public function receipt(Request $request){
        $Order=Order::find($request->id);
        $Meals=DB::table('order_meal')
        ->join('meals','order_meal.meal_id','meals.id')
        ->where('order_meal.order_id',$request->id)
        ->get();
        return view('cpanel.order.receipt',compact('Order','Meals'));
    }


}
