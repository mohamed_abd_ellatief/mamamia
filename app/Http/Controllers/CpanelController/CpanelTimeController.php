<?php

namespace App\Http\Controllers\CpanelController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kitchen;
use App\Day;
use App\Meal;
use App\availblehours;
use Carbon\Carbon;

class CpanelTimeController extends Controller
{
    public function index(Request $request,$kitchen_id,$day_id)
    {
        $Restaurant=Kitchen::find($kitchen_id);
        $Day=Day::find($day_id);
        $Times=availblehours::where('attribute_id',$kitchen_id)
        ->where('day_id',$day_id)
        ->where('attribute_category','kitchen')
        ->get();
        $count=1;
        if($request->message){
            $message=$request->message;
            return view('cpanel.kitchens.time',compact('Day','Restaurant','Times','count','message'));
        }
      return view('cpanel.kitchens.time',compact('Day','Restaurant','Times','count'));
    }
    public function timesmeal(Request $request,$meal_id)
    {
        $Meal=Meal::find($meal_id);
        $Times=availblehours::where('attribute_id',$meal_id)
        ->where('attribute_category','meal')
        ->get();
        if($request->message){
            $message=$request->message;
            return view('cpanel.meal.time',compact('Meal','Times','message'));
        }
      return view('cpanel.meal.time',compact('Meal','Times'));
    }

    public function storeTimeToKitchen(Request $request){
        
        $this->validate($request,[
            'attribute_id' => 'required|max:191',
            'type'=>'required',
            'from' => 'date_format:H:i',
            'to' => 'date_format:H:i|after:from',
        ]);
        // $from =new Carbon($request->from);
        // $to =new Carbon($request->to);
            $availblehours=new availblehours;
            $availblehours->attribute_id=$request->attribute_id;
            $availblehours->day_id=$request->day_id;
            $availblehours->attribute_category=$request->type;
            $availblehours->from=$request->from;
            $availblehours->to=$request->to;
            try{
                $availblehours->save();
            } catch (\Exception $e) {
                return redirect()->back()->with('message','Failed');
            }
            if($request->type == "kitchen"){
                $Restaurant=Kitchen::find($availblehours->attribute_id);
                $Day=Day::find($availblehours->day_id);
                $Times=availblehours::where('attribute_id',$availblehours->attribute_id)
                ->where('day_id',$availblehours->day_id)
                ->where('attribute_category','kitchen')
                ->get();
                $count=1;
                return view('cpanel.kitchens.time',compact('Day','Restaurant','Times','count'));
            }elseif($request->type == "meal"){
                $Meal=Meal::find($availblehours->attribute_id);
                $Times=availblehours::where('attribute_id',$availblehours->attribute_id)
                ->where('attribute_category','meal')
                ->get();
              return view('cpanel.meal.time',compact('Meal','Times'));
            }
    }

    public function DeleteKitchenTimes(Request $request){
        try{
            availblehours::whereIn('id',$request->id)->delete();
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
        return response()->json(['message'=>'Success']);
    }
}
