<?php

namespace App\Http\Controllers\CpanelController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use DB;
use App\Meal;
use App\Notification;
use App\Kitchen;
use Carbon\Carbon;
use App\Events\StatusLiked;
use Auth;

class CpanelBanquetController extends Controller
{
    public function index($id){
              if(Auth::user()->category == 'admin'){

            $Orders=Order::where('kitchen_id',$id)
            ->where('type','banquet')
            ->where('status',2)
            ->get();
        }else{
            $Orders=Order::where('vendor_id',Auth::user()->id)
            ->where('kitchen_id',$id)
            ->where('type','banquet')
            ->where('status',2)
            ->get();
        }
        $Restaurant=Kitchen::find($id);
        $ar_title="طلبات ولائم العميل";
        $en_title="Banquets";
        $type="new";
        return view('cpanel.Banquet.index',compact('Orders','Restaurant','ar_title','en_title','type'));
    }

    public function invoices($id){
        $Orders=Order::where('kitchen_id',$id)
        ->where('type','banquet')
        ->where('status',1)
        ->get();
        $Restaurant=Kitchen::find($id);
        $ar_title="فواتير ولائم العميل";
        $en_title="Invoices Banquet";
        $type="invoice";
        return view('cpanel.Banquet.index',compact('Orders','Restaurant','ar_title','en_title','type'));
    }

    public function archives($id){
        $Orders=Order::where('kitchen_id',$id)
        ->where('type','banquet')
        ->whereIn('status',[1,-2,-1])
        ->get();
        $Restaurant=Kitchen::find($id);
        $ar_title="أرشيف ولائم العميل";
        $en_title="Archives Banquet";
        $type="archives";
        return view('cpanel.Banquet.index',compact('Orders','Restaurant','ar_title','en_title','type'));
    }

    public function BanquetDetails(Request $request){
        $Order=Order::find($request->id);
        $Notification=Notification::where('order_id',$request->id)->where('views',0)->where('type','vendor')->first();
        if($Notification){
            $Notification->views=1;
            $Notification->save();
            $count=Notification::where('vendor_id',$Notification->vendor_id)
            ->where('type','vendor')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count,$Notification->vendor_id));
        }
        return view('cpanel.Banquet.modal',compact('Order'));
    }

    public function rejectOrder(Request $request){
        $Order=Order::find($request->id);
        $Order->status=-1;
        try{
            $Order->save();
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
        $Notification=Notification::where('order_id',$request->id)->delete();
        $count=Notification::where('user_id',$Order->user_id)
        ->where('type','user')
        ->where('views','0')
        ->count();
        event(new StatusLiked($count,$Order->user_id));  
        $count_vendor=Notification::where('vendor_id',$Order->vendor_id)
        ->where('type','vendor')
        ->where('views','0')
        ->count();
        event(new StatusLiked($count_vendor,$Order->vendor_id));
        return response()->json(['message'=>'reject']);
    }

    public function UpdateOrder(Request $request){
        $this->validate($request,[
            'id' => 'required',
            'neededtime' => 'required',
            'deliverytotalprice' => 'required',
            'totalprice' => 'required',
        ]);
        $Order=Order::find($request->id);
        $Order->vendornote=$request->vendornote;
        $Order->neededtime=$request->neededtime;
        $Order->deliverytotalprice=$request->deliverytotalprice;
        $Order->totalprice=$request->totalprice;
        $Order->status=3;
        try{
            $Order->save();
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        $Notification=Notification::where('order_id',$request->id)->first();
        if($Notification){
            $Notification->ar_description="قدم عرض بخصوص الوليمة الخاصة بكم  ( ".$Order->en_partyname." )";
            $Notification->en_description="Sent offer for Your Banquet ( ".$Order->en_partyname." )";
            $Notification->type="user";
            $Notification->views=0;
            try{
                $Notification->save();
                $count=Notification::where('user_id',$Order->user_id)
                ->where('type','user')
                ->where('views','0')
                ->count();
                event(new StatusLiked($count,$Order->user_id));  
                $count_vendor=Notification::where('vendor_id',$Order->vendor_id)
                ->where('type','vendor')
                ->where('views','0')
                ->count();
                event(new StatusLiked($count_vendor,$Order->vendor_id));    
                return redirect()->back()->with('message','accepted');
            } catch (\Exception $e) {
                return redirect()->back()->with('message','Failed');
            }
        }else{
            return redirect()->back()->with('message','accepted');
        }
    }
}
