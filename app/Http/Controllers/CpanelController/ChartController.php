<?php

namespace App\Http\Controllers\CpanelController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Order;
use App\User;
use DB;
use Auth;
use App\Kitchen;
use App\Review;

class ChartController extends Controller
{
    public function ganntChart(Request $request){
        $BeforeLastYear = Carbon::now()->startOfYear()->subYear(2)->format('Y');
        $LastYear = Carbon::now()->startOfYear()->subYear(1)->format('Y');
        $NowYear = Carbon::now()->startOfYear()->format('Y');
        for($i=1; $i<=12; $i++){
            $Order['0'][]=Order::where('vendor_id',Auth::user()->id)
            ->where('kitchen_id',$request->kitchen_id)
            ->where('status','1')
            ->whereYear('created_at', $NowYear)
            ->whereMonth('created_at',$i )->sum('totalprice');
            
        }
        for($i=1; $i<=12; $i++){
            $Order['1'][]=Order::where('vendor_id',Auth::user()->id)
            ->where('kitchen_id',$request->kitchen_id)
            ->where('status','1')
            ->whereYear('created_at', $LastYear)
            ->whereMonth('created_at',$i )->sum('totalprice');
        }
        for($i=1; $i<=12; $i++){
            $Order['2'][]=Order::where('vendor_id',Auth::user()->id)
            ->where('kitchen_id',$request->kitchen_id)
            ->where('status','1')
            ->whereYear('created_at', $BeforeLastYear)
            ->whereMonth('created_at',$i)->sum('totalprice');
        }
        return $Order; 
    }
    public function ganntChartAdmin(Request $request){
        $BeforeLastYear = Carbon::now()->startOfYear()->subYear(2)->format('Y');
        $LastYear = Carbon::now()->startOfYear()->subYear(1)->format('Y');
        $NowYear = Carbon::now()->startOfYear()->format('Y');
        for($i=1; $i<=12; $i++){
            $Order['0'][]=Order::where('kitchen_id',$request->kitchen_id)
            ->where('status','1')
            ->whereYear('created_at', $NowYear)
            ->whereMonth('created_at',$i )->sum('totalprice');
            
        }
        for($i=1; $i<=12; $i++){
            $Order['1'][]=Order::where('kitchen_id',$request->kitchen_id)
            ->where('status','1')
            ->whereYear('created_at', $LastYear)
            ->whereMonth('created_at',$i )->sum('totalprice');
        }
        for($i=1; $i<=12; $i++){
            $Order['2'][]=Order::where('kitchen_id',$request->kitchen_id)
            ->where('status','1')
            ->whereYear('created_at', $BeforeLastYear)
            ->whereMonth('created_at',$i)->sum('totalprice');
        }
        return $Order; 
    }

    public function Kitchen(Request $request){
        $now=Carbon::now();
        $Day=$now->format('Y-m-d');
        $Month=$now->format('m');
        $Kitchens=Kitchen::select('id','ar_title','en_title','image')
        ->where('user_id',Auth::user()->id)
        ->get();
        foreach($Kitchens as $Kitchen){
            $totalPrice=Order::where('kitchen_id',$Kitchen->id);
            if($request->filter == "today"){
                $totalPrice->whereDate('created_at',$Day);
            }elseif($request->filter == "week"){   
                $totalPrice->whereDate('created_at','<=',$Day);
                $totalPrice->whereDate('created_at','>=',$now->subDays(7));
            }else{
            $totalPrice->whereMonth('created_at',$Month);
            $totalPrice->whereYear('created_at',date('Y'));
            }
            $totalPrice->sum('totalprice');
            $Kitchen->Revenue=$totalPrice->sum('totalprice');
            $Kitchen->Posts=Review::where('kitchen_id',$Kitchen->id)->count();
            $totalRate=Review::where('kitchen_id',$Kitchen->id)->sum('rate');
            if($totalRate>0){
                $Kitchen->Reviews=$totalRate/Review::where('kitchen_id',$Kitchen->id)->count();
            }else{
                $Kitchen->Reviews=0;
            }
        }
        foreach($Kitchens as $key=> $Kitchen){
            if($Kitchen->Revenue == 0){
                unset($Kitchens[$key]);
            }
        }
        $Kitchenss=[];
        foreach ($Kitchens  as $key => $value) {
            $Kitchenss[] = $value;
        }
        return response($Kitchenss);
    }

    // Top Users
    public function UsersTable(Request $request){
        $Users=User::select('id','mobilenumber','name')->where('category','user')->get();
        foreach($Users as $User){
            $Kitchen=Kitchen::select('id')->where('user_id',Auth::user()->id)->first();
            
            $User->Orders=Order::where('vendor_id',Auth::user()->id)
            ->where('user_id',$User->id)
            ->where('kitchen_id',$request->kitchen)
            ->whereMonth('created_at',$request->month)
            ->where('status',1)
            ->count();
        }
        foreach($Users as $key=> $User){
            if($User->Orders == 0){
                unset($Users[$key]);
            }
        }
        $Userss=[];
        foreach ($Users  as $key => $value) {
            $Userss[] = $value;
        }
        return  response($Userss);
    }
    
    // issues Kitchen
    public function ReviewsTable(Request $request){
        $Reviews=Review::select('users.image','users.name','reviews.*')
        ->join('users','reviews.user_id','users.id')
        ->where('reviews.kitchen_id',$request->kitchen)
        ->orderby('reviews.created_at','desc')
        ->where('rate','<=',3)
        ->where('users.category','user')
        ->limit(10)->get();
        foreach($Reviews as $Review){
            if($request->language == 'ar'){
              $find_day = array ("Saturday", "Sunday", "Monday", "Tuesday", "Wednesday" , "Thursday", "Friday");
              $replace_day = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
              $date_day=str_replace($find_day, $replace_day, $Review->created_at->format('l j \\of F Y h:i:s A'));
              $find_months = array('January','February','March','April','May','June','July','August','September','October','November','December');   
              $replace_month = array("يناير","فبراير","مارس","أبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر");
              $date_month=str_replace($find_months, $replace_month, $date_day);
              $numbers_en = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
              $numbers_ar = ["٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩"];
              $date_number=str_replace($numbers_en, $numbers_ar, $date_month);
              $time_en = ['AM','PM','of'];
              $time_ar = ['صباحاً','مساءً','من'];
              $date=str_replace($time_en, $time_ar, $date_number);
              $Review->created=$date;
            }else{
              $Review->created=$Review->created_at->format('l jS \\of F Y h:i:s A');
            }
          }
        return response($Reviews);
    }

    // Most Offer
    public function most_offer(Request $request){
        if($request->language == 'ar'){
            $Orders=Order::join('order_meal','orders.id','order_meal.order_id')
            ->join('meals','order_meal.meal_id','meals.id')
            ->join('kitchens','orders.kitchen_id','kitchens.id')
            ->where('orders.kitchen_id',$request->kitchen)
            ->whereYear('orders.created_at',date('Y'))
            ->whereMonth('orders.created_at',$request->month)
            ->where('orders.status',1)
            ->where('meals.kitchentag_id',5)
            ->distinct('order_meal.meal_id')
            ->select('order_meal.meal_id','meals.ar_title as title')
            ->get();
        }else{
            $Orders=Order::join('order_meal','orders.id','order_meal.order_id')
            ->join('meals','order_meal.meal_id','meals.id')
            ->join('kitchens','orders.kitchen_id','kitchens.id')
            ->where('orders.kitchen_id',$request->kitchen)
            ->whereYear('orders.created_at',date('Y'))
            ->whereMonth('orders.created_at',$request->month)
            ->where('orders.status',1)
            ->where('meals.kitchentag_id',5)
            ->distinct('order_meal.meal_id')
            ->select('order_meal.meal_id','meals.en_title as title')
            ->get();
        }
        foreach ($Orders as $Order) {
            $Order->qty=DB::table('order_meal')->where('meal_id',$Order->meal_id)
            ->sum('amount');
        }
        return $Orders;
    }

    // Hot Days
    Public function hotDays(Request $request)
    {
        $counts=[];
        $Orders=Order::select('id','created_at')->where('kitchen_id',$request->kitchen)->whereMonth('created_at',$request->month)->whereYear('created_at',date('Y'))->where('status','1')->orderby('created_at')->get();
        foreach ($Orders as $Order) {
            if($request->language == 'ar'){
                $find_day = array ("Saturday", "Sunday", "Monday", "Tuesday", "Wednesday" , "Thursday", "Friday");
                $replace_day = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
                $date_day=str_replace($find_day, $replace_day, $Order->created_at->format('l d-m-Y'));
                $numbers_en = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
                $numbers_ar = ["٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩"];
                $date_number=str_replace($numbers_en, $numbers_ar, $date_day);
                $counts[$date_number]=Order::where('status','1')->where('kitchen_id',$request->kitchen)->whereDate('created_at',$Order->created_at->format('Y-m-d'))->count();
            }else{
                  $counts[$Order->created_at->format('l Y-m-d')]=Order::where('status','1')->where('kitchen_id',$Kit->id)->whereDate('created_at',$Order->created_at->format('Y-m-d'))->count();
            }
            // $counts[$Order->created_at->format('l Y-m-d ')]=Order::where('status','1')->where('kitchen_id',$request->kitchen)->whereDate('created_at',$Order->created_at->format('Y-m-d'))->count();
        }
        arsort($counts);
        return $counts;
    }    
}
