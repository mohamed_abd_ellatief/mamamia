<?php

namespace App\Http\Controllers\CpanelController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kitchen_Day;
use App\Kitchen;
use App\Day;
use App\availblehours;

class CpanelDayController extends Controller
{
    public function index(Request $request,$id)
    {
        $Restaurant=Kitchen::find($id);
        $kitchen_days=Kitchen_Day::where('kitchen_id',$id)->get();
        $Days=[];
        foreach($kitchen_days as $value){
            $Days[]= Day::find($value->day_id);
        }
        $Daysss=Day::all();
        $count=1;
        if($request->message){
            $message=$request->message;
            return view('cpanel.kitchens.days',compact('Days','Restaurant','Daysss','count','message'));
        }
      return view('cpanel.kitchens.days',compact('Days','Restaurant','Daysss','count'));
    }

    public function storeDaysToKitchen(Request $request){
        // return $request->all();
        $this->validate($request,[
            'kitchen_id' => 'required|numeric',
            'day_id' => 'required',
            ]);

            $Data=Kitchen_Day::where('kitchen_id',$request->kitchen_id)
            ->where('day_id',$request->day_id)
            ->get();
            if(count($Data)>0){
                return redirect()->back()->with('message','Failed');
                die();
            }else{
                $Kitchen_Day=new Kitchen_Day;
                $Kitchen_Day->kitchen_id=$request->kitchen_id;
                $Kitchen_Day->day_id=$request->day_id;
                $Kitchen_Day->save();
                return redirect("times/$request->kitchen_id/$request->day_id?message=success");
            }
    }

    public function DeleteKitchenDays(Request $request){
        try{
            Kitchen_Day::whereIn('day_id',$request->day_id)->where('kitchen_id',$request->kitchen_id)->delete();
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
        return response()->json(['message'=>'Success']);
    }

    
}
