<?php

namespace App\Http\Controllers\CpanelController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Meal;
use App\Kitchentag;
use DB;
use App\Kitchen;
use App\Mealimage;
use App\Eventcategory;
use App\Availblehours;
use Auth;
use File;
class CpanelMealsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $kitchentags=DB::table('kitchen_kitchentag')->where('kitchen_id',$id)->pluck('kitchentag_id');
        $Tags=Kitchentag::whereIn('id',$kitchentags)->get();
        $Meals_id=DB::table('kitchen_meal')->select('meal_id')->where('kitchen_id',$id)->get();
        $Restaurant=Kitchen::find($id);
        $id=[];
        foreach($Meals_id as $Meal_id){
            $id[]=$Meal_id->meal_id;
        }
        $Meals=Meal::whereIn('id',$id)->get();
        $CategoriesMeals=Eventcategory::whereIn('id',[1,2,3])->get();
        $CategoriesBanquets=Eventcategory::whereIn('id',[6,7,8])->get();
        return view('cpanel.meal.index',compact('Meals','Tags','Restaurant','CategoriesMeals','CategoriesBanquets'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'en_title' => 'required|max:191',
            'ar_title' => 'required|max:191',
            'en_description' => 'required',
            'ar_description' => 'required',
            'tag' => 'required',
            'price'=>'required',
            'image'=>'required|mimes:jpeg,jpg,gif,png',
            'images.*' => 'mimes:jpeg,jpg,gif,png'
        ]);

        $Meal = new Meal();
        $Meal->en_title=  $request->en_title;
        $Meal->ar_title=  $request->ar_title;
        $Meal->en_description=  $request->en_description;
        $Meal->ar_description=  $request->ar_description;
        $Meal->kitchentag_id=  $request->tag;
        $Meal->component=  $request->component;
        $Meal->price=  $request->price;
        $Meal->user_id=  Auth::user()->id;
        if($request->neededtime){
            $Meal->neededtime=$request->neededtime;
        }
        $Meal->customer_num=  $request->customer_num;
        if($request->hasfile('image'))
        {
            if($file=$request->file('image'))
            {
                $name=time().'.'.$file->getClientOriginalExtension();
                $file->move('cpanel/upload/meal',$name);
                $Meal->image=$name;
            }
        }
        try{
            $Meal->save();
            DB::table('kitchen_meal')->insert(
                ['kitchen_id' => $request->Restaurant_id, 'meal_id' => $Meal->id]
            );
            if($request->hasfile('images'))
            {
                $count=1;
                foreach($request->file('images') as $file)
                {
                    $name=$count.time().'.'.$file->getClientOriginalExtension();
                    $file->move('cpanel/upload/meal',$name);
                    $Image=new Mealimage;
                    $Image->meal_id=$Meal->id;
                    $Image->image=$name;
                    $Image->save();
                    $count++;
                }
            }

            foreach($request->Eventcategory as $value){
                DB::table('meal_eventcategory')->insert(
                    ['eventcategory_id' => $value, 'meal_id' => $Meal->id]
                );
            }
            

        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        return redirect("timesmeal/$Meal->id?message=success");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $mealstoshow=Meal::find($id);
        return view('cpanel.meal.show',compact('mealstoshow'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
      $Meal=Meal::select('meals.*','kitchen_meal.kitchen_id')->where('meals.id',$request->id)->join('kitchen_meal','meals.id','kitchen_meal.meal_id')->first();
      $kitchentags=DB::table('kitchen_kitchentag')->where('kitchen_id',$Meal->kitchen_id)->pluck('kitchentag_id');
      $Tags=Kitchentag::whereIn('id',$kitchentags)->get();


      $Meal_EventCategories=DB::table('meal_eventcategory')->where('meal_id',$request->id)->select('eventcategory_id')->get();
      $array=[];
      foreach($Meal_EventCategories as $value){
          $array[]=$value->eventcategory_id;
      }

      $CategoriesMeals=Eventcategory::whereIn('id',[1,2,3])->get();
      $CategoriesBanquets=Eventcategory::whereIn('id',[6,7,8])->get();

      return view('cpanel.meal.modal',compact('Meal','Tags','array','CategoriesMeals','CategoriesBanquets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
            $this->validate($request,[
            'id' => 'required',
            'en_title' => 'required|max:191',
            'ar_title' => 'required|max:191',
            'en_description' => 'required',
            'ar_description' => 'required',
            'tag' => 'required',
            'price'=>"required|regex:/^\d*(\.\d{1,2})?$/",
            'image'=>'mimes:jpeg,jpg,gif,png',
            ]);

        $Meal =Meal::find($request->id);
        $Meal->en_title=  $request->en_title;
        $Meal->ar_title=  $request->ar_title;
        $Meal->en_description=  $request->en_description;
        $Meal->ar_description=  $request->ar_description;
        $Meal->kitchentag_id=  $request->tag;
        $Meal->component=  $request->component;
        $Meal->price=  $request->price;
        $Meal->user_id=  Auth::user()->id;
        $Meal->neededtime=  $request->neededtime;
        $Meal->customer_num=  $request->customer_num;
        if($request->hasfile('image'))
        {
            if($file=$request->file('image'))
            {
                $name=time().'.'.$file->getClientOriginalExtension();
                $file->move('cpanel/upload/meal',$name);
                $Meal->image=$name;
            }
        }
        try{
            $Meal->save();
            $Meal_EventCategory=DB::table('meal_eventcategory')->select('eventcategory_id')->where('meal_id',$request->id)->get();
            if(count($Meal_EventCategory)>0){
                DB::table('meal_eventcategory')->where('meal_id', $request->id)->delete();
            }
            if($request->Eventcategory){
                foreach($request->Eventcategory as $value){
                    DB::table('meal_eventcategory')->insert(
                        ['meal_id' => $request->id, 'eventcategory_id' => $value]
                    );
                }
            }
            
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        return redirect()->back()->with('message','Success');
            // return redirect()->back()->with('message','Success');

    }

    public function UpdateStatus(Request $request)
    {
        $MealStatus=Meal::find($request->id);
        if($MealStatus->status == "off"){
            $Meal=Meal::find($request->id);
            $Meal->status="on";
                $Meal->save();
            return response('Success');
        }else{
            $Meal=Meal::find($request->id);
            $Meal->status="off";
                $Meal->save();
                return response('error');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        try{
            Meal::whereIn('id',$request->id)->delete();
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
        return response()->json(['message'=>'Success']);
    }

    public function images($id){
        $Meal=Meal::find($id);
        $Images=Mealimage::where('meal_id',$id)->get();
        return view('cpanel.meal.images',compact('Meal','Images'));
    }

    public function Delete_images(Request $request){
        try{
            $image=Mealimage::find($request->id)->image;
            // return $image;
            $image_path = 'cpanel/upload/meal/'.$image;  // Value is not URL but directory file path
            if(File::exists($image_path)) {
                File::delete($image_path);
            }
            Mealimage::where('id',$request->id)->delete();
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
        return response()->json(['message'=>'Success']);
    }

    public function StoreImagesMeal(Request $request){
        if($request->hasfile('images'))
        {
            $count=1;
            foreach($request->file('images') as $file)
            {
                $name=$count.time().'.'.$file->getClientOriginalExtension();
                $file->move('cpanel/upload/meal',$name);
                $Image=new Mealimage;
                $Image->meal_id=$request->meal_id;
                $Image->image=$name;
                $Image->save();
                $count++;
            }
        }
        return redirect()->back()->with('message','Success');
    }
    
}
