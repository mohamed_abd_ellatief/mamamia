<?php

namespace App\Http\Controllers\CpanelController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Verndorrule;
use App\Kitchen;
use Auth;
use App\Userpoint;

class CpanelRuleController extends Controller
{
    public function index($id){
        $Rule=Verndorrule::where('kitchen_id',$id)->where('vendor_id',Auth::user()->id)->orderBy('id','desc')->first();
        $Points=Userpoint::
        select('users.name','users.mobilenumber','userpoints.points','userpoints.id')
        ->join('users','userpoints.user_id','users.id')
        ->where('userpoints.kitchen_id',$id)
        ->where('userpoints.vendor_id',Auth::user()->id)
        ->orderBy('userpoints.id','desc')
        ->get();
        $Restaurant=Kitchen::find($id);
        return view('cpanel.kitchens.rules',compact('Rule','Restaurant','Points'));
    }
    

    public function insertUpdateRule(Request $request){
        $this->validate($request,[
            'rule' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'points' => 'required|max:191',
            'discount' => 'required|regex:/^\d*(\.\d{1,2})?$/',
            'kitchen_id' => 'required',
        ]);
        $KitchenRule=Verndorrule::where('kitchen_id',$request->kitchen_id)->first();
        if($KitchenRule){
            $Rule=Verndorrule::where('kitchen_id',$request->kitchen_id)->first();
        }else{
            $Rule=new Verndorrule;
            $Rule->kitchen_id=$request->kitchen_id;
            $Rule->vendor_id=Auth::user()->id;
        }
        $Rule->rule=$request->rule;
        $Rule->points=$request->points;
        $Rule->discount=$request->discount;
        try{
            $Rule->save();
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        return redirect()->back()->with('message','Success');
    }

}
