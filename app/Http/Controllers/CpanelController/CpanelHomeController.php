<?php

namespace App\Http\Controllers\CpanelController;

use Request;
// use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Notification;
use Validator;
use DB;
use Carbon\Carbon;
use App\Kitchen;
use App\Order;
use App\Review;


class CpanelHomeController extends Controller
{
    public function index(){
     
    
      // Preformance Kitchens
      $Kitchenss=Kitchen::select('id','ar_title','en_title','image')->where('user_id',Auth::user()->id)->limit(10)->get();
      // $names = array_pluck($Kitchenss, 'ar_title','id');
      // return $names;
      foreach($Kitchenss as $Kitchen){
        $Kitchen->Revenue=Order::whereYear('created_at',date('Y'))->where('kitchen_id',$Kitchen->id)->sum('totalprice');
        $Kitchen->Posts=Review::where('kitchen_id',$Kitchen->id)->count();
        $totalRate=Review::where('kitchen_id',$Kitchen->id)->sum('rate');
        if($totalRate>0){
          $Kitchen->Reviews=$totalRate/Review::where('kitchen_id',$Kitchen->id)->count();
        }else{
          $Kitchen->Reviews=0;
        }
      }
      foreach($Kitchenss as $key=> $Kitchen){
        if($Kitchen->Revenue == 0){
            unset($Kitchenss[$key]);
        }
      }
      $Kitchens=[];
      foreach ($Kitchenss  as $key => $value) {
            $Kitchens[] = $value;
      }
      
      $ar_title_Kitchen="أداء المطعم / الوضع السنوى";
      $en_title_Kitchen="Restuarent preformance / Yearly income";


      // Top Users of Make Orders
      $Userss=User::select('id','mobilenumber','name')->where('category','user')->get();
      foreach($Userss as $User){
        $Kitchen=Kitchen::select('id')->where('user_id',Auth::user()->id)->first();
        if($Kitchen){
        $User->Orders=Order::where('vendor_id',Auth::user()->id)
        ->where('user_id',$User->id)
        ->whereMonth('created_at',date('m'))
        ->where('kitchen_id',$Kitchen->id)
        ->where('status',1)
        ->count();
        }else{
          $User->Orders=0;
        }
      }
      foreach($Userss as $key=> $User){
        if($User->Orders == 0){
            unset($Userss[$key]);
        }
      }
      $Users=[];
      foreach ($Userss  as $key => $value) {
            $Users[] = $value;
      }
    

      // Issuse Kitchen From Users
      $Kit=Kitchen::select('id','ar_title','en_title')->where('user_id',Auth::user()->id)->first();
      
      if($Kit){
        $Reviews=Review::select('users.image','users.name','reviews.*')
        ->join('users','reviews.user_id','users.id')
        ->where('reviews.kitchen_id',$Kit->id)
        ->orderby('reviews.created_at','desc')
        ->where('rate','<=',3)
        ->where('users.category','user')->get();
        foreach($Reviews as $Review){
          if(Request::segment(1) == 'ar'){
            $find_day = array ("Saturday", "Sunday", "Monday", "Tuesday", "Wednesday" , "Thursday", "Friday");
            $replace_day = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
            $date_day=str_replace($find_day, $replace_day, $Review->created_at->format('l j \\of F Y h:i:s A'));
            $find_months = array('January','February','March','April','May','June','July','August','September','October','November','December');   
            $replace_month = array("يناير","فبراير","مارس","أبريل","مايو","يونيو","يوليو","أغسطس","سبتمبر","أكتوبر","نوفمبر","ديسمبر");
            $date_month=str_replace($find_months, $replace_month, $date_day);
            $numbers_en = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
            $numbers_ar = ["٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩"];
            $date_number=str_replace($numbers_en, $numbers_ar, $date_month);
            $time_en = ['AM','PM','of'];
            $time_ar = ['صباحاً','مساءً','من'];
            $date=str_replace($time_en, $time_ar, $date_number);
            $Review->created=$date;
          }else{
            $Review->created=$Review->created_at->format('l jS \\of F Y h:i:s A');
          }
        }
      }else{
        $Reviews=[];
      }

      // Hot Days
      $counts=[];
      $Orders=Order::select('id','created_at')->whereMonth('created_at',date('m'))->whereYear('created_at',date('Y'))->where('status','1')->orderby('created_at')->get();
      if($Kit){
      foreach ($Orders as $Order) {
        if(Request::segment(1) == 'ar'){
        $find_day = array ("Saturday", "Sunday", "Monday", "Tuesday", "Wednesday" , "Thursday", "Friday");
        $replace_day = array ("السبت", "الأحد", "الإثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة");
        $date_day=str_replace($find_day, $replace_day, $Order->created_at->format('l d-m-Y'));
        $numbers_en = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        $numbers_ar = ["٠", "١", "٢", "٣", "٤", "٥", "٦", "٧", "٨", "٩"];
        $date_number=str_replace($numbers_en, $numbers_ar, $date_day);
        $counts[$date_number]=Order::where('status','1')->where('kitchen_id',$Kit->id)->whereDate('created_at',$Order->created_at->format('Y-m-d'))->count();
        }else{
          $counts[$Order->created_at->format('l Y-m-d')]=Order::where('status','1')->where('kitchen_id',$Kit->id)->whereDate('created_at',$Order->created_at->format('Y-m-d'))->count();
        }
      }
      }
      $Total_Order=array_sum($counts);
      foreach($counts as $key=> $count){
        if($count == 0){
          unset($counts[$key]);
        }
      }
      // return $counts;
      $countss=[];
      foreach ($counts  as $key => $value) {
        $countss[$key] = $value;
      }
      arsort($countss);
      

      // Most Offers
      $Offers=[];
      if($Kit){
        if(Request::segment(1) == 'ar'){
          $Offerss=Order::join('order_meal','orders.id','order_meal.order_id')
          ->join('meals','order_meal.meal_id','meals.id')
          ->join('kitchens','orders.kitchen_id','kitchens.id')
          ->where('orders.kitchen_id',$Kit->id)
          ->whereYear('orders.created_at',date('Y'))
          ->whereMonth('orders.created_at',date('m'))
          ->where('orders.status',1)
          ->where('meals.kitchentag_id',5)
          ->distinct('order_meal.meal_id')
          ->select('order_meal.meal_id','meals.ar_title as title')
          ->get();
        }else{
          $Offerss=Order::join('order_meal','orders.id','order_meal.order_id')
          ->join('meals','order_meal.meal_id','meals.id')
          ->join('kitchens','orders.kitchen_id','kitchens.id')
          ->where('orders.kitchen_id',$Kit->id)
          ->whereYear('orders.created_at',date('Y'))
          ->whereMonth('orders.created_at',date('m'))
          ->where('orders.status',1)
          ->where('meals.kitchentag_id',5)
          ->distinct('order_meal.meal_id')
          ->select('order_meal.meal_id','meals.en_title as title')
          ->get();
        }
        foreach ($Offerss as $Offer) {
          $Offer->qty=DB::table('order_meal')->where('meal_id',$Offer->meal_id)
          ->sum('amount');
        }
        foreach($Offerss as $key=> $Offer){
          if($Offer->qty == 0){
            unset($Offerss[$key]);
          }
        }
        if(count($Offerss)){
          $Offers= $Offerss->sortByDesc('qty');
        }
      }
      return view('cpanel.index',compact('Total_Order','Kitchens','ar_title_Kitchen','en_title_Kitchen','Users','Reviews','countss','Offers'));
    }
}
