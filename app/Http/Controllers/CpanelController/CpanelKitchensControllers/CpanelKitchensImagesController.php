<?php

namespace App\Http\Controllers\CpanelController\CpanelKitchensControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kitchenimage;
class CpanelKitchensImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $imagepath=resource_path('assets/cpanel/dist/img/kitchen');
        foreach ($request->images as  $oneimage) {
          $newimage= new Kitchenimage();
          $newimage->image=uploadimage($oneimage,$imagepath);
          $newimage->kitchen_id=$request->kitchen_id;
          $newimage->save();
        }
        return redirect()->back()->with('success','image uploaded');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   $model="App\Kitchenimage";
        $imagepath=resource_path('assets/cpanel/dist/img/kitchen');
        $flag="1";
        autodelete($model,$imagepath,$id,$flag);
        return redirect()->back()->with('success','image deleted');
    }
}
