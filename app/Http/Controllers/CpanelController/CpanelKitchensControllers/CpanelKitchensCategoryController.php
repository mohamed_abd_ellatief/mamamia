<?php

namespace App\Http\Controllers\CpanelController\CpanelKitchensControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kitchencategory;
class CpanelKitchensCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allkitchenscategories=Kitchencategory::paginate(5);
        return view('cpanel.kitchenscategory.index',compact('allkitchenscategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cpanel.kitchenscategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // return $request->all();
        $pathtoupload=resource_path('assets/cpanel/dist/img/kitchen/kitchencategory');
        $newkitchencategory=new Kitchencategory();
        $newkitchencategory->title=$request->title;
        $newkitchencategory->image=uploadimage($request->image,$pathtoupload);
        $newkitchencategory->save();
        return redirect()->back()->with('success','new kitchen category added');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $kitchencategorytoedit=Kitchencategory::find($id);
        return view('cpanel.kitchenscategory.edit',compact('kitchencategorytoedit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $pathtoupload=resource_path('assets/cpanel/dist/img/kitchen/kitchencategory');
      $kitchencategorytoupdate=Kitchencategory::find($id);
      $kitchencategorytoupdate->title=$request->title;
      if(isset($request->image)){
        $path=$pathtoupload.'/'.$kitchencategorytoupdate->image;
        deletefile($path);
        $kitchencategorytoupdate->image=uploadimage($request->image,$pathtoupload);
      }
      $kitchencategorytoupdate->save();
      return redirect()->back()->with('success','kitchen category updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model="App\Kitchencategory";
        $imagepath=resource_path('assets/cpanel/dist/img/kitchen/kitchencategory');
        $flag="1";
        autodelete($model,$imagepath,$id,$flag);
        return redirect()->back()->with('success','kitchen category deleted');

    }
}
