<?php

namespace App\Http\Controllers\CpanelController\CpanelKitchensControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kitchentag;

class CpanelServictagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allkitchenstags=Kitchentag::paginate(5);
        return view('cpanel.kitchenstag.index',compact('allkitchenstags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cpanel.kitchenstag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // return $request->all();
        $pathtoupload=resource_path('assets/cpanel/dist/img/kitchen/kitchentag');
        $newkitchentag=new Kitchentag();
        $newkitchentag->title=$request->title;
        $newkitchentag->image=uploadimage($request->image,$pathtoupload);
        $newkitchentag->save();
        return redirect()->back()->with('success','new kitchen tag added');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $kitchentagtoedit=Kitchentag::find($id);
        return view('cpanel.kitchenstag.edit',compact('kitchentagtoedit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $pathtoupload=resource_path('assets/cpanel/dist/img/kitchen/kitchentag');
      $kitchentagtoupdate=Kitchentag::find($id);
      $kitchentagtoupdate->title=$request->title;
      if(isset($request->image)){
        $path=$pathtoupload.'/'.$kitchentagtoupdate->image;
        deletefile($path);
        $kitchentagtoupdate->image=uploadimage($request->image,$pathtoupload);
      }
      $kitchentagtoupdate->save();
      return redirect()->back()->with('success','kitchen tag updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model="App\Kitchentag";
        $imagepath=resource_path('assets/cpanel/dist/img/kitchen/kitchentag');
        $flag="1";
        autodelete($model,$imagepath,$id,$flag);
        return redirect()->back()->with('success','kitchen tag deleted');

    }
}
