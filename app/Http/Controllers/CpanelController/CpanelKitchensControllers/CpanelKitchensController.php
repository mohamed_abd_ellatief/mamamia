<?php

namespace App\Http\Controllers\CpanelController\CpanelKitchensControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kitchen;
use App\Kitchenimage;
use App\Kitchencategory;
use App\Kitchentag;
use App\Kitchen_Day;
use App\Day;
use Auth;
use DB;
class CpanelKitchensController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $Kitchens=Kitchen::where('user_id',Auth::user()->id)->orderby('id','desc')->get();
      $Categories=Kitchencategory::all();
      $Tags=Kitchentag::all();
      return view('cpanel.kitchens.index',compact('Kitchens','Categories','Tags'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'en_title' => 'required|max:191',
            'ar_title' => 'required|max:191',
            'address' => 'required',
            'phone' => 'required|regex:/(05)[0-9]{8}/|min:10',
            'phone2' => 'regex:/(05)[0-9]{8}/|nullable|min:10',
            'category'=>'required',
            'tags'=>'required|array',
            'lat'=>'required',
            'lng'=>'required',
            'image'=>'required|mimes:jpeg,jpg,gif,png',
            'images.*' => 'mimes:jpeg,jpg,gif,png'
            ]);

        $Kitchen = new Kitchen();
        $Kitchen->en_title=  $request->en_title;
        $Kitchen->ar_title=  $request->ar_title;
        $Kitchen->en_description=  $request->en_description;
        $Kitchen->ar_description=  $request->ar_description;
        $Kitchen->address=  $request->address;
        $Kitchen->phone=  $request->phone;
        $Kitchen->lat=  $request->lat;
        $Kitchen->lng=  $request->lng;
        $Kitchen->kitchencategory_id=  $request->category;
        $Kitchen->user_id=  Auth::user()->id;
        $Kitchen->minicharge=  $request->minicharge;
        $Kitchen->deliverypriceperkilo=  $request->deliverypriceperkilo;
        $Kitchen->delivermaxbill=  $request->delivermaxbill;
        if($request->hasfile('image'))
        {
            if($file=$request->file('image'))
            {
                $name=time().'.'.$file->getClientOriginalExtension();
                $file->move('cpanel/upload/kitchen',$name);
                $Kitchen->image=$name;
            }
        }
        try{
        $Kitchen->save();
        foreach($request->tags as $value){
            DB::table('kitchen_kitchentag')->insert(
                ['kitchen_id' => $Kitchen->id, 'kitchentag_id' => $value]
            );
        }
        if($request->hasfile('images'))
        {
            $count=1;
            foreach($request->file('images') as $file)
            {
                $name=$count.time().'.'.$file->getClientOriginalExtension();
                $file->move('cpanel/upload/kitchen',$name);
                $Image=new Kitchenimage;
                $Image->kitchen_id=$Kitchen->id;
                $Image->image=$name;
                $Image->save();
                $count++;
            }
        }
        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        $Restaurant=Kitchen::find($Kitchen->id);
        $kitchen_days=Kitchen_Day::where('kitchen_id',$Kitchen->id)->get();
        $Days=[];
        foreach($kitchen_days as $value){
            $Days[]= Day::find($value->day_id);
        }
        $Daysss=Day::all();
        $count=1;
        return redirect("days/$Kitchen->id?message=success");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $kitchenstoshow=Kitchen::find($id);
       $kitchenstoshow->vendor;
        return view('cpanel.kitchens.show',compact('kitchenstoshow'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {

      $kitchen=Kitchen::find($request->id);
    //   return response()->json(['message'=>$kitchen]);
    //   die();
      $Categories=Kitchencategory::all();
      $Tags=Kitchentag::all();
      $kitchen_kitchentag=DB::table('kitchen_kitchentag')->select('kitchentag_id')->where('kitchen_id',$request->id)->get();
      $array=[];
      foreach($kitchen_kitchentag as $value){
          $array[]=$value->kitchentag_id;
      }

    //   $kitchenTags=DB::table('kitchen_kitchentag')->where('kitchen_id',$kitchen->id)->get();
    //   $array=array($kitchen,$kitchenTags);

        return view('cpanel.kitchens.model',compact('kitchen','Categories','Tags','array'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request,[
            'id' => 'required',
            'en_title' => 'required|max:191',
            'ar_title' => 'required|max:191',
            'address' => 'required',
            'phone' => 'required|regex:/(05)[0-9]{8}/',
            'tags'=>'required|array',
            'phone2' => 'regex:/(05)[0-9]{8}/|nullable',
            'category'=>'required',
            'lat'=>'required',
            'lng'=>'required',
            'image'=>'mimes:jpeg,jpg,gif,png',
            ]);

        $Kitchen =Kitchen::find($request->id);
        $Kitchen->en_title=  $request->en_title;
        $Kitchen->ar_title=  $request->ar_title;
        $Kitchen->en_description=  $request->en_description;
        $Kitchen->ar_description=  $request->ar_description;
        $Kitchen->address=  $request->address;
        $Kitchen->lat=  $request->lat;
        $Kitchen->lng=  $request->lng;
        $Kitchen->phone=  $request->phone;
        $Kitchen->kitchencategory_id=  $request->category;
        $Kitchen->user_id=  Auth::user()->id;
        $Kitchen->minicharge=  $request->minicharge;
        $Kitchen->deliverypriceperkilo=  $request->deliverypriceperkilo;
        $Kitchen->delivermaxbill=  $request->delivermaxbill;

        if($request->hasfile('image'))
        {
            if($file=$request->file('image'))
            {
                $name=time().'.'.$file->getClientOriginalExtension();
                $file->move('cpanel/upload/kitchen',$name);
                $Kitchen->image=$name;
            }
        }
        try{
        $Kitchen->save();
        $kitchen_kitchentag=DB::table('kitchen_kitchentag')->select('kitchentag_id')->where('kitchen_id',$request->id)->get();
        if(count($kitchen_kitchentag)>0){
            DB::table('kitchen_kitchentag')->where('kitchen_id', $request->id)->delete();
        }
        foreach($request->tags as $value){
            DB::table('kitchen_kitchentag')->insert(
                ['kitchen_id' => $request->id, 'kitchentag_id' => $value]
            );
        }

        } catch (\Exception $e) {
            return redirect()->back()->with('message','Failed');
        }
        return redirect()->back()->with('message','Success');

    }

    public function UpdateStatus(Request $request)
    {
        // return response()->json(['message'=>$request->id]);
        // die();
        $KitchenStatus=Kitchen::find($request->id);
        if($KitchenStatus->status == "off"){
            $Kitchen=Kitchen::find($request->id);
            $Kitchen->status="on";
                $Kitchen->save();
        }else{
            $Kitchen=Kitchen::find($request->id);
            $Kitchen->status="off";
                $Kitchen->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function delete(Request $request)
    {
        try{
            Kitchen::whereIn('id',$request->id)->delete();
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
        return response()->json(['message'=>'Success']);
    }


    public function images($id){
        $Kitchen=Kitchen::find($id);
        $Images=Kitchenimage::where('kitchen_id',$id)->get();
        return view('cpanel.kitchens.images',compact('Kitchen','Images'));
    }
    public function Delete_images(Request $request){
        try{
            Kitchenimage::where('id',$request->id)->delete();
        } catch (\Exception $e) {
            return response()->json(['message'=>'Failed']);
        }
        return response()->json(['message'=>'Success']);
    }

    public function Store_imagesKitchen(Request $request){
        $this->validate($request, [
            'images' => 'required',
            'images.*' => 'mimes:jpeg,jpg,gif,png'
        ]);

        if($request->hasfile('images'))
        {
            $count=1;
            foreach($request->file('images') as $file)
            {
                $name=$count.time().'.'.$file->getClientOriginalExtension();
                $file->move('cpanel/upload/kitchen',$name);
                $Image=new Kitchenimage;
                $Image->kitchen_id=$request->kitchen_id;
                $Image->image=$name;
                $Image->save();
                $count++;
            }
        }
        return redirect()->back()->with('message','Success');
    }

}
