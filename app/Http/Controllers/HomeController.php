<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      return view('cpanel.index');
    }
    public function welcome()
    {
        if(auth::user()->status == 1 ){
            if(auth::user()->category == "cashier"){
                return redirect('orders_cashier');
            }elseif(auth::user()->category == "admin"){
                return redirect('admin');
            }elseif(auth::user()->category == "vendor"){
                return redirect('DashBoard');
            }
        }
        return view('welcome');
    }

    function lang ($locale)
  {
    
    session()->put('locale', $locale);
   return redirect()->back();
  
  }
}
