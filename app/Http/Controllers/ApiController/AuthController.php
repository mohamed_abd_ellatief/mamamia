<?php

namespace App\Http\Controllers\ApiController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Verefication;
use Auth;
use Hash;
class AuthController extends Controller
{
  /**
  * Create a new AuthController instance.
  *
  * @return void
  */
 // public function __construct()
 // {
 //     $this->middleware('auth:api', ['except' => ['login']]);
 // }

 /**
  * Get a JWT via given credentials.
  *
  * @return \Illuminate\Http\JsonResponse
  */

   public function login(Request $request){
     
     // return $request->apll();
     // return bcrypt($request->password);
    // $user=User::where([['mobilenumber',$request->mobilenumber],['password',bcrypt($request->password)]])->first();
    $newNumbers = range(0, 9);
    // 1. Persian HTML decimal
    $persianDecimal = array('&#1776;', '&#1777;', '&#1778;', '&#1779;', '&#1780;', '&#1781;', '&#1782;', '&#1783;', '&#1784;', '&#1785;');
    // 2. Arabic HTML decimal
    $arabicDecimal = array('&#1632;', '&#1633;', '&#1634;', '&#1635;', '&#1636;', '&#1637;', '&#1638;', '&#1639;', '&#1640;', '&#1641;');
    // 3. Arabic Numeric
    $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
    // 4. Persian Numeric
    $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');

    $Mobile =  str_replace($persianDecimal, $newNumbers, $request->mobilenumber);
    $Mobile =  str_replace($arabicDecimal, $newNumbers, $request->mobilenumber);
    $Mobile =  str_replace($arabic, $newNumbers, $request->mobilenumber);
    $Pass =  str_replace($persianDecimal, $newNumbers, $request->password);
    $Pass =  str_replace($arabicDecimal, $newNumbers, $request->password);
    $Pass =  str_replace($arabic, $newNumbers, $request->password);
    $MobileNumber= str_replace($persian, $newNumbers, $Mobile);
    $Password= str_replace($persian, $newNumbers, $Pass);
    $category=explode(',',$request->category);
    if(Auth::attempt(['mobilenumber'=>$MobileNumber,'password'=>$Password,'category'=>$category])){
      $user=Auth::user();
      if($request->devicetype == "ios" && $user->category == "cashier"){
        return response()->json(["code"=>1317,'Status'=>'error','message' => 'This Email is Cashier']);
        
      }
      if($user->status == 1 ){
          if($user->isverified == 1){
            $token = auth('api')->login($user);
            $user->token=$token;
            return response()->json(["code"=>200,'Status'=>'success','message' => 'you login successfully','token'=>$user->token,'userdata'=> $user]);
          }else{
            ////Verification///
            $newverification= new Verefication();
            $newverification->user_id=$user->id;
            // $newverification->token= rand(rand(999, 9999),rand(999, 9999));
            $to=$user->mobilenumber;
            $code=rand(rand(999, 9999),rand(999, 9999));
            $msg= str_replace(' ', '%20', 'نورتنا ! فضلا أدخل الرمز ')."%20".$code;

            $url = "http://www.jawalbsms.ws/api.php/sendsms?user=yahyainvetechs&pass=verificationSmS3&to=$to&message=$msg&sender=Mammamia";

            $ch = curl_init($url); // init the curl with jawalb API url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $data = curl_exec($ch);  //run curl api call, on success curl call, $data will contain the API call response
            curl_close($ch);



            $newverification->token=$code;

            $newverification->save();
            //$user->verificationcode= $newverification->token;
            return response()->json(["code"=>1314,'Status'=>'error','message' => 'you need to verifi code','userdata'=>$user]);
          }
      }else{
        return response()->json(["code"=>1313,'Status'=>'error','message' => 'you need admin confirmation']);
      }
    }else{
      $count=User::where('mobilenumber',$MobileNumber)->count();
      if($count >0 ){
        return response()->json(["code"=>1315,'Status'=>'error','message' => 'your  password is invalid']);
      }else{
        return response()->json(["code"=>1316,'Status'=>'error','message' => 'your mobile number is invalid']);
      }
    }
   }
   public function registration(Request $request){
    
    //  return response()->json(["code"=>1313,'Status'=>'error','message'=>'mobilenumber must be unique this mobile number is used before','userdata'=>$requestall]);
    if(checkmobilenumber($request->mobilenumber) == "false"){
      return response()->json(["code"=>1313,'Status'=>'error','message'=>'mobilenumber must be unique this mobile number is used before']);
    }else{
      $User=new User;
      $User->name=$request->name;
      $User->mobilenumber=$request->mobilenumber;
      $User->password=Hash::make($request->password);
      $User->email=$request->email;
      $User->category=$request->category;
       $file = @$request->file('image');
                  
      if(@$file !=""){
                    $name=time().'.'.$file->getClientOriginalExtension();
               $file->move(base_path('./cpanel/upload/user'),$name);
                                          $User->image=$name;

        //$name=time().'.'.$file->getClientOriginalExtension();
        //$file->move('cpanel/upload/user',$name);
        //$User->image=$name;
      }
              // if($flag == 0){
               /* Custom Code Of Helper */
        $User->status=1;
        $User->isverified=0;
        $User->save();

               ////Verification///
               $newverification= new Verefication();
               $newverification->user_id=$User->id;
               //$newverification->token= rand(rand(999, 9999),rand(999, 9999));
            $to=$User->mobilenumber;
            $code=rand(rand(999, 9999),rand(999, 9999));
               $msg= str_replace(' ', '%20', 'نورتنا ! فضلا أدخل الرمز ')."%20".$code;

      $url = "http://www.jawalbsms.ws/api.php/sendsms?user=yahyainvetechs&pass=verificationSmS3&to=$to&message=$msg&sender=Mammamia";

            $ch = curl_init($url); // init the curl with jawalb API url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $data = curl_exec($ch);  //run curl api call, on success curl call, $data will contain the API call response
            curl_close($ch);



            $newverification->token=$code;

            $newverification->save();
               $newverification->save();
               ///EndVerification////
               //$User->verificationcode=$newverification->token;
                 return response()->json(["code"=>200,'Status' => 'success','message'=>'Register new user done successfully','userdata' => $User]);
              //  }
     }
   }
   public function verify_verificationcode(Request $request){
     // return $request->all();
     $usertoverifi=User::find($request->user_id);
     if($usertoverifi->isverified == 0){
       $usertoverification=Verefication::where('user_id',$request->user_id)->get();
       if($usertoverification->last()->token == $request->token){
          foreach ($usertoverification as  $value) {
            $value->delete();
          }
          $usertoverifi=User::find($request->user_id);
          $usertoverifi->isverified=1;
          $usertoverifi->save();
          $token = auth('api')->login($usertoverifi);
          $usertoverifi->token=$token;
         return response()->json(["code"=>200,'Status' => 'success','message'=>'you verified successfully','token'=>$usertoverifi->token,'userdata'=>$usertoverifi]);
       }else{
         return response()->json(["code"=>1313,'Status' => 'error','message'=>'this token is invalid']);
       }
     }else{
       return response()->json(["code"=>1314,'Status' => 'error','message'=>'this user is verified']);
     }
   }

   public function foroget_passworod(Request $request){
    $userdata=User::where('mobilenumber',$request->mobilenumber)->where('category',$request->category)->first();
    if(isset($userdata)){
      ////Verification///
      $newverification= new Verefication();
      $newverification->user_id=$userdata->id;
      // $newverification->token= rand(rand(999, 9999),rand(999, 9999));
         $to=$userdata->mobilenumber;
          $code=rand(rand(999, 9999),rand(999, 9999));
              $msg= str_replace(' ', '%20', 'نورتنا ! فضلا أدخل الرمز ')."%20".$code;

      $url = "http://www.jawalbsms.ws/api.php/sendsms?user=yahyainvetechs&pass=verificationSmS3&to=$to&message=$msg&sender=Mammamia";

            $ch = curl_init($url); // init the curl with jawalb API url
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $data = curl_exec($ch);  //run curl api call, on success curl call, $data will contain the API call response
            curl_close($ch);



            $newverification->token=$code;
      $newverification->save();
     // $userdata->verificationcode=$newverification->token;

      return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully','userdata'=>$userdata]);

    }else{
      return response()->json(["code"=>1313,'Status' => 'error','message'=>'this mobile number is invalid']);

    }

   }
   public function verify_verificationcode_forget(Request $request){
     $userdata=User::where('mobilenumber',$request->mobilenumber)->where('category',$request->category)->first();
     $usertoverification=Verefication::where('user_id',$userdata->id)->get();
     $usertoverification->last()->token;
     if($usertoverification->last()->token == $request->token){
       foreach ($usertoverification as  $value) {
        $value->delete();
       }
       $usertoverifi=User::find($userdata->id);
       $usertoverifi->isverified=1;
       $usertoverifi->save();
       return response()->json(["code"=>200,'Status' => 'success','message'=>'you verified successfully','userdata'=>$userdata]);
     }else{
       return response()->json(["code"=>1313, 'Status' => 'error','message'=>'this token is invalid']);
     }
   }

   public function change_passworod(Request $request){
     $usertochangepassword=User::find($request->user_id);
     $usertochangepassword->password =Hash::make($request->password);
     $usertochangepassword->save();
     return response()->json(["code"=>200,'Status' => 'success','message'=>'password changed successfully','userdata'=>$usertochangepassword]);

   }

   public function UpdateProfile(Request $request){
       $id=$request->user_id;
      // return $request->all();
      if(checkmobilenumberupdate($request->mobilenumber,$id) == "false"){
         return response()->json(["code"=>1313,'Status'=>'error','message'=>'mobilenumber must be unique this mobile number is used before']);
       }else{
            $User=User::find($id);
            $User->name=$request->name;
            $User->mobilenumber=$request->mobilenumber;
            $User->password=Hash::make($request->password);
            $User->email=$request->email;
            if($file=$request->file('image')){
              $name=time().'.'.$file->getClientOriginalExtension();
              $file->move('cpanel/upload/user',$name);
              $User->image=$name;
            }
            $User->save();
            return response()->json(["code"=>200,'Status' => 'success','message'=>'update user data done successfully','userdata' => $User]);
       }
     }




    public function verification_apitoken(){
      // return response()->json();
      if(isset(auth('api')->user()->id)){
        $id=auth('api')->user()->id;
        $oneuserdata=User::find($id);
        return response()->json(['Status'=>'success','message' => 'your token is valid ','userdata'=> $oneuserdata]);
      }else{
        return response()->json(["code"=>1313,'Status'=>'error','message' => 'unauthinticated']);

      }
    }
    public function complet_registration(Request $request){
         // return $request->all();
        $usertocomppletregistration=User::find($request->user_id)->first();
        $usertocomppletregistration->Vendor->servicecategory_id=$request->servicecategory_id;
        $usertocomppletregistration->Servicestags()->attach($request->servicestags);
        $usertocomppletregistration->save();
        $usertocomppletregistration->Vendor->save();
        return response()->json(["code"=>200,'Status' => 'success','message'=>'Registrat Completed','userdata' => $usertocomppletregistration]);
   }
  }
