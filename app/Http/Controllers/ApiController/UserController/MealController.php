<?php

namespace App\Http\Controllers\ApiController\UserController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Kitchen;
use App\Meal;
use Carbon\Carbon;
use DB;
use App\availblehours;
use App\Mealimage;
use App\Additions;
class MealController extends Controller
{
    public function MealDetails(Request $request){
        $Meal=Meal::find($request->id);
        $Meal->category=DB::table('meal_eventcategory')
        ->join('eventcategories','meal_eventcategory.eventcategory_id','eventcategories.id')
        ->select('eventcategories.id')
        ->where('meal_id',$request->id)
        ->get();
        if(Mealimage::where('meal_id',$request->id)->count()>0){
            $Meal->images=Mealimage::select('image')->where('meal_id',$request->id)->get();
        }else{
            $Meal->images=Meal::select('image')->where('id',$request->id)->get();
        }
        $Meal->hours=availblehours::select('from','to')->where('attribute_id',$request->id)->where('attribute_category','meal')->get();
        $Meal->Outlets=DB::table('kitchen_meal')
        ->join('kitchens','kitchen_meal.kitchen_id','kitchens.id')
        ->select('kitchens.ar_title','kitchens.en_title')
        ->where('kitchen_meal.meal_id',$request->id)
        ->get();
		        $Additions = Additions::where('Meal_id',$request->id)->where('confirm',1)->OrderBy('created_at','desc')->select('id','ar_title','en_title','price')->get();

        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully','data'=>$Meal,'Additions'=>$Additions]);        
    }

    // /////////////////////////////////Search Meal ////////////////////////////
    public function Meals(Request $request){
        $dt = Carbon::now();
        $lat=$request->latitude;
        $lng=$request->longitude;
        $radius = 30;
       $string = "SELECT id, lat, lng, ( 6371 * acos( cos( radians(?) ) *
       cos( radians( lat ) ) * cos( radians( lng ) - radians(?) ) + sin( radians(?) ) * sin( radians( lat ) ) ) )
       AS distance FROM kitchens HAVING distance < ? ORDER BY distance LIMIT 0 , 30;";     
       $args = [$lat, $lng, $lat, $radius];
       $datas = DB::select($string, $args);
       $lats=[];
       $long=[];
       foreach($datas as $data){
           $lats[] = $data->lat;
           $long[] = $data->lng;
       }
       $days=array('Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday');
       $day="";
       foreach($days as $key => $value){
           if($value == $dt->format('l')){
            $day=$key;
           }
       }
    $count=0;

    $kitchenss=Kitchen::
        join('availblehours','kitchens.id','availblehours.attribute_id')
        ->where('availblehours.day_id',$day)
        ->where('availblehours.attribute_category','kitchen')
        ->whereTime('availblehours.from', '<', $dt->format('H:i'))
        ->whereTime('availblehours.to', '>', $dt->format('H:i'))
        ->where('kitchens.status','on')
        ->whereIn('lat',$lats)
        ->whereIn('lng',$long)
        ->distinct('kitchens.id');
        if($request->category != "0"){
            $category=explode(',',$request->category);
            $kitchenss->whereIn('kitchens.kitchencategory_id',$category);
        }
        if($request->tags != "0"){
            $Tagsss=explode(',',$request->tags);
            $kitchenss->join('kitchen_kitchentag','kitchens.id','kitchen_kitchentag.kitchen_id');
            $kitchenss->whereIn('kitchen_kitchentag.kitchentag_id',$Tagsss);
        }
        $Kitchens=$kitchenss->pluck('kitchens.id');
        if(count($Kitchens)>0){

            $Meals_id=DB::table('kitchen_meal')->whereIn('kitchen_id',$Kitchens)->pluck('meal_id');
            if(count($Meals_id)>0){
                $Mealss=Meal::select('meals.*')
                ->join('availblehours','meals.id','availblehours.attribute_id')
                ->whereNull('availblehours.day_id')
                ->where('availblehours.attribute_category','meal')
                ->whereTime('availblehours.from', '<', $dt->format('H:i'))
                ->whereTime('availblehours.to', '>', $dt->format('H:i'))
                ->whereIn('meals.id',$Meals_id)
                ->orderby('created_at','asc')
                ->where('meals.status','on')
                ->distinct('meals.id');

                if($request->search != "0"){
                    $validator = Validator::make($request->all(), [
                        'search' => 'regex:/^[a-zA-Z$@$!%*?&#^-_. +]+$/'
                    ]);
                    if($validator->fails()){
                        $Mealss->where('meals.ar_title', 'LIKE', "%$request->search%");
                    }else{
                        $Mealss->where('meals.en_title', 'LIKE', "%$request->search%");
                    }
                }

                if($request->tags != "0"){
                    $tags=explode(',',$request->tags);
                    $Mealss->whereIn('meals.kitchentag_id',$tags);
                }
                $Meals=$Mealss->get();
                foreach($Meals as $Meal){
                    $latt=Kitchen::select('kitchens.lat')->join('kitchen_meal','kitchens.id','kitchen_meal.kitchen_id')->where('kitchen_meal.meal_id',$Meal->id)->first()->lat;
                    $lngg=Kitchen::select('kitchens.lng')->join('kitchen_meal','kitchens.id','kitchen_meal.kitchen_id')->where('kitchen_meal.meal_id',$Meal->id)->first()->lng;
                    $Meal->kitchen_id=Kitchen::select('kitchens.id')->join('kitchen_meal','kitchens.id','kitchen_meal.kitchen_id')->where('kitchen_meal.meal_id',$Meal->id)->first()->id;
                    $Meal->ar_title_kitchen=Kitchen::select('kitchens.ar_title')->join('kitchen_meal','kitchens.id','kitchen_meal.kitchen_id')->where('kitchen_meal.meal_id',$Meal->id)->first()->ar_title;
                    $Meal->en_title_kitchen=Kitchen::select('kitchens.en_title')->join('kitchen_meal','kitchens.id','kitchen_meal.kitchen_id')->where('kitchen_meal.meal_id',$Meal->id)->first()->en_title;
                    $Meal->lat=Kitchen::select('kitchens.lat')->join('kitchen_meal','kitchens.id','kitchen_meal.kitchen_id')->where('kitchen_meal.meal_id',$Meal->id)->first()->lat;
                    $Meal->lng=Kitchen::select('kitchens.lng')->join('kitchen_meal','kitchens.id','kitchen_meal.kitchen_id')->where('kitchen_meal.meal_id',$Meal->id)->first()->lng;
                    $Meal->deliverypriceperkilo=Kitchen::select('kitchens.deliverypriceperkilo')->join('kitchen_meal','kitchens.id','kitchen_meal.kitchen_id')->where('kitchen_meal.meal_id',$Meal->id)->first()->deliverypriceperkilo;
                    $Meal->delivermaxbill=Kitchen::select('kitchens.delivermaxbill')->join('kitchen_meal','kitchens.id','kitchen_meal.kitchen_id')->where('kitchen_meal.meal_id',$Meal->id)->first()->delivermaxbill;
                    $Meal->distance=$this->distance($request->latitude, $request->longitude, $latt, $lngg, "K");
                }
            }else{
                return response()->json(["code"=>1314,'Status' => 'success','message'=>'code sent successfully','data'=>'No Meals']);
            }
        }else{
            return response()->json(["code"=>1313,'Status' => 'success','message'=>'code sent successfully','data'=>'No Kitchen']);
        }
        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully','data'=>$Meals]);
    
   
        
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
      
        if ($unit == "K") {
          return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}
