<?php

namespace App\Http\Controllers\ApiController\UserController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use DB;
use App\Kitchen;
use App\Kitchen_Day;
use App\Meal;
use App\Kitchenimage;
use App\Review;
use Carbon\Carbon;
use App\Mainsettings;
use App\Additions;

class KitchenController extends Controller
{
    public function Kitchens(Request $request){
        $dt = Carbon::now();
        $lat=$request->latitude;
        $lng=$request->longitude;
        $radius =  Mainsettings::find(1)->distance;
       // $string = "SELECT id, ar_title, en_title, lat, lng, ( 6371 * acos( cos( radians(?) ) *
       // cos( radians( lat ) ) * cos( radians( lng ) - radians(?) ) + sin( radians(?) ) * sin( radians( lat ) ) ) )
       // AS distance FROM kitchens HAVING distance < ? ORDER BY distance LIMIT 0 , 30;";     
       // $args = [$lat, $lng, $lat, $radius];

       // $datas = DB::table("kitchens")->select("*",DB::raw('6367 * acos( cos( radians(".$lat.") ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(".$lng.") ) + sin( radians(".$lat.") ) * sin( radians( lat ) ) ) ) AS distance'))
       //  ->groupBy("kitchens.id")
       //  ->HAVING('distance' ,'<',20)
       //  ->get();

        $datas = Kitchen::selectRaw('*, ( 6367 * acos( cos( radians( ? ) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians( ? ) ) + sin( radians( ? ) ) * sin( radians( lat ) ) ) ) AS distance', [$lat, $lng, $lat])
    ->having('distance', '<', sprintf("%.12f",20))
    ->orderBy('id','Desc')
    ->get();
   // return $datas;
       $lats=[];
       $long=[];
       foreach($datas as $data){
           $lats[] = $data->lat;
           $long[] = $data->lng;
       }
       $days=array('Saturday','Sunday','Monday','Tuesday','Wednesday','Thursday','Friday');
       $day="";
       foreach($days as $key => $value){
           if($value == $dt->format('l')){
            $day=$key;
           }
       }

        $count=0;
        $counttt=Kitchen::select('kitchens.id')
        ->join('availblehours','kitchens.id','availblehours.attribute_id')
        ->where('status','on')
        ->whereIn('lat',$lats)
        ->whereIn('lng',$long);
        if($request->category != 0){
            $counttt->where('kitchencategory_id',$request->category);
        }
        if($request->tags != 0){
            $counttt->join('kitchen_kitchentag','kitchens.id','kitchen_kitchentag.kitchen_id');
            $counttt->where('kitchentag_id',$request->tags);
        }
        $countt=$counttt->distinct('kitchens.id')
        ->get();
        $count=count($countt);
        $Kitchenss=Kitchen::select('kitchens.id','image','ar_title','en_title','minicharge','lat','lng','deliveryflag','deliverypriceperkilo','delivermaxbill')
        ->where('status','on')
        ->whereIn('lat',$lats)
        ->whereIn('lng',$long);
        if($request->category != 0){
            $Kitchenss->where('kitchencategory_id',$request->category);
        }
        if($request->tags != 0){
            $Kitchenss->join('kitchen_kitchentag','kitchens.id','kitchen_kitchentag.kitchen_id');
            $Kitchenss->where('kitchentag_id',$request->tags);
        }
        $Kitchens=$Kitchenss->distinct('kitchens.id')
        ->get();
        
        foreach($Kitchens as $Kitchen){
            $Kitchen->total_rate=Review::where('kitchen_id',$Kitchen->id)->sum('rate');
            $Kitchen->count_rate=Review::where('kitchen_id',$Kitchen->id)->count();
        }
        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully','count'=>$count,'data'=>$Kitchens]);
    }

    public function RestaurantDetails(Request $request){
        $dt = Carbon::now();
        $Kitchen['kitchen']=Kitchen::select('id','en_title','ar_title','lat','lng','image')
        ->where('id',$request->id)
        ->get();
        foreach($Kitchen['kitchen'] as $Kit){
            $Kit->total_rate=Review::where('kitchen_id',$Kit->id)->sum('rate');
            $Kit->count_rate=Review::where('kitchen_id',$Kit->id)->count();
        }
        $images=Kitchenimage::where('kitchen_id',$request->id)->get();
        if(count($images)>0){
            $Kitchen['images']=$images;
        }else{
            $Kitchen['images']=Kitchen::select('image')
            ->where('id',$request->id)
            ->get();
        }
        if($request->search == "0"){
            $request->search = null ;
        }
        $Mealss=Meal::where('status','on')->select('meals.id','image','ar_title','en_title','ar_description','en_description','price')
        ->join('kitchen_meal','meals.id','kitchen_meal.meal_id')
        ->join('availblehours','meals.id', '=', 'availblehours.attribute_id')
        ->whereNull('availblehours.day_id')
        ->where('availblehours.attribute_category','meal')
        ->whereTime('availblehours.from', '<', $dt->format('H:i'))
        ->whereTime('availblehours.to', '>', $dt->format('H:i'))
        ->where('kitchen_meal.kitchen_id',$request->id);

        if($request->tags != 0){
            $Tagss=DB::table('kitchen_kitchentag')->where('kitchen_id',$request->id)->get();
			if($Tagss){
				foreach($Tagss as $tagsss){
                $Tags[]=$tagsss->kitchentag_id;
            }
            if(in_array($request->tags,$Tags)){
                $Mealss->where('meals.kitchentag_id',$request->tags);
            }else{
                $Mealss->where('meals.kitchentag_id',-1);
            }
			}
        }else{
            $Tagss=DB::table('kitchen_kitchentag')->where('kitchen_id',$request->id)->pluck('kitchentag_id');
            $Mealss->whereIn('meals.kitchentag_id',$Tagss);
        }
        if($request->category == 4){
            $Mealss->join('meal_eventcategory','meals.id','meal_eventcategory.meal_id')
            ->whereIn('meal_eventcategory.eventcategory_id',[6,7,8]);
        }elseif($request->category == 0){
            $Mealss->join('meal_eventcategory','meals.id','meal_eventcategory.meal_id')
            ->whereIn('meal_eventcategory.eventcategory_id',[1,2,3]);
        }else{
            $Mealss->join('meal_eventcategory','meals.id','meal_eventcategory.meal_id')
            ->where('meal_eventcategory.eventcategory_id',$request->category);
        }

        if(isset($request->search)){
            $validator = Validator::make($request->all(), [
                'search' => 'regex:/^[a-zA-Z$@$!%*?&#^-_. +]+$/'
            ]);
            if($validator->fails()){
                $Mealss->where('meals.ar_title', 'LIKE', "%$request->search%");
            }else{
                $Mealss->where('meals.en_title', 'LIKE', "%$request->search%");
            }
        }
        $Mealss->distinct('id');
				
		
        $Meals=$Mealss->get();
		foreach($Meals as $Meal){
			$Meal->Additions = Additions::where('Meal_id',$Meal->id)->where('confirm',1)->select('id','ar_title','en_title','price')->get();
			
		}
        $Kitchen['meal']=$Meals;
	
        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully','data'=>$Kitchen]);
    }
}
