<?php

namespace App\Http\Controllers\ApiController\UserController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use App\Order;
use App\Cashier;
use DB;
use App\Meal;
use App\Kitchen;
use App\User;
use App\Notification;
use App\Events\StatusLiked;

class OrderController extends Controller
{
    public function OrdersByUser(Request $request){

        $count=0;
        $role=0;
        $Orders=Order::select('id','userplace','totalprice','status','updated_at')
        ->where('user_id',$request->user_id)
        ->whereIn('status',[1,-2,-1])
        ->where('type','meal')
        ->orderby('created_at','desc')
        ->get();
        $NewOrders =Order::select('id','userplace','totalprice','status','updated_at')
        ->where('user_id',$request->user_id)
        ->where('status',0)
        ->where('type','meal')
        ->orderby('created_at','desc')
        ->get();
        foreach($NewOrders as $NewOrder){
            $NewOrder->meal=DB::table('order_meal')
            ->select('ar_title','en_title')
            ->join('meals','order_meal.meal_id','meals.id')
            ->where('order_meal.order_id',$NewOrder->id)
            ->first();
        }
        foreach($Orders as $Order){
            $Order->meal=DB::table('order_meal')
            ->select('ar_title','en_title')
            ->join('meals','order_meal.meal_id','meals.id')
            ->where('order_meal.order_id',$Order->id)
            ->first();
        }
        $count=Order::where('user_id',$request->user_id)->count();
        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully','count'=>$count,'data'=>$Orders,'NewOrders'=>$NewOrders]);
    }

    public function InvoiceByUser(Request $request){
        $count=Order::whereIn('status',[1,-1])
            ->where('user_id',$request->user_id)->count();
        $Orders=Order::select('totalprice','userplace','id','status','type','created_at')
            ->whereIn('status',[1,-1])
            ->where('user_id',$request->user_id)
            ->orderby('id','desc')->get();
        if(count($Orders) > 0){
              foreach ($Orders as $Order) {
                $Order->ar_orderDate=$Order->created_at->format('Y-m-d');
                $Order->ar_orderTime=$Order->created_at->format('i:H');
                $Order->en_orderDate=$Order->created_at->format('d-m-Y');
                $Order->en_orderTime=$Order->created_at->format('H:i');
                $Meals= DB::table('order_meal')->where('order_id',$Order->id)->get();
                $CountMeals= DB::table('order_meal')->where('order_id',$Order->id)->count();
                if(count($Meals) > 0){
                  $Meal=Meal::find($Meals[0]->meal_id);
                  $Order->ar_title=$Meal->ar_title;
                  $Order->count=$CountMeals;
                  $Order->en_title=$Meal->en_title;
                }
              }

            }
            return response()->json(["code"=>200,'Status'=>'success','message' => 'Archives','count'=>$count,'Archives'=> $Orders]);
    }

    public function Store_banquetUser(Request $request){

        $Order=Order::where('user_id',$request->user_id)->where('type','banquet')->whereIn('status',[2,3])->count();
        if($Order>0){
        return response()->json(["code"=>1313,'Status'=>'success','message' => 'Banquet  ']);
        }

        $Kitchens=Kitchen::select('kitchens.id','kitchens.user_id','lat','lng')
        ->join('kitchen_kitchentag','kitchens.id','kitchen_kitchentag.kitchen_id')
        ->where('kitchentag_id',1)
        ->where('status','on')
        ->get();
        $code=strToTime(date('Y-m-d H:i:s'));
        foreach($Kitchens as $Kitchen){
        $Order=new Order;
        $Order->codeorder=$request->user_id.$code;
        $Order->kitchen_id=$Kitchen->id;
        $Order->vendor_id=$Kitchen->user_id;
        $Order->user_id=$request->user_id;
        $Order->component=$request->component;
        $Order->en_partyname=$request->partyname;
        $Order->partytype=$request->partytype;
        $Order->guestnum=$request->noofperson;
        $Order->partylocation=$request->partylocation;
        $Order->partytime=$request->partytime;
        $Order->partydate=$request->partydate;
        $Order->usernote=$request->usernote;
        $Order->lng=$request->lng;
        $Order->lat=$request->lat;
        $Order->distancetouser=$this->distance($Kitchen->lat, $Kitchen->lng,$request->lat, $request->lng, "K");
        $Order->status=2;
        $Order->type="banquet";
        $Order->save();
        $Notification=new Notification;
        $Notification->ar_description="طلب اعداد حفلة ( $Order->en_partyname )";
        $Notification->en_description="New Banquet Order ( $Order->en_partyname )";
        $Notification->type="vendor";
        $Notification->views=0;
        $Notification->user_id=$Order->user_id;
        $Notification->vendor_id=$Order->vendor_id;
        $Notification->order_id=$Order->id;
        $Notification->save();
        }
        $count=Notification::where('type','vendor')->where('views',0)->count();
        event(new StatusLiked($count,$Order->vendor_id));

        $Cashiers=Cashier::where('vendor_id',$Order->vendor_id)->get();
        if(count($Cashiers)>0){
            foreach($Cashiers as $Cashier){
                $count=Notification::select('notifications.id')
                ->join('orders','notifications.order_id','orders.id')
                ->where('notifications.vendor_id',$Cashier->vendor_id)
                ->where('orders.kitchen_id',$Cashier->kitchen_id)
                ->where('notifications.type','vendor')
                ->where('notifications.views','0')
                ->distinct('notifications.id')
                ->count();
                event(new StatusLiked($count,$Cashier->cashier_id));
            }
        }

        return response()->json(["code"=>200,'Status'=>'success','message' => 'Banquet Added Successfully']);
    }

    public function OffersBanquetByUser(Request $request){

        $Order=Order::select('en_partyname as partyname','partydate','status')
        ->where('user_id',$request->user_id)
        ->whereIn('status',[2,3])
        ->where('type','banquet')
        ->first();
        if($Order){
            $Order->Offers=Order::select('orders.kitchen_id','kitchens.ar_title','kitchens.en_title','en_partyname as partyname','codeorder','orders.id','partydate','totalprice')
            ->join('kitchens','orders.kitchen_id','kitchens.id')
            ->where('orders.user_id',$request->user_id)
            ->where('orders.status',3)
            ->orderby('totalprice','desc')
            ->get();
            return response()->json(["code"=>200,'Status'=>'success','message' => 'User Banquet','data'=>$Order]);
        }else{
            return response()->json(["code"=>1313,'Status'=>'success','message' => 'User Banquet','data'=>null]);
        }
    }

    public function OrdersBanquetByUser(Request $request){
        $NewOrder=Order::
        select('en_partyname as partyname','id','partydate','status','codeorder')
        ->where('user_id',$request->user_id)
        ->whereIn('status',[2,3])
        ->where('type','banquet')
        ->orderby('id','desc')
        ->first();

        $Order=Order::select('kitchens.ar_title','kitchens.en_title','en_partyname as partyname','orders.id','codeorder','partydate','orders.status','totalprice')
        ->join('kitchens','orders.kitchen_id','kitchens.id')
        ->where('orders.user_id',$request->user_id)
        ->whereIn('orders.status',[1,-2])
        ->where('orders.type','banquet')
        ->orderby('orders.id','desc')
        ->get();
        $Cancelled=[];
        $CancelledOrders=Order::select('codeorder','en_partyname as partyname','partydate','status')->where('user_id',$request->user_id)->where('status',-4)->distinct('codeorder')->get();

        return response()->json(["code"=>200,'Status'=>'success','message' => 'User Banquet','NewOrder'=>$NewOrder,'data'=>$Order,'CancelledOrders'=>$CancelledOrders]);
    }

    public function AcceptOfferBanquetByUser(Request $request)
    {
        $Order=Order::where('codeorder',$request->codeorder)->first();
        // return $Order;
        $Kitchen=Kitchen::find($request->kitchen_id);
        Order::where('codeorder', $request->codeorder)->whereIn('status',[3,2])
        ->update(['status' => -3]);
        Order::where('codeorder', $request->codeorder)
        ->where('kitchen_id',$request->kitchen_id)
        ->update(['status' => 1]);
        $order_acc=Order::where('codeorder', $request->codeorder)
        ->where('kitchen_id',$request->kitchen_id)->first();

        Notification::where('order_id',$order_acc->id)->update(['type'=>'vendor','views'=>'0']);
        $count_user=Notification::where('user_id',$Order->user_id)
        ->where('type','user')
        ->where('views','0')
        ->count();
        $count_vendor=Notification::where('vendor_id',$Order->vendor_id)
        ->where('type','vendor')
        ->where('views','0')
        ->count();
        event(new StatusLiked($count_user,$Order->user_id));
        event(new StatusLiked($count_vendor,$Order->vendor_id));

        $Cashiers=Cashier::where('vendor_id',$Order->vendor_id)->get();
        if(count($Cashiers)>0){
            foreach($Cashiers as $Cashier){
                $count=Notification::select('notifications.id')
                ->join('orders','notifications.order_id','orders.id')
                ->where('notifications.vendor_id',$Cashier->vendor_id)
                ->where('orders.kitchen_id',$Cashier->kitchen_id)
                ->where('notifications.type','vendor')
                ->where('notifications.views','0')
                ->distinct('notifications.id')
                ->count();
                event(new StatusLiked($count,$Cashier->cashier_id));
            }
        }

        return response()->json(["code"=>200,'Status'=>'success','message' => 'Accepted Banquet']);
    }

    public function RefusedOfferBanquetByUser(Request $request)
    {
        Order::where('codeorder', $request->codeorder)
        ->where('kitchen_id',$request->kitchen_id)
        ->update(['status' => -2]);
        $Order=Order::where('codeorder',$request->codeorder)->where('kitchen_id',$request->kitchen_id)->first();
        Notification::where('order_id',$Order->id)->update([
            'type'=>'vendor',
            'views'=>0,
            'ar_description'=>'تم رفض الوليمة من قبل العميل',
            'en_description'=>'Banquet has been rejected by Client'
            ]);
        $count=Notification::where('vendor_id',$Order->vendor_id)
        ->where('type','vendor')
        ->where('views','0')
        ->count();
        event(new StatusLiked($count,$Order->vendor_id));
        $Cashiers=Cashier::where('vendor_id',$Order->vendor_id)->get();
        if(count($Cashiers)>0){
            foreach($Cashiers as $Cashier){
                $count=Notification::select('notifications.id')
                ->join('orders','notifications.order_id','orders.id')
                ->where('notifications.vendor_id',$Cashier->vendor_id)
                ->where('orders.kitchen_id',$Cashier->kitchen_id)
                ->where('notifications.type','vendor')
                ->where('notifications.views','0')
                ->distinct('notifications.id')
                ->count();
                event(new StatusLiked($count,$Cashier->cashier_id));
            }
        }
        $count_user=Notification::where('user_id',$Order->user_id)
        ->where('type','user')
        ->where('views','0')
        ->count();
        event(new StatusLiked($count_user,$Order->user_id));
        return response()->json(["code"=>200,'Status'=>'success','message' => 'Refused Banquet']);
    }

    public function CancelOffersBanquetByUser(Request $request)
    {
        // return $Orders_vendor_id;
        Order::where('codeorder', $request->codeorder)
        ->update(['status' => -4]);
        $Orders_id=Order::where('codeorder',$request->codeorder)->pluck('id');
        Notification::whereIn('order_id',$Orders_id)->delete();
        $Orders_vendor_id=Order::select('vendor_id','user_id')->where('codeorder',$request->codeorder)->distinct('vendor_id')->get();
        foreach($Orders_vendor_id as $Order_vendor_id){
        $count=Notification::where('vendor_id',$Order_vendor_id->vendor_id)
        ->where('type','vendor')
        ->where('views','0')
        ->count();
        event(new StatusLiked($count,$Order_vendor_id->vendor_id));
        $Cashiers=Cashier::where('vendor_id',$Order_vendor_id->vendor_id)->get();
        if(count($Cashiers)>0){
            foreach($Cashiers as $Cashier){
                $count=Notification::select('notifications.id')
                ->join('orders','notifications.order_id','orders.id')
                ->where('notifications.vendor_id',$Cashier->vendor_id)
                ->where('orders.kitchen_id',$Cashier->kitchen_id)
                ->where('notifications.type','vendor')
                ->where('notifications.views','0')
                ->distinct('notifications.id')
                ->count();
                event(new StatusLiked($count,$Cashier->cashier_id));
            }
        }
        $count_user=Notification::where('vendor_id',$Order_vendor_id->user_id)
        ->where('type','user')
        ->where('views','0')
        ->count();
        event(new StatusLiked($count_user,$Order_vendor_id->user_id));
        }
        return response()->json(["code"=>200,'Status'=>'success','message' => 'Cancelled Banquet']);
    }

    public function OrderDetailsCancelled(Request $request)
    {
        $CancelledOrders=Order::select('codeorder','user_id','component','en_partyname','partytype',
        'partylocation','guestnum','partytime','partydate','usernote','status','created_at')
        ->where('codeorder',$request->codeorder)->first();
        $CancelledOrders->orderDate=$CancelledOrders->created_at->format('Y-m-d');
        $CancelledOrders->orderTime=$CancelledOrders->created_at->format('H:i');
        $CancelledOrders['user']=User::find($CancelledOrders->user_id);
        return response()->json(["code"=>200,'Status'=>'success','message' => 'User Banquet','oneorder'=>$CancelledOrders]);
    }

    public function NotificationsByVendor(Request $request)
    {
      $Notification=Notification::select('notifications.*','orders.status')
        ->join('orders','notifications.order_id','orders.id')
        ->where('notifications.user_id',$request->user_id)
        ->where('notifications.type','user')
        ->where('notifications.views',0)
		  ->whereIn('orders.status',[1,-2,-1])
        ->orderBy('notifications.created_at','desc')
        ->get();
      return response()->json(["code"=>200,'Status'=>'success','message' => 'Notification','data'=>$Notification]);

    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        if ($unit == "K") {
          return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

}
