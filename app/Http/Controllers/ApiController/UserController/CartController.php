<?php

namespace App\Http\Controllers\ApiController\UserController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Userpoint;
use App\Verndorrule;
use App\Order;
use App\Kitchen;
use App\Meal;
use App\Notification;
use App\Cashier;
use App\Pending;
use App\Mainsettings;
use DB;
use App\Events\StatusLiked;

class CartController extends Controller
{
    public function PointsUser(Request $request)
    {
        
        $MinmumCharge=Kitchen::find($request->kitchen_id);
        $PointsUser=Userpoint::select('userpoints.points as Point_User')
        ->join('verndorrules','userpoints.kitchen_id','verndorrules.kitchen_id')
        ->where('userpoints.user_id',$request->user_id)
        ->where('userpoints.kitchen_id',$request->kitchen_id)
        ->whereColumn('userpoints.points','>=','verndorrules.points')
        ->first();
        $vat=Mainsettings::find(1)->vat;
        if($PointsUser){
            $PointsKitchen=Verndorrule::selectRaw('(discount/points) as discountPerPoint')
            ->where('kitchen_id',$request->kitchen_id)->first()->discountPerPoint;
            return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully','minCharge'=>$MinmumCharge->minicharge,'Delivery'=>$MinmumCharge->deliverypriceperkilo,'delivermaxbill'=>$MinmumCharge->delivermaxbill,'lat'=>$MinmumCharge->lat,'lng'=>$MinmumCharge->lng,'discountPerPoint'=>$PointsKitchen,'point'=>$PointsUser->Point_User,'vat'=>$vat]);
        }else{
            return response()->json(["code"=>1313,'Status' => 'success','message'=>'No have Point','minCharge'=>$MinmumCharge->minicharge,'Delivery'=>$MinmumCharge->deliverypriceperkilo,'delivermaxbill'=>$MinmumCharge->delivermaxbill,'lat'=>$MinmumCharge->lat,'lng'=>$MinmumCharge->lng,'vat'=>$vat]);
            
        }
    }

    public function OrderCart(Request $request)
    {
        
         $bodyContent = $request->getContent();
         
        $Meals=$request->meals;
        //$Additions=$Meals->additions;
        $Meals_id=[];
        $Meals_additions=[];
        /*foreach($Meals as $Additions){
            $Meal_additions[]=$Additions['additions'];
        }*/
        
        
         foreach($Meals as $Meal_id){
            $Meals_id[]=$Meal_id['meal_id'];
            //$Meals_additions[]=@$Meal_id['additions'];
        }
        $needTime=Meal::whereIn('id',$Meals_id)->select('neededtime')->max('neededtime');
        
        $timetodeliver=Meal::whereIn('id',$Meals_id)->select('neededtime')->max('neededtime')+intval($request->distancetouser)+11;
        $Kitchen=Kitchen::find($request->kitchen_id);
                
        $Order=new Order;
        $Order->codeorder=$request->user_id.strToTime(date('Y-m-d H:i:s'));
        $Order->user_id=$request->user_id;
        $Order->kitchen_id=$request->kitchen_id;
        $Order->vendor_id=$Kitchen->user_id;
        $Order->lat=$request->lat;
        $Order->lng=$request->lng;
        $Order->userplace=$request->userplace;
        $Order->neededtime=$needTime;
        $Order->distancetouser=$request->distancetouser;
        $Order->deliverytotalprice=$request->deliverytotalprice;
        $Order->totalprice=$request->totalprice;
        $Order->mealstotalprice=$request->BillTotal;
        $Order->status=0;
        $Order->type="meal";
        $Order->priceVat=$request->priceVat;

        if($request->checkuserpoint == 1){
            $Order->discountpoint=$request->discountpoint;
            $Order->usepoint=$request->point;
        }
        
        $Order->timetodeliver=$timetodeliver;
        $Order->save();

        if($request->checkuserpoint == 1){

            $Pending=new Pending;
            $Pending->order_id=$Order->id;
            $Pending->point=$request->point;
            $Pending->save();

            $UserPoint=Userpoint::where('kitchen_id',$Order->kitchen_id)->where('user_id',$Order->user_id)->first();
            $UserPoint->points=$UserPoint->points-$Pending->point;
            $UserPoint->save();
        }
        $i=0;
        foreach($Meals as $Meal){
            
               $additions=join(",",$Meal["additions"]);
               
            DB::table('order_meal')->insert(
                ['order_id' => $Order->id, 'meal_id' => $Meal['meal_id'], 'amount'=>$Meal['amount'],'note'=>$Meal['note'],'additions'=>@$additions]
            );
            $i++;
        }
        $Meal_id=DB::table('order_meal')->where('order_id',$Order->id)->first();
          if($Meal_id){
            $Meal=Meal::find($Meal_id->meal_id);
          }
        $Notification=new Notification;
        $Notification->ar_description="طلب جديد مطعم  $Meal->ar_title ";
        $Notification->en_description="New Order From $Meal->en_title Retaurant";
        $Notification->type="vendor";
        $Notification->views=0;
        $Notification->user_id=$Order->user_id;
        $Notification->vendor_id=$Order->vendor_id;
        $Notification->order_id=$Order->id;
        $Notification->save();

        $count=Notification::select('notifications.id')
        ->join('orders','notifications.order_id','orders.id')
        ->where('notifications.vendor_id',$Order->vendor_id)
        ->where('notifications.type','vendor')
        ->where('notifications.views','0')
        ->where('orders.status','!=',-3)
        ->count();
        event(new StatusLiked($count,$Order->vendor_id));

        $Cashiers=Cashier::where('vendor_id',$Order->vendor_id)->get();
        if(count($Cashiers)>0){
            foreach($Cashiers as $Cashier){
                $count=Notification::select('notifications.id')
                ->join('orders','notifications.order_id','orders.id')
                ->where('notifications.vendor_id',$Cashier->vendor_id)
                ->where('orders.kitchen_id',$Cashier->kitchen_id)
                ->where('notifications.type','vendor')
                ->where('notifications.views','0')
                ->distinct('notifications.id')
                ->count();
                event(new StatusLiked($count,$Cashier->cashier_id));
            }
        }
        return response()->json(["code"=>200,'Status'=>'success','message' => 'Order Added Successfully']);        
    }
}
