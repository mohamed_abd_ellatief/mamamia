<?php

namespace App\Http\Controllers\ApiController\UserController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class ProfileController extends Controller
{
    public function update(Request $request){
        $data=User::find($request->id);
        // $data=User::where('mobilenumber',$request->mobilenumber)->first();
        // return $data;
        if($request->mobilenumber == $data->mobilenumber){
            $User=User::find($request->id);
            $User->name=$request->name;
			if($file=$request->image){
			$name=time().'.'.$file->getClientOriginalExtension();
			$file->move('cpanel/upload/user',$name);
			$User->image=$name;
			}
				
            $User->password=Hash::make($request->password);
            $User->save();
        }else{
            
        }
    }
}
