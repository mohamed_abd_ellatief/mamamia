<?php

namespace App\Http\Controllers\ApiController\UserController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Review;

class ReviewController extends Controller
{
    public function getReview(Request $request){
        $there_user=Review::where('user_id',$request->user_id)->where('kitchen_id',$request->kitchen_id)->count();
        $count=Review::where('kitchen_id',$request->kitchen_id)->count();
        $Reviews=Review::
        select('users.id as user_id','reviews.rate','reviews.review','reviews.created_at','users.name')
        ->join('users','reviews.user_id','users.id')
        ->where('kitchen_id',$request->kitchen_id)->get();
        foreach($Reviews as $Review){
            $Review->date=$Review->created_at->format('Y-m-d');
        }
        if($there_user>0){
            return response()->json(["code"=>1313,'Status' => 'success','message'=>'code sent successfully','count'=>$count,'data'=>$Reviews]);
        }
        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully','count'=>$count,'data'=>$Reviews]);
    }

    public function store(Request $request){
        $Review=new Review;
        $Review->rate=$request->rate;
        $Review->user_id=$request->user_id;
        $Review->kitchen_id=$request->kitchen_id;
        $Review->review=$request->review;
        $Review->save();
        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully']);
    }
}
