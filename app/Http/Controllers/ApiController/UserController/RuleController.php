<?php

namespace App\Http\Controllers\ApiController\UserController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Verndorrule;
use App\Userpoint;

class RuleController extends Controller
{
    public function RulesByKitchen(Request $request){
        $Rules=Verndorrule::select('rule as money','points','discount')
        ->where('kitchen_id',$request->kitchen_id)
        ->orderby('points','desc')
        ->get();
        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully','data'=>$Rules]);
    }

    public function PointsByUser(Request $request){
        $Points=Userpoint::select('kitchens.id as kitchen_id','kitchens.en_title','kitchens.ar_title','userpoints.points')
        ->join('kitchens','userpoints.kitchen_id','kitchens.id')
        ->where('userpoints.user_id',$request->user_id)
        ->get();
        
        foreach($Points as $Point){
            $Rule=Verndorrule::where('kitchen_id',$Point->kitchen_id)->first();
            $Discount=intval(($Point->points*$Rule->discount)/$Rule->points);
            $Point->discount=$Discount;
        }
        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully','data'=>$Points]);

    }
}
