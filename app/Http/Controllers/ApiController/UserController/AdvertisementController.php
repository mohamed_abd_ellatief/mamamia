<?php

namespace App\Http\Controllers\ApiController\UserController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Advertisement;
use App\Notification;
use App\Events\StatusLiked;
use App\Download;

class AdvertisementController extends Controller
{
    public function Advertisement(Request $request){

        // Notification
      //  $count=Notification::where('user_id',$request->user_id)
      //  ->where('type','user')
      //  ->where('views','0')
      //  ->count();
		                $count=Notification::select('notifications.*','orders.status')
        ->join('orders','notifications.order_id','orders.id')
        ->where('notifications.user_id',$request->user_id)
        ->where('notifications.type','user')
        ->where('notifications.views',0)
	  ->where('orders.status',[1,-2,-1])
        ->orderBy('notifications.created_at','desc')
        ->count();

        event(new StatusLiked($count,$request->user_id));

        // Downloaded
        $MacAddress=Download::where('device',$request->mac_address)->count();
        if($MacAddress==0){
            $Download=new Download;
            $Download->device=$request->mac_address;
            $Download->type=$request->type;
            $Download->save();
        }
        // Advertisement
        $now=Carbon::now()->format('Y-m-d');
        $Advertisement=Advertisement::select('image')
        ->whereDate('startdate', '<=' ,$now)
        ->whereDate('enddate', '>=' ,$now)
        ->get();
        if(count($Advertisement)>0){
            return response()->json(["code"=>200,'Status' => 'success','message'=>'data sent successfully','data'=>$Advertisement]);
        }
        return response()->json(["code"=>200,'Status' => 'success','message'=>'data sent successfully','data'=>[['image'=>'csm_TI_RESTAURANT_TROPICAL-GARDEN1_RGB_2000x860_c3a4238088.jpg']]]);
    }
}
