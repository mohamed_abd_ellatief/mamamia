<?php

//namespace App\Http\Controllers\ApiController\UserController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Drivers;

class DriverController extends Controller
{


    public function index()
    {
        return('welcome');
    }
    public function getAddressesByUser(Request $request){
        $Addresses=Addresses::select('users.name','addresses.*')
        ->join('users','addresses.user_id','users.id')
        ->where('user_id',$request->user_id)->get();
        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully','data'=>$Addresses]);
    }

    public function store(Request $request){
        $addre=Addresses::where('address',$request->address)
        ->where('note',$request->note)
        ->count();
        if($addre>0){
            return response()->json(["code"=>1313,'Status' => 'error','message'=>'This Address is Found']);
        }
        $Address=new Addresses;
        $Address->address=$request->address;
        $Address->lat=$request->lat;
        $Address->lng=$request->lng;
        $Address->user_id=$request->user_id;
        $Address->note=$request->note;
        $Address->save();
        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully']);
    }

    public function update(Request $request){
        $Address=Addresses::find($request->id);
        $Address->address=$request->address;
        $Address->lat=$request->lat;
        $Address->lng=$request->lng;
        $Address->note=$request->note;
        $Address->save();
        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully']);
    }
    public function delete(Request $request){
        Addresses::where('id',$request->id)->delete();
        return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully']);
    }
}
