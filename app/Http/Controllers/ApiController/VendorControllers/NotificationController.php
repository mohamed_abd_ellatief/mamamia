<?php

namespace App\Http\Controllers\ApiController\VendorControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;
use App\Events\StatusLiked;
use Carbon\Carbon;
use App\User;
use App\Cashier;
use App\Order;

class NotificationController extends Controller
{
    public function NotificationByUser(Request $request){
        $current = Carbon::now();
        $Vendor = User::find($request->vendor_id);
        if($Vendor->category == 'cashier'){
            $Cashier=Cashier::where('cashier_id',$request->vendor_id)->first();
            $Notifications=Notification::select('notifications.*','orders.status')
                ->join('orders','notifications.order_id','orders.id')
                ->where('notifications.vendor_id',$Cashier->vendor_id)
                ->where('orders.kitchen_id',$Cashier->kitchen_id)
                ->where('notifications.type','vendor')
                ->where('notifications.views','0')
                ->whereIn('orders.status',[1,-2,-1])
                ->Orderby('notifications.created_at','desc')
                ->distinct('notifications.id')
                ->get();
        }else{
            $Notifications=Notification::select('notifications.*','orders.status')
            ->join('orders','notifications.order_id','orders.id')
            ->where('notifications.vendor_id',$request->vendor_id)
            ->where('notifications.type','vendor')
            ->where('notifications.views',0)
                ->whereIn('orders.status',[1,-2,-1])
            ->Orderby('notifications.created_at','desc')
				                ->distinct('notifications.id')

            ->get();
        }

            foreach($Notifications as $Notification){
                    $min=intval((strtotime($current->toDateTimeString())-strtotime($Notification->created_at))/60);
                    if($min>1440){
                        $days=intval($min/1440);
                        $Notification->ar_ago="مضت $days يوم";
                        $Notification->en_ago="$days Days Ago";
                    }elseif($min>60){
                        $hours=intval($min/60);
                        $Notification->ar_ago="مضت $hours ساعة";
                        $Notification->en_ago="$hours Hours Ago";
                    }else{

                        $Notification->ar_ago="مضت $min دقيقة";
                        $Notification->en_ago="$min Minutes Ago";
                    }
                    $Notification->partyName=Order::find($Notification->order_id)->en_partyname;
                    $Notification->typeOrder=Order::find($Notification->order_id)->type;

            }
        return response()->json(["code"=>200,'Status'=>'success','message' => 'Notification','data'=>$Notifications]);
    }

    // public function convertViewsNotificationVendor(Request $request){
    //     $Notification=Notification::find($request->id);
    //     $Notification->views=1;
    //     $Notification->save();
    //     $count=Notification::where('vendor_id',$Notification->vendor_id)
    //     ->where('type','vendor')
    //     ->where('views','0')
    //     ->count();
    //     event(new StatusLiked($count,$Notification->vendor_id));
    //     return response()->json(["code"=>200,'Status'=>'success','message' => 'Notification']);
    // }
}
