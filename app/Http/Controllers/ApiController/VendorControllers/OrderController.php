<?php

namespace App\Http\Controllers\ApiController\VendorControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use DB;
use App\Meal;
use App\Notification;
use App\Kitchen;
use App\Additions;
use Carbon\Carbon;
use App\Events\StatusLiked;
use App\User;
use App\Cashier;
use App\Verndorrule;
use App\Userpoint;

class OrderController extends Controller
{
        public function NewOrdersByVender(Request $request){ //New Orders
          $vendor=User::find($request->vendor_id);
          if($vendor->category == "cashier"){
              $Cashier=Cashier::where('cashier_id',$request->vendor_id)->first();
              $count=Notification::select('notifications.*','orders.status')
              ->join('orders','notifications.order_id','orders.id')
              ->where('notifications.vendor_id',$Cashier->vendor_id)
              ->where('orders.kitchen_id',$Cashier->kitchen_id)
              ->where('notifications.type','vendor')
              ->where('notifications.views','0')
              ->distinct('notifications.id')
              ->count();
              // return $count;
                event(new StatusLiked($count,$Cashier->cashier_id));
          }
          if(isset($request->offset)){
            $offset=$request->offset;
          }else{
            $offset=0;
          }
          if($request->from == 0 && $request->to== 0){
            $request->from=null;
            $request->to=null;
          }
          if($request->id == 0){
            $request->id=null;
          }
          $count=0;
          $ActualValue = $offset  * 10;
          if(isset($request->id)){   //Search New Orders By id
            if($vendor->category == "cashier"){
            $count=Order::select('totalprice','userplace','id')
              ->where('status',0)
              ->where('id',$request->id)
              ->where('kitchen_id',$Cashier->kitchen_id)
              ->count();
            $Orders=Order::select('totalprice','userplace','id')
              ->where('status',0)
              ->where('id',$request->id)
              ->where('kitchen_id',$Cashier->kitchen_id)
              ->orderby('created_at','desc')
              ->skip($ActualValue)->take(10)->get();
            }else{
              $count=Order::select('totalprice','userplace','id')
                ->where('status',0)
                ->where('id',$request->id)
                ->where('vendor_id',$request->vendor_id)
                ->count();
              $Orders=Order::select('totalprice','userplace','id')
                ->where('status',0)
                ->where('id',$request->id)
                ->where('vendor_id',$request->vendor_id)
                ->orderby('created_at','desc')
                ->skip($ActualValue)->take(10)->get();

            }
          }elseif(isset($request->from) && isset($request->to)){ //Search New Orders By Date
            $from=date('Y-m-d', strtotime($request->from));
            $to=date('Y-m-d', strtotime($request->to));
            if($vendor->category == "cashier"){   
            $count=Order::select('totalprice','userplace','id')
              ->where('status',0)
              ->where('kitchen_id',$Cashier->kitchen_id)
              ->whereBetween('created_at', [$from, $to])
              ->count();
            $Orders=Order::select('totalprice','userplace','id')
              ->where('status',0)
              ->where('kitchen_id',$Cashier->kitchen_id)
              ->whereBetween('created_at', [$from, $to])
              ->orderby('created_at','desc')
              ->skip($ActualValue)->take(10)->get();
            }else{
              $count=Order::select('totalprice','userplace','id')
                ->where('status',0)
                ->where('vendor_id',$request->vendor_id)
                ->whereBetween('created_at', [$from, $to])
                ->count();
              $Orders=Order::select('totalprice','userplace','id')
                ->where('status',0)
                ->where('vendor_id',$request->vendor_id)
                ->whereBetween('created_at', [$from, $to])
                ->orderby('created_at','desc')
                ->skip($ActualValue)->take(10)->get();
            }
            }else{                                                  // Search All New Orders 
              if($vendor->category == "cashier"){   
              $count=Order::select('totalprice','userplace','id') 
                ->where('status',0)
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->count();
              $Orders=Order::select('totalprice','userplace','id') 
                ->where('status',0)
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->orderby('created_at','desc')
                ->skip($ActualValue)->take(10)->get();
              }else{
                $count=Order::select('totalprice','userplace','id') 
                  ->where('status',0)
                  ->where('vendor_id',$request->vendor_id)
                  ->count();
                $Orders=Order::select('totalprice','userplace','id') 
                  ->where('status',0)
                  ->where('vendor_id',$request->vendor_id)
                  ->orderby('created_at','desc')
                  ->skip($ActualValue)->take(10)->get();

              }
          }
            if(count($Orders) > 0){
              foreach ($Orders as $Order) {
                $Meals= DB::table('order_meal')->where('order_id',$Order->id)->get();
                $CountMeals= DB::table('order_meal')->where('order_id',$Order->id)->count();
                if(count($Meals) > 0){
                  $Meal=Meal::find($Meals[0]->meal_id);
                  $Order->ar_title=$Meal->ar_title;
                  $Order->count=$CountMeals;
                  $Order->en_title=$Meal->en_title;
                }
              }
              return response()->json(["code"=>200,'Status'=>'success','message' => 'New Orders','count'=>$count,'NewOrders'=> $Orders]);
            }else{
              return response()->json(["code"=>1313,'Status'=>'error','message' => ' No Orders','count'=>$count]);

            }
        }
        public function BanquetsByVender(Request $request){  //Banquets
          $vendor=User::find($request->vendor_id);
          if($vendor->category == "cashier"){
            $Cashier=Cashier::where('cashier_id',$request->vendor_id)->first();
          }
          if(isset($request->offset)){
            $offset=$request->offset;
          }else{
            $offset=0;
          }
          if($request->from == 0 && $request->to== 0){
            $request->from=null;
            $request->to=null;
          }
          if($request->id == 0){
            $request->id=null;
          }
          $count=0;
          $ActualValue = $offset  * 10;
          if(isset($request->id)){                          //Banquets By Id
            if($vendor->category == "cashier"){
            $count=Order::select('partylocation','id','ar_partyname','en_partyname')
              ->where('status',2)
              ->where('id',$request->id)
              ->where('kitchen_id',$Cashier->kitchen_id)
              ->count();
            $Orders=Order::select('partylocation','id','ar_partyname','en_partyname')
              ->where('status',2)
              ->where('id',$request->id)
              ->where('kitchen_id',$Cashier->kitchen_id)
              ->orderby('created_at','desc')
              ->skip($ActualValue)->take(10)->get();
            }else{
              $count=Order::select('partylocation','id','ar_partyname','en_partyname')
                ->where('status',2)
                ->where('id',$request->id)
                ->where('vendor_id',$request->vendor_id)
                ->count();
              $Orders=Order::select('partylocation','id','ar_partyname','en_partyname')
                ->where('status',2)
                ->where('id',$request->id)
                ->where('vendor_id',$request->vendor_id)
                ->orderby('created_at','desc')
                ->skip($ActualValue)->take(10)->get();

            }
          }elseif(isset($request->from) && isset($request->to)){ //Banquets By Date
            $from=date('Y-m-d', strtotime($request->from));
            $to=date('Y-m-d', strtotime($request->to));
            if($vendor->category == "cashier"){
              $count=Order::select('partylocation','id','ar_partyname','en_partyname')
                ->where('status',2)
                ->whereBetween('created_at', [$from, $to])
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->count();
              $Orders=Order::select('partylocation','id','ar_partyname','en_partyname')
                ->where('status',2)
                ->whereBetween('created_at', [$from, $to])
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->orderby('created_at','desc')
                ->skip($ActualValue)->take(10)->get();
              }else{
                $count=Order::select('partylocation','id','ar_partyname','en_partyname')
                  ->where('status',2)
                  ->whereBetween('created_at', [$from, $to])
                  ->where('vendor_id',$request->vendor_id)
                  ->count();
                $Orders=Order::select('partylocation','id','ar_partyname','en_partyname')
                  ->where('status',2)
                  ->whereBetween('created_at', [$from, $to])
                  ->where('vendor_id',$request->vendor_id)
                  ->orderby('created_at','desc')
                  ->skip($ActualValue)->take(10)->get();
            }
          }else{                                           // All Banquets
            if($vendor->category == "cashier"){
              $count=Order::select('partylocation','id','ar_partyname','en_partyname')
                ->where('status',2)
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->count();
              $Orders=Order::select('partylocation','id','ar_partyname','en_partyname')
                ->where('status',2)
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->orderby('created_at','desc')
                ->skip($ActualValue)->take(10)->get();
              }else{
                $count=Order::select('partylocation','id','ar_partyname','en_partyname')
                  ->where('status',2)
                  ->where('vendor_id',$request->vendor_id)
                  ->count();
                $Orders=Order::select('partylocation','id','ar_partyname','en_partyname')
                  ->where('status',2)
                  ->where('vendor_id',$request->vendor_id)
                  ->orderby('created_at','desc')
                  ->skip($ActualValue)->take(10)->get();

            }
          }
            if(count($Orders) > 0){
              return response()->json(["code"=>200,'Status'=>'success','message' => 'Banquets','count'=>$count,'Banquets'=> $Orders]);
            }else{
              return response()->json(["code"=>1313,'Status'=>'error','message' => ' No Banquets','count'=>$count]);

            }
        }

        public function ArchivesByVender(Request $request){
          if(isset($request->offset)){
            $offset=$request->offset;
          }else{
            $offset=0;
          }
          if($request->from == 0 && $request->to== 0){
            $request->from=null;
            $request->to=null;
          }
          if($request->id == 0){
            $request->id=null;
          }
          $count_meal=0;
          $count_banquet=0;
          $ActualValue = $offset  * 10;
          if(isset($request->id)){
            $count_meal=Order::select('totalprice','userplace','id','status','type','created_at')
              ->whereIn('status',[1,-1])
              ->where('type','meal')
              ->where('id',$request->id)
              ->where('vendor_id',$request->vendor_id)
              ->count();
            $count_banquet=Order::select('totalprice','en_partyname','userplace','id','status','type','created_at')
              ->whereIn('status',[1,-1])
              ->where('type','banquet')
              ->where('id',$request->id)
              ->where('vendor_id',$request->vendor_id)
              ->count();
            $Orders=Order::select('totalprice','en_partyname','userplace','id','status','type','created_at')
              ->whereIn('status',[1,-1])
              ->where('id',$request->id)
              ->where('vendor_id',$request->vendor_id)
              ->skip($ActualValue)->take(10)->orderby('id','desc')->get();
            }elseif(isset($request->from) && isset($request->to)){
              $from=date('Y-m-d', strtotime($request->from));;
              $to=date('Y-m-d', strtotime($request->to));;
              $count_meal=Order::select('totalprice','userplace','id','status','type','created_at')
                ->whereIn('status',[1,-1])
                ->where('type','meal')
                ->whereBetween('created_at', [$from, $to])
                ->where('vendor_id',$request->vendor_id)
                ->count();
              $count_banquet=Order::select('totalprice','userplace','id','status','type','created_at')
                ->whereIn('status',[1,-1])
                ->where('type','banquet')
                ->whereBetween('created_at', [$from, $to])
                ->where('vendor_id',$request->vendor_id)
                ->count();
              $Orders=Order::select('totalprice','en_partyname','userplace','id','status','type','created_at')
                ->whereIn('status',[1,-1])
                ->whereBetween('created_at', [$from, $to])
                ->where('vendor_id',$request->vendor_id)
                ->skip($ActualValue)->take(10)->orderby('id','desc')->get();
            }else{
              $count_meal=Order::select('totalprice','userplace','id','status','type','created_at')
                ->whereIn('status',[1,-1])
                ->where('type','meal')
                ->where('vendor_id',$request->vendor_id)
                ->count();
              $count_banquet=Order::select('totalprice','userplace','id','status','type','created_at')
                ->whereIn('status',[1,-1])
                ->where('type','banquet')
                ->where('vendor_id',$request->vendor_id)
                ->count();
              $Orders=Order::select('totalprice','en_partyname','userplace','id','status','type','created_at')
                ->whereIn('status',[1,-1])
                ->where('vendor_id',$request->vendor_id)
                ->skip($ActualValue)->take(10)->orderby('id','desc')->get();
            }
            if(count($Orders) > 0){
              foreach ($Orders as $Order) {
                $Meals= DB::table('order_meal')->where('order_id',$Order->id)->get();
                $CountMeals= DB::table('order_meal')->where('order_id',$Order->id)->count();
                if(count($Meals) > 0){
                  $Meal=Meal::find($Meals[0]->meal_id);
                  $Order->ar_title=$Meal->ar_title;
                  $Order->count=$CountMeals;
                  $Order->en_title=$Meal->en_title;
                }
              }
              return response()->json(["code"=>200,'Status'=>'success','message' => 'Archives','count_meal'=>$count_meal,'count_banquet'=>$count_banquet,'Archives'=> $Orders]);
            }else{
              return response()->json(["code"=>1313,'Status'=>'error','message' => ' No Archives','count_meal'=>$count_meal,'count_banquet'=>$count_banquet]);

            }
        }
        public function NewArchivesByVender(Request $request){
          $vendor=User::find($request->vendor_id);
          if($vendor->category == "cashier"){
            $Cashier=Cashier::where('cashier_id',$request->vendor_id)->first();
          }
          if(isset($request->offset)){
            $offset=$request->offset;
          }else{
            $offset=0;
          }
          if($request->from == 0 && $request->to== 0){
            $request->from=null;
            $request->to=null;
          }
          if($request->id == 0){
            $request->id=null;
          }
          $count=0;
          $ActualValue = $offset  * 10;
          if(isset($request->id)){
            if($vendor->category == "cashier"){
              $count=Order::whereIn('status',[1,-1])
                ->where('type',$request->type)
                ->where('id',$request->id)
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->count();
              $Orders=Order::select('partylocation','en_partyname','component','totalprice','userplace','id','status','type','created_at')
                ->whereIn('status',[1,-1])
                ->where('type',$request->type)
                ->where('id',$request->id)
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->orderby('updated_at','desc')
                ->skip($ActualValue)->take(10)->get();
            }else{
              $count=Order::whereIn('status',[1,-1])
                ->where('type',$request->type)
                ->where('id',$request->id)
                ->where('vendor_id',$request->vendor_id)
                ->count();
              $Orders=Order::select('partylocation','en_partyname','component','totalprice','userplace','id','status','type','created_at')
                ->whereIn('status',[1,-1])
                ->where('type',$request->type)
                ->where('id',$request->id)
                ->where('vendor_id',$request->vendor_id)
                ->orderby('updated_at','desc')
                ->skip($ActualValue)->take(10)->get();
            }
            }elseif(isset($request->from) && isset($request->to)){
              $from=date('Y-m-d', strtotime($request->from));
              $to=date('Y-m-d', strtotime($request->to));
              if($vendor->category == "cashier"){
              $count=Order::whereIn('status',[1,-1])
                ->where('type',$request->type)
                ->whereBetween('created_at', [$from, $to])
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->count();
              $Orders=Order::select('partylocation','en_partyname','component','totalprice','userplace','id','status','type','created_at')
                ->whereIn('status',[1,-1])
                ->where('type',$request->type)
                ->whereBetween('created_at', [$from, $to])
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->orderby('updated_at','desc')
                ->skip($ActualValue)->take(10)->get();
              }else{
                $count=Order::
                  whereIn('status',[1,-1])
                  ->where('type',$request->type)
                  ->whereBetween('created_at', [$from, $to])
                  ->where('vendor_id',$request->vendor_id)
                  ->count();
                $Orders=Order::select('partylocation','en_partyname','component','totalprice','userplace','id','status','type','created_at')
                  ->whereIn('status',[1,-1])
                  ->where('type',$request->type)
                  ->whereBetween('created_at', [$from, $to])
                  ->where('vendor_id',$request->vendor_id)
                  ->orderby('updated_at','desc')
                  ->skip($ActualValue)->take(10)->get();

              }
            }else{
              if($vendor->category == "cashier"){
              $count=Order::whereIn('status',[1,-1])
                ->where('type',$request->type)
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->count();
              $Orders=Order::select('partylocation','en_partyname','component','totalprice','userplace','id','status','type','created_at')
                ->whereIn('status',[1,-1])
                ->where('type',$request->type)
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->orderby('updated_at','desc')
                ->skip($ActualValue)->take(10)->get();
              }else{
                $count=Order::whereIn('status',[1,-1])
                  ->where('type',$request->type)
                  ->where('vendor_id',$request->vendor_id)
                  ->count();
                $Orders=Order::select('partylocation','en_partyname','component','totalprice','userplace','id','status','type','created_at')
                  ->whereIn('status',[1,-1])
                  ->where('type',$request->type)
                  ->where('vendor_id',$request->vendor_id)
                  ->orderby('updated_at','desc')
                  ->skip($ActualValue)->take(10)->get();

              }
            }
            if(count($Orders) > 0){
              foreach ($Orders as $Order) {
                $Meals= DB::table('order_meal')->where('order_id',$Order->id)->get();
                $CountMeals= DB::table('order_meal')->where('order_id',$Order->id)->count();
                if(count($Meals) > 0){
                  $Meal=Meal::find($Meals[0]->meal_id);
                  $Order->ar_title=$Meal->ar_title;
                  $Order->count=$CountMeals;
                  $Order->en_title=$Meal->en_title;
                }
              }
              return response()->json(["code"=>200,'Status'=>'success','message' => 'Archives','count'=>$count,'Archives'=> $Orders]);
            }else{
              return response()->json(["code"=>1313,'Status'=>'error','message' => ' No Archives','count'=>$count]);
            }
        }
        public function InvoiceByVender(Request $request){
          $vendor=User::find($request->vendor_id);
          if($vendor->category == "cashier"){
            $Cashier=Cashier::where('cashier_id',$request->vendor_id)->first();
          }
          if(isset($request->offset)){
            $offset=$request->offset;
          }else{
            $offset=0;
          }
          if($request->from == 0 && $request->to== 0){
            $request->from=null;
            $request->to=null;
          }
          if($request->id == 0){
            $request->id=null;
          }
          $ActualValue = $offset  * 10;
          $count=0;
          if(isset($request->id)){
            if($vendor->category == "cashier"){
            $count=Order::where('status',1)
              ->where('id',$request->id)
              ->where('kitchen_id',$Cashier->kitchen_id)
              ->count();
            $Orders=Order::select('totalprice','userplace','id','status','type','created_at')
              ->where('status',1)
              ->where('id',$request->id)
              ->where('kitchen_id',$Cashier->kitchen_id)
              ->orderby('updated_at','desc')
              ->skip($ActualValue)->take(10)->orderby('id','desc')->get();
            }else{
              $count=Order::where('status',1)
                ->where('id',$request->id)
                ->where('vendor_id',$request->vendor_id)->count();
              $Orders=Order::select('totalprice','userplace','id','status','type','created_at')
                ->where('status',1)
                ->where('id',$request->id)
                ->where('vendor_id',$request->vendor_id)
                ->orderby('updated_at','desc')
                ->skip($ActualValue)->take(10)->orderby('id','desc')->get();
            }
            }elseif(isset($request->from) && isset($request->to)){
              $newNumbers = range(0, 9);
          //     // Arabic Numeric
              $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
              $fro =  str_replace($arabic, $newNumbers, $request->from);
              $t =  str_replace($arabic, $newNumbers, $request->to);
              $from=date('Y-m-d', strtotime($fro));
              $to=date('Y-m-d', strtotime($t));
              if($vendor->category == "cashier"){
              $count=Order::where('status',1)
                ->whereBetween('created_at', [$from, $to])
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->count();
              $Orders=Order::select('totalprice','userplace','id','status','type','created_at')
                ->where('status',1)
                ->whereBetween('created_at', [$from, $to])
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->orderby('updated_at','desc')
                ->skip($ActualValue)->take(10)->orderby('id','desc')->get();
              }else{
                $count=Order::where('status',1)
                  ->whereBetween('created_at', [$from, $to])
                  ->where('vendor_id',$request->vendor_id)
                  ->count();
                $Orders=Order::select('totalprice','userplace','id','status','type','created_at')
                  ->where('status',1)
                  ->whereBetween('created_at', [$from, $to])
                  ->where('vendor_id',$request->vendor_id)
                  ->orderby('updated_at','desc')
                  ->skip($ActualValue)->take(10)->orderby('id','desc')->get();

              }
            }else{
              if($vendor->category == "cashier"){
              $count=Order::where('status',1)
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->count();
              $Orders=Order::select('totalprice','userplace','id','status','type','created_at')
                ->where('status',1)
                ->where('kitchen_id',$Cashier->kitchen_id)
                ->orderby('updated_at','desc')
                ->skip($ActualValue)->take(10)->orderby('id','desc')->get();
              }else{
                $count=Order::where('status',1)
                  ->where('vendor_id',$request->vendor_id)
                  ->count();
                $Orders=Order::select('totalprice','userplace','id','status','type','created_at')
                  ->where('status',1)
                  ->where('vendor_id',$request->vendor_id)
                  ->orderby('updated_at','desc')
                  ->skip($ActualValue)->take(10)->orderby('id','desc')->get();
              }
            }
            
            if(count($Orders) > 0){
              foreach ($Orders as $Order) {
                $Order->ar_orderDate=$Order->created_at->format('Y-m-d');
                $Order->ar_orderTime=$Order->created_at->format('i:H');
                $Order->en_orderDate=$Order->created_at->format('d-m-Y');
                $Order->en_orderTime=$Order->created_at->format('H:i');
                $Meals= DB::table('order_meal')->where('order_id',$Order->id)->get();
                $CountMeals= DB::table('order_meal')->where('order_id',$Order->id)->count();
                if(count($Meals) > 0){
                  $Meal=Meal::find($Meals[0]->meal_id);
                  $Order->ar_title=$Meal->ar_title;
                  $Order->count=$CountMeals;
                  $Order->en_title=$Meal->en_title;
                }
              }
              return response()->json(["code"=>200,'Status'=>'success','message' => 'Archives','count'=>$count,'Archives'=> $Orders]);
            }else{
              return response()->json(["code"=>1313,'Status'=>'error','message' => ' No Archives','count'=>$count]);

            }
        }
        public function Orderdetails(Request $request){
          $oneorder=Order::where('id',$request->id)->select('id','codeorder','component','user_id',
          'kitchen_id','userplace','orderdate','ordertime','usernote','vendornote','neededtime',
          'deliverytotalprice','mealstotalprice','priceVat','discountpoint','totalprice',
          'distancetouser','status','type','en_partyname','partydate','partytime','partytype',
          'partylocation','guestnum','timetodeliver','created_at')->first();
          
          if($request->type == "user"){
            $Notification=Notification::where('order_id',$request->id)->where('type','user')->first();
            if($Notification){
            $Notification->views=1;
            $Notification->save();
                    $count=Notification::select('notifications.*','orders.status')
        ->join('orders','notifications.order_id','orders.id')
        ->where('notifications.user_id',$request->user_id)
        ->where('notifications.type','user')
        ->where('notifications.views',0)
    ->whereIn('orders.status',[1,-2,-1])
        ->orderBy('notifications.created_at','desc')
        ->count();
            event(new StatusLiked($count,$Notification->user_id));
            }
          }else{
            $Notification=Notification::where('order_id',$request->id)->where('type','vendor')->first();
            if($Notification){
            $Notification->views=1;
            $Notification->save();
          $count=Notification::select('notifications.id')
    ->join('orders','notifications.order_id','orders.id')
    ->where('notifications.vendor_id',$Notification->vendor_id)
        ->where('notifications.type','vendor')
        ->where('notifications.views','0')
    ->whereIn('orders.status',[1,-2,-1])
        ->count();
            
            event(new StatusLiked($count,$Notification->vendor_id));
            }
          }
          if(isset($oneorder) ){
            $oneorder->orderdate=$oneorder->created_at->format('Y-m-d');
            $oneorder->ordertime=$oneorder->created_at->format('H:i');
            $MealName=[];
            $usernote="";
            $Meals_Order=DB::table('order_meal')->where('order_id',$request->id)->get();
            $count=1;
            foreach($Meals_Order as $Meal_Order){
              $MealsName=Meal::select('meals.ar_title','meals.en_title')
              ->join('kitchentags','meals.kitchentag_id','kitchentags.id')
              ->where('meals.id',$Meal_Order->meal_id)
              ->first();
              $MealsName['amount']=$Meal_Order->amount;
        
        
              $Addition_ids[]=explode(",",$Meal_Order->additions);
        $d=0;$temp=[];
        foreach($Addition_ids[0] as $Addition_id)
        {
         $AdditionsName=Additions::select('additions.ar_title','additions.en_title')
              
              ->where('additions.id',$Addition_id)
              ->first();  
        
         $temp[]=$AdditionsName;
       
               $d++;
        }
         $MealsName['additions']=$temp;
        //$MealsName['additions']=$temp;
        
        //$MealsName['additions']="[".$Meal_Order->additions."]"; // hamza
       // return  $Addition_ids[0][0];
        
        
              $MealName[]=$MealsName;
              if($oneorder->type == "meal"){
                if($count ==1){
                  $usernote .= "$Meal_Order->note";
                }else{
                  $usernote .= "\n\n$Meal_Order->note";
                }
                $count++;
              }
            }
            $maxtime=Meal::join('order_meal','meals.id','order_meal.meal_id')
              ->where('order_meal.order_id',$request->id)
              ->max('meals.neededtime');
              $distancetime=$oneorder->distancetouser;
              $oneorder->deliverytime=$oneorder->timetodeliver;
              $oneorder['meals']=$MealName;
        
              $oneorder->user=User::select('mobilenumber','name')->where('id',$oneorder->user_id)->first();
              $oneorder->kitchen=Kitchen::where('id',$oneorder->kitchen_id)->select('ar_title','en_title')->first();
              if($oneorder->type == "meal"){
                $oneorder->usernote=$usernote;
              }
              return response()->json(["code"=>200,'Status'=>'success','message' => 'one order','oneorder'=> $oneorder]);
            }else{
              return response()->json(["code"=>1313,'Status'=>'error','message' => ' no orders']);
            }
        }
        public function acceptorder(Request $request){
          $ordertoaccept=Order::find($request->id);
          $ordertoaccept->vendornote=$request->vendornote;
          $ordertoaccept->status=1;
          $ordertoaccept->save();
          $Meal_id=DB::table('order_meal')->where('order_id',$request->id)->first();
          if (empty($Meal_id)) {
           return response()->json(["code"=>400,'Status'=>'error','message' => 'order not found']);
          }
          $Meal=Meal::find($Meal_id->meal_id);
          $Notification=Notification::where('order_id',$request->id)->first();
          $Notification->ar_description="تم قبول الطلب ( ".$Meal->ar_title." )";
          $Notification->en_description="Your Order accepted ( ".$Meal->en_title." )";
          $Notification->type="user";
          $Notification->views=0;
          $Notification->save();
          $count=Notification::where('user_id',$ordertoaccept->user_id)
          ->where('type','user')
          ->where('views','0')
          ->count();
          //event(new StatusLiked($count,$ordertoaccept->user_id));
          $Rule=Verndorrule::where('vendor_id',$ordertoaccept->vendor_id)
          ->where('kitchen_id',$ordertoaccept->kitchen_id)
          ->where('rule','<=',$ordertoaccept->totalprice)
          ->first();
          if($Rule){   // Give Point to User
            $Point=intval(($ordertoaccept->totalprice*$Rule->points)/$Rule->rule);
            $PointUser=Userpoint::where('user_id',$ordertoaccept->user_id)
            ->where('vendor_id',$ordertoaccept->vendor_id)
            ->where('kitchen_id',$ordertoaccept->kitchen_id)
            ->first();
            if($PointUser){
                $Userpoint=Userpoint::find($PointUser->id);
                $Userpoint->points=$Userpoint->points+$Point;
                $Userpoint->save();
            }else{
                $Userpoint=new Userpoint;
                $Userpoint->user_id=$ordertoaccept->user_id;
                $Userpoint->vendor_id=$ordertoaccept->vendor_id;
                $Userpoint->kitchen_id=$ordertoaccept->kitchen_id;
                $Userpoint->points=$Point;
                $Userpoint->save();
            }
          }
        
          
          return response()->json(["code"=>200,'Status'=>'success','message' => 'order accepted']);
        }

        public function ignororder(Request $request){
          
          $ordertoignor=Order::find($request->order_id);
          if($request->type == "user"){
            $ordertoignor->status=-2;
          }else{
            $ordertoignor->vendornote=$request->vendornote;
            $ordertoignor->status=-1;
          }
          $ordertoignor->save();
          if($ordertoignor->usepoint> 0){
            $PointUser=Userpoint::where('user_id',$ordertoignor->user_id)
              ->where('vendor_id',$ordertoignor->vendor_id)
              ->where('kitchen_id',$ordertoignor->kitchen_id)
              ->first();
              if($PointUser){
                  $Userpoint=Userpoint::find($PointUser->id);
                  $Userpoint->points=$Userpoint->points+$ordertoignor->usepoint;
                  $Userpoint->save();
              }else{
                  $Userpoint=new Userpoint;
                  $Userpoint->user_id=$ordertoignor->user_id;
                  $Userpoint->vendor_id=$ordertoignor->vendor_id;
                  $Userpoint->kitchen_id=$ordertoignor->kitchen_id;
                  $Userpoint->points=$ordertoignor->usepoint;
                  $Userpoint->save();
              }
          }
          if($ordertoignor->type == "meal" && $ordertoignor->status == -1){
            $Meal_id=DB::table('order_meal')->where('order_id',$request->order_id)->first();
            $Meal=Meal::find($Meal_id->meal_id);
            $Notification=Notification::where('order_id',$request->order_id)->first();
            $Notification->ar_description="تم رفض الطلب ( ".$Meal->ar_title." )";
            $Notification->en_description="Your Order rejected ( ".$Meal->en_title." )";
            $Notification->type="user";
            $Notification->views=0;
            $Notification->save();
            $count=Notification::where('user_id',$ordertoignor->user_id)
            ->where('type','user')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count,$ordertoignor->user_id));
          }else{
            $Notification=Notification::where('order_id',$request->order_id)->delete();
            $count=Notification::where('vendor_id',$ordertoignor->vendor_id)
            ->where('type','vendor')
            ->where('views','0')
            ->count();
            event(new StatusLiked($count,$ordertoignor->vendor_id));
          }
          return response()->json(["code"=>200,'Status'=>'success','message' => 'order ignored']);
        }

        public function OrderAccept(Request $request){
          $ordertoaccept=Order::find($request->id);

          if($request->price){
            $ordertoaccept->totalprice=$request->price;
          }
          if($request->deliverytotalprice){
            $ordertoaccept->deliverytotalprice=$request->deliverytotalprice;
          }
          if($request->vendornote){
            $ordertoaccept->vendornote=$request->vendornote;
          }
          if($request->neededTime){
            $ordertoaccept->neededTime=$request->neededTime;
          }
          $ordertoaccept->status=3;
          $ordertoaccept->save();
          
          $Notification=Notification::where('order_id',$request->id)->first();
          $Notification->ar_description="قدم عرض بخصوص الوليمة الخاصة بكم ( ".$ordertoaccept->en_partyname." )";
          $Notification->en_description="sent offer for your Banquet ( ".$ordertoaccept->en_partyname." )";
          $Notification->type="user";
          $Notification->views=0;
          $Notification->save();
          $count=Notification::where('user_id',$ordertoaccept->user_id)
          ->where('type','user')
          ->where('views','0')
          ->count();
          event(new StatusLiked($count,$ordertoaccept->user_id));
          return response()->json(["code"=>200,'Status'=>'success','message' => 'order accepted']);
        }
}
