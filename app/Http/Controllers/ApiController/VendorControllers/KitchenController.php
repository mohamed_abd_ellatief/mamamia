<?php

namespace App\Http\Controllers\ApiController\VendorControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kitchencategory;
use App\Kitchentag;
use App\Vendor;
use App\Kitchen;
use App\Meal;
use App\Cashier;
use App\Day;
use App\User;
use App\Availblehours;
use App\Kitchenimage;
use App\Timecategory;
use App\Notification;
use App\Events\StatusLiked;

class KitchenController extends Controller
{

  public function Selectalltimescategories(){
      $alltimecategories=Timecategory::all();
      if(isset($alltimecategories)){
        return response()->json(["code"=>200,'Status'=>'success','message' => 'all time categories','alltimecategories'=> $alltimecategories]);
      }else{
        return response()->json(["code"=>1313,'Status'=>'error','message' => ' no time categories']);

      }
  }
  Public function Selectonvendorkitchens(Request $request){
    $Vendor=User::find($request->user_id)->category;
    if($Vendor == "cashier"){
      $Kitchen_id=Cashier::where('cashier_id',$request->user_id)->first()->kitchen_id;
      $allkitchenforoneuser=Kitchen::select('id','ar_title','en_title','image')->where('id',$Kitchen_id)->get();
    }else{
      $allkitchenforoneuser=Kitchen::select('id','ar_title','en_title','image')->where('user_id',$request->user_id)->get();
    }
      // Notification

	          $count=Notification::select('notifications.id')
		->join('orders','notifications.order_id','orders.id')
		->where('notifications.vendor_id',$request->user_id)
        ->where('notifications.type','vendor')
        ->where('notifications.views','0')
		->whereIn('orders.status',[1,-1,-2])
        ->count();
      event(new StatusLiked($count,intval($request->user_id)));
    if(count($allkitchenforoneuser) > 0 ){
      return response()->json(["code"=>200,'Status'=>'success','message' => ' kitchens selected','kitchensdata'=> $allkitchenforoneuser]);
    }else{
      return response()->json(["code"=>1313,'Status'=>'error','message' => 'no kitchen for this user']);
    }
  }
  Public function SelectonecKitchendata(Request $request){
    $onekitchen=Kitchen::find($request->kitchen_id);
    $onekitchen->kitchencategory;
    $onekitchen->kitchenimages;
    $onekitchen->kitchentags;
    $onekitchen->hours=Availblehours::where([['attribute_id',$request->kitchen_id],['attribute_category','kitchen']])->orderby('day_id','asc')->get();
    return response()->json(["code"=>200,'Status'=>'success','message' => 'kitchen selected','onekitchendata'=> $onekitchen]);
  }

  Public function ChangeKitchenopenstatus(Request $request){
    $onekitchendata=Kitchen::find($request->kitchen_id);
    if($onekitchendata->openstatus == 1){
      $onekitchendata->openstatus = 0;
    }else{
      $onekitchendata->openstatus = 1;
    }
    $onekitchendata->save();
    return response()->json(["code"=>200,'Status'=>'success','message' => 'kitchen openstatus changed']);

  }

  Public function SelectallKitchensCategories(){
    $allkitchencategories=Kitchencategory::all();
    if(count($allkitchencategories) > 0 ){
      return response()->json(["code"=>200,'Status'=>'success','message' => 'all kitchen categories selected','allkitchencategoriesdata'=> $allkitchencategories]);
    }else{
      return response()->json(["code"=>1313,'Status'=>'success','message' => 'no kitchen categories']);
    }
  }

  Public function SelectallKitchensTags(){
    $allkitchentags=Kitchentag::all();
    if(count($allkitchentags) > 0 ){
      return response()->json(["code"=>200,'Status'=>'success','message' => 'all kitchen tags selected','allkitchentagsdata'=> $allkitchentags]);
    }else{
      return response()->json(["code"=>1313,'Status'=>'success','message' => 'no kitchen tags']);
    }
  }

  Public function Selectalldays(){
    $alldays=Day::all();
    if(count($alldays) > 0 ){
      return response()->json(["code"=>200,'Status'=>'success','message' => 'all Days selected','alldaysdata'=> $alldays]);
    }else{
      return response()->json(["code"=>1313,'Status'=>'error','message' => 'no Days']);
    }
  }

  public function checkuseractivation(Request $request){
    // return $request->all();
    $usertocheck=User::find($request->id);

    if($usertocheck->status == 1){
      return response()->json(["code"=>200,'Status'=>'success','message' => 'this user is active']);
    }else{
      return response()->json(["code"=>1313,'Status'=>'error','message' => 'this user is not active']);

    }
  }


  Public function GetTime(Request $request){
    $onekitchen=Kitchen::find($request->kitchen_id);
    $onekitchen->kitchencategory;
    $onekitchen->kitchenimages;
    $onekitchen->kitchentags;
    $hour=[];
    $days=Availblehours::select('day_id')->where([['attribute_id',$request->kitchen_id],['attribute_category','kitchen']])->distinct('day_id')->get();
    foreach($days as $day){
      $id=$day->day_id+1;
      $hour[$id]=Availblehours::select('from','to')->where([['attribute_id',$request->kitchen_id],['attribute_category','kitchen'],['day_id',$day->day_id]])->get();
    }
    $onekitchen->hours=$hour;
    return response()->json(["code"=>200,'Status'=>'success','message' => 'kitchen selected','onekitchendata'=> $onekitchen]);
  }
}
