<?php

namespace App\Http\Controllers\ApiController\VendorControllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Kitchentag;
use App\Vendor;
use App\Kitchen;
use App\Meal;
use App\Day;
use App\User;
use App\Availblehours;
use App\Kitchenimage;
use App\Mealimage;
use App\Eventcategory;
class MealsController extends Controller
{


          public function Selectalleventscategories(){
              $alleventscategories=Eventcategory::all();
              if(isset($alleventscategories)){
                return response()->json(["code"=>200,'Status'=>'success','message' => ' events categories','alleventscategories'=> $alleventscategories]);
              }else{
                return response()->json(["code"=>1313,'Status'=>'error','message' => ' no events categories']);

              }
          }
          Public function Selectonekitchenmeals(Request $request){
            $mealsinkitchen=array('');
            $kitchen=Kitchen::find($request->kitchen_id);
            $kitchen->meals;
            foreach ($kitchen->meals as $value) {
              if($value->status ==  1){
                array_push($mealsinkitchen,$value);
              }
            }
            array_splice($mealsinkitchen,0,1);
            if(count($mealsinkitchen) > 0 ){
              return response()->json(["code"=>200,'Status'=>'success','message' => ' meals selected','mealsdata'=> $mealsinkitchen]);
            }else{
              return response()->json(["code"=>1313,'Status'=>'error','message' => 'no meals for this kitchen']);
            }
          }
          Public function Selectonecmealdata(Request $request){
              $timecategory=array();
              $onemeal=Meal::find($request->meal_id);
              $vendor=user::find($onemeal->user_id);
              $onemeal->vendor=$vendor;
              $onemeal->mealimages;
              $onemeal->timecategory;
              $onemeal->kitchens;
              $onemeal->hours=Availblehours::where([['attribute_id',$request->meal_id],['attribute_category','meal']])->get();
            return response()->json(["code"=>200,'Status'=>'success','message' => 'meal selected','onemealdata'=> $onemeal]);
          }
          Public function Changemealopenstatus(Request $request){
            $onemealendata=Meal::find($request->meal_id);
            if($onemealendata->openstatus == 1){
              $onemealendata->openstatus = 0;
            }else{
              $onemealendata->openstatus = 1;
            }
            $onemealendata->save();
            return response()->json(["code"=>200,'Status'=>'success','message' => 'meal openstatus changed','mealstatus'=>$onemealendata->openstatus  ]);

          }



          // public function addmeal(Request $request){
          //  // return $request->all();
          //     /* Default Code Of Helper */
          //            $flag="1";
          //            $uploadimageflag="api";
          //            $model='App\Meal';
          //            $uploadimagepath = resource_path('assets/cpanel/dist/img/meal');
          //            $arrayofindexes=array('ar_title','en_title','ar_description','en_description', 'price','neededtime',
          //            'component','user_id','kitchentag_id','customer_num');
          //             $newmeal=autoinsert($model,$request,$arrayofindexes,$uploadimagepath,$flag,$uploadimageflag);
          //              $meal=Meal::find($newmeal->id);

          //         if($request->kitchens_ids){
          //           $meal->kitchens()->attach($request->kitchens_ids);
          //         }
          //         if($request->timecategories_ids){
          //           $meal->timecategory()->attach($request->timecategories_ids);
          //         }
          //         if($request->eventcategory){
          //           $meal->eventcategory()->attach($request->eventcategory);
          //         }

          //             return response()->json(["code"=>200,'Status' => 'success','message'=>'new meal added successfully','newmealdata' => $newmeal]);
          // }


          
          // public function addimagestomeal(Request $request){
          //   $uploadimagepath = resource_path('assets/cpanel/dist/img/meal');
          //   $updatemealimage=Meal::find($request->meal_id);
          //   $updatemealimage->image=uploadimage($request->mainimage,$uploadimagepath);
          //   $updatemealimage->save();
          //   if(isset($request->images)){
          //     foreach ($request->images as $oneimage) {
          //       $newimage=new Mealimage();
          //       $newimage->meal_id=$request->meal_id;
          //       $newimage->image=uploadimage($oneimage,$uploadimagepath);
          //       $newimage->save();
          //     }
          //   }
          //   return response()->json(["code"=>200,'Status' => 'success','message'=>'new image added successfully']);
          // }



          // public function addavailabledaystomeal(Request $request){

          //   foreach ($request->availabledays as $oneday) {
          //     $newday=new Availblehours();
          //     $newday->attribute_id=$request->meal_id;
          //     $newday->from=$oneday['from'];
          //     $newday->to=$oneday['to'];
          //     $newday->attribute_category="meal";
          //     $newday->save();
          //   }
          //   return response()->json(["code"=>200,'Status' => 'success','message'=>'new days added successfully']);
          // }




          // public function updatemeal(Request $request){
          //   // return $request->all();
          //   /* Default Code Of Helper */
          //   $flag="0";
          //   $id=$request->id;
          //   $uploadimageflag="api";
          //   $model='App\Meal';
          //   $uploadimagepath = resource_path('assets/cpanel/dist/img/meal');
          //   $arrayofindexes=array('ar_title','en_title','ar_description','en_description', 'price','neededtime',
          //   'component','user_id','kitchentag_id','customer_num');
          //     $mealtoupdate=autoupdate($model,$request,$arrayofindexes,$uploadimagepath,$flag,$id,$uploadimageflag);
          //     $mealtoupdate->save();
          //     $meal=Meal::find($mealtoupdate->id);
          //     if($request->kitchens_ids){
          //       $meal->kitchens()->sync($request->kitchens_ids);
          //     }
          //     if($request->timecategory){
          //       $meal->timecategory()->sync($request->timecategory);
          //     }
          //     if($request->eventcategory){
          //       $meal->eventcategory()->sync($request->eventcategory);
          //     }
          //   return response()->json(["code"=>200,'Status' => 'success','message'=>' meal updated successfully','mealstoupdatesdata' => $mealtoupdate]);
          // }



          // public function updateimagesofmeal(Request $request){

          //   $uploadimagepath = resource_path('assets/cpanel/dist/img/meal');
          //   if(isset($request->images)){
          //     foreach ($request->images as $oneimage) {
          //       $image=Mealimage::find($oneimage);
          //       deletefile($uploadimagepath.$image->image);
          //       $image->delete();
          //     }
          //   }
          //   return response()->json(["code"=>200,'Status' => 'success','message'=>'meal images updated successfully']);
          // }



          // public function updatevailabledaysofmeal(Request $request){
          //   $daytodelte=Availblehours::where('attribute_id',$request->meal_id)->get();
          //   if(count($daytodelte)>0){
          //     foreach ($daytodelte as $value) {
          //       $value->delete();
          //         }
          //   }
          //   foreach ($request->availabledays as $oneday) {
          //     $newday=new Availblehours();
          //     $newday->attribute_id=$request->meal_id;
          //     $newday->from=$oneday['from'];
          //     $newday->to=$oneday['to'];
          //     $newday->attribute_category="meal";
          //     // $newday->save();
          //   }
          //   return response()->json(["code"=>200,'Status' => 'success','message'=>' days updated successfully']);
          // }

}
