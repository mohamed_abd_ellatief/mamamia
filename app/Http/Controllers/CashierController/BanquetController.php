<?php

namespace App\Http\Controllers\CashierController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Cashier;
use App\Order;
use App\Kitchen;
use Auth;

class BanquetController extends Controller
{
    public function index(){
        $Cashier=Cashier::where('cashier_id',Auth::user()->id)->first();
        $Orders=Order::where('kitchen_id',$Cashier->kitchen_id)
        ->where('type','banquet')
        ->where('status',2)
        ->get();
        $Restaurant=Kitchen::find($Cashier->kitchen_id);
        $ar_title="طلبات ولائم العميل";
        $en_title="Banquets";
        return view('cashier.banquet',compact('Orders','Restaurant','ar_title','en_title'));
    }

    public function invoice(){
        $Cashier=Cashier::where('cashier_id',Auth::user()->id)->first();
        $Orders=Order::where('kitchen_id',$Cashier->kitchen_id)
        ->where('type','banquet')
        ->where('status',1)
        ->get();
        $Restaurant=Kitchen::find($Cashier->kitchen_id);
        $ar_title="فواتير ولائم العميل";
        $en_title="Invoices Banquet";
        return view('cashier.banquet',compact('Orders','Restaurant','ar_title','en_title'));
    }

    public function archive(){
        $Cashier=Cashier::where('cashier_id',Auth::user()->id)->first();
        $Orders=Order::where('kitchen_id',$Cashier->kitchen_id)
        ->where('type','banquet')
        ->whereIn('status',[1,-2,-1])
        ->get();
        $Restaurant=Kitchen::find($Cashier->kitchen_id);
        $ar_title="أرشيف ولائم العميل";
        $en_title="Archives Banquet";
        return view('cashier.banquet',compact('Orders','Restaurant','ar_title','en_title'));
    }
}
