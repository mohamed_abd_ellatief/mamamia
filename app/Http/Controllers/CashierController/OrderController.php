<?php

namespace App\Http\Controllers\CashierController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Cashier;
use App\Order;
use App\Kitchen;
use Auth;

class OrderController extends Controller
{
    public function index(){
        $Cashier=Cashier::where('cashier_id',Auth::user()->id)->first();
        $Orders=Order::
        where('kitchen_id',$Cashier->kitchen_id)
        ->where('status',0)
        ->where('type','meal')
        ->get();
        $Meal_Order=[];
        foreach($Orders as $Order){
            $Meal_Order[]=DB::table('order_meal')
            ->select('order_id','meal_id')
            ->where('order_id',$Order->id)
            ->first();
        }
        $Restaurant=Kitchen::find($Cashier->kitchen_id);
        $ar_title="الطلبات الجديدة";
        $en_title="Orders";
        return view('cashier.orders',compact('Orders','Restaurant','Meal_Order','ar_title','en_title'));
    }


    public function invoice(){
        $Cashier=Cashier::where('cashier_id',Auth::user()->id)->first();
        $Orders=Order::
        where('kitchen_id',$Cashier->kitchen_id)
        ->where('status',1)
        ->where('type','meal')
        ->get();
        $Meal_Order=[];
        foreach($Orders as $Order){
            $Meal_Order[]=DB::table('order_meal')
            ->select('order_id','meal_id')
            ->where('order_id',$Order->id)
            ->first();
        }
        $Restaurant=Kitchen::find($Cashier->kitchen_id);
        $ar_title="فواتير الطلبات";
        $en_title="Invoice Orders";
        return view('cashier.orders',compact('Orders','Restaurant','Meal_Order','ar_title','en_title'));
    }


    public function archive(){
        $Cashier=Cashier::where('cashier_id',Auth::user()->id)->first();
        $Orders=Order::
        where('kitchen_id',$Cashier->kitchen_id)
        ->whereIn('status',[1,-1])
        ->where('type','meal')
        ->get();
        $Meal_Order=[];
        foreach($Orders as $Order){
            $Meal_Order[]=DB::table('order_meal')
            ->select('order_id','meal_id')
            ->where('order_id',$Order->id)
            ->first();
        }
        $Restaurant=Kitchen::find($Cashier->kitchen_id);
        $ar_title="أرشيف الطلبات";
        $en_title="Archive Orders";
        return view('cashier.orders',compact('Orders','Restaurant','Meal_Order','ar_title','en_title'));
    }
}
