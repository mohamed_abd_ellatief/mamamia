<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\kitchen;
use App\Drivers;
use App\Order;
use App\order_driver;
use App\DriverVerefication;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Hash;
use DB;
use App\Meal;
use App\Notification;
use App\Additions;
use Carbon\Carbon;
use App\Events\StatusLiked;
use App\User;
use App\Cashier;
use App\Verndorrule;
use App\Userpoint;
use App\DriverIncome;
class DriverController extends Controller
{


    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
            'password'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(["code"=>1330,'Status'=>'error','message' => $validator->errors()]);
        }
        $MobileNumber=$request->mobile_number;
        $Password=$request->password;
        $driver = DB::table('drivers')->where('phone', $MobileNumber)->first();

        if ($driver && Hash::check($request->password, $driver->password)) {
          // here you know data is valid
            //return 1;
         
             if($driver->isverified == 1){
              if($driver->status == 1 ){
               $token = Str::random(60);
               $d=Drivers::find($driver->id);
               $d->tokken=$token;
               $driver->tokken=$d->tokken;
               $d->save();
               return response()->json(["code"=>200,'Status'=>'success','message' => 'you login successfully','userdata'=> $driver]);
             }
              else{
           return response()->json(["code"=>1313,'Status'=>'error','message' => 'you need admin confirmation']);
         }
             }else{
               ////Verification///
               $newverification= new DriverVerefication();
               $newverification->driver_id=$driver->id;
               // $newverification->token= rand(rand(999, 9999),rand(999, 9999));
               $to=$driver->phone;
               $code=rand(rand(999, 9999),rand(999, 9999));
               $msg= str_replace(' ', '%20', 'نورتنا ! فضلا أدخل الرمز ')."%20".$code;
   
               $url = "http://www.jawalbsms.ws/api.php/sendsms?user=yahyainvetechs&pass=verificationSmS3&to=$to&message=$msg&sender=Mammamia";
   
               $ch = curl_init($url); // init the curl with jawalb API url
               curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
               curl_setopt($ch, CURLOPT_HEADER, 0);
               $data = curl_exec($ch);  //run curl api call, on success curl call, $data will contain the API call response
               curl_close($ch);
   
   
   
               $newverification->token=$code;
   
               $newverification->save();
               //$user->verificationcode= $newverification->token;
               return response()->json(["code"=>1314,'Status'=>'error','message' => 'you need to verifi code','userdata'=>$driver]);
             }
         // }else{
         //   return response()->json(["code"=>1313,'Status'=>'error','message' => 'you need admin confirmation']);
         // }
       }else{
         $count=Drivers::where('phone',$MobileNumber)->count();
         if($count >0 ){
           return response()->json(["code"=>1315,'Status'=>'error','message' => 'your  password is invalid']);
         }else{
           return response()->json(["code"=>1316,'Status'=>'error','message' => 'your mobile number is invalid']);
         }
       }
      }

      public function registration(Request $request){
   

        $validator = Validator::make($request->all(), [
            'phone' => 'unique:drivers,phone',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        if ($validator->fails()) {
            return response()->json(['code'=>1313,'error'=>$validator->errors()]);
        }else{

         $driver=new Drivers;
         $driver->username=$request->username;
         $driver->phone=$request->phone;
         $driver->lisence_number=$request->lisence_number;
         $driver->password=Hash::make($request->password);
        // $file = @$request->file('image');
         $fileName = null;
        if (request()->hasFile('image')) {
            $file = request()->file('image');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('./cpanel/upload/user/', $fileName);    
            $driver->lisence_image = $fileName;
          }
         // if(@$file !=""){
         //               $name=time().'.'.$file->getClientOriginalExtension();
         //          $file->move(base_path('./cpanel/upload/user'),$name);
         //                                     $User->image=$name;
   
         //   //$name=time().'.'.$file->getClientOriginalExtension();
         //   //$file->move('cpanel/upload/user',$name);
         //   //$User->image=$name;
         // }
                 // if($flag == 0){
                  /* Custom Code Of Helper */
           $driver->status=0;
           $driver->isverified=0;
           $token = Str::random(60);
           $driver->tokken=$token;
           $driver->save();
   // return 1;
                  ////Verification///
               $newverification= new DriverVerefication();
               $newverification->driver_id=$driver->id;
                  //$newverification->token= rand(rand(999, 9999),rand(999, 9999));
               $to=$driver->phone;
               $code=rand(rand(999, 9999),rand(999, 9999));
               $msg= str_replace(' ', '%20', 'نورتنا ! فضلا أدخل الرمز ')."%20".$code;
   
               $url = "http://www.jawalbsms.ws/api.php/sendsms?user=yahyainvetechs&pass=verificationSmS3&to=$to&message=$msg&sender=Mammamia";
   
               $ch = curl_init($url); // init the curl with jawalb API url
               curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
               curl_setopt($ch, CURLOPT_HEADER, 0);
               $data = curl_exec($ch);  //run curl api call, on success curl call, $data will contain the API call response
               curl_close($ch);
   
   
   
               $newverification->token=$code;
   
               $newverification->save();
                  //$newverification->save();
                  ///EndVerification////
                  //$User->verificationcode=$newverification->token;

                    return response()->json(["code"=>200,'Status' => 'success','message'=>'Register new user done successfully','userdata' => $driver]);
                 //  }
        }
      }
      public function verify_verificationcode(Request $request){
        // return $request->all();
        $usertoverifi=Drivers::find($request->driver_id);
        if($usertoverifi->isverified == 0){
          $usertoverification=DriverVerefication::where('driver_id',$request->driver_id)->get();
          if($usertoverification->last()->token == $request->token){
             foreach ($usertoverification as  $value) {
               $value->delete();
             }
             $usertoverifi=Drivers::find($request->driver_id);
             $usertoverifi->isverified=1;
             $usertoverifi->save();
             $token = Str::random(60);
             $usertoverifi->token=$token;
             if ($usertoverifi->status==1) {
      return response()->json(["code"=>200,'Status' => 'success','message'=>'you verified successfully','token'=>$usertoverifi->token,'userdata'=>$usertoverifi]);
           }
           else
           {
            return response()->json(["code"=>1315,'Status' => 'error','message'=>'the user disactived']);
           }
      
          }else{
            return response()->json(["code"=>1313,'Status' => 'error','message'=>'this token is invalid']);
          }
        }else{
          return response()->json(["code"=>1314,'Status' => 'error','message'=>'this user is verified']);
        }
      }
   
      public function foroget_passworod(Request $request){
       $driver=Drivers::where('phone',$request->phone)->first();
       //return $driver;
       if(isset($driver)){
         ////Verification///
         $newverification= new DriverVerefication();
         $newverification->driver_id=$driver->id;
         // $newverification->token= rand(rand(999, 9999),rand(999, 9999));
            $to=$driver->phone;
             $code=rand(rand(999, 9999),rand(999, 9999));
                 $msg= str_replace(' ', '%20', 'نورتنا ! فضلا أدخل الرمز ')."%20".$code;
   
         $url = "http://www.jawalbsms.ws/api.php/sendsms?user=yahyainvetechs&pass=verificationSmS3&to=$to&message=$msg&sender=Mammamia";
   
               $ch = curl_init($url); // init the curl with jawalb API url
               curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
               curl_setopt($ch, CURLOPT_HEADER, 0);
               $data = curl_exec($ch);  //run curl api call, on success curl call, $data will contain the API call response
               curl_close($ch);
   
   
   
               $newverification->token=$code;
         $newverification->save();
        // $userdata->verificationcode=$newverification->token;
   
         return response()->json(["code"=>200,'Status' => 'success','message'=>'code sent successfully','userdata'=>$driver]);
   
       }else{
         return response()->json(["code"=>1313,'Status' => 'error','message'=>'this mobile number is invalid']);
   
       }
   
      }
      public function verify_verificationcode_forget(Request $request){
        $driver=Drivers::where('phone',$request->phone)->first();
        $usertoverification=DriverVerefication::where('driver_id',$driver->id)->get();
        $usertoverification->last()->token;
        if($usertoverification->last()->token == $request->token){
          foreach ($usertoverification as  $value) {
           $value->delete();
          }
          $usertoverifi=Drivers::find($driver->id);
          $usertoverifi->isverified=1;
          $usertoverifi->save();
          if ($usertoverifi->status==1) {
 return response()->json(["code"=>200,'Status' => 'success','message'=>'you verified successfully','userdata'=>$driver]);
           }
           else
           {
            return response()->json(["code"=>1315,'Status' => 'error','message'=>'the user disactived']);
           }
         
        }else{
          return response()->json(["code"=>1313, 'Status' => 'error','message'=>'this token is invalid']);
        }
      }
   
      public function change_passworod(Request $request){
        //return $request->all();
        $driver=Drivers::find($request->driver_id);
        if ($driver && Hash::check($request->password, $driver->password)) {
          return 'its the same password ';
        }

        $driver->password =Hash::make($request->password);
        $driver->update();

        return response()->json(["code"=>200,'Status' => 'success','message'=>'password changed successfully','userdata'=>$driver]);
   
      }
   
      public function UpdateProfile(Request $request){
          $id=$request->user_id;
         // return $request->all();
         if(checkmobilenumberupdate($request->mobilenumber,$id) == "false"){
            return response()->json(["code"=>1313,'Status'=>'error','message'=>'mobilenumber must be unique this mobile number is used before']);
          }else{
               $User=User::find($id);
               $User->name=$request->name;
               $User->mobilenumber=$request->mobilenumber;
               $User->password=Hash::make($request->password);
               $User->email=$request->email;
               if($file=$request->file('image')){
                 $name=time().'.'.$file->getClientOriginalExtension();
                 $file->move('cpanel/upload/user',$name);
                 $User->image=$name;
               }
               $User->save();
               return response()->json(["code"=>200,'Status' => 'success','message'=>'update user data done successfully','userdata' => $User]);
          }
        }
   
   // to find the nearst order from the specific driver
   public function nearst_order(Request $request)
   {
     //return $request->all();
    $driver=Drivers::find($request->driver_id);
    $driver->lat=$request->lat;
    $driver->lng=$request->lng;
    $driver->save();

    if ($driver->status == 1) {
      # code...
    if(!empty($driver)) {
    $lat=$request->lat;
    $lng=$request->lng;
    $distance=3;
    $res=DB::table('order_driver')
    ->select('order_id')
    ->get();
    //return $res;
    //$ids=array();
    if (!empty($res)) {
 for ($i=0; $i < count($res) ; $i++) { 
      $ids[]=$res[$i]->order_id;
    }    }
   
    //return $ids;
    if (empty($ids) && !isset($ids)) {
     $results = DB::select(DB::raw('SELECT id,deliverytotalprice,timetodeliver,kitchen_id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(lat) ) ) ) AS distance FROM orders  HAVING distance < ' . $distance . ' ORDER BY distance ASC LIMIT 1') );
     //return $results;
    }
    else
    {
        $results = DB::select(DB::raw('SELECT id,deliverytotalprice,timetodeliver,kitchen_id, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(lat) ) ) ) AS distance FROM orders WHERE id  NOT IN ( ' . implode( ', ' , $ids ) . ' ) HAVING distance < ' . $distance . ' ORDER BY distance ASC LIMIT 1') );

    }
    //return $results;
if(!empty($results)){
      $results[0]->id=$results[0]->id;
      if (!empty($results)) {
      $kitchen=kitchen::find($results[0]->kitchen_id);
      $results[0]->kitchen_ar_title=$kitchen->ar_title;
      $results[0]->kitchen_en_title=$kitchen->en_title;
      return response()->json(["code"=>200,'Status' => 'success','order_number'=>$results[0]->id,'deliverytotalprice'=>$results[0]->id,'timetodeliver'=>$results[0]->timetodeliver,'distance'=>$results[0]->distance,'kitchen_ar_title'=>$results[0]->kitchen_ar_title,'kitchen_en_title'=>$results[0]->kitchen_en_title]);

}
else
{
       return response()->json(["code"=>1313,'Status' => 'error','message'=>'no orders founded']);


}


        
      }
      else
      {
     return response()->json(["code"=>1313,'Status' => 'error','message'=>'no orders founded']);

      }


    }
    else{
        return response()->json(["code"=>1314,'Status' => 'error','message'=>'no driver  founded']);
    }
    }
    else
    {
        return response()->json(["code"=>1315,'Status' => 'error','message'=>'susspended']);
    }
   }
   // reject specific order
   public function reject_order(Request $request)
   {
      $order_rejected=order_driver::updateOrCreate([
        'order_id' =>$request->order_id,
        'driver_id'=>$request->driver_id,
        'status'   =>'R',
        'u' =>  date("h:i:s")
    ]);

order_driver::where('order_id',$request->order_id)
->where('driver_id',$request->driver_id)
->update(array('u' =>date("h:i:s")));

    DB::table('orders')
            ->where('id', $request->order_id)
            ->update(['order_status'=>'R']);

     //$driver=Drivers::find($request->driver_id);
      $res=DB::table('order_driver')
     ->where('driver_id',$request->driver_id)
     ->where('status','R')
     ->count();
     //return $res;

     if ($res >= 3) {
      $driver=Drivers::find($request->driver_id);
      $driver->status=0;
      $driver->save();
      return response()->json(["code"=>200,'Status' => 'error','en_message'=>'susspended','ar_message'=>'معلق']);
        }
   elseif ($res == 1) {
     return response()->json(["code"=>200,'Status' => 'success','ar_message'=>'رسالة تحذير شريكنا العزيز ، لديك فقط مرتان آخرتان لرفض الطلب وإلا سيتم تعليق شراكتك','en_message'=>' 
Dear Partner you have only two  more time to reject the order or your  partnership will be suspend 
']);   
   }elseif ($res == 2) {
     return response()->json(["code"=>200,'Status' => 'success','ar_message'=>'رسالة تحذير شريكنا العزيز ، لديك فقط مرة واحدة اخرى لرفض الطلب وإلا سيتم تعليق شراكتك','en_message'=>' 
Dear Partner you have only one 
more time to reject the order or your  partnership will be suspend  
']);
  
}
   
        else
        {
   
     return response()->json(["code"=>200,'Status' => 'success','message'=>'order rejected']);
             } 
    
   }
 public function accept_order(Request $request)
   {
   //	return date("h:i:s");

    $order_rejected=order_driver::firstOrCreate([
        'order_id' =>$request->order_id,
        'driver_id'=>$request->driver_id,
        'status'   =>'A',
    ]);

    order_driver::where('order_id',$request->order_id)
->where('driver_id',$request->driver_id)
->update(array('u' =>date("h:i:s")));


    DB::table('orders')
            ->where('id', $request->order_id)
            ->update(['order_status'=>'A']);
   // return self::$order_id;
     return response()->json(["code"=>200,'Status' => 'success','message'=>'order accepted']);
              
    
   }
    public function update_driver_location(Request $request)
   {
     $driver= Drivers::find($request->driver_id);
     $driver->lat=$request->lat;
     $driver->lng=$request->lng;
     if($driver->save())
     return response()->json(["code"=>200,'Status' => 'success','message'=>'location updated']);
              else
              {
                 return response()->json(["code"=>1313,'Status' => 'error']);  
              }
    
   }

   public function Driver_Order(Request $request){
    $oneorder=Order::where('id',$request->id)->select('id','user_id',
    'kitchen_id','orderdate','ordertime','totalprice',
    'distancetouser','status','type','timetodeliver','created_at')->first();
    
   $order_driver=order_driver::where('order_id',$request->id)->where('status','A')->get();
   $driver_id=$order_driver[0]->driver_id;
  $driver=Drivers::find($driver_id);
  $oneorder->driver_lat=$driver->lat;
  $oneorder->driver_lng=$driver->lng;
    if(isset($oneorder) ){
      $oneorder->orderdate=$oneorder->created_at->format('Y-m-d');
      $oneorder->ordertime=$oneorder->created_at->format('H:i');
      //
        $min=$oneorder->timetodeliver/60;
        $sec=$oneorder->timetodeliver;

$min=(int)$min;

        if($min>60){
            $hours=intval($min/60);
            $hours=(int)$hours;
            $oneorder->ar_time_hours=" $hours ساعة";
            $oneorder->en_time_hours="$hours Hours ";
        }else{

            $oneorder->ar_time_minute=" $min دقيقة";
            $oneorder->en_time_minute="$min Minutes ";
        }
         if($sec<60)
         {
              $oneorder->ar_time_second=" $sec ثانية";
              $oneorder->en_time_second="$sec second ";
         }

      //
      $MealName=[];
      $usernote="";
      $Meals_Order=DB::table('order_meal')->where('order_id',$request->id)->get();
      $count=1;
      foreach($Meals_Order as $Meal_Order){
        $MealsName=Meal::select('meals.ar_title','meals.en_title')
        ->join('kitchentags','meals.kitchentag_id','kitchentags.id')
        ->where('meals.id',$Meal_Order->meal_id)
        ->first();
        $MealsName['amount']=$Meal_Order->amount;
  
  
        $Addition_ids[]=explode(",",$Meal_Order->additions);
  $d=0;$temp=[];
  foreach($Addition_ids[0] as $Addition_id)
  {
   $AdditionsName=Additions::select('additions.ar_title','additions.en_title')
        ->where('additions.id',$Addition_id)
        ->first();  
  
   $temp[]=$AdditionsName;
 
         $d++;
  }
   $MealsName['additions']=$temp;
 
        $MealName[]=$MealsName;
        if($oneorder->type == "meal"){
          if($count ==1){
            $usernote .= "$Meal_Order->note";
          }else{
            $usernote .= "\n\n$Meal_Order->note";
          }
          $count++;
        }
      }
      $maxtime=Meal::join('order_meal','meals.id','order_meal.meal_id')
        ->where('order_meal.order_id',$request->id)
        ->max('meals.neededtime');
        $distancetime=$oneorder->distancetouser;
        $oneorder->deliverytime=$oneorder->timetodeliver;
        $oneorder->kitchen=Kitchen::where('id',$oneorder->kitchen_id)->select('lat','lng','ar_title','en_title')->first();



        return response()->json(["code"=>200,'Status'=>'success','message' => 'one order','oneorder'=> $oneorder]);
      }else{
        return response()->json(["code"=>1313,'Status'=>'error','message' => ' no orders']);
      }
  }
  
  public function Driver_Orderdetails(Request $request){
    $oneorder=Order::where('id',$request->id)->select('id','codeorder','component','user_id',
    'kitchen_id','userplace','orderdate','ordertime','usernote','vendornote','neededtime',
    'deliverytotalprice','mealstotalprice','priceVat','discountpoint','totalprice',
    'distancetouser','status','type','en_partyname','partydate','partytime','partytype',
    'partylocation','guestnum','timetodeliver','created_at')->first();
    
    if($request->type == "user"){
      $Notification=Notification::where('order_id',$request->id)->where('type','user')->first();
      if($Notification){
      $Notification->views=1;
      $Notification->save();
              $count=Notification::select('notifications.*','orders.status')
  ->join('orders','notifications.order_id','orders.id')
  ->where('notifications.user_id',$request->user_id)
  ->where('notifications.type','user')
  ->where('notifications.views',0)
->whereIn('orders.status',[1,-2,-1])
  ->orderBy('notifications.created_at','desc')
  ->count();
      //event(new StatusLiked($count,$Notification->user_id));
      }
    }else{
      $Notification=Notification::where('order_id',$request->id)->where('type','vendor')->first();
      if($Notification){
      $Notification->views=1;
      $Notification->save();
    $count=Notification::select('notifications.id')
->join('orders','notifications.order_id','orders.id')
->where('notifications.vendor_id',$Notification->vendor_id)
  ->where('notifications.type','vendor')
  ->where('notifications.views','0')
->whereIn('orders.status',[1,-2,-1])
  ->count();
      
     // event(new StatusLiked($count,$Notification->vendor_id));
      }
    }
    if(isset($oneorder) ){
      $oneorder->orderdate=$oneorder->created_at->format('Y-m-d');
      $oneorder->ordertime=$oneorder->created_at->format('H:i');
      $MealName=[];
      $usernote="";
      $Meals_Order=DB::table('order_meal')->where('order_id',$request->id)->get();
      $count=1;
      foreach($Meals_Order as $Meal_Order){
        $MealsName=Meal::select('meals.ar_title','meals.en_title')
        ->join('kitchentags','meals.kitchentag_id','kitchentags.id')
        ->where('meals.id',$Meal_Order->meal_id)
        ->first();
        $MealsName['amount']=$Meal_Order->amount;
  
  
        $Addition_ids[]=explode(",",$Meal_Order->additions);
  $d=0;$temp=[];
  foreach($Addition_ids[0] as $Addition_id)
  {
   $AdditionsName=Additions::select('additions.ar_title','additions.en_title')
        
        ->where('additions.id',$Addition_id)
        ->first();  
  
   $temp[]=$AdditionsName;
 
         $d++;
  }
   $MealsName['additions']=$temp;
  //$MealsName['additions']=$temp;
  
  //$MealsName['additions']="[".$Meal_Order->additions."]"; // hamza
 // return  $Addition_ids[0][0];
  
  
        $MealName[]=$MealsName;
        if($oneorder->type == "meal"){
          if($count ==1){
            $usernote .= "$Meal_Order->note";
          }else{
            $usernote .= "\n\n$Meal_Order->note";
          }
          $count++;
        }
      }
      $maxtime=Meal::join('order_meal','meals.id','order_meal.meal_id')
        ->where('order_meal.order_id',$request->id)
        ->max('meals.neededtime');
        $distancetime=$oneorder->distancetouser;
        $oneorder->deliverytime=$oneorder->timetodeliver;
        $oneorder['meals']=$MealName;
  
        $oneorder->user=User::select('mobilenumber','name')->where('id',$oneorder->user_id)->first();
        $oneorder->kitchen=Kitchen::where('id',$oneorder->kitchen_id)->select('ar_title','en_title')->first();
        if($oneorder->type == "meal"){
          $oneorder->usernote=$usernote;
        }
        return response()->json(["code"=>200,'Status'=>'success','message' => 'one order','oneorder'=> $oneorder]);
      }else{
        return response()->json(["code"=>1313,'Status'=>'error','message' => ' no orders']);
      }
  }
    public function Driver_Ready(Request $request){
       $oneorder=Order::where('id',$request->id)->select('id','user_id',
    'kitchen_id','orderdate','ordertime','totalprice',
    'distancetouser','status','type','timetodeliver','created_at','lat','lng')->first();
    

    if(isset($oneorder) ){
      $oneorder->orderdate=$oneorder->created_at->format('Y-m-d');
      $oneorder->ordertime=$oneorder->created_at->format('H:i');
           $min=$oneorder->timetodeliver/60;
        $sec=$oneorder->timetodeliver;
        $min=(int)$min;

        if($min>60){
            $hours=intval($min/60);
            $hours=(int)$hours;
            $oneorder->ar_time_hours=" $hours ساعة";
            $oneorder->en_time_hours="$hours Hours ";
        }else{

            $oneorder->ar_time_minute=" $min دقيقة";
            $oneorder->en_time_minute="$min Minutes ";
        }
         if($sec<60)
         {
              $oneorder->ar_time_second=" $sec ثانية";
              $oneorder->en_time_second="$sec second ";
         }
      $MealName=[];
      $usernote="";
      $Meals_Order=DB::table('order_meal')->where('order_id',$request->id)->get();
      $count=1;
      foreach($Meals_Order as $Meal_Order){
        $MealsName=Meal::select('meals.ar_title','meals.en_title')
        ->join('kitchentags','meals.kitchentag_id','kitchentags.id')
        ->where('meals.id',$Meal_Order->meal_id)
        ->first();
        $MealsName['amount']=$Meal_Order->amount;
  
  
        $Addition_ids[]=explode(",",$Meal_Order->additions);
  $d=0;$temp=[];
  foreach($Addition_ids[0] as $Addition_id)
  {
   $AdditionsName=Additions::select('additions.ar_title','additions.en_title')
        ->where('additions.id',$Addition_id)
        ->first();  
  
   $temp[]=$AdditionsName;
 
         $d++;
  }
   $MealsName['additions']=$temp;
 
        $MealName[]=$MealsName;
        if($oneorder->type == "meal"){
          if($count ==1){
            $usernote .= "$Meal_Order->note";
          }else{
            $usernote .= "\n\n$Meal_Order->note";
          }
          $count++;
        }
      }
      $maxtime=Meal::join('order_meal','meals.id','order_meal.meal_id')
        ->where('order_meal.order_id',$request->id)
        ->max('meals.neededtime');
        $distancetime=$oneorder->distancetouser;
        $oneorder->deliverytime=$oneorder->timetodeliver;
       // $oneorder->kitchen=Kitchen::where('id',$oneorder->kitchen_id)->select('ar_title','en_title','lat','lng')->first();
$oneorder->user_phone=User::select('mobilenumber','name')->where('id',$oneorder->user_id)->first()->mobilenumber;
DB::table('order_driver')
            ->where('order_id', $request->id)
            ->where('driver_id',$request->driver_id)
            ->update(['picked_time' =>  date("h:i:s"),'status'=>'D']);
             DB::table('orders')
            ->where('id', $request->id)
            ->update(['order_status'=>'D']);


            order_driver::where('order_id',$request->id)
->where('driver_id',$request->driver_id)
->update(array('u' => date("h:i:s")));


            $oneorder->picked_time=DB::table('order_driver')
            ->select('picked_time')
            ->where('order_id', $request->id)
            ->where('driver_id',$request->driver_id)->get()[0]->picked_time;
 $driver_pos=Drivers::find($request->driver_id);
           $oneorder->driver_lat=$driver_pos->lat;
           $oneorder->driver_lng=$driver_pos->lng;
        return response()->json(["code"=>200,'Status'=>'success','message' => 'one order','oneorder'=> $oneorder]);
      }else{
        return response()->json(["code"=>1313,'Status'=>'error','message' => ' no orders']);
      }
}
public function Driver_Deliver(Request $request)
{


  DB::table('order_driver')
            ->where('order_id', $request->order_id)
            ->where('driver_id',$request->driver_id)
            ->update(['delivered_at' =>  date("h:i:s"),'status'=>'F']);

    DB::table('orders')
            ->where('id', $request->order_id)
            ->update(['order_status'=>'F']);


         order_driver::where('order_id',$request->order_id)
->where('driver_id',$request->driver_id)
->update(array('u' => date("h:i:s")));



     return response()->json(["code"=>200,'Status' => 'success','message'=>'order delivered']);
}

public function Driver_history(Request $request)
{
    $orders=DB::table('order_driver')
            ->where('status', 'F')
            ->where('driver_id',$request->driver_id)
            ->get();
            if(count($orders) == 0) {
 return response()->json(["code"=>1313,'Status' => 'error','message'=>'no order delivered']);
}            
else{
  for ($i=0; $i <count($orders) ; $i++) { 
    $o=Order::find($orders[$i]->order_id);
    $orders[$i]->deliveryCost=$o->deliverytotalprice;
    $orders[$i]->date=$o->created_at->format('m/d/Y');
    
  }
return response()->json(["code"=>200,'Status' => 'success','orders'=>$orders]);
}
}
public function Driver_profile(Request $request)
{
  $driver=Drivers::find($request->id);
  if (!empty($driver)) {
    $driver->lisence_image='http://mammamiaa.com/cpanel/upload/user/'.$driver->lisence_image;

return response()->json(["code"=>200,'Status' => 'success','userdata'=>$driver]);
  }
  else
  {
  return response()->json(["code"=>1313,'Status' => 'error','message'=>'no driver found ']);

  }
}
public function Driver_Update_profile(Request $request)
{
  $driver=Drivers::find($request->id);

           if(!$driver){
               return response()->json(['code' =>'1155','Status' => 'error','message'=>'the user not found']);

           }else{
               $validator = Validator::make($request->all(), [
                   'phone' => 'unique:drivers,phone,'.$driver->id,
               ]);
               if ($validator->fails()) {
                   return response()->json(['code'=>1233,'Status' => 'error']);
               }else{
                   $driver->username=$request->username;
                   $driver->phone=$request->phone;
                   $driver->lisence_number=$request->lisence_number;
                   $driver->password=Hash::make($request->password);
                     $fileName = null;
        if (request()->hasFile('image')) {
            $file = request()->file('image');
            $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            $file->move('./cpanel/upload/user/', $fileName);    
            $driver->lisence_image = $fileName;
          }
                   $driver->update();
            
    
return response()->json(["code"=>200,'Status' => 'success','userdata'=>$driver]);
  }

  
}
}

public function order_invoice(Request $request){
return response()->json(["code"=>200,'Status' => 'success','link'=>"mammamiaa.com/api/order_invoice_view?id=".$request->id]);
;

    }
    public function order_invoice_view(Request $request){
//return $request->header('tokken');

$order = Order::find($request->id);
$user=User::find($order->user_id);
$kitchen=Kitchen::find($order->kitchen_id);
return view('api_views.invoice',compact('order','user','kitchen'));

    }

    public function Driver_income(Request $request)
{
    $income=DriverIncome::where('driver_id',$request->driver_id)->get();
            if(empty($income)) {
 return response()->json(["code"=>1313,'Status' => 'error','message'=>'no driver founded']);
}            
else{

return response()->json(["code"=>200,'Status' => 'success','total_income'=>$income[0]->total_income,'remaining_balance'=>$income[0]->remaining_balance,'paid_amount'=>$income[0]->paid_amount]);
}
}
public function Driver_summary(Request $request)
{
 $oneorder=Order::where('id',$request->id)->select('id','ordertime','distancetouser','created_at')->first();
    if(isset($oneorder) ){
      $oneorder->ordertime=$oneorder->created_at->format('H:i');
        $distancetime=$oneorder->distancetouser;
            $oneorder->delivered_at=DB::table('order_driver')
            ->where('order_id', $request->id)
            ->where('driver_id',$request->driver_id)->get()[0]->delivered_at;

        return response()->json(["code"=>200,'Status'=>'success','message' => 'one order','oneorder'=> $oneorder]);
      }else{
        return response()->json(["code"=>1313,'Status'=>'error','message' => ' no orders']);
      }
    }



      public function order_status(Request $request)
      {
       $oneorder= order_driver::where('driver_id', $request->driver_id)
      ->where(function($q) {
          $q->where('status','A')
            ->orWhere('status','D');
      })
      ->orderBy('id', 'desc')->first();

       // $oneorder= DB::table('order_driver')
       // ->where('driver_id','=',$request->driver_id)
       // ->where('status','A')
       // ->Where('status','D')
       // ->orderBy('id', 'desc')->first();
 
            //return   $this->order_id;
     if(empty($oneorder)){
  return response()->json(["code"=>1313,'status'=>'n']);

}
else
{

  return response()->json(["code"=>200,'status'=> $oneorder->status]);

}
  }

}
