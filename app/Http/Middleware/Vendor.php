<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Vendor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd($request);

        if(auth::user()->category == 'vendor' && auth::user()->status == '1'){
            return $next($request);
        }
        return redirect('welcome');
    }
}
