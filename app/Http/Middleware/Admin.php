<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('locale')) {
            App::setLocale(session()->get('locale'));
        }
     
        if(auth::user()->category == 'admin'){
            return $next($request);
        }elseif(Auth::user()->category == 'vendor'){
			return redirect('/DashBoard');
		
		}elseif(Auth::user()->category == 'cashier'){
		return redirect('/orders_cashier');

		}else{
			
        return redirect()->back();
  	  }
	}
}
