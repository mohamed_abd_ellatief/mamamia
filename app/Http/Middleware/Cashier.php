<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Cashier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth::user()->category == 'cashier' && auth::user()->status == '1'){
            return $next($request);
        }
        return redirect('welcome');
    }
}
