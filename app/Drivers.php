<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drivers extends Model
{
	public $timestamps = false;
    protected $table='drivers';
    public function orders()
    {
        return $this->belongsToMany('App\Order','order_driver','order_id','driver_id')
            ->withPivot('status')
            ->withTimestamps();
    }
}
