<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\NotificationData;
use Auth;
use Carbon\Carbon;
use App\Cashier;

class Notification extends Model
{
    public function data(){
        $current = Carbon::now();
        if(Auth::user()->category == 'vendor'){
            $data=Notification::select('notifications.*','orders.status','orders.type as type_order')
            ->join('orders','notifications.order_id','orders.id')
            ->where('notifications.vendor_id',Auth::user()->id)
            ->where('notifications.type','vendor')
            ->where('notifications.views',0)
		    ->whereIn('orders.status',[1,-1,-2])
            ->get();
        }elseif(Auth::user()->category == 'cashier'){
            $Kitchen=Cashier::where('cashier_id',Auth::user()->id)->first();
            $data=Notification::select('notifications.*','orders.status','orders.type as type_order')
            ->join('orders','notifications.order_id','orders.id')
            ->where('notifications.vendor_id',$Kitchen->vendor_id)
            ->where('notifications.type','vendor')
            ->where('notifications.views',0)
		    ->whereIn('orders.status',[1,-1,-2])
            ->get();
        }elseif(Auth::user()->category == 'admin'){
            $data=Notification::where('type','admin')
            ->where('views',0)
            ->get();
        }
        foreach($data as $Notification){
            $min=intval((strtotime($current->toDateTimeString())-strtotime($Notification->created_at))/60);
            if($min>1440){
                $days=intval($min/1440);
                $Notification->ar_ago="مضت $days يوم";
                $Notification->en_ago="$days Days Ago";
            }elseif($min>60){
                $hours=intval($min/60);
                $Notification->ar_ago="مضت $hours ساعة";
                $Notification->en_ago="$hours Hours Ago";
            }else{
                
                $Notification->ar_ago="مضت $min دقيقة";
                $Notification->en_ago="$min Minutes Ago";
            }
        }
        if(count($data)>0){
            
            return $data;
        }else{
            return false;
        }
    }
}
