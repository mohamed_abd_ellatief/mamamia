<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Carbon\Carbon;

class Order extends Model
{

    function Meals(){
      return $this->belongsToMany('App\Meal', 'order_meal');
    }
    function user(){
      return $this->belongsTo('App\User');
    }
    function kitchen(){
      return $this->belongsTo('App\Kitchen');
    }
    public function drivers()
    {
      return $this->belongsToMany('App\Drivers', 'order_driver', 'order_id', 'driver_id');

    }

    public function Notification(){
      $Notification=Order::where('vendor_id',Auth::user()->id)->whereIn('status',[0,2])->orderby('created_at','asc')->get();
      if(count($Notification)>0){
         
          return array("Notification"=>$Notification);
          die();
        
      }
      return false;
      die();

    }

    public function getActiveUser(){
      $pastt     = Carbon::now();
      $past   = $pastt->subMonth();
      $now = Carbon::now();
        $Users=Order::select('user_id')
        ->where('vendor_id',Auth::user()->id)
        ->whereDate('created_at','>',$past)
        ->whereDate('created_at','<',$now)
        ->distinct('user_id')
        ->count('user_id');
            return $Users;
    }

    public function getInActiveUser(){
      $pastt     = Carbon::now();
      $past   = $pastt->subMonth();
      $now = Carbon::now();
        $Users=Order::select('user_id')
        ->where('vendor_id',Auth::user()->id)
        ->whereDate('created_at','<',$past)
        ->distinct('user_id')
        ->count('user_id');
            return $Users;
    }

    public function MonthlySales(){
      $dateS = Carbon::now()->startOfMonth()->subMonth(1);
      $dateE = Carbon::now()->startOfMonth();
      $Sales=Order::where('vendor_id',Auth::user()->id)
      ->whereBetween('created_at',[$dateS,$dateE])
      ->where('status',1)
      ->sum('totalprice');
      if($Sales){
        return $Sales;
      }
      return 0;
    }
    public function LastMonthlySales(){
      $dateS = Carbon::now()->startOfMonth()->subMonth(2);
      $dateE = Carbon::now()->startOfMonth()->subMonth(1);
      $Sales=Order::where('vendor_id',Auth::user()->id)
      ->where('status',1)
      ->whereBetween('created_at',[$dateS,$dateE])
      ->sum('totalprice');
      if($Sales){
        return $Sales;
      }
      return 0;
    }
    public function YearlySales(){
      $dateS = Carbon::now()->startOfYear()->subYear(1);
      $dateE = Carbon::now()->startOfYear();
      $Sales=Order::where('vendor_id',Auth::user()->id)
      ->whereBetween('created_at',[$dateS,$dateE])
      ->where('status',1)
      ->sum('totalprice');
      if($Sales){
        return $Sales;
      }
      return 0;
    }
    public function LastYearlySales(){
      $dateS = Carbon::now()->startOfYear()->subYear(2);
      $dateE = Carbon::now()->startOfYear()->subYear(1);
      $Sales=Order::where('vendor_id',Auth::user()->id)
      ->where('status',1)
      ->whereBetween('created_at',[$dateS,$dateE])
      ->sum('totalprice');
      if($Sales){
        return $Sales;
      }
      return 0;
    }

    
}
