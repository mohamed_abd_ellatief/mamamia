<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\User;

class StatusLiked implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $count;
    public $user_id;
    
    /**
     * Create a new event instance.
     *
     * @return void
     */
    
    public function __construct($count,$user_id)
    {
				if(User::find($user_id)){

        $this->count = $count;
        $this->user_id = $user_id;
        $this->type = User::find($user_id)->category;
		
    }
	}

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        // return new PrivateChannel('status-liked');
        return ['my-channel'];
    }

    public function broadcastAs() {

        return 'my-event';
        
        }

}
