@extends('layouts.cpanel_layout.master')
@section('title')
{{trans('word.Vendors')}}
@endsection

@section('css')
<!--alerts CSS -->
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('cpanel/assets/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />

@endsection

@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{trans('word.Vendors')}}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('word.Vendors')}}</li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <button id="delete" class="btn pull-right hidden-sm-down btn-danger m-l-10">{{trans('word.Delete 0 row')}}</button>
            <button data-toggle="modal" data-target="#add-Vendor" class="btn pull-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> {{trans('word.Create')}}</button>

            {{-- <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#add-contact">Add New Contact</button> --}}
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    @if($errors->any())
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i>warning!</h4>
        <ul>
          @foreach($errors->all() as $error)
          <li style="font-weight: 40"><h4 style="font-weight:40">{{$error}}</h4></li>
          @endforeach
        </ul>
      </div>
    @endif

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                        <h4 class="card-title">
                        {{trans('word.Vendors')}}
                        </h4>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('word.No Vendor')}}</th>
                                    <th>{{trans('word.Name')}}</th>
                                    <th>{{trans('word.image')}}</th>
                                    <th>{{trans('word.Mobile')}}</th>
                                    <th>{{trans('word.Active')}}</th>
                                    <th>{{trans('word.Status')}}</th>
                                    <th>{{trans('word.Edit')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($Vendors as $Vendor)
                                <tr id="{{$Vendor->id}}">
                                    <td>{{$Vendor->id}}</td>
                                    <td>
                                        {{$Vendor->name}}
                                    </td>
                                    <td>
                                        @if($Vendor->image)
                                        <div class="image-popups">
                                            <a href="{{asset('cpanel/upload/user/'.$Vendor->image)}}" data-effect="mfp-newspaper">
                                                <img style="width:50px" src="{{asset('cpanel/upload/user/'.$Vendor->image)}}" class="img-responsive"/>
                                            </a>
                                        </div>
                                        @endif
                                    </td>
                                    <td>
                                        {{$Vendor->mobilenumber}}
                                    </td>
                                    <td>
                                        <div class="switchery-demo m-b-30" data-id="{{$Vendor->id}}" style="width: min-content;border-radius:20px;">
                                            <input type="checkbox" name="status" @if($Vendor->status == "1") checked @endif class="js-switch UpdateStatus" data-color="#009efb" />
                                        </div>
                                    </td>
                                    <td class="Activted">
                                        @if($Vendor->status == "1")
                                        <p class="label label-success">{{trans('word.Activted')}}</p>
                                        @else
                                        <p class="label label-danger">{{trans('word.No Activted')}}</p>
                                        @endif
                                    </td>
                                    <td>
                                        <button data-toggle="modal" class="btn hidden-sm-down btn-info edit-Vendor" data-id="{{$Vendor->id}}"><i class="mdi mdi-border-color"></i> {{trans('word.Edit')}}</button>
                                    </td>
                                  </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

    <!-- insert modal content -->
    <div id="add-Vendor" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <button  class="btn btn-success" id="print">طباعه</button>
                    <h4 class="modal-title" id="myModalLabel">{{trans('word.New Member')}}</h4> </div>
                    <form class="form-horizontal form-material" method="POST" action="{{url('Store_Vendor')}}" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="form-group col-md-12 m-b-20">
                                <label class="control-label">{{trans('word.Name')}}</label>
                                <div class="controls">
                                    <input type="text" class="form-control name" name="name" required data-validation-required-message="{{trans('word.This field is required')}}">
                                </div>
                            </div>
                            <div class="form-group col-md-12 m-b-20">
                                <label class="control-label">{{trans('word.Email')}}</label>
                                <div class="controls">
                                    <input type="text" class="form-control" name="email" data-validation-regex-regex="([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})" data-validation-regex-message="{{trans('word.Enter Valid Email')}}">
                                </div>
                            </div>
                            <div class="form-group col-md-12 m-b-20">
                                <label class="control-label">{{trans('word.Mobile')}}</label>
                                <div class="controls">
                                    <input type="text" class="form-control mobilenumber" name="mobilenumber" required data-validation-required-message="{{trans('word.This field is required')}}" data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="{{trans('word.No Characters Allowed, Only Numbers')}}" data-validation-regex-regex="(05)[0-9]{8}" data-validation-regex-message="{{trans('word.Invalid Mobile Number')}}">
                                </div>
                            </div>
                            <div class="form-group col-md-12 m-b-20">
                                <label class="control-label">{{trans('word.Password')}}</label>
                                <div class="controls">
                                    <input type="password" class="form-control password" name="password" required data-validation-required-message="{{trans('word.This field is required')}}" data-validation-min-message="{{trans('word.Password must be at least 4 letters')}}" min="4">
                                </div>
                            </div>
                            <div class="form-group col-md-12 m-b-20">
                                <div class="fileupload btn btn-danger btn-rounded waves-effect waves-light"><span><i class="ion-upload m-r-5"></i>{{trans('word.Upload Cashier Image')}}</span>
                                    <input type="file" class="upload" name="image"> </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect">{{trans('word.Save')}}</button>
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="edit-model-Vendor" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    </div>
@endsection

@section('js')
    {{-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIWl0KLlaJV31fhfzuAul2sJA_u2gQuVA&libraries=places"></script> --}}
    <!-- This is data table -->
    <script src="{{asset('cpanel/en/js/validation.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
    {{-- <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script> --}}
    <script src="{{asset('cpanel/assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('cpanel/assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <script>

        //DataTable
        var table = $('#myTable').DataTable({
            dom: 'Bfrtip',
            "ordering":false,
            paging:false,
            buttons: [
                { extend: 'copy', text: '{{trans('word.copy')}}' },
                { extend: 'excel', text: '{{trans('word.excel')}}' },
                { extend: 'pdf', text: '{{trans('word.pdf')}}' },
                { extend: 'print', text: '{{trans('word.print')}}' },
            ],
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
                }
            @endif
        });
        //End DataTable

        // For select 2
        $(".select2").select2();

        //Select Row From Table
        $('#myTable tbody').on( 'click', 'tr', function (e) {
            if (e.ctrlKey) {
                $(this).toggleClass('selected');
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
                @else
                $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
                @endif
            }else{
                var isselected = false
                var numSelected= table.rows('.selected').data().length
                if($(this).hasClass('selected') && numSelected==1){
                    isselected = true
                }
                $('#myTable tbody tr').removeClass('selected');
                if(!isselected){
                    $(this).toggleClass('selected');
                }
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
                @else
                $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
                @endif
            }
        });
        //End Select Row From Table

        //Delete Row
        $("body").on("click", "#delete", function () {
            var dataList=[];
            $("#myTable .selected").each(function(index) {
                dataList.push($(this).find('td:first').text())
            })
            if(dataList.length >0){
                swal({
                    title: "{{trans('word.Are you sure?')}}",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('word.Yes, Sure it!')}}",
                        cancelButtonText: "{{trans('word.No')}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function(isConfirm){
                    if (isConfirm) {
                        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                        $.ajax({
                            url:'{{url("Delete_Vendor")}}',
                            type:"post",
                            data:{'id':dataList,_token: CSRF_TOKEN},
                            dataType:"JSON",
                            success: function (data) {
                                if(data.message == "Success")
                                {
                                    $("#myTable .selected").hide();
                                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                                    $('#delete').text('حذف 0 سجل');
                                    @else
                                    $('#delete').text('Delete 0 row');
                                    @endif
                                    swal("{{trans('word.Deleted')}}", "{{trans('word.Message_Delete')}}", "success");
                                }else{
                                    swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error");
                                }
                            },
                            fail: function(xhrerrorThrown){
                                swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error");
                            }
                        });
                    } else {
                        swal("{{trans('word.Cancelled')}}", "{{trans('word.Message_Cancelled_Delete')}}", "error");
                    }
                });
            }
        });
        //End Delete Row


        // Validation
        ! function(window, document, $) {
            "use strict";
            $("input,select").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);


        // Edit Status Vendor
        $(".switchery-demo").click(function(){
            var id=$(this).data('id')
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                type: "POST",
                url: "{{url('UpdateStatusVendor')}}",
                data: {"id":id,_token:CSRF_TOKEN},
                success: function (data) {
					console.log(data);
                    var alert=``;
                    if(data == 1){
                        alert=`<p class="label label-success">{{trans('word.Activted')}}</p>`;
                    } else {
                        alert=`<p class="label label-danger">{{trans('word.No Activted')}}</p>`;
                    }
                    $(`#`+id).find('.Activted').html(alert)
                }
            })
        })

        // popup Image
        $('.image-popups').magnificPopup({
            delegate: 'a',
            type: 'image',
            removalDelay: 500, //delay removal by X to allow out-animation
            callbacks: {
                beforeOpen: function() {
                    // just a hack that adds mfp-anim class to markup
                    this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                    this.st.mainClass = this.st.el.attr('data-effect');
                }
            },
            closeOnContentClick: true,
            midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
        });

        // Edit Vendor Modal
        $(".edit-Vendor").click(function(){
            var id=$(this).data('id')
            $.ajax({
                type: "GET",
                url: "{{url('Edit_Vendor')}}",
                data: {"id":id},
                success: function (data) {
                    $("#edit-model-Vendor").html(data)
                    $("#edit-model-Vendor").modal('show')
                    $("#edit-model-Vendor").on('hidden.bs.modal',function (e){
                        $('#edit-model-Vendor').hide();
                    })
                }
            })
        })

        // Switch Button
        jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
        
        });

</script>
<?php
$message=session()->get("message");
?>
@if( session()->has("message"))
    @if( $message == "Success")
        <script>
            swal({
                title: "{{trans('word.Succeeded')}}",
                text: "{{trans('word.Operation completed successfully')}}",
                type:"success" ,
                timer: 1000,
                showConfirmButton: false
            });
        </script>
    @elseif ( $message == "Failed")
        <script>
            swal({
                title: "{{trans('word.Sorry')}}",
                text: "{{trans('word.the operation failed')}}",
                type:"error" ,
                timer: 2000,
                showConfirmButton: false
            });
        </script>
    @endif
@endif
<script type="text/javascript">
	
</script>

@endsection
