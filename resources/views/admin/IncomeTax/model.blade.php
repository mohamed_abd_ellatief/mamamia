<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Ã—</button>
            <h4 class="modal-title" id="myModalLabel">{{trans('word.Cashier')}}</h4> </div>
            <form class="form-horizontal form-material" method="POST" action="/StoreIncome" enctype="multipart/form-data" novalidate>
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-group col-md-12 m-b-20">
                            <label class="control-label">@if(Request::segment(1) == 'ar')  الملبغ المحصل @else Price @endif</label>
                            <div class="controls">
                                <input type="text"  class="form-control" name="price" required data-validation-required-message="{{trans('word.This field is required')}}">
                                <input type="hidden" value="{{$Kitchen}}" name="kitchen_id">
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect">{{trans('word.Save')}}</button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                </div>
              </div>

            </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    <script src="{{asset('cpanel/en/js/validation.js')}}"></script>
    <script>
        ! function(window, document, $) {
            "use strict";
            $("input,select").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);
    </script>
