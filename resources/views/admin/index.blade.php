@extends('layouts.cpanel_layout.master')
@section('title')
    {{trans('word.Home')}}
@endsection
@section('css')
<!-- chartist CSS -->
<link href="{{asset('cpanel/assets/plugins/chartist-js/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('cpanel/assets/plugins/chartist-js/dist/chartist-init.css')}}" rel="stylesheet">
<link href="{{asset('cpanel/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
<link href="{{asset('cpanel/assets/plugins/css-chart/css-chart.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{trans('word.Admin Panel')}}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('word.Dashboard')}}</li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="row">
                    <div class="col-lg-6 col-md-4">
                        <div class="card-block">
                            <h3>{{trans('word.Android')}} Vs {{trans('word.iOS')}}</h3>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 b-r align-self-center">
                        <div class="card-block">
                            <div class="d-flex flex-row">
                                <div class="col-8 p-0 align-self-center">
                                    <h3 class="m-b-0">{{\App\Download::where('type','Android')->count()}}</h3>
                                </div>
                                <div class="col-4 text-right">
                                    <div class="round align-self-center round-success"><i class="mdi mdi-android"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 align-self-center">
                        <div class="card-block">
                            <div class="d-flex flex-row">
                                <div class="col-8 p-0 align-self-center">
                                    <h3 class="m-b-0">{{\App\Download::where('type','ios')->count()}}</h3>
                                </div>
                                <div class="col-4 text-right">
                                    <div class="round align-self-center bg-inverse"><i class="mdi mdi-apple"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <div class="card card-inverse card-info">
                <div class="box bg-info text-center">
                    <h1 class="font-light text-white">
                        @inject('Vendors','App\User')
                        @if(\App\User::where('category','vendor')->count()>0)
                        {{\App\User::where('category','vendor')->count()}}
                        @else
                        0
                        @endif
                    </h1>
                    <h6 class="text-white">{{trans('word.Vendors')}}</h6>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <div class="card card-primary card-inverse">
                <div class="box text-center">
                    <h1 class="font-light text-white">
                        @inject('Vendors','App\User')
                        @if(\App\User::where('category','user')->count()>0)
                            {{\App\User::where('category','user')->count()}}
                        @else
                            0
                        @endif
                    </h1>
                    <h6 class="text-white">{{trans('word.Users')}}</h6>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <div class="card card-inverse card-success">
                <div class="box text-center">
                    <h1 class="font-light text-white">{{\App\Mainsettings::find(1)->vat}}%</h1>
                    <h6 class="text-white">{{trans('word.Vat')}}</h6>
                </div>
            </div>
        </div>
        <!-- Column -->
        <div class="col-md-6 col-lg-3 col-xlg-3">
            <div class="card card-inverse card-warning">
                <div class="box text-center">
                    <h1 class="font-light text-white">{{\App\Mainsettings::find(1)->distance}}</h1>
                    <h6 class="text-white">{{trans('word.distance')}}</h6>
                </div>
            </div>
        </div>
    </div>
    <!-- Row -->
    <div class="row">
            <div class="col-lg-12">
                    <div class="card">
                        <div class="card-block">
                            <ul class="list-inline pull-right">
                                <li>
                                    <h6 class="text-muted"><i class="fa fa-circle m-r-5" style="color:#820505"></i>{{Carbon\Carbon::now()->startOfYear()->format('Y')}}</h6>
                                </li>
                                <li>
                                    <h6 class="text-muted"><i class="fa fa-circle m-r-5" style="color:#e68000"></i>{{Carbon\Carbon::now()->startOfYear()->subYear(1)->format('Y')}}</h6>
                                </li>
                                <li>
                                    <h6 class="text-muted"><i class="fa fa-circle m-r-5" style="color:#b5b715"></i>{{Carbon\Carbon::now()->startOfYear()->subYear(2)->format('Y')}}</h6>
                                </li>
                            </ul>
                            <select class="custom-select choose_gannt_Kitchen">
                                @if(\App\Kitchen::count() > 0)
                                @foreach(\App\Kitchen::all() as $Kitchen)
                                <option value="{{$Kitchen->id}}">@if( LaravelLocalization::getCurrentLocale() == "ar"){{$Kitchen->ar_title}}@else{{$Kitchen->en_title}}@endif</option>
                                @endforeach
                                @endif
                            </select>
                            <div class="clear"></div>
                            <div class="total-sales" style="height: 365px;"></div>
                        </div>
                    </div>
                </div>

    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-block">
              <h4 class="card-title">@if(Request::segment(1) == 'ar') اكثر المطاعم في المبيعات   @else Most Restaurants in Sales   @endif</h4>
                <div class="table-responsive m-t-40">
                    <table id="myTable" class="table stylish-table">
                        <thead>
                            <tr>
                                <th colspan="1">id.</th>
                                <th> @if(Request::segment(1) == 'ar' )  اسم المطعم  @else Kitchens  @endif </th>
                                <th>@if(Request::segment(1) == 'ar' )  اسم البائع  @else  Vendor  @endif</th>
                                <th>@if(Request::segment(1) == 'ar' ) عدد الطلبات   @else Orders Count  @endif</th>
                            </tr>
                        </thead>
                        <tbody>
                          @inject('Kitchens','App\Kitchen')
                          @inject('Vendors','App\User')
                          @inject('Orders','App\Order')
                          <?php
                          $count = 1;
                          ?>
                          @foreach($Kitchens->all() as $kitchen)
                            <tr>
                              <td style="width:50px;"><span class="round">{{$kitchen->id}}</span></td>
                                <td>
                                    <h6>@if(Request::segment(1) == 'ar') {{$kitchen->ar_title}} @else {{$kitchen->en_title}} @endif </h6></td>
                                    <td>{{$Vendors->find($kitchen->user_id)->name}}</td>
                                <td><span class="label label-light-success">{{$Orders->where('kitchen_id',$kitchen->id)->count()}}</span></td>
                            </tr>
                            <?php
                            $count++;
                            ?>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="card-block">
              <h4 class="card-title">@if(Request::segment(1) == 'ar') اكثر العملاء استخدام للتطبيق   @else Most customers use the application   @endif</h4>
                <div class="table-responsive m-t-40">
                    <table id="myTable2" class="table stylish-table">
                        <thead>
                            <tr>
                                <th colspan="1">No.</th>
                                <th> @if(Request::segment(1) == 'ar' )  العميل  @else User  @endif </th>
                                <th>@if(Request::segment(1) == 'ar' ) عدد الطلبات   @else Orders Count  @endif</th>
                            </tr>
                        </thead>
                        <tbody>
                          @inject('Kitchens','App\Kitchen')
                          @inject('Users','App\User')
                          @inject('Orders','App\Order')
                          <?php
                          $count = 1;
                          ?>
                          @foreach($Users->where('category','user')->get() as $User)
                            <tr>
                              <td style="width:50px;"><span class="round">{{$User->id}}</span></td>
                                <td>
                                    <h6>{{$User->name}} </h6></td>
                                <td><span class="label label-light-success">{{$Orders->where('user_id',$User->id)->count()}}</span></td>
                            </tr>
                            <?php
                            $count++;
                            ?>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
<script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
       <!-- ============================================================== -->
    <!-- chartist chart -->
    <script src="{{asset('cpanel/assets/plugins/chartist-js/dist/chartist.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <!--Morris JavaScript -->
    <script src="{{asset('cpanel/assets/plugins/raphael/raphael-min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/morrisjs/morris.js')}}"></script>
    <!-- Chart JS -->
    <script src="{{asset('cpanel/assets/plugins/echarts/echarts-all.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/toast-master/js/jquery.toast.js')}}"></script>

<script>
$(function () {
        "use strict";
            // ============================================================== 
            // Total revenue chart
            // ============================================================== 
        $(".choose_gannt_Kitchen").on('change', function() {
            var kitchen_id=$(this).val();
            alert(kitchen_id)
            $.ajax({
                url:'{{url("ganntChartAdmin")}}',
                data:{'kitchen_id': kitchen_id },
                type:"get",
                success: function (data) {
                    data.toString()
                    new Chartist.Bar('.total-sales', {
                        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept','Oct','Nov','Dec']
                        , series: data
                        
                    }, {
                        high: 200000
                        , low: 10000
                        , fullWidth: true
                        , plugins: [
                        Chartist.plugins.tooltip()]
                        , stackBars: true
                        , axisX: {
                            showGrid: false
                        }
                        , axisY: {
                            labelInterpolationFnc: function (value) {
                                return (value / 1000) + 'k';
                            }
                        }
                    }).on('draw', function (data) {
                        if (data.type === 'bar') {
                            data.element.attr({
                                style: 'stroke-width: 30px'
                            });
                        }
                    });

                }
            }) 

        });

        var kitchen_id= @if(\App\Kitchen::first()) {{\App\Kitchen::first()->id}} @endif;
        
            $.ajax({
                url:'{{url("ganntChartAdmin")}}',
                data:{'kitchen_id': kitchen_id },
                type:"get",
                success: function (data) {
                    data.toString()
                    new Chartist.Bar('.total-sales', {
                        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept','Oct','Nov','Dec']
                        , series: data
                        
                    }, {
                        high: 200000
                        , low: 10000
                        , fullWidth: true
                        , plugins: [
                        Chartist.plugins.tooltip()]
                        , stackBars: true
                        , axisX: {
                            showGrid: false
                        }
                        , axisY: {
                            labelInterpolationFnc: function (value) {
                                return (value / 1000) + 'k';
                            }
                        }
                    }).on('draw', function (data) {
                        if (data.type === 'bar') {
                            data.element.attr({
                                style: 'stroke-width: 30px'
                            });
                        }
                    });

                }
            }) 

    });

</script>    
<script>
//DataTable
var table = $('#myTable').DataTable({
    dom: 'Bfrtip',
    "order": [[ 3, "desc" ]],
    @if( LaravelLocalization::getCurrentLocale() == "ar")
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
        }
    @endif
});
//End DataTable

</script>
<script>
//DataTable
var table = $('#myTable2').DataTable({
    dom: 'Bfrtip',
    "order": [[ 2, "desc" ]],
    @if( LaravelLocalization::getCurrentLocale() == "ar")
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
        }
    @endif
});
//End DataTable

</script>
    <script type="text/javascript">
        
        $("#all_drivers").click(function(e){
            e.preventDefault();
               $.ajax({
               type:'GET',
               url:'/all_drivers',
               success:function(data) {
                  $("#container-fluid").html(data);
               }

});
    </script>
@endsection
