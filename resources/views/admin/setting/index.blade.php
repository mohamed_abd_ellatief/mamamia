@extends('layouts.cpanel_layout.master')
@section('title')
{{trans('word.Setting')}}
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('cpanel/assets/plugins/dropify/dist/css/dropify.min.css')}}">
<!--alerts CSS -->
<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
<!-- Bootstrap core CSS -->


<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('content')


<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{trans('word.Setting')}}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('word.Setting')}}</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">{{trans('word.Setting')}}</h4>
                    <form class="m-t-40" action="{{url('UpdateSetting')}}" method="POST" enctype="multipart/form-data" novalidate>
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>{{trans('word.WebSite Name')}}</h5>
                                    <div class="controls">
                                        <input type="text" name="title" value="{{$Setting->title}}" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>{{trans('word.distance')}} </h5>
                                    <div class="controls">
                                        <div class="input-group">
                                            <input type="number" value="{{$Setting->distance}}" name="distance" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}" data-validation-required-message="{{trans('word.Must be a number')}}">
                                            <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>{{trans('word.Vat')}} </h5>
                                    <div class="controls">
                                        <div class="input-group">
                                            <input type="number" value="{{$Setting->vat}}" name="vat" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}"> <span class="input-group-addon">%</span> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>@if(Request::segment(1) == 'ar')  نسبة الارباح   @else  Profit Rate @endif   </h5>
                                    <div class="controls">
                                        <div class="input-group">
                                            <input type="number" value="{{$Setting->profitRate}}" name="profitRate" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}"> <span class="input-group-addon">%</span> </div>
                                    </div>
                                </div>
                            </div>

<!--Textarea with icon prefix-->
{{-- <div class="md-form">
  <i class="fas fa-pencil-alt prefix">الشروط والاحكام باللغة العربيه</i>
             
<textarea id="form10" class="md-textarea form-control" rows="3"  name="ar_title">{{$Setting->ar_title}}</textarea><br>
</div>
  <label for="form10">terms and conditions</label>
</div>
      <div class="md-form">
<textarea id="form10" class="md-textarea form-control" rows="3"  name="en_title">{{$Setting->en_title}}</textarea><br>
                  

</div> --}}
                   <!--Material textarea-->
<div class="md-form" style="margin-left: 50px">
  <i class="fas fa-pencil-alt prefix"></i>
  <label for="form10">الشروط والاحكام  </label>
  <textarea name="ar_title" id="form10" class="md-textarea form-control" rows="15" cols="20">{{ $Setting->ar_title }}</textarea>
  
</div>  
<br><br>
<!--Textarea with icon prefix-->
<div class="md-form">
  <i class="fas fa-pencil-alt prefix"></i>
  <label for="form10">conditions and terms </label>
  <textarea name="en_title" id="form10" class="md-textarea form-control" rows="15" cols="20">{{ $Setting->en_title }}</textarea>
  
</div>  
                        
                  

                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="card">
                                    <div class="card-block">
                                        <h4 class="card-title">{{trans('word.Logo')}}</h4>
                                        <input type="file" name="logo" id="input-file-now-custom-1" class="dropify" @if($Setting->logo) data-default-file="{{asset('cpanel/upload/mainSetting/'.$Setting->logo)}}" @endif />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="card">
                                    <div class="card-block">
                                        <h4 class="card-title">{{trans('word.image Sidebar')}}</h4>
                                        <input type="file" name="image" id="input-file-now-custom-1" class="dropify" @if($Setting->image) data-default-file="{{asset('cpanel/upload/mainSetting/'.$Setting->image)}}" @endif />
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="text-xs-right">
                            <button type="submit" class="btn btn-info">{{trans('word.Save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
@endsection
@section('js')
    <!-- jQuery file upload -->
    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/js/froala_editor.min.js'></script>

    <script src="{{asset('cpanel/assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
    <!-- This is data table -->
    <script src="{{asset('cpanel/en/js/validation.js')}}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
    <script>
         $('.dropify').dropify();
        ! function(window, document, $) {
            "use strict";
            $("input,select").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);
    </script>
    <!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.0/js/mdb.min.js"></script>
   
@endsection
