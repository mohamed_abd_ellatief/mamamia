<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">{{trans('word.Cashier')}}</h4> </div>
            <form class="form-horizontal form-material" method="POST" action="{{url('Update_Vendor')}}" enctype="multipart/form-data" novalidate>
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-group col-md-12 m-b-20">
                            <label class="control-label">{{trans('word.Name')}}</label>
                            <div class="controls">
                                <input type="text" value="{{$Vendor->name}}" class="form-control" name="name" required data-validation-required-message="{{trans('word.This field is required')}}"> 
                                <input type="hidden" value="{{$Vendor->id}}" name="id"> 
                            </div>
                        </div> 
                        <div class="form-group col-md-12 m-b-20">
                            <label class="control-label">{{trans('word.Email')}}</label>
                            <input type="email" class="form-control" name="email" value="{{$Vendor->email}}" data-validation-regex-regex="([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})" data-validation-regex-message="Enter Valid Email"> 
                        </div>
                        <div class="form-group col-md-12 m-b-20">
                            <label class="control-label">{{trans('word.Mobile')}}</label>
                            <div class="controls">
                                <input type="text" class="form-control" value="{{$Vendor->mobilenumber}}" name="mobilenumber" required data-validation-required-message="{{trans('word.This field is required')}}"  data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="{{trans('word.No Characters Allowed, Only Numbers')}}" data-validation-regex-regex="(05)[0-9]{8}" data-validation-regex-message="{{trans('word.Invalid Mobile Number')}}"> 
                            </div> 
                        </div>
                        <div class="form-group col-md-12 m-b-20">
                            <label class="control-label">{{trans('word.Password')}}</label>
                            <div class="controls">
                                <input type="password" class="form-control" name="password" data-validation-min-message="{{trans('word.Password must be at least 4 letters')}}" min="4"> 
                            </div> 
                        </div>
                        <div class="form-group col-md-12 m-b-20">
                            <div class="fileupload btn btn-danger btn-rounded waves-effect waves-light"><span><i class="ion-upload m-r-5"></i>{{trans('word.Upload Vendor Image')}}</span>
                                <input type="file" class="upload" name="image"> </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect">{{trans('word.Save')}}</button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    <script src="{{asset('cpanel/en/js/validation.js')}}"></script>
    <script>
        ! function(window, document, $) {
            "use strict";
            $("input,select").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);
    </script>