
@extends('layouts.cpanel_layout.master')
@section('title')
    {{trans('word.Home')}}
@endsection
@section('css')
<!-- chartist CSS -->
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('cpanel/assets/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
<style>
.Activeswitch,.DisActiveswitch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.Activeswitch input,.DisActiveswitch input { 
  opacity: 0;
  width: 0;
  height: 0;
}






table {

  table-layout: fixed ;
  width: 100% ;
}
td {
  width: 25% ;
}

/*End style*/
#contact-form label {
    float: right;
}
table {
    width: 100%;
}
td {
    max-width: 500px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}
.modal-body h3{
  color: blue;
}
</style>
@endsection
@section('content')
<div style="width:100%;margin-top: 10px; margin-bottom: 20px;">
    <center>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    </div>
    </center>
<table id="example" class="display nowrap" style="width:100%">
        <thead>
               <th>اسم السائق </th>

                <th>رقم السائق</th>
                 <th>مكان السائق </th>
                <th>تفاصيل الطلب </th>


                <th>حالة الطلب </th>
                <th>اجمالي الطلبات</th>
                <th>الطلبات الملغاه</th>
                <th>الطلبات  المقبولة</th>
              
                

    
        </thead>
        <tbody>

             @if(isset($order_driver))
                @for($i=0;$i<count($order_driver);$i++)
                @inject('Order_Driver','App\order_driver')
            <tr>
              {{--    <td>{{ $driver->drivers->username }}</td>

                <td>{{ $driver->drivers->phone }}</td> --}}
                <td>{{ $order_driver[$i]->drivers[0]->username }}</td>

                <td>
                    {{ $order_driver[$i]->drivers[0]->phone }}
                </td>

  <td style="width: 50px;"><a style="color: white;text-decoration: none;" href="driver_place/{{ $order_driver[$i]->drivers[0]->lat }}/{{  $order_driver[$i]->drivers[0]->lng}}"><button class="btn btn-success ">مكان السائق</button></a></td>


             <td>
                   <button data-order_id="{{ $order_driver[$i]->orders[0]->codeorder }}" data-order_address="{{ $order_driver[$i]->orders[0]->userplace }}" data-ordertime="{{ $order_driver[$i]->orders[0]->created_at }}" data-needTime="{{ $order_driver[$i]->orders[0]->neededtime }}" data-deliverytotalprice="{{ $order_driver[$i]->orders[0]->deliverytotalprice }}" data-totalprice="{{ $order_driver[$i]->orders[0]->totalprice }}" 
                   data-distancetouser="{{ $order_driver[$i]->orders[0]->distancetouser }}"
                   data-priceVat="{{ $order_driver[$i]->orders[0]->priceVat }}"
                   data-kitchen="{{ $order_driver[$i]->orders[0]->kitchen->ar_title }}" 
                   data-name="{{ $order_driver[$i]->orders[0]->user->name }}" 
                   data-mobilenumber="{{ $order_driver[$i]->orders[0]->user->mobilenumber }}" 
                    class="detailsButton btn-primary btn" data-toggle="modal" data-target="#exampleModalCenter">
                      تفاصيل الطلب
                    </button>
                  </td>

            
                <td style="width: 250px">
                  @if($order_driver[$i]->status == "A" )
<span class="badge badge-primary">جاري التحضير</span>
@elseif($order_driver[$i]->status == "R")
<span class="badge badge-danger">مرفوضة</span>
@elseif($order_driver[$i]->status == "D")
<span class="badge badge-warning">مع السائق</span>
@elseif($order_driver[$i]->status == "F")
<span class="badge badge-success">تم التوصيل</span>
{{-- <span class="badge badge-secondary">Secondary</span>
<span class="badge badge-success">Success</span>

<span class="badge badge-warning">Warning</span>
<span class="badge badge-info">Info</span>
<span class="badge badge-light">Light</span>
<span class="badge badge-dark">Dark</span> --}}

@endif

                </td>
               

                         <td style="text-align: center;font-size: 25"><span class="badge badge-primary">
                          {{count($Order_Driver::where('driver_id',$order_driver[$i]->driver_id)->get())}}
                          </span>
                        </td>
                         
                          <td style="text-align: center;font-size: 25"><span class="badge badge-danger">
                            {{count($Order_Driver::where('driver_id',$order_driver[$i]->driver_id)->where('status','R')->get())}}
                          </span>
                          </td>
                         <td style="text-align: center;font-size: 25"><span style="font-size: 25" class="badge badge-success">

                          {{count($Order_Driver::where('driver_id',$order_driver[$i]->driver_id)->where('status','!=','R')->get())}}
                        </span>
                        </td>
                
             
            </tr>
      @endfor
                @endif     
        </tbody>

    </table>
    </div>
<!-- Button trigger modal -->

<!-- Modal -->



<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Launch demo modal
</button>
 --}}
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content" style="width: 800px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">تفاصيل الطلب</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body row" id="order_det" style="direction: rtl;">

        <div class="col-lg-6">
        <h3>رقم الطلب </h3><div style="display: inline;margin-bottom: 10px;" id="order_id"></div><hr>
      </div>
      <div class="col-lg-6">
        <h3>اسم المطعم </h3><div style="display: inline;margin-bottom: 10px;" id="kitchen"></div><hr>
      </div>
<div class="col-lg-6">
        <h3>اسم العميل </h3><div style="display: inline;margin-bottom: 10px;" id="name"></div><hr>
      </div>
<div class="col-lg-6">
        <h3>رقم العميل </h3><div style="display: inline;margin-bottom: 10px;" id="mobilenumber"></div><hr>
      </div>
<div class="col-lg-6">
        <h3>عنوان  الطلب  </h3><div style="display: block;" id="order_address"></div><hr>
      </div>
<div class="col-lg-6">
        <h3>وقت  الطلب </h3><div style="display: block;" id="ordertime"></div><hr>
      </div>
<div class="col-lg-6">
        <h3>الوقت اللازم للتحضير</h3><div style="display: block;" id="needTime"></div><hr>
      </div>
<div class="col-lg-6">
        <h3>الدليفيري</h3><div style="display: block;" id="deliverytotalprice"></div><hr>
      </div>
<div class="col-lg-6">
         <h3> القيمة المضافة</h3><div style="display: block;" id="priceVat"></div><hr>
</div>
<div class="col-lg-6">
        <h3>اجمالي سعر الطلب</h3><div style="display: block;" id="totalprice"></div><hr>
      </div>
<div class="col-lg-6">
        <h3> المسافة </h3><div style="display: block;" id="distancetouser"></div><hr>
      </div>
    {{--     <table class="table table-bordered">
          <tr>
            
            <span>رقم الطلب   </span>
           <td id="order_id"></td>
          </tr>
          <tr>
            <span>عنوان الطلب </span>
                        <td id="order_address"></td>

          </tr>
          
         
        </table> --}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-danger" onclick='printJS({
    printable: "order_det",
    type: "html",
    style: ".heavy {font-weight: 800;}"
  });'>طباعه</button>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')
    <script src="{{asset('cpanel/en/js/validation.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
    {{-- <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script> --}}
    <script src="{{asset('cpanel/assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('cpanel/assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>

<script type="text/javascript" src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
<script type="text/javascript" src="https://printjs-4de6.kxcdn.com/print.min.css"></script>
<script>

$(document).ready(function() {
    $(".detailsButton").on('click', function () {
    $("#order_id").html($(this).data("order_id"));
    $("#order_address").html($(this).data("order_address"));
    $("#ordertime").html($(this).data("ordertime"));
    $("#needTime").html($(this).data("needTime"));
    $("#deliverytotalprice").html($(this).data("deliverytotalprice"));
    $("#totalprice").html($(this).data("totalprice"));
    $("#distancetouser").html($(this).data("distancetouser") +" "+ "كيلو متر");
    $("#priceVat").html($(this).data("priceVat"));
    $("#kitchen").html($(this).data("kitchen"));
    $("#name").html($(this).data("name"));
    $("#mobilenumber").html($(this).data("mobilenumber"));
  });
  var arr = [];

    var table = $('#example').DataTable( {
    dom: 'Bfrtip',
           buttons: [
                { extend: 'copy', text: '{{trans('word.copy')}}' },
                { extend: 'excel', text: '{{trans('word.excel')}}' },
                { extend: 'pdf', text: '{{trans('word.pdf')}}' },
                { extend: 'print', text: '{{trans('word.print')}}' },
            ],
            "oLanguage": {
  "sUrl": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
                           },
              
      
    } );


  

  // 
  //alert(arr);
 
 
 


     $("#print").click(function(){

        var doc = new jsPDF()
 doc.text(["إذا لم تستح فاصنع ما شئت", "إذا لم تستح", "فاصنع ما شئت"], 200, 10, {lang: 'ar', align: 'right'});
 // doc.fromHTML($('#order_det').html(), 15, 15, {
 //        'width': 170,
 //            'elementHandlers': specialElementHandlers
 //    });
doc.save('a4.pdf')
        });


} );


</script>

<script type="text/javascript">

$('.Activeswitch').on('click', function() {
    //alert('ok');
    //e.preventDefault();
    //console.log($(this).attr("value"));
          // e.stopPropagation();

         $.ajax({
           type: 'post',
          url: '/driver_dis_active',
          data: {
              '_token': "{{ csrf_token() }}",
                'id': $(this).attr("value")
            },
            success: function(data) {
                
                    swal({
                title: "{{trans('word.Succeeded')}}",
                text: "تم تعطيل السائق بنجاح",
                type:"success" ,
                timer: 1000,
                showConfirmButton: false
            });
                             location.reload();

            }
        });
    });
$('.DisActiveswitch').on('click', function() {
       // ظظ alert('ok');

    $.ajax({
           type: 'post',
          url: '/driver_active',
          data: {
              '_token': "{{ csrf_token() }}",
                'id': $(this).attr("value")
            },
            success: function(data) {
                
                    swal({
                title: "{{trans('word.Succeeded')}}",
                text: "تم تتنشيط  السائق بنجاح",
                type:"success" ,
                timer: 1000,
                showConfirmButton: false
            });
                             location.reload();

            }
        });
    });
$(function () {
    "use strict";
    
    $(".popup img").click(function () {
        var $src = $(this).attr("src");
        $(".show").fadeIn();
        $(".img-show img").attr("src", $src);
    });
    
    $("span, .overlay").click(function () {
        $(".show").fadeOut();
    });
    

  

});
</script>

@if( session()->has("message"))
    
        <script>
            swal({
                title: "{{trans('word.Succeeded')}}",
                text: "{{trans('word.Operation completed successfully')}}",
                type:"success" ,
                timer: 1000,
                showConfirmButton: false
            });
        </script>
        @endif
@endsection
