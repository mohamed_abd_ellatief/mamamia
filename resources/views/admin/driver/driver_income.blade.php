
@extends('layouts.cpanel_layout.master')
@section('title')
    {{trans('word.Home')}}
@endsection
@section('css')
<!-- chartist CSS -->
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('cpanel/assets/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
<style>
.Activeswitch,.DisActiveswitch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.Activeswitch input,.DisActiveswitch input { 
  opacity: 0;
  width: 0;
  height: 0;
}


table {

  table-layout: fixed ;
  width: 100% ;
}
td {
  width: 25% ;
}
.btn-info, .btn-info.disabled {
    padding-right: 5px;
    background: #009efb;
    border: 1px solid #009efb;
    display: table-footer-group;
}
#b,#d{
    width: 40px;
    padding: 5px;
}
/*End style*/
#contact-form label {
    float: right;
}
</style>
@endsection
@section('content')
<div style="width:100%;margin-top: 10px;margin-bottom: 20px;">
    <center>
    <button class="btn btn-danger " id="delete_multi">حذف 0 سائق</button>
    <button class="btn btn-info "><a href="add_form_driverIncome" style="color: white;text-decoration: none" >انشاء سائق</a></button>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    </div>
    </center>
<table id="example" class="display" style="width:100%;margin-top: 10px;">
        <thead>
               <th>#</th>

                <th>الاسم</th>
                <th>الدخل الكلي</th>

                <th>االرصيد المتبقي</th>
                <th>المبلغ المدفوع </th>

                <th> الاجراء </th>
                <th> الاجراء </th>
        </thead>
        <tbody>
             @if($drivers)
                @foreach($drivers as $driver)
            <tr>
                 <td>{{ $driver->id }}</td>

                <td>{{ $driver->drivers->username }}</td>
                <td>{{ $driver->total_income }}</td>

               


                <td>{{ $driver->remaining_balance }}</td>
                <td>{{ $driver->paid_amount }}</td>



<td style="width: 50px;">
              <form action="pay" method="post">
                {{ csrf_field() }}
                 <button class="btn btn-info">
                            دفع
                          </button>
                            <input type="number" name="pay" min="0" max="{{ $driver->remaining_balance }}" required>
                            <input type="hidden" name="driver_id"  value ="{{ $driver->driver_id }}">
                       
                
              </form>
                       </td>    
                                 <td>{{ $driver->id }}</td>

            </tr>
      @endforeach
                @endif     
        </tbody>

    </table>
    </div>
<!-- Button trigger modal -->

<!-- Modal -->

@endsection
@section('js')
<script src="{{asset('cpanel/en/js/validation.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
    {{-- <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script> --}}
    <script src="{{asset('cpanel/assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('cpanel/assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>

<script>

$(document).ready(function() {
  var arr = [];

    var table = $('#example').DataTable( {
    dom: 'Bfrtip',
           buttons: [
                { extend: 'copy', text: '{{trans('word.copy')}}' },
                { extend: 'excel', text: '{{trans('word.excel')}}' },
                { extend: 'pdf', text: '{{trans('word.pdf')}}' },
                { extend: 'print', text: '{{trans('word.print')}}' },
            ],
            "oLanguage": {
  "sUrl": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
                           },
        "columnDefs": [ {
            "targets": -1,
            "data": null,
            "defaultContent": "&nbsp <button  id='d' class='btn btn-danger'>Delete  </button>"
        } ]
    } );


    $('#example tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');
    $('#delete_multi').text(" حذف   " +table.rows('.selected').data().length +' ' +' سائق' );
    arr = [];

   $.map(table.rows('.selected').data(), function (item) {
        arr.push(item[0])
            });
  // 
  //alert(arr);
    } );
 
    $('#delete_multi').click( function () {
    $('#delete_multi').text(" حذف   "  +table.rows('.selected').data().length + ' '+' سائق' );
    
      if (arr.length == 0) {
         swal({
  title: " يجب عليك اختيار السائق لحذفه",
  text: "Once deleted, you will not be able to recover this imaginary file!",
  icon: "warning",
  dangerMode: true,
});
         return ;
      }
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  swal({
  title: "هل انت متأكد من مسح السائق ؟",
  text: "Once deleted, you will not be able to recover this imaginary file!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
    $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
   // alert(ids);
                   $.ajax({
           type: 'post',
          url: "{{ url('Delete_multi_DriverIncome')}}",
          data: {
                'arr':arr
            },
            success: function(data) {
              //alert(data);
                    swal({
                title: "{{trans('word.Succeeded')}}",
                text: "تم ت حذف ائق بنجاح",
                type:"success" ,
                timer: 1000,
                showConfirmButton: false
            });
            location.reload();

            },error:function(){ 
                 swal({
                title: "{{trans('word.Succeeded')}}",
                text: "خطأ",
                type:"Failed" ,
                timer: 1000,
                showConfirmButton: false
            });
        }
        });
                 } else {
    swal("Your imaginary file is safe!");
  }
});
         
});



    $('#example tbody').on( 'click', '#b', function () {
        //alert('edit');
        var data = table.row( $(this).parents('tr') ).data();
        //alert( "edti " + data[0] );
     window.location.assign("edit/"+data[0])
  });
     $('#example tbody').on( 'click', '#d', function () {
        //alert('edit');
        var data = table.row( $(this).parents('tr') ).data();
       // alert( "delete " + data[0] );
       var driver_id = data[0];
        //alert('dfe');


        swal({
  title: "هل انت متأكد من مسح السائق ؟",
  text: "Once deleted, you will not be able to recover this imaginary file!",
  icon: "warning",
  buttons: true,
  dangerMode: true,
})
.then((willDelete) => {
  if (willDelete) {
        $.ajax({
            type: "get",
            url: "{{ url('DeleteDriverIncome')}}"+'/'+driver_id,
            success: function (data) {
                       swal({
                title: "{{trans('word.Succeeded')}}",
                text: "{{trans('تم مسح السائق بنجاح')}}",
                type:"success" ,
                timer: 1000,
                showConfirmButton: false
            });
              location.reload();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });

  } else {
    swal("Your imaginary file is safe!");
  }
});



 
    
    } );

        $('#example tbody').on( 'click', '#a', function () {
        //alert('edit');
        var a = $("#a").attr("value");
        alert( "active " + a );
    } );


} );


</script>

<script type="text/javascript">
$('.Activeswitch').on('click', function() {
    //alert('ok');
    //e.preventDefault();
    //console.log($(this).attr("value"));
          // e.stopPropagation();

         $.ajax({
           type: 'post',
          url: '/driver_dis_active',
          data: {
              '_token': "{{ csrf_token() }}",
                'id': $(this).attr("value")
            },
            success: function(data) {
                location.reload();
                    swal({
                title: "{{trans('word.Succeeded')}}",
                text: "تم تعطيل السائق بنجاح",
                type:"success" ,
                timer: 1000,
                showConfirmButton: false
            });
            }
        });
         
    });
$('.DisActiveswitch').on('click', function() {
       // ظظ alert('ok');

    $.ajax({
           type: 'post',
          url: '/driver_active',
          data: {
              '_token': "{{ csrf_token() }}",
                'id': $(this).attr("value")
            },
            success: function(data) {
                location.reload();
                    swal({
                title: "{{trans('word.Succeeded')}}",
                text: "تم تتنشيط  السائق بنجاح",
                type:"success" ,
                timer: 1000,
                showConfirmButton: false
            });
            }
        });
//location.reload();
    });
$(function () {
    "use strict";
    
    $(".popup img").click(function () {
        var $src = $(this).attr("src");
        $(".show").fadeIn();
        $(".img-show img").attr("src", $src);
    });
    
    $("span, .overlay").click(function () {
        $(".show").fadeOut();
    });
    

  

});
</script>

@if( session()->has("message"))
  
        <script>
            swal({
                title: "{{trans('word.Succeeded')}}",
                text: "{{trans('word.Operation completed successfully')}}",
                type:"success" ,
                timer: 1000,
                showConfirmButton: false
            });
        </script>
        @endif
@endsection
