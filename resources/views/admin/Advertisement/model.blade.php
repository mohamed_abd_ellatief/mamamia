<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-block">
                <form action="{{url('Update_Advert')}}" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>{{trans('word.ar_title')}}</h5>
                                    <input type="text" name="ar_title" value="{{$Advertisement->ar_title}}" class="form-control">
                                    <input type="hidden" name="id" value="{{$Advertisement->id}}">
                                    <div class="form-control-feedback"><small>{{trans('word.optional')}}</small></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>{{trans('word.en_title')}}</h5>
                                    <input type="text" name="en_title" value="{{$Advertisement->en_title}}" class="form-control">
                                    <div class="form-control-feedback"><small>{{trans('word.optional')}}</small></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h5 class="box-title m-t-30">{{trans('word.Date of validity of the advertisement')}}</h5>
                                    <div class="controls">
                                    <div class="input-daterange input-group" id="date-ranges">
                                        {{-- <div class="controls"> --}}
                                            <input type="date" class="form-control" value="{{$Advertisement->startdate}}" name="start" id="start" required data-validation-required-message="{{trans('word.This field is required')}}" />
                                            <span class="input-group-addon bg-info b-0 text-white">{{trans('word.To')}}</span>
                                            <input type="date" class="form-control" value="{{$Advertisement->enddate}}" name="end" id="end" required data-validation-required-message="{{trans('word.This field is required')}}" />
                                        {{-- </div> --}}
                                    </div>
                                    </div>
                                </div>
                            </div>
							
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-block">
                                        <h4 class="card-title">{{trans('word.image')}}</h4>
                                        <input type="file" name="image" id="input-file-now-custom-1" class="dropify" data-default-file="{{asset('cpanel/upload/advertisement/'.$Advertisement->image)}}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{trans('word.Save')}}</button>
                        <button type="button" class="btn btn-inverse" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('cpanel/assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="{{asset('cpanel/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('cpanel/en/js/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
            "use strict";
            $("input,select").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);

   

        $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>

<!-- Script -->
<script type="text/javascript">
	
          //  jQuery('#date-ranges').datepicker({
            //    toggleActive: true,
            // });
	
	$("#end").change(function () {
    var startDate = document.getElementById("start").value;
    var endDate = document.getElementById("end").value;

    if ((Date.parse(startDate) >= Date.parse(endDate))) {
        alert("@if(Request::segment(1) == 'ar')  
			  لا يجب ان يكون تاريخ النهاية اقل من اتاريخ البداية 
			  @else
			  End date should be greater than Start date
			  @endif
			  ");
        document.getElementById("EndDate").value = "";
    }
});

</script>

