@extends('layouts.cpanel_layout.master')
@section('title')
{{trans('word.Advertisement')}}
@endsection

@section('css')
<!--alerts CSS -->

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('cpanel/assets/plugins/dropify/dist/css/dropify.min.css')}}">
<!-- Date picker plugins css -->
<link href="{{asset('cpanel/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{trans('word.Advertisement')}}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('word.Advertisement')}}</li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <button id="delete" class="btn pull-right hidden-sm-down btn-danger m-l-10">{{trans('word.Delete 0 row')}}</button>
            <button data-toggle="modal" data-target=".bs-example-modal-lg" class="btn pull-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> {{trans('word.Create')}}</button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    @if($errors->any())
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i>warning!</h4>
        <ul>
          @foreach($errors->all() as $error)
          <li style="font-weight: 40"><h4 style="font-weight:40">{{$error}}</h4></li>
          @endforeach
        </ul>
      </div>
    @endif

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('word.No Advertisement')}}</th>
                                    <th>{{trans('word.image')}}</th>
                                    <th>{{trans('word.Active')}}</th>
                                    <th>{{trans('word.Start')}}</th>
                                    <th>{{trans('word.End')}}</th>
                                    <th>{{trans('word.Status')}}</th>
                                    <th>{{trans('word.Edit')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($Advertisements as $Advertisement)
                                <tr id="{{$Advertisement->id}}">
                                    <td>
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                        {{$Advertisement->ar_title}}
                                        @else
                                        {{$Advertisement->en_title}}
                                        @endif
                                    </td>
                                    <td>
                                        <div class="image-popups">
                                            <a href="{{asset('cpanel/upload/advertisement/'.$Advertisement->image)}}" data-effect="mfp-newspaper">
                                                <img style="width:100px" src="{{asset('cpanel/upload/advertisement/'.$Advertisement->image)}}" class="img-responsive"/>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="switchery-demo m-b-30" data-id="{{$Advertisement->id}}">
                                            <input type="checkbox" name="status" @if($Advertisement->status == "on") checked @endif class="js-switch UpdateStatus" data-color="#55ce63" />
                                        </div>
                                    </td>
                                    <td>
                                        {{$Advertisement->startdate}}
                                    </td>
                                    <td>
                                        {{$Advertisement->enddate}}
                                    </td>
                                    <td>
                                        @if($Advertisement->startdate < date('Y-m-d') && $Advertisement->enddate > date('Y-m-d'))
                                        <h1 class="label label-success">{{trans('word.Not Expired')}}</h1>
                                        @else
                                        <h1 class="label label-danger">{{trans('word.Expired')}}</h1>
                                        @endif
                                    </td>
                                    <td>
                                        <a class="edit-Advert btn btn-info" href="#" data-id="{{$Advertisement->id}}" data-original-title="Edit">{{trans('word.Edit')}}</a>
                                    </td>
                                  </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

<!-- insert modal content -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h3 class="modal-title m-b-0 text-white" id="myLargeModalLabel">{{trans('word.New Advertisement')}}</h3>
                <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-block">
                                <form action="{{url('Store_Advert')}}" method="post" enctype="multipart/form-data" novalidate>
                                    @csrf
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>{{trans('word.ar_title')}}</h5>
                                                    <input type="text" name="ar_title" class="form-control">
                                                    <div class="form-control-feedback"><small>{{trans('word.optional')}}</small></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>{{trans('word.en_title')}}</h5>
                                                    <input type="text" name="en_title" class="form-control">
                                                    <div class="form-control-feedback"><small>{{trans('word.optional')}}</small></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <h5 class="box-title m-t-30">{{trans('word.Date of validity of the advertisement')}}</h5>
                                                    <div class="controls">
                                                    <div class="input-daterange input-group" id="date-">
                                                        {{-- <div class="controls"> --}}
                                                            <input type="date" class="form-control" name="start" required data-validation-required-message="{{trans('word.This field is required')}}" />
                                                            <span class="input-group-addon bg-info b-0 text-white">{{trans('word.To')}}</span>
                                                            <input type="date" class="form-control" name="end" required data-validation-required-message="{{trans('word.This field is required')}}" />
                                                        {{-- </div> --}}
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-block">
                                                        <h4 class="card-title">{{trans('word.image')}}</h4>
                                                        <div class="controls">
                                                            <input type="file" id="input-file-now" class="dropify" name="image" required data-validation-required-message="{{trans('word.This field is required')}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{trans('word.Save')}}</button>
                                        <button type="button" class="btn btn-inverse" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.modal -->
<div class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h3 class="modal-title text-white" id="myLargeModalLabel">{{trans('word.Edit Advertisement')}}</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection

@section('js')
    <!-- This is data table -->
    <script src="{{asset('cpanel/en/js/validation.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
    <!-- jQuery file upload -->
    <script src="{{asset('cpanel/assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{asset('cpanel/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

    <script>



        //DataTable
        var table = $('#myTable').DataTable({
            dom: 'Bfrtip',
            "ordering":false,
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
                }
            @endif
        });
        //End DataTable

            //Select Row From Table
            $('#myTable tbody').on( 'click', 'tr', function (e) {
                if (e.ctrlKey) {
                    $(this).toggleClass('selected');
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                    $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
                    @else
                    $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
                    @endif
                }else{
                    var isselected = false
                    var numSelected= table.rows('.selected').data().length
                    if($(this).hasClass('selected') && numSelected==1){
                        isselected = true
                    }
                    $('#myTable tbody tr').removeClass('selected');
                    if(!isselected){
                        $(this).toggleClass('selected');
                    }
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                    $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
                    @else
                    $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
                    @endif
                }
            });
            //End Select Row From Table

            $('.image-popups').magnificPopup({
                delegate: 'a',
                type: 'image',
                removalDelay: 500, //delay removal by X to allow out-animation
                callbacks: {
                beforeOpen: function() {
                    // just a hack that adds mfp-anim class to markup
                    this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                    this.st.mainClass = this.st.el.attr('data-effect');
                }
                },
                closeOnContentClick: true,
                midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
            });


            jQuery('#date-range').datepicker({
                toggleActive: true,
            });

            //Delete Row
            $("body").on("click", "#delete", function () {

                var dataList=[];
                $("#myTable .selected").each(function(index) {
                    dataList.push($(this).find('td:first').text())
                })
                if(dataList.length >0){
                    swal({
                        title: "{{trans('word.Are you sure?')}}",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{trans('word.Yes, Sure it!')}}",
                        cancelButtonText: "{{trans('word.No')}}",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    }, function(isConfirm){
                        if (isConfirm) {
                            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                            $.ajax({
                            url:'{{url("Delete_Advert")}}',
                            type:"post",
                            data:{'id':dataList,_token: CSRF_TOKEN},
                            dataType:"JSON",
                            success: function (data) {
                                if(data.message == "Success")
                                {
                                    $("#myTable .selected").hide();
                                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                                    $('#delete').text('حذف 0 سجل');
                                    @else
                                    $('#delete').text('Delete 0 row');
                                    @endif
                                    swal("{{trans('word.Deleted')}}", "{{trans('word.Message_Delete')}}", "success");
                                }else{
                                    swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error");
                                }
                            },
                            fail: function(xhrerrorThrown){
                                swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error");
                            }
                        });
                        } else {
                            swal("{{trans('word.Cancelled')}}", "{{trans('word.Message_Cancelled_Delete')}}", "error");
                        }
                    });
                }
            });
            //End Delete Row

    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });


        ! function(window, document, $) {
            "use strict";
            $("input,select").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);

        $(".switchery-demo").click(function(){
            var id=$(this).data('id')
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "get",
                url: "{{url('UpdateStatusAdvert')}}",
                data: {"id":id,_token:CSRF_TOKEN},
                success: function (data) {
                }
            })
        })

    $(".edit-Advert").click(function(){
      var id=$(this).data('id')
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
        type: "GET",
         url: "{{url('Edit_Advert')}}",
         data: {"id":id},
         success: function (data) {
              $(".bs-edit-modal-lg .modal-body").html(data)
              $(".bs-edit-modal-lg").modal('show')
              $(".bs-edit-modal-lg").on('hidden.bs.modal',function (e){
                //   $('.bs-edit-modal-lg').empty();
                  $('.bs-edit-modal-lg').hide();
              })
          }
      })
    })

    jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
    });
</script>
<?php
$message=session()->get("message");
?>
@if( session()->has("message"))
    @if( $message == "Success")
        <script>
            swal({
                title: "{{trans('word.Succeeded')}}",
                text: "{{trans('word.Operation completed successfully')}}",
                type:"success" ,
                timer: 1000,
                showConfirmButton: false
            });
        </script>
    @elseif ( $message == "Failed")
        <script>
            swal({
                title: "{{trans('word.Sorry')}}",
                text: "{{trans('word.the operation failed')}}",
                type:"error" ,
                timer: 2000,
                showConfirmButton: false
            });
        </script>
    @endif
@endif


@endsection
