@extends('layouts.cpanel_layout.master')
@section('title')
@if(Request::segment(1) == 'ar')  نسبة الارباح   @else  Profit Rate @endif
@endsection

@section('css')
<!--alerts CSS -->
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/select2/dist/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('cpanel/assets/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" />
<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />

@endsection

@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">@if(Request::segment(1) == 'ar')  نسبة الارباح   @else  Profit Rate @endif</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                <li class="breadcrumb-item active">@if(Request::segment(1) == 'ar')  نسبة الارباح   @else  Profit Rate @endif</li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            {{-- <button id="delete" class="btn pull-right hidden-sm-down btn-danger m-l-10">{{trans('word.Delete 0 row')}}</button>
            <button data-toggle="modal" data-target="#add-Vendor" class="btn pull-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> {{trans('word.Create')}}</button> --}}
            {{-- <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#add-contact">Add New Contact</button> --}}
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    @if($errors->any())
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i>warning!</h4>
        <ul>
          @foreach($errors->all() as $error)
          <li style="font-weight: 40"><h4 style="font-weight:40">{{$error}}</h4></li>
          @endforeach
        </ul>
      </div>
    @endif

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">


        <div class="col-12">
            <div class="card">
                <div class="card-block">
                        <h4 class="card-title">
        @if(Request::segment(1) == 'ar')  نسبة الارباح في الشهر  @else  Profit Rate @endif
                        </h4>
                    <div class="table-responsive m-t-40">
                        <table id="myTable2" class="table table-bordered display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>@if(Request::segment(1) == 'ar') شعار المطعم  @else kitchen Logo  @endif</th>
                                    <th>@if(Request::segment(1) == 'ar')   المطعم @else kitchen @endif</th>
                                    <th>@if(Request::segment(1) == 'ar') الايرادات  @else income  @endif</th>
                                    <th>@if(Request::segment(1) == 'ar') نسبة التطبيق @else Profit Rate  @endif</th>
                                    <th>@if(Request::segment(1) == 'ar') اجمالي الايرادات المستحقة  @else Total income due @endif</th>
                                    <th>@if(Request::segment(1) == 'ar')  المحصل @else Amount received  @endif</th>
                                    <th>@if(Request::segment(1) == 'ar')  المتبقى  @else Remaining amount @endif</th>

                                </tr>
                            </thead>
                            <tbody>
                              @inject('kitchens','App\Kitchen')
                              @inject('Orders','App\Order')
                              @inject('Setting','App\Mainsettings')
							@inject('Tax','App\IncomeTax')
                              @foreach($kitchens->all() as $Kitchen)
                                <tr id="{{$Kitchen->id}}">
                                    <td>{{$Kitchen->id}}</td>
                                    <td>
                                        @if($Kitchen->image)
                                        <div class="image-popups">
                                            <a href="{{asset('cpanel/upload/kitchen/'.$Kitchen->image)}}" data-effect="mfp-newspaper">
                                                <img style="width:50px" src="{{asset('cpanel/upload/kitchen/'.$Kitchen->image)}}" class="img-responsive"/>
                                            </a>
                                        </div>
                                        @endif
                                    </td>
                                    <td>
                                      @if(Request::segment(1) == 'ar')
                                      {{$Kitchen->ar_title}}

                                      @else
                                      {{$Kitchen->en_title}}
                                      @endif
                                    </td>
                                    <td>
                                      {{$Orders->where('kitchen_id',$Kitchen->id)->whereMonth('created_at',date('Y-m'))->sum('totalprice')}}
                                    </td>

                                    <td>
                                      {{$Setting->find(1)->profitRate}}
                                    </td>
                                    <?php
                                    $total =$Orders->where('kitchen_id',$Kitchen->id)->whereMonth('created_at',date('Y-m'))->sum('totalprice');
                                    $prfitRate =           $Setting->find(1)->profitRate;
                                    $income = ($total * $prfitRate ) / 100 ;
                                    ?>

                                    <td>
                                      {{$income}}
                                    </td>
                                    <td>
                                            {{$Tax->where('kitchen_id',$Kitchen->id)->whereMonth('created_at',date('Y-m'))->sum('price')}}
                                          </td>
                                          <?php
                                          $collect = $income - $Tax->where('kitchen_id',$Kitchen->id)->whereMonth('created_at',date('Y-m'))->sum('price');
                                          ?>
                                          <td>
                                            {{$collect}}
                                          </td>
                                    <td>
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="{{$Kitchen->id}}">@if(Request::segment(1) == 'ar') تحصيل @else Collect @endif</button>

                                    </td>
                                  </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

    <!-- insert modal content -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">@if(Request::segment(1) == 'ar')  تحصيل @else collect @endif</h4> </div>
                    <form class="form-horizontal form-material" method="POST" action="/StoreIncome" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="form-group col-md-12 m-b-20">
                                <label class="control-label">@if(Request::segment(1) == 'ar')  الملبغ المحصل @else Price @endif</label>
                                <div class="controls">
                                    <input type="number"  class="form-control" name="price" required data-validation-required-message="{{trans('word.This field is required')}}" required>
                                    <input type="hidden"  name="kitchen_id" id="kitchen_id">
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect">{{trans('word.Save')}}</button>
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="edit-model-Vendor" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    </div>
@endsection

@section('js')
    {{-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIWl0KLlaJV31fhfzuAul2sJA_u2gQuVA&libraries=places"></script> --}}
    <!-- This is data table -->
    <script src="{{asset('cpanel/en/js/validation.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
    {{-- <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script> --}}
    <script src="{{asset('cpanel/assets/plugins/select2/dist/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('cpanel/assets/plugins/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <script>
$('#exampleModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var recipient = button.data('whatever') // Extract info from data-* attributes
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this)
  modal.find('.modal-body #kitchen_id').val(recipient)
})
        //DataTable
        var table = $('#myTable').DataTable({
            dom: 'Bfrtip',
            "ordering":false,
            paging:true,
            buttons: [
                { extend: 'copy', text: '{{trans('word.copy')}}' },
                { extend: 'excel', text: '{{trans('word.excel')}}' },
                { extend: 'pdf', text: '{{trans('word.pdf')}}' },
                { extend: 'print', text: '{{trans('word.print')}}' },
            ],
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
                }
            @endif
        });
        var table = $('#myTable1').DataTable({
            dom: 'Bfrtip',
            "ordering":false,
            paging:true,
            buttons: [
                { extend: 'copy', text: '{{trans('word.copy')}}' },
                { extend: 'excel', text: '{{trans('word.excel')}}' },
                { extend: 'pdf', text: '{{trans('word.pdf')}}' },
                { extend: 'print', text: '{{trans('word.print')}}' },
            ],
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
                }
            @endif
        });
        var table = $('#myTable2').DataTable({
            dom: 'Bfrtip',
            "ordering":false,
            paging:true,
            buttons: [
                { extend: 'copy', text: '{{trans('word.copy')}}' },
                { extend: 'excel', text: '{{trans('word.excel')}}' },
                { extend: 'pdf', text: '{{trans('word.pdf')}}' },
                { extend: 'print', text: '{{trans('word.print')}}' },
            ],
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
                }
            @endif
        });
        //End DataTable

        // For select 2
        $(".select2").select2();

        //Select Row From Table
        $('#myTable tbody').on( 'click', 'tr', function () {
            if (event.ctrlKey) {
                $(this).toggleClass('selected');
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
                @else
                $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
                @endif
            }else{
                var isselected = false
                var numSelected= table.rows('.selected').data().length
                if($(this).hasClass('selected') && numSelected==1){
                    isselected = true
                }
                $('#myTable tbody tr').removeClass('selected');
                if(!isselected){
                    $(this).toggleClass('selected');
                }
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
                @else
                $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
                @endif
            }
        });
        //End Select Row From Table

        //Delete Row
        $("body").on("click", "#delete", function () {
            var dataList=[];
            $("#myTable .selected").each(function(index) {
                dataList.push($(this).find('td:first').text())
            })
            if(dataList.length >0){
                swal({
                    title: "{{trans('word.Are you sure?')}}",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('word.Yes, Sure it!')}}",
                        cancelButtonText: "{{trans('word.No')}}",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function(isConfirm){
                    if (isConfirm) {
                        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                        $.ajax({
                            url:'{{url("Delete_Vendor")}}',
                            type:"post",
                            data:{'id':dataList,_token: CSRF_TOKEN},
                            dataType:"JSON",
                            success: function (data) {
                                if(data.message == "Success")
                                {
                                    $("#myTable .selected").hide();
                                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                                    $('#delete').text('حذف 0 سجل');
                                    @else
                                    $('#delete').text('Delete 0 row');
                                    @endif
                                    swal("{{trans('word.Deleted')}}", "{{trans('word.Message_Delete')}}", "success");
                                }else{
                                    swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error");
                                }
                            },
                            fail: function(xhrerrorThrown){
                                swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error");
                            }
                        });
                    } else {
                        swal("{{trans('word.Cancelled')}}", "{{trans('word.Message_Cancelled_Delete')}}", "error");
                    }
                });
            }
        });
        //End Delete Row


        // Validation
        ! function(window, document, $) {
            "use strict";
            $("input,select").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);


        // Edit Status Vendor
        $(".switchery-demo").click(function(){
            var id=$(this).data('id')
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                type: "POST",
                url: "{{url('UpdateStatusVendor')}}",
                data: {"id":id,_token:CSRF_TOKEN},
                success: function (data) {
					console.log(data);
                    var alert=``;
                    if(data == 1){
                        alert=`<p class="label label-success">{{trans('word.Activted')}}</p>`;
                    } else {
                        alert=`<p class="label label-danger">{{trans('word.No Activted')}}</p>`;
                    }
                    $(`#`+id).find('.Activted').html(alert)
                }
            })
        })

        // popup Image
        $('.image-popups').magnificPopup({
            delegate: 'a',
            type: 'image',
            removalDelay: 500, //delay removal by X to allow out-animation
            callbacks: {
                beforeOpen: function() {
                    // just a hack that adds mfp-anim class to markup
                    this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                    this.st.mainClass = this.st.el.attr('data-effect');
                }
            },
            closeOnContentClick: true,
            midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
        });

        // Edit Vendor Modal
        $(".edit-Vendor").click(function(){
            var id=$(this).data('id')
            $.ajax({
                type: "GET",
                url: "{{url('Edit_Vendor')}}",
                data: {"id":id},
                success: function (data) {
                    $("#edit-model-Vendor").html(data)
                    $("#edit-model-Vendor").modal('show')
                    $("#edit-model-Vendor").on('hidden.bs.modal',function (e){
                        $('#edit-model-Vendor').hide();
                    })
                }
            })
        })

        // Switch Button
        jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
        });

</script>
<?php
$message=session()->get("message");
?>
@if( session()->has("message"))
    @if( $message == "Success")
        <script>
            swal({
                title: "{{trans('word.Succeeded')}}",
                text: "{{trans('word.Operation completed successfully')}}",
                type:"success" ,
                timer: 1000,
                showConfirmButton: false
            });
        </script>
    @elseif ( $message == "Failed")
        <script>
            swal({
                title: "{{trans('word.Sorry')}}",
                text: "{{trans('word.the operation failed')}}",
                type:"error" ,
                timer: 2000,
                showConfirmButton: false
            });
        </script>
    @endif
@endif


@endsection
