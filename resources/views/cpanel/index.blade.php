@extends('layouts.cpanel_layout.master')
@section('title')
    {{trans('word.Home')}}
@endsection

@section('css')
<!-- chartist CSS -->
<link href="{{asset('cpanel/assets/plugins/chartist-js/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('cpanel/assets/plugins/chartist-js/dist/chartist-init.css')}}" rel="stylesheet">
<link href="{{asset('cpanel/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
<link href="{{asset('cpanel/assets/plugins/css-chart/css-chart.css')}}" rel="stylesheet">
<!--This page css - Morris CSS -->
<link href="{{asset('cpanel/assets/plugins/morrisjs/morris.css')}}" rel="stylesheet">
<!-- toast CSS -->
<link href="{{asset('cpanel/assets/plugins/toast-master/css/jquery.toast.css')}}" rel="stylesheet">
<style>
tr {
width: 100%;
display: inline-table;
table-layout: fixed;
}
table{
 height:590px;
 display: -moz-groupbox;
}
tbody{
  overflow-y: scroll;
  height: 580px;
  width: 96%;
  position: inherit;
}
.slimScrollDiv{
    overflow-y: scroll;
    height: 270px;
    width: 96%;
    position: absolute;
}
</style>
@endsection
@section('content')
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================word== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">{{trans('word.Dashboard')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                            <li class="breadcrumb-item active">{{trans('word.Dashboard')}}</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-8">
                        <div class="card">
                            <div class="card-block">
                                <ul class="list-inline pull-right">
                                    <li>
                                        <h6 class="text-muted"><i class="fa fa-circle m-r-5" style="color:#820505"></i>{{Carbon\Carbon::now()->startOfYear()->format('Y')}}</h6>
                                    </li>
                                    <li>
                                        <h6 class="text-muted"><i class="fa fa-circle m-r-5" style="color:#e68000"></i>{{Carbon\Carbon::now()->startOfYear()->subYear(1)->format('Y')}}</h6>
                                    </li>
                                    <li>
                                        <h6 class="text-muted"><i class="fa fa-circle m-r-5" style="color:#b5b715"></i>{{Carbon\Carbon::now()->startOfYear()->subYear(2)->format('Y')}}</h6>
                                    </li>
                                </ul>
                                <select class="custom-select choose_gannt_Kitchen">
                                    @if(\App\Kitchen::where('user_id',auth::user()->id)->count() > 0)
                                    @foreach(\App\Kitchen::where('user_id',auth::user()->id)->get() as $Kitchen)
                                    <option value="{{$Kitchen->id}}">@if( LaravelLocalization::getCurrentLocale() == "ar"){{$Kitchen->ar_title}}@else{{$Kitchen->en_title}}@endif</option>
                                    @endforeach
                                    @endif
                                </select>
                                <div class="clear"></div>
                                <div class="total-sales" style="height: 365px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-block">
                                        <h4 class="card-title">{{trans('word.Active Users')}}</h4>
                                        <hr>
                                        <div id="morris-donut-chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-block">
                                <div class="pull-right">
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <button type="button" value="today" class="btn btn-secondary filter">{{trans('word.Today')}}</button>
                                        <button type="button" value="week" class="btn btn-secondary filter">{{trans('word.Week')}}</button>
                                        <button type="button" value="month" class="btn btn-secondary filter">{{trans('word.Month')}}</button>
                                    </div>
                                </div>
                                <h4 class="card-title">{{trans('word.Kitchen')}}</h4>
                                <h6 class="card-subtitle head_table">
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                    {{$ar_title_Kitchen}}
                                    @else
                                    {{$en_title_Kitchen}}
                                    @endif
                                </h6>
                                <hr>
                                <div class="table-responsive m-t-40">
                                    <table class="table table-hover v-middle ">
                                        <thead>
                                            <tr class="text-center">
                                                <th > {{trans('word.Kitchen')}} </th>
                                                <th> {{trans('word.Name')}} </th>
                                                <th> {{trans('word.Revenue')}} </th>
                                                <th> {{trans('word.Posts')}} </th>
                                                <th> {{trans('word.Reviews')}} </th>
                                            </tr>
                                        </thead>
                                        <tbody id="table_kitchen">
                                            <?php
                                            $Revenue = array();
                                            foreach ($Kitchens as $key => $row)
                                            {
                                                $Revenue[$key] = $row['Revenue'];
                                            }
                                            array_multisort($Revenue, SORT_DESC, $Kitchens);    
                                            ?>
                                            @foreach($Kitchens as $Kitchen)
                                            <tr>
                                                <td>
                                                    <img class="img-circle" src="{{asset('cpanel/upload/kitchen/'.$Kitchen->image)}}" alt="user" width="50"> </td>
                                                <td>
                                                    <a href="javascript:;">
                                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                        {{$Kitchen->ar_title}}
                                                        @else
                                                        {{$Kitchen->en_title}}
                                                        @endif
                                                    </a>
                                                </td>
                                                <td> {{$Kitchen->Revenue}} </td>
                                                <td> {{$Kitchen->Posts}} </td>
                                                <td>
                                                    <?php
                                                        $intt=intval($Kitchen->Reviews);
                                                        $floatt=$Kitchen->Reviews;
                                                        $newVal=$floatt-$intt;
                                                    ?>
                                                    @if($newVal>0)
                                                        @for ($x = 1; $x <= $intt; $x++) 
                                                        <i class="fa fa-star text-warning"></i>
                                                        @endfor
                                                        <i class="fa fa-star-half-full text-warning"></i>
                                                    @else
                                                        @for ($x = 1; $x <= $intt; $x++) 
                                                        <i class="fa fa-star text-warning"></i>
                                                        @endfor
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">{{trans('word.Customer issues')}}</h4>
                                <hr>
                                <select class="custom-select choose_kitchen_issues">
                                    @foreach(App\Kitchen::select('id','ar_title','en_title')->where('user_id',Auth::user()->id)->get() as $Kitchen)
                                    <option value="{{$Kitchen->id}}">
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                        {{$Kitchen->ar_title}}
                                        @else
                                        {{$Kitchen->en_title}}
                                        @endif
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="comment-widgets" >
                                <div class="message-box">
                                    <div class="message-widget message-scroll" id="div_reviews">
                                        @foreach ($Reviews as $Review)
                                        <!-- Comment Row -->
                                        <div class="d-flex flex-row comment-row">
                                            <div class="p-2"><img src="{{asset('cpanel/upload/user/'.$Review->image)}}" alt="user" width="50"></div>
                                            <div class="comment-text w-100">
                                                <h5>{{$Review->name}}</h5>
                                                <p class="m-b-5">{{$Review->review}}</p>
                                                <div class="comment-footer">
                                                    <span class="text-muted pull-right">{{$Review->created}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title pull-right" id="Total_Order">{{trans('word.Total Order')}} {{$Total_Order}} </h4>
                                <h4 class="card-title">{{trans('word.Hot Days')}}</h4>
                                <hr>
                                <select class="custom-select pull-right choose_month_hotDays">
                                    <?php
                                        $months = array('1'=>'January','2'=>'February','3'=>'March',
                                            '4'=>'April','5'=>'May','6'=>'June','7'=>'July',
                                            '8'=>'August','9'=>'September','10'=>'October',
                                            '11'=>'November','12'=>'December');    
                                    ?>
                                    @for($i=1; $i <= date('m'); $i++)
                                        @foreach ($months as $key => $value)
                                            @if($i == $key)
                                                @if($key == date('m'))
                                                    <option selected value="{{$key}}">{{trans("word.$value")}}</option>
                                                @else
                                                    <option value="{{$key}}">{{trans("word.$value")}}</option>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endfor
                                </select>
                                <select class="custom-select choose_kitchen_hotDays">
                                    @foreach(App\Kitchen::select('id','ar_title','en_title')->where('user_id',Auth::user()->id)->get() as $Kitchen)
                                    <option value="{{$Kitchen->id}}">
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                        {{$Kitchen->ar_title}}
                                        @else
                                        {{$Kitchen->en_title}}
                                        @endif
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="comment-widgets" >
                                <!-- Comment Row -->
                                <div class="d-flex flex-row comment-row">
                                    <div class="comment-text w-100">
                                        <h4 class="pull-right">{{trans('word.NOrder')}}</h4>
                                        <h5>{{trans('word.Day')}}</h5>
                                    </div>
                                </div>
                                <div class="message-box">
                                    <div class="message-widget message-scroll" id="Div_hotDays">
                                        @foreach ($countss as $date => $kitchen)
                                        <!-- Comment Row -->
                                        <div class="d-flex flex-row comment-row">
                                            <div class="comment-text w-100">
                                                <h4 class="pull-right">{{$kitchen}}</h4>
                                                <h5 >{{$date}}</h5>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">{{trans('word.Most Offers')}}</h4>
                                <hr>
                                <select class="custom-select pull-right choose_month_MostOffers">
                                    <?php
                                        $months = array('1'=>'January','2'=>'February','3'=>'March',
                                            '4'=>'April','5'=>'May','6'=>'June','7'=>'July',
                                            '8'=>'August','9'=>'September','10'=>'October',
                                            '11'=>'November','12'=>'December');    
                                    ?>
                                    @for($i=1; $i <= date('m'); $i++)
                                        @foreach ($months as $key => $value)
                                            @if($i == $key)
                                                @if($key == date('m'))
                                                    <option selected value="{{$key}}">{{trans("word.$value")}}</option>
                                                @else
                                                    <option value="{{$key}}">{{trans("word.$value")}}</option>
                                                @endif
                                            @endif
                                        @endforeach
                                    @endfor
                                </select>
                                <select class="custom-select choose_kitchen_MostOffers">
                                    @foreach(App\Kitchen::select('id','ar_title','en_title')->where('user_id',Auth::user()->id)->get() as $Kitchen)
                                    <option value="{{$Kitchen->id}}">
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                        {{$Kitchen->ar_title}}
                                        @else
                                        {{$Kitchen->en_title}}
                                        @endif
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="comment-widgets" >
                                <!-- Comment Row -->
                                <div class="d-flex flex-row comment-row">
                                    <div class="comment-text w-100">
                                        <h4 class="pull-right">
                                            {{trans('word.NOrder')}}
                                        </h4>
                                        <h5>{{trans('word.meal')}}</h5>
                                    </div>
                                </div>
                                <div class="message-box">
                                    <div class="message-widget message-scroll" id="Div_MostOffers">
                                        @foreach ($Offers as $Offer)
                                        <!-- Comment Row -->
                                        <div class="d-flex flex-row comment-row">
                                            <div class="comment-text w-100">
                                                <h4 class="pull-right">
                                                    {{$Offer->title}}
                                                </h4>
                                                <h5>{{$Offer->qty}}</h5>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="card earning-widget">
                            <div class="card-block">
                                <div class="card-title">
                                    <select class="custom-select choose_month pull-right">
                                        <?php
                                        $months = array('1'=>'January','2'=>'February','3'=>'March',
                                            '4'=>'April','5'=>'May','6'=>'June','7'=>'July',
                                            '8'=>'August','9'=>'September','10'=>'October',
                                            '11'=>'November','12'=>'December');    
                                        ?>
                                        @for($i=1; $i <= date('m'); $i++)
                                        @foreach ($months as $key => $value)
                                        @if($i == $key)
                                        @if($key == date('m'))
                                        <option selected value="{{$key}}">{{trans("word.$value")}}</option>
                                        @else
                                        <option value="{{$key}}">{{trans("word.$value")}}</option>
                                        @endif
                                        @endif
                                        @endforeach
                                        @endfor
                                    </select>                                    
                                    <select class="custom-select choose_kitchen pull-right" style="margin-right: 3%;">
                                        @foreach(App\Kitchen::select('id','ar_title','en_title')->where('user_id',Auth::user()->id)->get() as $Kitchen)
                                        <option value="{{$Kitchen->id}}">
                                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                                            {{$Kitchen->ar_title}}
                                            @else
                                            {{$Kitchen->en_title}}
                                            @endif
                                        </option>
                                        @endforeach
                                    </select>
                                        {{-- <hr> --}}
                                        <h4 class="card-title">{{trans('word.Top Users')}}</h4>
                                </div>
                            </div>
                            <div class="card-block b-t">
                                <table class="table v-middle no-border">
                                    <thead>
                                        <tr>
                                            <th>{{trans('word.Name')}}</th>
                                            <th>{{trans('word.Mobile')}}</th>
                                            <th>{{trans('word.NOrder')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table_users">
                                        <?php
                                            $Orders = array();
                                            foreach ($Users as $key => $row)
                                            {
                                                $Orders[$key] = $row['Orders'];
                                            }
                                            array_multisort($Orders, SORT_DESC, $Users);
                                        ?>
                                        @foreach($Users as $User)
                                        <tr>
                                            <td>{{$User->name}}</td>
                                            <td>{{$User->mobilenumber}}</td>
                                            <td>{{$User->Orders}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
@endsection

@section('js')
            <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <script src="{{asset('cpanel/assets/plugins/chartist-js/dist/chartist.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <!--Morris JavaScript -->
    <script src="{{asset('cpanel/assets/plugins/raphael/raphael-min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/morrisjs/morris.js')}}"></script>
    <!-- Chart JS -->
    <script src="{{asset('cpanel/assets/plugins/echarts/echarts-all.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/toast-master/js/jquery.toast.js')}}"></script>
    <script>
    $(function () {
        "use strict";
            // ============================================================== 
            // Total revenue chart
            // ============================================================== 
        $(".choose_gannt_Kitchen").on('change', function() {
            var kitchen_id=$(this).val();
            $.ajax({
                url:'{{url("ganntChart")}}',
                data:{'kitchen_id': kitchen_id },
                type:"get",
                success: function (data) {
                    data.toString()
                    new Chartist.Bar('.total-sales', {
                        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept','Oct','Nov','Dec']
                        , series: data
                        
                    }, {
                        high: 200000
                        , low: 10000
                        , fullWidth: true
                        , plugins: [
                        Chartist.plugins.tooltip()]
                        , stackBars: true
                        , axisX: {
                            showGrid: false
                        }
                        , axisY: {
                            labelInterpolationFnc: function (value) {
                                return (value / 1000) + 'k';
                            }
                        }
                    }).on('draw', function (data) {
                        if (data.type === 'bar') {
                            data.element.attr({
                                style: 'stroke-width: 30px'
                            });
                        }
                    });

                }
            }) 

        });

        var kitchen_id= @if(\App\Kitchen::where('user_id',auth::user()->id)->first()) {{\App\Kitchen::where('user_id',auth::user()->id)->first()->id}} @endif
        
            $.ajax({
                url:'{{url("ganntChart")}}',
                data:{'kitchen_id': kitchen_id },
                type:"get",
                success: function (data) {
                    data.toString()
                    new Chartist.Bar('.total-sales', {
                        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept','Oct','Nov','Dec']
                        , series: data
                        
                    }, {
                        high: 200000
                        , low: 10000
                        , fullWidth: true
                        , plugins: [
                        Chartist.plugins.tooltip()]
                        , stackBars: true
                        , axisX: {
                            showGrid: false
                        }
                        , axisY: {
                            labelInterpolationFnc: function (value) {
                                return (value / 1000) + 'k';
                            }
                        }
                    }).on('draw', function (data) {
                        if (data.type === 'bar') {
                            data.element.attr({
                                style: 'stroke-width: 30px'
                            });
                        }
                    });

                }
            }) 

    });

                
    // Rate And Revenue Kitchen
$('.filter').click(function(){
    var filter=$(this).val();
    $.ajax({
        url:'{{url("filter_Kitchen")}}',
        data:{'filter':filter},
        type:"get",
        success: function (data) {
            var rows='';
            var intt;
            var floatt;
            var newVal;
            var star;
            var halfstar
                data.sort(function (a, b) {
                    return b.Revenue - a.Revenue;
                });
            $.each(data, function(key, value) {
                intt=parseInt(value.Reviews);
                floatt=value.Reviews;
                newVal=floatt-intt;
                var x;      
                rows += `
                <tr>
                    <td>
                        <img class="img-circle" src="{{asset('cpanel/upload/kitchen')}}/`+value.image+`" alt="user" width="50"> </td>
                    <td>
                        <a href="javascript:;">`
                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                            +value.ar_title+
                            @else
                            +value.en_title+
                            @endif
                        `</a>
                    </td>
                    <td>`+value.Revenue+`</td>
                    <td> `+value.Posts+` </td>
                    <td>`;
                        if(newVal>0){
                            for (x = 1; x <= intt; x++) {
                                
                                rows +=`<i class="fa fa-star text-warning"></i>`;
                            }
                            rows += `<i class="fa fa-star-half-full text-warning"></i>`;
                        } else{
                            for (x = 1; x <= intt; x++) {
                                rows +=`<i class="fa fa-star text-warning"></i>`;
                            }
                        }  
                    rows +=`</td>
                </tr>
                `;
            });
            if(filter == 'today'){
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                $('.head_table').text("أداء المطعم / الوضع اليومى")                    
                @else
                $('.head_table').text("Restuarent preformance / Daily income")                    
                @endif
            } else if (filter == 'week') {
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                $('.head_table').text("أداء المطعم / الوضع الأسبوعى")                    
                @else
                $('.head_table').text("Restuarent preformance / Weekly income")                    
                @endif
            }else{
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                $('.head_table').text("أداء المطعم / الوضع الشهرى")                    
                @else
                $('.head_table').text("Restuarent preformance / Monthly income")                    
                @endif
            }
            $('#table_kitchen').html(rows)
        }
    })
})

    // Top Users Make Order
    $(".choose_month").on('change', function() {
        var month=$(this).val();
        var kitchen=$( ".choose_kitchen option:selected" ).val();
        var language = "{{Request::segment(1)}}"
        $.ajax({
            url:'{{url("UsersTable")}}',
            data:{'month':month , 'kitchen':kitchen ,'language':language},
            type:"get",
            success: function (data) {
                var tablss='' ;
                data.sort(function (a, b) {
                    return b.Orders - a.Orders;
                });
                var val=[];
                $.each(data, function(key, value) {
                    val.push(value.Orders)
                    tablss +=`
                        <tr>
                            <td>`+value.name+`</td>
                            <td>`+value.mobilenumber+`</td>
                            <td>`+value.Orders+`</td>
                        </tr>
                    `;
                })
                
                $('#table_users').html(tablss);
            }
        })
    })      

    $(".choose_kitchen").on('change', function() {
        var kitchen=$(this).val();
        var month=$( ".choose_month option:selected" ).val();
        var language = "{{Request::segment(1)}}"
        $.ajax({
            url:'{{url("UsersTable")}}',
            data:{'month':month , 'kitchen':kitchen ,'language':language},
            type:"get",
            success: function (data) {
                var tablss='' ;
                data.sort(function (a, b) {
                    return b.Orders - a.Orders;
                });
                $.each(data, function(key, value) {
                tablss +=`
                    <tr>
                        <td>`+value.name+`</td>
                        <td>`+value.mobilenumber+`</td>
                        <td class="text-right"><span class="label label-light-megna">`+value.Orders+`</span></td>
                    </tr>
                `;
                })

                $('#table_users').html(tablss)
            }
        })
    })      

    //////////////////////////////////////////////////////// 

    // Hot Days
    $(".choose_month_hotDays").on('change', function() {
        var month=$(this).val();
        var kitchen=$( ".choose_kitchen_hotDays option:selected" ).val();
        var language = "{{Request::segment(1)}}"
        $.ajax({
            url:'{{url("hotDays")}}',
            data:{'month':month , 'kitchen':kitchen ,'language':language},
            type:"get",
            success: function (data) {
                var tablss='' ;
                var val=[];
                $.each(data, function(key, value) {
                    val.push(value)
                    tablss +=`
                        <div class="d-flex flex-row comment-row">
                            <div class="comment-text w-100">
                                <h4 class="pull-right">`+value+`</h4>
                                <h5 >`+key+`</h5>
                            </div>
                        </div>
                    `;
                })
                var sum = val.reduce((a, b) => a + b, 0);
                @if(LaravelLocalization::getCurrentLocale() == 'ar')
                $('#Total_Order').text(`مجموع الطلبات: `+sum)
                @else
                $('#Total_Order').text(`Total Order: `+sum)
                @endif
                $('#Div_hotDays').html(tablss);
            }
        })
    })      

    $(".choose_kitchen_hotDays").on('change', function() {
        var kitchen=$(this).val();
        var month=$( ".choose_month_hotDays option:selected" ).val();
        var language = "{{Request::segment(1)}}"
        $.ajax({
            url:'{{url("hotDays")}}',
            data:{'month':month , 'kitchen':kitchen ,'language':language},
            type:"get",
            success: function (data) {
                var tablss='' ;
                var val=[];
                $.each(data, function(key, value) {
                    val.push(value)
                    tablss +=`
                        <div class="d-flex flex-row comment-row">
                            <div class="comment-text w-100">
                                <h4 class="pull-right">`+value+`</h4>
                                <h5 >`+key+`</h5>
                            </div>
                        </div>
                    `;
                })
                var sum = val.reduce((a, b) => a + b, 0);
                
                $('#Total_Order').text(`Total Order: `+sum)
                $('#Div_hotDays').html(tablss);
            }
        })
    })
////////////////////////////////////////////////////////////////////////

    // Most Offers
    $(".choose_month_MostOffers").on('change', function() {
        var month=$(this).val();
        var kitchen=$( ".choose_kitchen_MostOffers option:selected" ).val();
        var language = "{{Request::segment(1)}}"
        $.ajax({
            url:'{{url("MostOffers")}}',
            data:{'month':month , 'kitchen':kitchen ,'language':language},
            type:"get",
            success: function (data) {
                var tablss='' ;
                data.sort(function (a, b) {
                    return b.qty - a.qty;
                });
                
                $.each(data, function(key, value) {
                    tablss +=`
                        <div class="d-flex flex-row comment-row">
                            <div class="comment-text w-100">
                                <h4 class="pull-right">`+value.qty+`</h4>
                                <h5 >`+value.title+`</h5>
                            </div>
                        </div>
                    `;
                })
                $('#Div_MostOffers').html(tablss);
            }
        })
    })      

    $(".choose_kitchen_MostOffers").on('change', function() {
        var kitchen=$(this).val();
        var month=$( ".choose_month_MostOffers option:selected" ).val();
        var language = "{{Request::segment(1)}}"
        $.ajax({
            url:'{{url("MostOffers")}}',
            data:{'month':month , 'kitchen':kitchen ,'language':language},
            type:"get",
            success: function (data) {
                var tablss='' ;
                data.sort(function (a, b) {
                    return b.qty - a.qty;
                });
                $.each(data, function(key, value) {
                    tablss +=`
                        <div class="d-flex flex-row comment-row">
                            <div class="comment-text w-100">
                                    <h4 class="pull-right">`+value.qty+`</h4>
                                <h5 >`+value.title+`</h5>
                            </div>
                        </div>
                    `;
                })
                $('#Div_MostOffers').html(tablss);
            }
        })
    })
////////////////////////////////////////////////////////////////////////

    // Kitchen issues
    $(".choose_kitchen_issues").on('change', function() {
        var kitchen=$(this).val();
        var language = "{{Request::segment(1)}}"
        $.ajax({
            url:'{{url("ReviewsTable")}}',
            data:{'kitchen':kitchen ,'language':language},
            type:"get",
            success: function (data) {
                var divs='';
                $.each(data, function(key, value) {
                    divs +=`
                    <div class="d-flex flex-row comment-row">
                        <div class="p-2"><span class="round"><img src="{{asset('cpanel/upload/user')}}/`+value.image+`" alt="user" width="50"></span></div>
                        <div class="comment-text w-100">
                            <h5>`+value.name+`</h5>
                            <p class="m-b-5">`+value.review+`</p>
                            <div class="comment-footer">
                                <span class="text-muted pull-right">`+value.created+`</span>
                            </div>
                        </div>
                    </div>
                    `;
                })
                $('#div_reviews').html(divs)
            }
        })
    })

    // Morris donut chart
    @inject('Active','App\Order')
    @inject('InActive','App\Order')
    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Active Users",
            value: {{$Active->getActiveUser()}},

        }, {
            label: "InActive User",
            value: {{$Active->getInActiveUser()}}
        }],
        resize: true,
        colors:['#009efb', '#2f3d4a']
    });
    </script>
    
@endsection