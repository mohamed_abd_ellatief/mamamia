@extends('layouts.cpanel_layout.cpanel_layout')
@section('title')
All Vendors
@endsection
@section('moduleoptions')
  <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
    {{-- <li class="nav-item active">
      <a href="{{ route('vendors.create') }}" class="nav-link">
        <i class="mdi mdi-plus"></i>Add New Service
      </a>
      </li> --}}
      {{-- <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
        </li> --}}
      </ul>
    @endsection
@section('content')

  @if (\Session::has('success'))
    <div class="col-12">
    <div class="row purchace-popup alert alert-success">
        <strong>Success!</strong>{{ session()->get('success') }}

          <i class="mdi mdi-close popup-dismiss"></i>
      </div>
    </div>
  @endif




      <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Vendors Table</h4>
              <div class="table-responsive">
                <table class="table table-striped" id="example">
                  <thead>
                      <tr>
                        <th>User</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobilenumber</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>

                    @foreach ($allvendors as $onevendor)

                      <tr>
                        @if($onevendor->image)
                        <td class="py-1"><img src="{{asset('cpanel/dist/img/user/'.$onevendor->image)}}" alt="image" /></td>
                        @else
                          <td class="py-1"><img src="{{asset('cpanel/dist/img/user/no_user_logo.jpeg')}}" alt="image" /></td>
                        @endif
                        <td class="py-1">{{ $onevendor->name }}</td>
                        <td>{{ $onevendor->email }}</td>
                        <td>{{ $onevendor->mobilenumber }}</td>
                        <td>
                                <label class="badge badge-danger">Pending</label>
                                <label class="badge badge-success">Active</label>
                            
                          </td>
                        <td>
                                <a type="button" class="btn btn-icons btn-rounded btn-primary" href="{{ route('vendors.edit',$onevendor->id) }}" style="color:#fff">
                                  <i class="mdi mdi-update"></i>
                                </a>
                                  @if($onevendor->status == 0)
                              <a type="button" class="activate btn btn-icons btn-rounded btn-success" data-id="{{$onevendor->id}}" style="color:#fff">
                                      <i class="mdi mdi-checkbox-marked-circle-outline"></i>
                                    </a>
                                  @elseif($onevendor->status == 1)
                                    <a type="button" class="disactivate btn btn-icons btn-rounded btn-danger" data-id="{{$onevendor->id}}" style="color:#fff">
                                      <i class="mdi mdi-delete-empty"></i>
                                    </a>
                                  @endif

                              {{-- <form method="post" action="{{ route('vendors.destroy',$onevendor->id) }}" style="display:inline">
                                {{ csrf_field () }}
                                {{method_field('Delete')}}
                                <button class="btn btn-icons btn-rounded btn-danger"  href=""style="color:#fff">
                                   <i class="mdi mdi-delete-empty"></i>
                                </button>
                              </form> --}}

                                  <a type="button" class="btn btn-icons btn-rounded btn-info"  href="{{ route('vendors.show',$onevendor->id) }}" style="color:#fff">
                                    <i class="mdi mdi-information-variant"></i>
                                  </a>
                                </td>
                              </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
    


  $(".disactivate").click(function() {
    var id = $(this).data("id")
    alert(id)
      $.ajax({
      url: "{{url('activatevendor')}}",
      data:{"id":id},
      type: "GET"
    }).done(function() {
      $( this ).addClass( "done" );
    });
  });
  </script>
@endsection
