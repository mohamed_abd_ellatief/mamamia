@extends('layouts.cpanel_layout.cpanel_layout')
@section('title')
{{ $usertoshow->name  }} Data
@endsection
@section('moduleoptions')
  <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
    <li class="nav-item active">
      <a href="{{ route('vendors.edit',$usertoshow->id) }}" class="nav-link">
        <i class="mdi mdi-update"></i>Edite This Vendor
      </a>
      </li>
  @if($usertoshow->status == 0)
    <li class="nav-item ">
      <a href="{{ route('vendors.activate',$usertoshow->id) }}" class="nav-link">
        <i class="mdi mdi-checkbox-marked-circle-outline"></i>Activate This Vendor
      </a>
      </li>
  @else
    <li class="nav-item">
      <a href="{{ route('vendors.disactivate',$usertoshow->id) }}" class="nav-link">
        <i class="mdi mdi-delete-empty"></i>Disctivate This Vendor
      </a>
      </li>
  @endif
  <li class="nav-item active">
    <a href="{{ route('vendors.index') }}" class="nav-link">
      <i class="mdi mdi-arrow-left"></i>Back To list All Vendors
    </a>
    </li>
      {{-- <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
        </li> --}}
      </ul>
    @endsection
@section('content')
  @if (\Session::has('success'))
    <div class="col-12">
    <div class="row purchace-popup alert alert-success">
        <strong>Success!</strong>{{ session()->get('success') }}

          <i class="mdi mdi-close popup-dismiss"></i>
      </div>
    </div>
  @endif
      <div class="row">
                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <h4 class="card-title">Name</h4>
                        <div class="media">
                          {{-- <i class="mdi mdi-earth icon-md text-info d-flex align-self-start mr-3"></i> --}}
                          <div class="media-body">
                            <p class="card-text">{{ $usertoshow->name }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <h4 class="card-title">Email</h4>
                        <div class="media">
                          {{-- <i class="mdi mdi-earth icon-md text-info d-flex align-self-center mr-3"></i> --}}
                          <div class="media-body">
                            <p class="card-text">{{ $usertoshow->email }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <h4 class="card-title">Mobilenumber</h4>
                        <div class="media">
                          {{-- <i class="mdi mdi-earth icon-md text-info d-flex align-self-end mr-3"></i> --}}
                          <div class="media-body">
                            <p class="card-text">  {{ $usertoshow->mobilenumber }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>


                <div class="row">
                  <div class="col-12 grid-margin">
                    <div class="card">
                      <div class="card-body">
                        <h5 class="card-title mb-4">Manage Kitchens</h5>
                        <div class="fluid-container">
                          @foreach ($usertoshow->kitchen as  $onekitchen)
                            <div class="row ticket-card mt-3 pb-2 border-bottom pb-3 mb-3">
                            {{-- <div class="col-md-1">
                              <img class="img-sm rounded-circle mb-4 mb-md-0" src="images/faces/face1.jpg" alt="kitchen image">
                            </div> --}}
                            <div class="ticket-details col-md-9">
                              <div class="d-flex">
                                <p class="text-dark font-weight-semibold mr-2 mb-0 no-wrap">{{ $onekitchen->en_title }} :</p>
                                <p class="mb-0 ellipsis">{{ $onekitchen->en_description }}.</p>
                              </div>
                            </div>
                            <div class="ticket-actions col-md-2">
                              <div class="btn-group dropdown">
                                <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Manage
                                </button>
                                <div class="dropdown-menu">
                                  <a class="dropdown-item" href="{{ route('kitchens.edit',$onekitchen->id) }}">
                                    <i class="fa fa-reply fa-fw"></i>Edit Kitchen Data
                                  </a>
                                  <a class="dropdown-item" href="{{ route('kitchens.show',$onekitchen->id) }}">
                                    <i class="fa fa-reply fa-fw"></i>View Kitchen Page
                                  </a>
                                  @if($onekitchen->status == 0)
                                    <a class="dropdown-item" href="{{ route('kitchens.activate',$onekitchen->id) }}">
                                      <i class="fa fa-reply fa-fw"></i>Activate This Kitchen
                                    </a>
                                  @else
                                    <a class="dropdown-item" href="{{ route('kitchens.disactivate',$onekitchen->id) }}">
                                      <i class="fa fa-reply fa-fw"></i>Disactivate This Kitchen
                                    </a>

                                  @endif

                                </div>
                              </div>
                            </div>
                          </div>
                          @endforeach

                        </div>
                      </div>
                    </div>
                  </div>
                </div>


@endsection
