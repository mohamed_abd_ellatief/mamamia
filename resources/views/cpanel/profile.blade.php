@extends('layouts.cpanel_layout.master')
@section('title')
    {{trans('word.Profile')}}
@endsection

@section('css')
<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{asset('cpanel/assets/plugins/dropify/dist/css/dropify.min.css')}}">
@endsection
@section('content')
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">{{trans('word.Profile')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                            <li class="breadcrumb-item active">{{trans('word.Profile')}}</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> 
                                    <img src="{{asset('cpanel/upload/user/'.Auth::user()->image)}}" class="img-circle" width="150" />
                                </center>
                            </div>
                            <div>
                                <hr> </div>
                            <div class="card-block"> 
                                <small class="text-muted">{{trans('word.Email')}}</small>
                                <h6>{{Auth::user()->email}}</h6> 
                                <small class="text-muted p-t-30 db">{{trans('word.Mobile')}}</small>
                                <h6>{{Auth::user()->mobilenumber}}</h6>
                                <br/>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#settings" role="tab">{{trans('word.Setting')}}</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                
                                <div class="tab-pane active" id="settings" role="tabpanel">
                                    <div class="card-block">
                                        <form class="form-horizontal form-material" method="POST" action={{url('UpdateProfile')}} enctype="multipart/form-data">
                                            @csrf
                                            <div class="form-group">
                                                <label class="col-md-12">{{trans('word.Name')}}</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="name" value="{{Auth::user()->name}}" placeholder="Full Name" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">{{trans('word.Email')}}</label>
                                                <div class="col-md-12">
                                                    <input type="email" name="email" value="{{Auth::user()->email}}" placeholder="johnathan@admin.com" class="form-control form-control-line" name="example-email" id="example-email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">{{trans('word.Mobile')}}</label>
                                                <div class="col-md-12">
                                                    <input type="text" name="mobilenumber" value="{{Auth::user()->mobilenumber}}" placeholder="123 456 7890" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="card">
                                                        <div class="card-block">
                                                            <label for="input-file-now-custom-2">You can set the Profile Image</label>
                                                            <input name="image" type="file" id="input-file-now-custom-2" class="dropify" data-height="200" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success">{{trans('word.Update Profile')}}</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            @endsection
            @section('js')
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>

            <script>
                $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
                var Success =function(){
                    swal("Updated Profile!", "Your Profile has been updated.", "success"); 
                };
                var Failed =function(){
                    swal("Sorry", "Your imaginary file is safe :)", "error");
                };
            </script>
            <?php
            $message=session()->get("message");
            ?>
            @if( session()->has("message"))
              @if( $message == "Success")
                <script>
                $(Success).click();
                </script>
              @elseif ( $message == "Failed")
                <script>
                  $(Failed).click();
                </script>
              @endif
            @endif
            @endsection