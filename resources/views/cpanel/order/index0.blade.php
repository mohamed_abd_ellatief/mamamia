@extends('layouts.cpanel_layout.master')
    @section('title')
    @if( LaravelLocalization::getCurrentLocale() == "ar")
    {{$ar_title}}
    @else
    {{$en_title}}
    @endif
    @endsection

@section('css')
<!--alerts CSS -->

<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
        <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">
                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                            {{$ar_title}}
                            @else
                            {{$en_title}}
                            @endif
                        </h3>
                </div>
            </div>

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">
                            @if( LaravelLocalization::getCurrentLocale() == "ar")
الطلبات                             @else
orders
                            @endif
                    </h4>
                    <h6 class="card-subtitle">{{trans('word.Restaurant Orders')}}</h6>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('word.NoOrder')}} </th>
                                    <th>{{trans('word.Order Name')}} </th>
                                    <th>@if(Request::segment(1) == 'ar') اسم المطعم  @else  Kitchen  @endif</th>
                                    <th>{{trans('word.Customer Location')}} </th>
                                    <th>{{trans('word.Price')}} </th>
                                    @if($en_title != "Invoice Orders")
                                    <th>{{trans('word.Status')}} </th>
                                    @endif
                                    <th>{{trans('word.Action')}} </th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($Orders as $Order)
                              @inject('kitchen','App\Kitchen')
                              @inject('meal_name','App\Meal')
                                <tr id="{{$Order->id}}">
                                    <td>{{$Order->id}}</td>
                                    <td>
                                        @if($meal_name->mealname($Order->id))
                                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                {{$meal_name->mealname($Order->id)->ar_title}}
                                            @else
                                                {{$meal_name->mealname($Order->id)->en_title}}
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                      @if($kitchen->find($Order->kitchen_id))
                                      @if(Request::segment(1) == 'ar')
                                      {{$kitchen->find($Order->kitchen_id)->ar_title}}
                                      @else
                                      {{$kitchen->find($Order->kitchen_id)->en_title}}

                                      @endif
                                      @else
                                      تم حذف المطعم
                                      @endif
                                    </td>
                                    <td>{{str_limit($Order->userplace,40)}}</td>
                                    <td>{{$Order->totalprice}} SR</td>
                                    @if($en_title != "Invoice Orders")
                                    @if($Order->status == 1)
                                    <td><h4><span class="badge badge-pill badge-success">{{trans('word.accepted')}}</span></h4></td>
                                    @elseif($Order->status == -1)
                                    <td><h4><span class="badge badge-pill badge-danger">{{trans('word.rejected')}}</span></h4></td>
                                    @elseif($Order->status == 0)
                                    <td><h4><span class="badge badge-pill badge-info">{{trans('word.new')}}</span></h4></td>
                                    @elseif($Order->status == -2)
                                    <td><h4><span class="badge badge-pill badge-danger">@if(Request::segment(1) == 'ar') ملغاء @else Cancelled @endif</span></h4></td>
                                    @endif
                                    @endif
                                    <td>
                                        @if($Order->status == 1 || $Order->status == -1)
                                        <a data-toggle="modal" data-target="#a"  data-order_id="{{ $Order->codeorder }}" data-order_address="{{ $Order->userplace }}" data-ordertime="{{ $Order->created_at }}" data-needTime="{{ $Order->neededtime }}" data-deliverytotalprice="{{ $Order->deliverytotalprice }}" data-totalprice="{{ $Order->totalprice }}" 
                   data-distancetouser="{{ $Order->distancetouser }}"
                   data-priceVat="{{ $Order->priceVat }}"
                   data-kitchen="{{ $Order->kitchen->ar_title }}" 
                   data-name="{{ $Order->user->name }}" 
                   data-mobilenumber="{{ $Order->user->mobilenumber }}" 
                    data-id="{{$Order->id}}" class="btn btn-sm btn-rounded btn-info text-white edit detailsButton"  href="#" data-toggle="tooltip" data-original-title="view">{{trans('word.view')}}</a>
                                        @else
                                        @if(Auth::user()->category == 'vendor')
                                        <a class="btn btn-sm btn-rounded btn-success text-white acceptOrder" data-id="{{$Order->id}}">{{trans('word.accept')}}</a>
                                        <a class="btn btn-sm btn-rounded btn-danger text-white rejectOrder" data-id="{{$Order->id}}">{{trans('word.reject')}}</a>
                                        @endif
     <a data-toggle="modal" data-target="#a"  data-order_id="{{ $Order->codeorder }}" data-order_address="{{ $Order->userplace }}" data-ordertime="{{ $Order->created_at }}" data-needTime="{{ $Order->neededtime }}" data-deliverytotalprice="{{ $Order->deliverytotalprice }}" data-totalprice="{{ $Order->totalprice }}" 
                   data-distancetouser="{{ $Order->distancetouser }}"
                   data-priceVat="{{ $Order->priceVat }}"
                   data-kitchen="{{ $Order->kitchen->ar_title }}" 
                   data-name="{{ $Order->user->name }}" 
                   data-mobilenumber="{{ $Order->user->mobilenumber }}" 
                    data-id="{{$Order->id}}" class="btn btn-sm btn-rounded btn-info text-white edit detailsButton"  href="#" data-toggle="tooltip" data-original-title="view">{{trans('word.view')}}</a>
                                        @endif
                                    </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
{{ $Orders->links()}}
<!-- Edit modal content -->
<div class="modal fade bs-edit-modal-lg" tabindex="-1" id="a" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg ">
    <div class="modal-content" style="width: 800px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">تفاصيل الطلب</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body row" id="order_det" style="direction: rtl;">

        <div class="col-lg-6">
        <h3>رقم الطلب </h3><div style="display: inline;margin-bottom: 10px;" id="order_id"></div><hr>
      </div>
      <div class="col-lg-6">
        <h3>اسم المطعم </h3><div style="display: inline;margin-bottom: 10px;" id="kitchen"></div><hr>
      </div>
<div class="col-lg-6">
        <h3>اسم العميل </h3><div style="display: inline;margin-bottom: 10px;" id="name"></div><hr>
      </div>
<div class="col-lg-6">
        <h3>رقم العميل </h3><div style="display: inline;margin-bottom: 10px;" id="mobilenumber"></div><hr>
      </div>
<div class="col-lg-6">
        <h3>عنوان  الطلب  </h3><div style="display: block;" id="order_address"></div><hr>
      </div>
<div class="col-lg-6">
        <h3>وقت  الطلب </h3><div style="display: block;" id="ordertime"></div><hr>
      </div>
<div class="col-lg-6">
        <h3>الوقت اللازم للتحضير</h3><div style="display: block;" id="needTime"></div><hr>
      </div>
<div class="col-lg-6">
        <h3>الدليفيري</h3><div style="display: block;" id="deliverytotalprice"></div><hr>
      </div>
<div class="col-lg-6">
         <h3> القيمة المضافة</h3><div style="display: block;" id="priceVat"></div><hr>
</div>
<div class="col-lg-6">
        <h3>اجمالي سعر الطلب</h3><div style="display: block;" id="totalprice"></div><hr>
      </div>
<div class="col-lg-6">
        <h3> المسافة </h3><div style="display: block;" id="distancetouser"></div><hr>
      </div>
    {{--     <table class="table table-bordered">
          <tr>
            
            <span>رقم الطلب   </span>
           <td id="order_id"></td>
          </tr>
          <tr>
            <span>عنوان الطلب </span>
                        <td id="order_address"></td>

          </tr>
          
         
        </table> --}}
      </div>
             <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-danger" onclick='printJS({
    printable: "order_det",
    type: "html",
    style: ".heavy {font-weight: 800;}"
  });'>طباعه</button>
      </div>
        </div>

        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection

@section('js')

<!-- This is data table -->
<script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
<script type="text/javascript" src="https://printjs-4de6.kxcdn.com/print.min.css"></script>
    <script>
    $(".detailsButton").on('click', function () {
    $("#order_id").html($(this).data("order_id"));
    $("#order_address").html($(this).data("order_address"));
    $("#ordertime").html($(this).data("ordertime"));
    $("#needTime").html($(this).data("needTime"));
    $("#deliverytotalprice").html($(this).data("deliverytotalprice"));
    $("#totalprice").html($(this).data("totalprice"));
    $("#distancetouser").html($(this).data("distancetouser") +" "+ "كيلو متر");
    $("#priceVat").html($(this).data("priceVat"));
    $("#kitchen").html($(this).data("kitchen"));
    $("#name").html($(this).data("name"));
    $("#mobilenumber").html($(this).data("mobilenumber"));
  });
    $('#').DataTable({
        dom: 'Bfrtip',
        buttons: [
                { extend: 'copy', text: '{{trans('word.copy')}}' },
                { extend: 'excel', text: '{{trans('word.excel')}}' },
                { extend: 'pdf', text: '{{trans('word.pdf')}}' },
                { extend: 'print', text: '{{trans('word.print')}}' },
            ],
        @if( LaravelLocalization::getCurrentLocale() == "ar")
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
            }
        @endif

    });

     $("#print").click(function(){

        var doc = new jsPDF()
 doc.text(["إذا لم تستح فاصنع ما شئت", "إذا لم تستح", "فاصنع ما شئت"], 200, 10, {lang: 'ar', align: 'right'});
 // doc.fromHTML($('#order_det').html(), 15, 15, {
 //        'width': 170,
 //            'elementHandlers': specialElementHandlers
 //    });
doc.save('a4.pdf')
        });

    $(".edit").click(function(){
      var id=$(this).data('id')
    //   var CSRF_TOKEN = {{ csrf_field() }};
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
        type: "POST",
        url: "{{url(LaravelLocalization::getCurrentLocale().'/OrderDetails')}}",
        data: {"id":id,_token:CSRF_TOKEN},
        success: function (data) {
            $(".bs-edit-modal-lg .modal-body").html(data)
            $(".bs-edit-modal-lg").modal('show')
            $(".bs-edit-modal-lg").on('hidden.bs.modal',function (e){
            //   $('.bs-edit-modal-lg').empty();
                $('.bs-edit-modal-lg').hide();
            })
        }
      })
    })



    $(".acceptOrder").click(function(){
      var id=$(this).data('id')
      var tr=$(this).parent().parent()
      console.log(tr[0])
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      swal({
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                title: "هل انت متأكد من قبول الطلب ؟",
            @else
                title: "Are you sure accept Order?",
            @endif
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                confirmButtonText: "نعم, انا متأكد!",
                cancelButtonText: "لأ, من فضلك",
            @else
                confirmButtonText: "Yes, Sure it!",
                cancelButtonText: "No, cancel please!",
            @endif
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                url:'{{url("acceptOrder")}}',
                type:"post",
                data:{'id':id,_token: CSRF_TOKEN},
                dataType:"JSON",
                success: function (data) {
                    if(data.message == "accepted")
                    {
                        $(tr[0]).hide()
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("طلب مقبول!", "تم قبول الطلب.", "success");
                        @else
                        swal("accepted!", "Order has been accepted.", "success");
                        @endif
                    }else{
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("خطأ!", "فشل العملية", "error");
                        @else
                        swal("ًWrong!", "The Operation Failed", "error");
                        @endif
                    }
                },
                fail: function(xhrerrorThrown){
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                    swal("خطأ!", "فشل العملية", "error");
                    @else
                    swal("ًWrong!", "The Operation Failed", "error");
                    @endif
                }

            });
            } else {
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                swal("ألغاء", "تم ألغاء العملية", "error");
                @else
                swal("Cancelled", "Order has been Cancelled", "error");
                @endif
            }
        });
    })



    $(".rejectOrder").click(function(){
      var id=$(this).data('id')
      var tr=$(this).parent().parent()
      console.log(tr[0])
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      swal({
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                title: "هل انت متأكد من رفض الطلب ؟",
            @else
                title: "Are you sure reject Order?",
            @endif
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                confirmButtonText: "نعم, انا متأكد!",
                cancelButtonText: "لأ, من فضلك",
            @else
                confirmButtonText: "Yes, Sure it!",
                cancelButtonText: "No, cancel please!",
            @endif
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                url:'{{url("rejectOrder")}}',
                type:"post",
                data:{'id':id,_token: CSRF_TOKEN},
                dataType:"JSON",
                success: function (data) {
                    if(data.message == "reject")
                    {
                        $(tr[0]).hide()
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("طلب مرفوض!", "تم رفض الطلب.", "success");
                        @else
                        swal("reject!", "Order has been rejected.", "success");
                        @endif
                    }else{
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("خطأ!", "فشل العملية", "error");
                        @else
                        swal("ًWrong!", "The Operation Failed", "error");
                        @endif
                    }
                },
                fail: function(xhrerrorThrown){
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                    swal("خطأ!", "فشل العملية", "error");
                    @else
                    swal("ًWrong!", "The Operation Failed", "error");
                    @endif
                }

            });
            } else {
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                swal("ألغاء", "تم ألغاء العملية", "error");
                @else
                swal("Cancelled", "Order has been Cancelled", "error");
                @endif
            }
        });
    })

</script>
<?php
$message=session()->get("message");
?>
@if( session()->has("message"))
  @if( $message == "accepted")
    <script>
        @if( LaravelLocalization::getCurrentLocale() == "ar")
            swal("طلب مقبول!", "تم قبول الطلب.", "success");
        @else
            swal("accepted!", "Order has been accepted.", "success");
        @endif
    </script>
    @elseif( $message == "reject")
    <script>
        @if( LaravelLocalization::getCurrentLocale() == "ar")
        swal("طلب مرفوض!", "تم رفض الطلب.", "success");
        @else
        swal("reject!", "Order has been rejected.", "success");
        @endif
    </script>
  @elseif ( $message == "Failed")
    <script>
        @if( LaravelLocalization::getCurrentLocale() == "ar")
        swal("اسف", "فشلت العملية :(", "error");
        @else
        swal("Sorry", "Oops, the operation failed :(", "error");
        @endif
    </script>
  @endif
@endif
<script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>


@endsection
