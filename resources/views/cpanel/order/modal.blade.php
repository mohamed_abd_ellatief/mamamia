<div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-block">
                    <form class="form-horizontal" role="form" action="{{url('acceptOrder')}}" method="POST">
                            @csrf
                        <div class="form-body">
                            <div class="row">
                                <input type="hidden" name="id" value="{{$Order->id}}"/>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <h3 class="card-title">{{trans('word.Component')}}</h3>
                                        <div class="col-md-12">
                                            <table class="table color-table inverse-table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>{{trans('word.Title')}}</th>
                                                        <th>{{trans('word.Quantity')}} × {{trans('word.Price')}}</th>
                                                        <th>{{trans('word.total price')}}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @inject('Meals','App\Meal')

                                                    <?php
                                                    $count=1;
                                                    $totalorder=[];
                                                    ?>
                                                    @foreach($Meals->meals($Order->id) as $Meall)
                                                    <tr>
                                                        <td>{{$count}}</td>
                                                        <td>
                                                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                            {{$Meall->ar_title}}
                                                            @else
                                                            {{$Meall->en_title}}
                                                            @endif
                                                        </td>
                                                        <td>{{$Meall->amount}} × {{\App\Meal::find($Meall->id)->price}}</td>
                                                        <td>{{$Meall->amount* \App\Meal::find($Meall->id)->price}}</td>
                                                        <?php
                                                        $totalorder[]=$Meall->amount* \App\Meal::find($Meall->id)->price;
                                                        ?>
                                                    </tr>
                                                    <?php $count++ ?>
                                                    @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr class="text-center"><td colspan="4">{{trans('word.total Order')}}: {{array_sum($totalorder)}}{{trans('word.SR')}}</td></tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label  col-md-5 text-bold text-info">{{trans('word.Order no')}}</label>
                                        <div class="col-md-7">
                                            <p class="form-control-static"> {{$Order->id}} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Status')}}</label>
                                        <div class="col-md-7">
                                 @if($Order->status == 1)
                                    <p><span class="badge badge-pill badge-success">{{trans('word.accepted')}}</span></p>
                                    @elseif($Order->status == -1)
                                   <p><span class="badge badge-pill badge-danger">{{trans('word.rejected')}}</span></p>
                                    @elseif($Order->status == 0)
                                   <p><span class="badge badge-pill badge-info">{{trans('word.new')}}</span></p>
                                    @elseif($Order->status == -2)
                                    <p><span class="badge badge-pill badge-danger">@if(Request::segment(1) == 'ar') ملغاء @else Cancelled @endif</span></p>
                                    @endif
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Customer Name')}} </label>
                                        @if(\App\User::find($Order->user_id))
                                            <p class="form-control-static col-md-7"> {{\App\User::find($Order->user_id)->name}}</p>
                                        @else
                                            <p class="form-control-static col-md-7">
                                                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                    تم حذف بيانات العميل
                                                    @else
                                                    Customer has been Deleted
                                                    @endif
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Mobile')}} </label>
                                        @if(\App\User::find($Order->user_id))
                                            <p class="form-control-static col-md-7"> {{\App\User::find($Order->user_id)->mobilenumber}}</p>
                                        @else
                                            <p class="form-control-static col-md-7">تم حذف بيانات العميل</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Order Date')}} </label>
                                        <p class="form-control-static col-md-7"> {{$Order->created_at->toDateString()}}</p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Order Time')}} </label>
                                        <p class="form-control-static col-md-7"> {{$Order->created_at->format("h:i A")}}</p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Location')}}: </label>
                                            <p class="form-control-static text-center col-md-12"> {{$Order->userplace}}</p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Preparation')}}: </label>
                                        <p class="form-control-static text-center col-md-12">
                                                {{intdiv($Meals->Preparation($Order->id), 60).':'. ($Meals->Preparation($Order->id) % 60)}}
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Kitchen Name')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                @inject('Kitchen_Name','App\Kitchen')
                                                @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                {{$Kitchen_Name->getName($Order->kitchen_id)->ar_title}}
                                                @else
                                                {{$Kitchen_Name->getName($Order->kitchen_id)->en_title}}
                                                @endif
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.distance to user')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->distancetouser}} {{trans('word.km')}}
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.delivery total price')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->deliverytotalprice}}
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Vat')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->priceVat}}
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Discount Price')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->discountpoint}} {{trans('word.SR')}}
                                            </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Discount Points')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->usepoint}}
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.total price')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->totalprice}}
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.User Note')}}:    </label>
                                        @foreach($Meals->meals($Order->id) as $Meall)
                                            <p class="form-control-static text-center col-md-12">
                                                @if($Meall->note != "none")
                                                    {{$Meall->note}}
                                                @endif
                                            </p>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Vendor Note')}}:</label>
                                        @if($Order->status == 1 || $Order->status == -1)
                                        <p class="form-control-static text-center col-md-12">
                                            @if($Order->vendornote != null)
                                            {{$Order->vendornote}}
                                            @else
                                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                                            لا يوجد تعليق من البائع
                                            @else
                                            There is no comment from the Vendor
                                            @endif
                                            @endif
                                        </p>
                                        @else
                                        <textarea rows="6" class="form-control" name="vendornote"></textarea>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            @if($Order->status == 0)
                                            <button type="submit" class="btn btn-success" name="status" value="1"> <i class="fa fa-check"></i> {{trans('word.accept')}}</button>
                                            <button type="submit" class="btn btn-danger" name="status" value="-1"> <i class="fa fa-close"></i> {{trans('word.reject')}}</button>
                                            @endif
                                            <button type="button" class="btn btn-inverse " data-dismiss="modal">{{trans('word.Cancel')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
