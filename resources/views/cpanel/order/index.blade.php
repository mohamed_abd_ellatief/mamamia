@extends('layouts.cpanel_layout.master')
    @section('title')
    @if( LaravelLocalization::getCurrentLocale() == "ar")
    {{$ar_title}}
    @else
    {{$en_title}}
    @endif
    @endsection

@section('css')
<!--alerts CSS -->

<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
        <div class="row page-titles">
                <div class="col-md-6 col-8 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0">
                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                            {{$ar_title}}
                            @else
                            {{$en_title}}
                            @endif
                        </h3>
                </div>
            </div>

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">
                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                            {{$Restaurant->ar_title}}
                            @else
                            {{$Restaurant->en_title}}
                            @endif
                    </h4>
                    <h6 class="card-subtitle">{{trans('word.Restaurant Orders')}}</h6>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>{{trans('word.NoOrder')}} </th>
                                    <th>{{trans('word.Order Name')}} </th>
                                    <th>{{trans('word.Customer Location')}} </th>
                                    <th>{{trans('word.Price')}} </th>
                                    @if($en_title != "Invoice Orders")
                                    <th>{{trans('word.Status')}} </th>
                                    @endif
                                    <th>{{trans('word.Action')}} </th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($Orders as $Order)
                              @inject('meal_name','App\Meal')
                                <tr id="{{$Order->id}}">
                                    <td>{{$Order->id}}</td>
                                    <td>
                                        @if($meal_name->mealname($Order->id))
                                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                {{$meal_name->mealname($Order->id)->ar_title}}
                                            @else
                                                {{$meal_name->mealname($Order->id)->en_title}}
                                            @endif
                                        @endif
                                    </td>
                                    <td>{{str_limit($Order->userplace,40)}}</td>
                                    <td>{{$Order->totalprice}} SR</td>
                                    @if($en_title != "Invoice Orders")
                                    @if($Order->status == 1)
                                    <td><h4><span class="badge badge-pill badge-success">{{trans('word.accepted')}}</span></h4></td>
                                    @elseif($Order->status == -1)
                                    <td><h4><span class="badge badge-pill badge-danger">{{trans('word.rejected')}}</span></h4></td>
                                    @else
                                    <td><h4><span class="badge badge-pill badge-info">{{trans('word.new')}}</span></h4></td>
                                    @endif
                                    @endif
                                    <td>
                                        @if($Order->status == 1 || $Order->status == -1)
                                        <a data-toggle="modal" data-target=".bs-Edit-modal-lg" data-id="{{$Order->id}}" class="btn btn-sm btn-rounded btn-info text-white edit" href="#" data-toggle="tooltip" data-original-title="view">{{trans('word.view')}}</a>
                                        @else
                                        @if(Auth::user()->category == 'vendor')
										@if( $Order->status == 0 && $Order->status == 2)
                                        <a class="btn btn-sm btn-rounded btn-success text-white acceptOrder2" data-id="{{$Order->id}}">{{trans('word.accept')}}</a>
                                        <a class="btn btn-sm btn-rounded btn-danger text-white rejectOrder2" data-id="{{$Order->id}}">{{trans('word.reject')}}</a>
                                        @endif
										 @endif
                                        <a data-toggle="modal" data-target=".bs-Edit-modal-lg" data-id="{{$Order->id}}" class="btn btn-sm btn-rounded btn-info text-white edit" href="#" data-toggle="tooltip" data-original-title="view">{{trans('word.view')}}</a>
                                        @endif
                                    </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

<!-- Edit modal content -->
<div class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h4 class="modal-title m-b-0 text-white" id="myLargeModalLabel">
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                        {{$ar_lable}}
                        @else
                        {{$en_lable}}
                    @endif
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection

@section('js')

<!-- This is data table -->
<script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <script>

    $('#myTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
                { extend: 'copy', text: '{{trans('word.copy')}}' },
                { extend: 'excel', text: '{{trans('word.excel')}}' },
                { extend: 'pdf', text: '{{trans('word.pdf')}}' },
                { extend: 'print', text: '{{trans('word.print')}}' },
            ],
        @if( LaravelLocalization::getCurrentLocale() == "ar")
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
            }
        @endif

    });


    $(".edit").click(function(){
      var id=$(this).data('id')
    //   var CSRF_TOKEN = {{ csrf_field() }};
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
        type: "POST",
        url: "{{url(LaravelLocalization::getCurrentLocale().'/OrderDetails')}}",
        data: {"id":id,_token:CSRF_TOKEN},
        success: function (data) {
            $(".bs-edit-modal-lg .modal-body").html(data)
            $(".bs-edit-modal-lg").modal('show')
            $(".bs-edit-modal-lg").on('hidden.bs.modal',function (e){
            //   $('.bs-edit-modal-lg').empty();
                $('.bs-edit-modal-lg').hide();
            })
        }
      })
    })



    $(".acceptOrder").click(function(){
      var id=$(this).data('id')
      var tr=$(this).parent().parent()
      console.log(tr[0])
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      swal({
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                title: "هل انت متأكد من قبول الطلب ؟",
            @else
                title: "Are you sure accept Order?",
            @endif
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                confirmButtonText: "نعم, انا متأكد!",
                cancelButtonText: "لأ, من فضلك",
            @else
                confirmButtonText: "Yes, Sure it!",
                cancelButtonText: "No, cancel please!",
            @endif
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
				                console.log(12)

                $.ajax({
                url:'{{url("acceptOrder")}}',
                type:"post",
                data:{'id':id,_token: CSRF_TOKEN},
                dataType:"JSON",
                success: function (data) {
                    if(data.message == "accepted")
                    {                     console.log(3)

                        $(tr[0]).hide()
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("طلب مقبول!", "تم قبول الطلب.", "success");
                        @else
                        swal("accepted!", "Order has been accepted.", "success");
                        @endif
                    }else{
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("خطأ!", "فشل العملية", "error");
                        @else
                        swal("ًWrong!", "The Operation Failed", "error");
                        @endif
                    }
                },
                fail: function(xhrerrorThrown){
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                    swal("خطأ!", "فشل العملية", "error");
                    @else
                    swal("ًWrong!", "The Operation Failed", "error");
                    @endif
                }

            });
            } else {
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                swal("ألغاء", "تم ألغاء العملية", "error");
                @else
                swal("Cancelled", "Order has been Cancelled", "error");
                @endif
            }
        });
    })



    $(".rejectOrder").click(function(){
      var id=$(this).data('id')
      var tr=$(this).parent().parent()
      console.log(tr[0])
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      swal({
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                title: "هل انت متأكد من رفض الطلب ؟",
            @else
                title: "Are you sure reject Order?",
            @endif
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                confirmButtonText: "نعم, انا متأكد!",
                cancelButtonText: "لأ, من فضلك",
            @else
                confirmButtonText: "Yes, Sure it!",
                cancelButtonText: "No, cancel please!",
            @endif
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                url:'{{url("rejectOrder")}}',
                type:"post",
                data:{'id':id,_token: CSRF_TOKEN},
                dataType:"JSON",
                success: function (data) {
                    if(data.message == "reject")
                    {
                        $(tr[0]).hide()
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("طلب مرفوض!", "تم رفض الطلب.", "success");
                        @else
                        swal("reject!", "Order has been rejected.", "success");
                        @endif
                    }else{
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("خطأ!", "فشل العملية", "error");
                        @else
                        swal("ًWrong!", "The Operation Failed", "error");
                        @endif
                    }
                },
                fail: function(xhrerrorThrown){
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                    swal("خطأ!", "فشل العملية", "error");
                    @else
                    swal("ًWrong!", "The Operation Failed", "error");
                    @endif
                }

            });
            } else {
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                swal("ألغاء", "تم ألغاء العملية", "error");
                @else
                swal("Cancelled", "Order has been Cancelled", "error");
                @endif
            }
        });
    })

	
    $(".acceptOrder2").click(function(){
      var id=$(this).data('id')
      var tr=$(this).parent().parent()
      console.log(tr[0])
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      swal({
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                title: "هل انت متأكد من قبول الطلب ؟",
            @else
                title: "Are you sure accept Order?",
            @endif
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                confirmButtonText: "نعم, انا متأكد!",
                cancelButtonText: "لأ, من فضلك",
            @else
                confirmButtonText: "Yes, Sure it!",
                cancelButtonText: "No, cancel please!",
            @endif
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
				                console.log(12)

                $.ajax({
                url:'{{url("acceptOrder2")}}',
                type:"post",
                data:{'id':id,_token: CSRF_TOKEN},
                dataType:"JSON",
                success: function (data) {
                    if(data.message == "accepted")
                    {                     console.log(3)

                        $(tr[0]).hide()
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("طلب مقبول!", "تم قبول الطلب.", "success");
                        @else
                        swal("accepted!", "Order has been accepted.", "success");
                        @endif
                    }else{
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("خطأ!", "فشل العملية", "error");
                        @else
                        swal("ًWrong!", "The Operation Failed", "error");
                        @endif
                    }
                },
                fail: function(xhrerrorThrown){
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                    swal("خطأ!", "فشل العملية", "error");
                    @else
                    swal("ًWrong!", "The Operation Failed", "error");
                    @endif
                }

            });
            } else {
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                swal("ألغاء", "تم ألغاء العملية", "error");
                @else
                swal("Cancelled", "Order has been Cancelled", "error");
                @endif
            }
        });
    })



    $(".rejectOrder2").click(function(){
      var id=$(this).data('id')
      var tr=$(this).parent().parent()
      console.log(tr[0])
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      swal({
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                title: "هل انت متأكد من رفض الطلب ؟",
            @else
                title: "Are you sure reject Order?",
            @endif
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                confirmButtonText: "نعم, انا متأكد!",
                cancelButtonText: "لأ, من فضلك",
            @else
                confirmButtonText: "Yes, Sure it!",
                cancelButtonText: "No, cancel please!",
            @endif
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                url:'{{url("rejectOrder2")}}',
                type:"post",
                data:{'id':id,_token: CSRF_TOKEN},
                dataType:"JSON",
                success: function (data) {
                    if(data.message == "reject")
                    {
                        $(tr[0]).hide()
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("طلب مرفوض!", "تم رفض الطلب.", "success");
                        @else
                        swal("reject!", "Order has been rejected.", "success");
                        @endif
                    }else{
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("خطأ!", "فشل العملية", "error");
                        @else
                        swal("ًWrong!", "The Operation Failed", "error");
                        @endif
                    }
                },
                fail: function(xhrerrorThrown){
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                    swal("خطأ!", "فشل العملية", "error");
                    @else
                    swal("ًWrong!", "The Operation Failed", "error");
                    @endif
                }

            });
            } else {
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                swal("ألغاء", "تم ألغاء العملية", "error");
                @else
                swal("Cancelled", "Order has been Cancelled", "error");
                @endif
            }
        });
    })
	
</script>
<?php
$message=session()->get("message");
?>
@if( session()->has("message"))
  @if( $message == "accepted")
    <script>
        @if( LaravelLocalization::getCurrentLocale() == "ar")
            swal("طلب مقبول!", "تم قبول الطلب.", "success");
        @else
            swal("accepted!", "Order has been accepted.", "success");
        @endif
    </script>
    @elseif( $message == "reject")
    <script>
        @if( LaravelLocalization::getCurrentLocale() == "ar")
        swal("طلب مرفوض!", "تم رفض الطلب.", "success");
        @else
        swal("reject!", "Order has been rejected.", "success");
        @endif
    </script>
  @elseif ( $message == "Failed")
    <script>
        @if( LaravelLocalization::getCurrentLocale() == "ar")
        swal("اسف", "فشلت العملية :(", "error");
        @else
        swal("Sorry", "Oops, the operation failed :(", "error");
        @endif
    </script>
  @endif
@endif
<script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>


@endsection
