@extends('layouts.cpanel_layout.cpanel_layout')
@section('title')
Dashboard Main Page
@endsection
@section('moduleoptions')
  <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
    <li class="nav-item active">
      <a href="{{ route('services.create') }}" class="nav-link">
        <i class="mdi mdi-plus"></i>Add New Service
      </a>
      </li>
      <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
        </li>
      </ul>
    @endsection
@section('content')
      {{-- <div class="row purchace-popup">
        <div class="col-12">
          <span class="d-flex alifn-items-center">
            <p>Like what you see? Check out our premium version for more.</p>
            <a href="https://github.com/BootstrapDash/StarAdmin-Free-Bootstrap-Admin-Template" target="_blank" class="btn ml-auto download-button">Download Free Version</a>
            <a href="https://www.bootstrapdash.com/product/star-admin-pro/" target="_blank" class="btn purchase-button">Upgrade To Pro</a>
            <i class="mdi mdi-close popup-dismiss"></i>
          </span>
        </div>
      </div> --}}
      <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Services Table</h4>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                      <tr>
                        <th>Service Name</th>
                        <th>Email</th>
                        <th>Mobilenumber</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
{{--
                    @for ($i=0; $i < 10; $i++)

                    @endfor --}}
                    <tr>
                      <td class="py-1">
                        service name
                      </td>
                      <td>
                        email
                      </td>
                      <td>
                        01010100101010101
                      </td>
                      <td>
                        <label class="badge badge-danger">Pending</label>
                      </td>
                      <td>
                        <a type="button" class="btn btn-icons btn-rounded btn-primary" href="{{ route('services.edit',1) }}" style="color:#fff">
                          <i class="mdi mdi-update"></i></a>
                        <a type="button" class="btn btn-icons btn-rounded btn-success"  style="color:#fff">
                          <i class="mdi mdi-checkbox-marked-circle-outline"></i></a>
                        <a type="button" class="btn btn-icons btn-rounded btn-danger"  style="color:#fff">
                          <i class="mdi mdi-delete-empty"></i></a>
                        <a type="button" class="btn btn-icons btn-rounded btn-info"  href="{{ route('services.show',1) }}" style="color:#fff">
                          <i class="mdi mdi-information-variant"></i></a>
                      </td>
                    </tr>
                    <tr>
                      <td class="py-1">
                        service name
                      </td>
                      <td>
                        email
                      </td>
                      <td>
                        01010100101010101
                      </td>
                      <td>
                        <label class="badge badge-success">Activ</label>
                      </td>
                      <td>
                        <a type="button" class="btn btn-icons btn-rounded btn-primary"  style="color:#fff">
                          <i class="mdi mdi-update"></i></a>
                        {{-- <a type="button" class="btn btn-icons btn-rounded btn-success"  style="color:#fff">
                          <i class="mdi mdi-checkbox-marked-circle-outline"></i></a> --}}
                        <a type="button" class="btn btn-icons btn-rounded btn-danger"  style="color:#fff">
                          <i class="mdi mdi-delete-empty"></i></a>
                        <a type="button" class="btn btn-icons btn-rounded btn-info"  style="color:#fff">
                          <i class="mdi mdi-information-variant"></i></a>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
@endsection
