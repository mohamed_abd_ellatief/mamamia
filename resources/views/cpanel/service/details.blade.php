@extends('layouts.cpanel_layout.cpanel_layout')
@section('title')
Dashboard Main Page
@endsection
@section('moduleoptions')
  <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
    <li class="nav-item active">
      <a href="{{ route('services.create') }}" class="nav-link">
        <i class="mdi mdi-plus"></i>Add New Service
      </a>
      </li>
      <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
        </li>
      </ul>
    @endsection
@section('content')
      {{-- <div class="row purchace-popup">
        <div class="col-12">
          <span class="d-flex alifn-items-center">
            <p>Like what you see? Check out our premium version for more.</p>
            <a href="https://github.com/BootstrapDash/StarAdmin-Free-Bootstrap-Admin-Template" target="_blank" class="btn ml-auto download-button">Download Free Version</a>
            <a href="https://www.bootstrapdash.com/product/star-admin-pro/" target="_blank" class="btn purchase-button">Upgrade To Pro</a>
            <i class="mdi mdi-close popup-dismiss"></i>
          </span>
        </div>
      </div> --}}
      <div class="row">

        <div class="col-md-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Blockquotes</h4>
              <p class="card-description">
              </p>
              <blockquote class="blockquote">
                <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
              </blockquote>
            </div>
            <div class="card-body">
              <h4 class="card-title">Lead</h4>
              <p class="card-description">
              </p>
              <p class="lead">
                Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
              </p>
            </div>
          </div>
        </div>
        <div class="col-md-6 d-flex align-items-stretch">
          <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Paragraph</h4>
                  <p class="card-description">
                  </p>
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
                    text ever since the 1500s, when an unknown printer took a galley not only five centuries,
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Icon size</h4>
                  <p class="card-description">
                  </p>
                  <div class="row">
                    <div class="col-md-4 d-flex align-items-center">
                      <div class="d-flex flex-row align-items-center">
                        <i class="mdi mdi-compass icon-lg text-warning"></i>
                        <p class="mb-0 ml-1">
                          Icon-lg
                        </p>
                      </div>
                    </div>
                    <div class="col-md-4 d-flex align-items-center">
                      <div class="d-flex flex-row align-items-center">
                        <i class="mdi mdi-compass icon-md text-success"></i>
                        <p class="mb-0 ml-1">
                          Icon-md
                        </p>
                      </div>
                    </div>
                    <div class="col-md-4 d-flex align-items-center">
                      <div class="d-flex flex-row align-items-center">
                        <i class="mdi mdi-compass icon-sm text-danger"></i>
                        <p class="mb-0 ml-1">
                          Icon-sm
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 d-flex align-items-stretch">
                    <div class="row">
                      <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                          <div class="card-body">
                            <h4 class="card-title">Paragraph</h4>
                            <p class="card-description">
                            </p>
                            <p>
                              Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy
                              text ever since the 1500s, when an unknown printer took a galley not only five centuries,
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                          <div class="card-body">
                            <h4 class="card-title">Icon size</h4>
                            <p class="card-description">
                            </p>
                            <div class="row">
                              <div class="col-md-4 d-flex align-items-center">
                                <div class="d-flex flex-row align-items-center">
                                  <i class="mdi mdi-compass icon-lg text-warning"></i>
                                  <p class="mb-0 ml-1">
                                    Icon-lg
                                  </p>
                                </div>
                              </div>
                              <div class="col-md-4 d-flex align-items-center">
                                <div class="d-flex flex-row align-items-center">
                                  <i class="mdi mdi-compass icon-md text-success"></i>
                                  <p class="mb-0 ml-1">
                                    Icon-md
                                  </p>
                                </div>
                              </div>
                              <div class="col-md-4 d-flex align-items-center">
                                <div class="d-flex flex-row align-items-center">
                                  <i class="mdi mdi-compass icon-sm text-danger"></i>
                                  <p class="mb-0 ml-1">
                                    Icon-sm
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <h4 class="card-title">Top aligned media</h4>
                        <div class="media">
                          <i class="mdi mdi-earth icon-md text-info d-flex align-self-start mr-3"></i>
                          <div class="media-body">
                            <p class="card-text">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <h4 class="card-title">Center aligned media</h4>
                        <div class="media">
                          <i class="mdi mdi-earth icon-md text-info d-flex align-self-center mr-3"></i>
                          <div class="media-body">
                            <p class="card-text">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <h4 class="card-title">Bottom aligned media</h4>
                        <div class="media">
                          <i class="mdi mdi-earth icon-md text-info d-flex align-self-end mr-3"></i>
                          <div class="media-body">
                            <p class="card-text">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>



@endsection
