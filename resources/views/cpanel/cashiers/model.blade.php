<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">{{trans('word.Cashier')}}</h4> </div>
            <form class="form-horizontal form-material" method="POST" action="{{url('Update_Cashier')}}" enctype="multipart/form-data" novalidate>
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-group col-md-12 m-b-20">
                            <label class="control-label">{{trans('word.Name')}}</label>
                            <div class="controls">
                                <input type="text" value="{{$Cashier->name}}" class="form-control" name="name" required data-validation-required-message="{{trans('word.This field is required')}}"> 
                                <input type="hidden" value="{{$Cashier->id}}" name="id"> 
                            </div>
                        </div> 
                        <div class="form-group col-md-12 m-b-20">
                            <label class="control-label">{{trans('word.Email')}}</label>
                            <input type="email" class="form-control" name="email" value="{{$Cashier->email}}" > 
                        </div>
                        <div class="form-group col-md-12 m-b-20">
                            <label class="control-label">{{trans('word.Mobile')}}</label>
                            <div class="controls">
                                <input type="text" class="form-control" value="{{$Cashier->mobilenumber}}" name="mobilenumber" required data-validation-required-message="{{trans('word.This field is required')}}"> 
                            </div> 
                        </div>
                        <div class="form-group col-md-12 m-b-20">
                            <label class="control-label">{{trans('word.Password')}}</label>
                            <div class="controls">
                                <input type="password" class="form-control" name="password"  required data-validation-required-message="{{trans('word.This field is required')}}"> 
                            </div> 
                        </div>
                        <div class="form-group col-md-12 m-b-20">
                            <div class="fileupload btn btn-danger btn-rounded waves-effect waves-light"><span><i class="ion-upload m-r-5"></i>{{trans('word.Upload Cashier Image')}}</span>
                                <input type="file" class="upload" name="image"> </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect">{{trans('word.Save')}}</button>
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->

    <script>
        // For select 2
        $(".select2").select2();
    </script>