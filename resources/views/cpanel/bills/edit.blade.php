@extends('layouts.cpanel_layout.cpanel_layout')
@section('title')
Edit {{ $kitchenstoedite->en_title  }} Data
@endsection
@section('moduleoptions')
  <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
    <li class="nav-item active">
      <a href="{{ route('kitchens.index') }}" class="nav-link">
        <i class="mdi mdi-arrow-left"></i>Back To list All Kitchens
      </a>
      </li>
    {{-- <li class="nav-item active">
      <a href="{{ route('services.create') }}" class="nav-link">
        <i class="mdi mdi-plus"></i>Add New Service
      </a>
      </li>
      <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
        </li> --}}
      </ul>
    @endsection
@section('content')
  @if (\Session::has('success'))
    <div class="col-12">
    <div class="row purchace-popup alert alert-success">
        <strong>Success!</strong>{{ session()->get('success') }}

          <i class="mdi mdi-close popup-dismiss"></i>
      </div>
    </div>
  @endif




      <div class="row">
          <div class="col-12 grid-margin">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Update Kitchens Data</h4>
                <form class="form-sample"  action="{{route('kitchens.update',$kitchenstoedite->id)}}" method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  {{method_field('PUT')}}

                  <p class="card-description">
                    Kitchens info
                  </p>
                  <div class="row">
                          <div class="col-md-6">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label"> Title </label>
                              <div class="col-sm-9">
                                <input class="form-control" type="text" name="en_title" value="{{ $kitchenstoedite->en_title }}">
                              </div>
                            </div>
                          </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <div class="col-sm-9">
                              <input class="form-control" type="text" name="ar_title" value="{{ $kitchenstoedite->ar_title }}">
                            </div>
                            <label class="col-sm-3 col-form-label"> الاسم</label>

                          </div>
                        </div>

                  </div>




                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label">Description</label>
                        <div class="col-sm-9">
                          <textarea class="form-control" id="exampleTextarea1" name="en_description" rows="2">{{ $kitchenstoedite->en_description }}</textarea>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group row">
                        <div class="col-sm-9">
                          <textarea class="form-control" id="exampleTextarea1" name="ar_description" rows="2">{{ $kitchenstoedite->ar_description }}</textarea>
                        </div>
                        <label class="col-sm-3 col-form-label">الوصف</label>
                      </div>
                    </div>

                  </div>




                  <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Mnicharge</label>
                            <div class="col-sm-9">
                              <input class="form-control" type="text" name="minicharge" value="{{ $kitchenstoedite->minicharge  }}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Phone</label>
                            <div class="col-sm-9">
                              <input class="form-control" type="text" name="phone" value="{{ $kitchenstoedite->phone  }}">
                            </div>
                          </div>
                        </div>
                  </div>

                  <div class="row">
                        <div class="col-md-12">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Address</label>
                            <div class="col-sm-9">
                              <input class="form-control" type="text" name="address" value="{{ $kitchenstoedite->address  }}">
                            </div>
                          </div>
                        </div>

                  </div>

                  <div class="row" style="display:none">
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">lat</label>
                            <div class="col-sm-9">
                              <input class="form-control" type="text" name="lat" value="{{ $kitchenstoedite->lat  }}">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group row">
                            <label class="col-sm-3 col-form-label">lng</label>
                            <div class="col-sm-9">
                              <input class="form-control" type="text" name="lat" value="{{ $kitchenstoedite->lng  }}">
                            </div>
                          </div>
                        </div>
                  </div>
                  <div class="row">

                        <div class="col-md-6">
                          <div class="form-group row">
                            <input  type="hidden"  class="form-control"  name="lat" value="{{$kitchenstoedite->lat}}" id="ketchlat" placeholder="Enter lat ">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group row">
                            <input  type="hidden"  class="form-control"  name="lng" value="{{$kitchenstoedite->lng}}" id="ketchlng" placeholder="Enter lat ">
                          </div>
                        </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                              <div class="form-group row">
                                  <label class="col-sm-12 col-form-label"> ابحث بالعنوان</label>
                                  <div class="col-sm-12">
                                  <input  type="text" class="form-control" id="autocomplete" placeholder="Enter your address">
                                </div>
                              </div>
                           </div>
                    </div>
                    <div class="col-md-12">
                          <div class="form-group row">
                              <label class="col-sm-12 col-form-label">المكان على الخريطه</label>
                              <div class="col-sm-12">

                              <div id="ketchmap" style="width:100%;height:350px"></div>
                            </div>
                          </div>
                  </div>
                  <script>
                      var marker = null;
                      var placeSearch, autocomplete;

                    function initMap() {
                      autocomplete =
                               new google.maps.places.Autocomplete((document.getElementById('autocomplete')),
                               {types: ['geocode']});
                     var map = new google.maps.Map(document.getElementById('ketchmap'), {
                        zoom: 7,
                        center: {lat: {{$kitchenstoedite->lat}}, lng: {{$kitchenstoedite->lng}} }
                      });

                        var MaekerPos = new google.maps.LatLng({{$kitchenstoedite->lat}} , {{$kitchenstoedite->lng}});
                        marker = new google.maps.Marker({
                        position: MaekerPos,
                        map: map
                      });
                      autocomplete.addListener('place_changed', function(){
                                    placeMarkerAndPanTo(autocomplete.getPlace().geometry.location, map);
                                    document.getElementById("ketchlat").value=autocomplete.getPlace().geometry.location.lat();
                                    document.getElementById("ketchlng").value=autocomplete.getPlace().geometry.location.lng();
                                });
                      map.addListener('click', function(e) {
                           placeMarkerAndPanTo(e.latLng, map);
                           document.getElementById("ketchlat").value=e.latLng.lat();
                           document.getElementById("ketchlng").value=e.latLng.lng();

                      });
                    }

                    function placeMarkerAndPanTo(latLng, map) {
                      map.setZoom(9);

                      marker.setPosition(latLng);
                      map.panTo(latLng);
                    }
                  </script>
                  <script async defer
                  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDPN_XufKy-QTSCB68xFJlqtUjHQ8m6uUY&libraries=places&callback=initMap">
                  </script>

                  {{-- <div class="row">
                    <div class="col-md-12">
                      <div class="form-group row">
                        <div class="col-sm-6" style="width:50%;margin:auto">
                          <label for="file-upload" class="custom-file-upload">
                            <i class="mdi mdi-cloud-upload"></i>  Upload  Images
                          </label>
                          <input id="file-upload" type="file" name="images[]" multiple accept="image/"/>
                        </div>
                      </div>
                    </div>
                  </div> --}}
                  <button type="submit"  class="btn btn-success mr-2">Submit</button>

                </form>
              </div>
            </div
          </div>
      </div>
@endsection
