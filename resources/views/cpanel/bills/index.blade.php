@extends('layouts.cpanel_layout.cpanel_layout')
@section('title')
All Kitchens
@endsection
@section('moduleoptions')
  <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
    {{-- <li class="nav-item active">
      <a href="{{ route('kitchens.create') }}" class="nav-link">
        <i class="mdi mdi-plus"></i>Add New Service
      </a>
      </li> --}}
      {{-- <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
        </li> --}}
      </ul>
    @endsection
@section('content')

  @if (\Session::has('success'))
    <div class="col-12">
    <div class="row purchace-popup alert alert-success">
        <strong>Success!</strong>{{ session()->get('success') }}

          <i class="mdi mdi-close popup-dismiss"></i>
      </div>
    </div>
  @endif




      <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Kitchens Table</h4>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                      <tr>
                        <th>En Title</th>
                        <th>الاسم</th>
                        <th>En Description</th>
                        <th>الوصف</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>

                    @foreach ($allkitchens as $onekitchen)

                      <tr>
                        <td class="py-1">{{ $onekitchen->en_title }}</td>
                        <td class="py-1">{{ $onekitchen->ar_title }}</td>
                        <td>{{ $onekitchen->en_description }}</td>
                        <td>{{ $onekitchen->ar_description }}</td>
                        @if($onekitchen->kitchencategory_id != NULL)
                          <td>{{ $onekitchen->kitchencategory->title}}</td>
                        @else
                          <td></td>

                        @endif
                        <td>@if($onekitchen->status == 0)
                                <label class="badge badge-danger">Pending</label>
                            @elseif($onekitchen->status == 1)
                                <label class="badge badge-success">Active</label>
                            @endif
                          </td>
                        <td>
                                <a type="button" class="btn btn-icons btn-rounded btn-primary" href="{{ route('kitchens.edit',$onekitchen->id) }}" style="color:#fff">
                                  <i class="mdi mdi-update"></i>
                                </a>
                                  @if($onekitchen->status == 0)
                                    <a type="button" class="btn btn-icons btn-rounded btn-success" href="{{ route('kitchens.activate',$onekitchen->id) }}" style="color:#fff">
                                      <i class="mdi mdi-checkbox-marked-circle-outline"></i>
                                    </a>
                                  @elseif($onekitchen->status == 1)
                                    <a type="button" class="btn btn-icons btn-rounded btn-danger" href="{{ route('kitchens.disactivate',$onekitchen->id) }}" style="color:#fff">
                                      <i class="mdi mdi-delete-empty"></i>
                                    </a>
                                  @endif

                              {{-- <form method="post" action="{{ route('kitchens.destroy',$onekitchen->id) }}" style="display:inline">
                                {{ csrf_field () }}
                                {{method_field('Delete')}}
                                <button class="btn btn-icons btn-rounded btn-danger"  href=""style="color:#fff">
                                   <i class="mdi mdi-delete-empty"></i>
                                </button>
                              </form> --}}

                                  <a type="button" class="btn btn-icons btn-rounded btn-info"  href="{{ route('kitchens.show',$onekitchen->id) }}" style="color:#fff">
                                    <i class="mdi mdi-information-variant"></i>
                                  </a>
                                </td>
                              </tr>
                    @endforeach
                  </tbody>
                </table>

                {{ $allkitchens->links() }}
              </div>
            </div>
          </div>
        </div>

      </div>
@endsection
