@extends('layouts.cpanel_layout.cpanel_layout')
@section('title')
One Kitchen Data
@endsection
@section('moduleoptions')
  <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
    <li class="nav-item active">
      <a href="{{ route('kitchens.edit',$kitchenstoshow->id) }}" class="nav-link">
        <i class="mdi mdi-update"></i>Edite This kitchen
      </a>
      </li>
  @if($kitchenstoshow->status == 0)
    <li class="nav-item ">
      <a href="{{ route('kitchens.activate',$kitchenstoshow->id) }}" class="nav-link">
        <i class="mdi mdi-checkbox-marked-circle-outline"></i>Activate This kitchen
      </a>
      </li>
  @else
    <li class="nav-item">
      <a href="{{ route('kitchens.disactivate',$kitchenstoshow->id) }}" class="nav-link">
        <i class="mdi mdi-delete-empty"></i>Disctivate This kitchen
      </a>
      </li>
  @endif
  <li class="nav-item active">
    <a href="{{ route('kitchens.index') }}" class="nav-link">
      <i class="mdi mdi-arrow-left"></i>Back To list All Kitchens
    </a>
    </li>
      {{-- <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
        </li> --}}
      </ul>
    @endsection
@section('content')
  @if (\Session::has('success'))
    <div class="col-12">
    <div class="row purchace-popup alert alert-success">
        <strong>Success!</strong>{{ session()->get('success') }}

          <i class="mdi mdi-close popup-dismiss"></i>
      </div>
    </div>
  @endif
      <div class="row">
                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <h4 class="card-title">En Tilte </h4>
                        <div class="media">
                          {{-- <i class="mdi mdi-earth icon-md text-info d-flex align-self-start mr-3"></i> --}}
                          <div class="media-body">
                            <p class="card-text">{{ $kitchenstoshow->en_title }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 grid-margin stretch-card" >
                    <div class="card">
                      <div class="card-body">
                        <h4 class="card-title">الاسم بالعربيه</h4>
                        <div class="media">
                          {{-- <i class="mdi mdi-earth icon-md text-info d-flex align-self-center mr-3"></i> --}}
                          <div class="media-body">
                            <p class="card-text">{{ $kitchenstoshow->ar_title }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <h4 class="card-title">Description</h4>
                        <div class="media">
                          {{-- <i class="mdi mdi-earth icon-md text-info d-flex align-self-end mr-3"></i> --}}
                          <div class="media-body">
                            <p class="card-text">  {{ $kitchenstoshow->en_description }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <h4 class="card-title">الوصف</h4>
                        <div class="media">
                          {{-- <i class="mdi mdi-earth icon-md text-info d-flex align-self-end mr-3"></i> --}}
                          <div class="media-body">
                            <p class="card-text">  {{ $kitchenstoshow->ar_description }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
    <div class="row">
                <div class="col-md-4 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Category</h4>
                      <div class="media">
                        {{-- <i class="mdi mdi-earth icon-md text-info d-flex align-self-start mr-3"></i> --}}
                        <div class="media-body">
                          @if($kitchenstoshow->kitchencategory_id != NULL)<p class="card-text">{{ $kitchenstoshow->kitchencategory->title }}</p>@endif
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-4 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Minimum Charge</h4>
                      <div class="media">
                        {{-- <i class="mdi mdi-earth icon-md text-info d-flex align-self-end mr-3"></i> --}}
                        <div class="media-body">
                          <p class="card-text">  {{ $kitchenstoshow->minicharge }}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h4 class="card-title">Vendor</h4>
                      <div class="media">
                        {{-- <i class="mdi mdi-earth icon-md text-info d-flex align-self-end mr-3"></i> --}}
                        <div class="media-body">
                          <p class="card-text">  </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div class="row">
                <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:960px;height:480px;overflow:hidden;visibility:hidden;background-color:#24262e;">
                    <!-- Loading Screen -->
                    <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                        <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="{{url('resources/assets/cpanel/dist/jssor/spin.svg')}}" />
                    </div>
                    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:240px;width:720px;height:480px;overflow:hidden;">
                      @foreach ($kitchenstoshow->kitchenimages as $oneimage)
                        <div>
                          <form method="post" action="{{ route('kitchensimage.destroy',$oneimage->id) }}" style="display:inline">
                            {{ csrf_field () }}
                            {{method_field('Delete')}}
                            <button class="btn btn-icons btn-rounded btn-danger"  href=""style="color:#fff">
                               <i class="mdi mdi-delete-empty"></i>
                            </button>
                          </form>
                          <img data-u="image" src="{{ url('resources/assets/cpanel/dist/img/kitchen',$oneimage->image) }}" />
                          <img data-u="thumb" src="{{ url('resources/assets/cpanel/dist/img/kitchen',$oneimage->image) }}" />
                        </div>
                      @endforeach
                    </div>
                    <!-- Thumbnail Navigator -->
                    <div data-u="thumbnavigator" class="jssort101" style="position:absolute;left:0px;top:0px;width:240px;height:480px;background-color:#000;" data-autocenter="2" data-scale-left="0.75">
                        <div data-u="slides">
                            <div data-u="prototype" class="p" style="width:99px;height:66px;">
                                <div data-u="thumbnailtemplate" class="t"></div>
                                <svg viewbox="0 0 16000 16000" class="cv">
                                    <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                                    <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                                    <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <!-- Arrow Navigator -->
                    <div data-u="arrowleft" class="jssora093" style="width:50px;height:50px;top:0px;left:270px;" data-autocenter="2">
                        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                            <polyline class="a" points="7777.8,6080 5857.8,8000 7777.8,9920 "></polyline>
                            <line class="a" x1="10142.2" y1="8000" x2="5857.8" y2="8000"></line>
                        </svg>
                    </div>
                    <div data-u="arrowright" class="jssora093" style="width:50px;height:50px;top:0px;right:30px;" data-autocenter="2">
                        <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                            <circle class="c" cx="8000" cy="8000" r="5920"></circle>
                            <polyline class="a" points="8222.2,6080 10142.2,8000 8222.2,9920 "></polyline>
                            <line class="a" x1="5857.8" y1="8000" x2="10142.2" y2="8000"></line>
                        </svg>
                    </div>
                </div>
    </div>
    <div  class="row" style="margin-top:100px;float:right">

      <form method="post" action="{{ route('kitchensimage.store') }}" enctype="multipart/form-data">
        {{ csrf_field () }}
        <input type="hidden" name="kitchen_id" value="{{ $kitchenstoshow->id }}">
        <div class="col-md-12">
          <div class="form-group row">
            <div class="col-sm-9">
              <label for="file-upload" class="custom-file-upload">
                <i class="mdi mdi-cloud-upload"></i>  Upload  Images
              </label>
              <input id="file-upload" type="file" name="images[]" multiple accept="image/"/>
            </div>
            <div class="col-sm-3">
              <button type="submit"  class="btn btn-success mr-2">Submit</button>
            </div>
          </div>
        </div>
      </form>
    </div>

@endsection
