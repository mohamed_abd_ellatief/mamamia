@extends('layouts.cpanel_layout.master')
@section('title')
{{trans('word.Invoice')}}
@endsection
@section('content')
<div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">{{trans('word.Invoice')}}</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                    <li class="breadcrumb-item active">{{trans('word.Invoice')}}</li>
                </ol>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card card-block printableArea text-center">
                    <h3><span>{{$Order->id}}</span></h3>
                    @if($Order->type == "banquet")
                    <h3><b>{{$Order->component}}</b> </h3>
                    @else
                    @inject('meal_name','App\Meal')
                    @if($meal_name->mealname($Order->id))
                    <h3>
                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                            <b>{{$meal_name->mealname($Order->id)->ar_title}}</b>
                            @else
                            <b>{{$meal_name->mealname($Order->id)->en_title}}</b>
                            @endif
                    </h3>
                    @endif
                    @endif
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                                    @if($Order->type == 'meal')
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <table class="table color-table inverse-table m-t-40">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>{{trans('word.Title')}}</th>
                                                            <th>{{trans('word.Quantity')}}</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @inject('Mals','App\Meal')
                                                        @if($Mals->meals($Order->id) != false)
                                                        <?php 
                                                        $count=1;
                                                        ?>
                                                        @foreach($Mals->meals($Order->id) as $Meall)
                                                        <tr>
                                                            <td>{{$count}}</td>
                                                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                            <td>{{$Meall->ar_title}}</td>
                                                            @else
                                                            <td>{{$Meall->en_title}}</td>
                                                            @endif
                                                            <td>{{$Meall->amount}}</td>
                                                        </tr>
                                                        <?php $count++ ?>
                                                        @endforeach
                                                        @else
                                                        <tr class="text-center">
                                                            <td colspan="3">{{$Order->en_partyname}}</td>
                                                        </tr>
                                                        @endif 
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="control-label  col-md-3">{{trans('word.Component')}}:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$Order->component}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    @endif
                            </div>
                        </div>
                        <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label  col-md-5 text-bold text-info">{{trans('word.Order no')}}:</label>
                                        <div class="col-md-7">
                                            <p class="form-control-static"> {{$Order->id}} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Status')}}:</label>
                                        <div class="col-md-7">
                                                @if($Order->status == 0 || $Order->status == 2)
                                                <p class="form-control-static text-primary">{{trans('word.New')}}</p>
                                                @elseif($Order->status == 1)
                                                <p class="form-control-static text-success">{{trans('word.accepted')}}</p>
                                                @elseif($Order->status == -1)
                                                <p class="form-control-static text-danger">{{trans('word.rejected')}}</p>
                                                @endif
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Customer Name')}}: </label>
                                        @if(\App\User::find($Order->user_id))
                                            <p class="form-control-static col-md-7"> {{\App\User::find($Order->user_id)->name}}</p>
                                        @else
                                            <p class="form-control-static col-md-7">
                                                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                    تم حذف بيانات العميل
                                                    @else
                                                    Customer has been Deleted
                                                    @endif
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Mobile')}}: </label>
                                        @if(\App\User::find($Order->user_id))
                                            <p class="form-control-static col-md-7"> {{\App\User::find($Order->user_id)->mobilenumber}}</p>
                                        @else
                                            <p class="form-control-static col-md-7">
                                                @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                تم حذف بيانات العميل
                                                @else
                                                Customer has been Deleted
                                                @endif
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Order Date')}}:</label>
                                        <div class="col-md-7">
                                            <p class="form-control-static">{{$Order->created_at->format('l d M Y')}}</p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Order Time')}}:</label>
                                        <div class="col-md-7">
                                            <p class="form-control-static">{{$Order->created_at->format('h:i A')}}</p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            @if($Order->type == "banquet")
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-4 text-bold text-info">{{trans('word.Party Date')}}:</label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">{{$Order->partydate}}</p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label  col-md-4 text-bold text-info">{{trans('word.Party Time')}}:</label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">{{$Order->partytime}}</p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-4 text-bold text-info">{{trans('word.Banquet Type')}}:</label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">{{$Order->partytype}}</p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label  col-md-4 text-bold text-info">{{trans('word.Guest Number')}}:</label>
                                        <div class="col-md-8">
                                            <p class="form-control-static">{{$Order->guestnum}}</p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            @endif
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Location')}}: </label>
                                            @if($Order->type == "meal")
                                            <p class="form-control-static text-center col-md-12"> {{$Order->userplace}}</p>
                                            @else
                                            <p class="form-control-static text-center col-md-12"> {{$Order->partylocation}}</p>
                                            @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Preparation')}}: </label>
                                        @if($Order->type == "meal")
                                            <p class="form-control-static text-center col-md-12"> 
                                                {{-- {{$Meals->Preparation($Order->id)}} --}}
                                                {{intdiv($Mals->Preparation($Order->id), 60).':'. ($Mals->Preparation($Order->id) % 60)}}
                                            </p>
                                        @else
                                            <input type="text" name="neededtime" class="form-control text-center" value="">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Kitchen Name')}}: </label>
                                            <p class="form-control-static text-center col-md-12"> 
                                                @inject('Kitchen_Name','App\Kitchen')
                                                @if($Kitchen_Name->getName($Order->kitchen_id))
                                                @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                {{$Kitchen_Name->getName($Order->kitchen_id)->ar_title}}
                                                @else
                                                {{$Kitchen_Name->getName($Order->kitchen_id)->en_title}}
                                                @endif
                                                @else
                                                    Kitchen has been Deleted
                                                @endif
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.distance to user')}}: </label>
                                            <p class="form-control-static text-center col-md-12"> 
                                                {{$Order->distancetouser}} {{trans('word.km')}}
                                            </p>
                                    </div>
                                </div> 
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.delivery total price')}}: </label>
                                            <p class="form-control-static text-center col-md-12"> 
                                                {{$Order->deliverytotalprice}}
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.total price')}}: </label>
                                            <p class="form-control-static text-center col-md-12"> 
                                                {{$Order->totalprice}}
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.User Note')}}: </label>
                                            <textarea rows="6" class="form-control" disabled>{{$Order->usernote}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Vendor Note')}}: </label>
                                            <textarea rows="6" class="form-control" disabled>{{$Order->vendornote}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
@endsection

@section('js')
<script src="{{asset('cpanel/js/jquery.PrintArea.js')}}" type="text/JavaScript"></script>
    <script>
    $(document).ready(function() {
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });
    </script>
@endsection