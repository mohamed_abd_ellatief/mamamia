<div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-block">
                    <form class="form-horizontal" role="form" action="{{url('UpdateBanquet')}}" method="POST"  novalidate>
                            @csrf
                        <div class="form-body">
                            <div class="row">
                                <input type="hidden" name="id" value="{{$Order->id}}"/>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <h3 class="card-title">{{trans('word.Component')}}</h3>
                                        <div class="col-md-12">
                                            {{$Order->component}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label  col-md-5 text-bold text-info">{{trans('word.Order no')}}:</label>
                                        <div class="col-md-7">
                                            <p class="form-control-static"> {{$Order->id}} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Status')}}:</label>
                                        <div class="col-md-7">
                                                @if($Order->status == 2 )
                                                <p class="form-control-static text-primary">{{trans('word.New')}}</p>
                                                @elseif($Order->status == 1)
                                                <p class="form-control-static text-success">{{trans('word.accepted')}}</p>
                                                @elseif($Order->status == -1)
                                                <p class="form-control-static text-danger">{{trans('word.rejected by vendor')}}</p>
                                                @elseif($Order->status == -2)
                                                <p class="form-control-static text-danger">{{trans('word.rejected by user')}}</p>
                                                @endif
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Customer Name')}}: </label>
                                        @if(\App\User::find($Order->user_id))
                                            <p class="form-control-static col-md-7"> {{\App\User::find($Order->user_id)->name}}</p>
                                        @else
                                            <p class="form-control-static col-md-7">
                                                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                    تم حذف بيانات العميل
                                                    @else
                                                    Customer has been Deleted
                                                    @endif
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">{{trans('word.Mobile')}}: </label>
                                        @if(\App\User::find($Order->user_id))
                                            <p class="form-control-static col-md-7"> {{\App\User::find($Order->user_id)->mobilenumber}}</p>
                                        @else
                                            <p class="form-control-static col-md-7">تم حذف بيانات العميل</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label col-md-5 text-bold text-info">@if(Request::segment(1) == 'ar') اسم الحفلة   @else Party Name  @endif: </label>
                                            <p class="form-control-static col-md-7">{{$Order->en_partyname}} </p>

                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Order Time')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->created_at->format('H:i a')}}
                                            </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Party Time')}}: </label>
                                        <p class="form-control-static text-center col-md-12">
                                                {{$Order->partytime}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Order Date')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->created_at->format('d/m/Y')}}
                                            </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Party Date')}}: </label>
                                        <p class="form-control-static text-center col-md-12">
                                                {{$Order->partydate}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Party Type')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->partytype}}
                                            </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Guest Number')}}: </label>
                                        <p class="form-control-static text-center col-md-12">
                                                {{$Order->guestnum}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Party Location')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->partylocation}}
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Preparation')}}: </label>
                                        @if($Order->status == 2)
                                        <div class="controls col-md-12">
                                        <input class="form-control" type="text" name="neededtime" required data-validation-required-message="{{trans('word.This field is required')}}">
                                        </div>
                                        @else
                                        <p class="form-control-static text-center col-md-12">
                                            {{$Order->neededtime}}
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Kitchen Name')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                @inject('Kitchen_Name','App\Kitchen')
                                                @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                {{$Kitchen_Name->getName($Order->kitchen_id)->ar_title}}
                                                @else
                                                {{$Kitchen_Name->getName($Order->kitchen_id)->en_title}}
                                                @endif
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.distance to user')}}: </label>
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->distancetouser}} {{trans('word.km')}}
                                            </p>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.delivery total price')}}: </label>
                                            @if($Order->status == 2)
                                            <div class="controls col-md-12">
                                                <div class="input-group"> <span class="input-group-addon">$</span>
                                                    <input class="form-control" type="number" name="deliverytotalprice" required data-validation-required-message="{{trans('word.This field is required')}}"><span class="input-group-addon">.00</span> </div>
                                            </div>
                                            @else
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->deliverytotalprice}}
                                            </p>
                                            @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.total price')}}: </label>
                                        @if($Order->status == 2)
                                        <div class="controls col-md-12">
                                        <div class="input-group"> <span class="input-group-addon">$</span>
                                        <input class="form-control" type="number" name="totalprice" required data-validation-required-message="{{trans('word.This field is required')}}"><span class="input-group-addon">.00</span> </div>
                                        </div>
                                        @else
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->totalprice}}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.User Note')}}: </label>
                                        <p class="form-control-static text-center col-md-12">
                                            {{$Order->usernote}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Vendor Note')}}:</label>
                                            @if($Order->status == 2)
                                            <textarea rows="6" class="form-control" name="vendornote"></textarea>
                                            @else
                                            <p class="form-control-static text-center col-md-12">
                                                {{$Order->vendornote}}
                                            </p>
                                            @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            @if($Order->status == 2)
                                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{trans('word.accept')}}</button>
                                            <a class="btn btn-danger text-white rejectBanquet" data-id="{{$Order->id}}" data-dismiss="modal">{{trans('word.reject')}}</a>
                                            @endif
                                            <button type="button" class="btn btn-inverse" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('cpanel/en/js/validation.js')}}"></script>
<script>
    ! function(window, document, $) {
        "use strict";
        $("input,select").not("[type=submit]").jqBootstrapValidation()
    }(window, document, jQuery);

    $(".rejectBanquet").click(function(){
      var id=$(this).data('id')
      var tr=$(this).parent().parent()
      console.log(tr[0])
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      swal({
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                title: "هل انت متأكد من رفض الطلب الوليمة؟",
            @else
                title: "Are you sure reject Banquet?",
            @endif
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                confirmButtonText: "نعم, انا متأكد!",
                cancelButtonText: "لأ, من فضلك",
            @else
                confirmButtonText: "Yes, Sure it!",
                cancelButtonText: "No, cancel please!",
            @endif
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                url:'{{url("rejectBanquet")}}',
                type:"post",
                data:{'id':id,_token: CSRF_TOKEN},
                dataType:"JSON",
                success: function (data) {
                    if(data.message == "reject")
                    {
                        $(`#`+id).hide()
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("طلب مرفوض!", "تم رفض الطلب.", "success");
                        @else
                        swal("reject!", "Order has been rejected.", "success");
                        @endif
                    }else{
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        swal("خطأ!", "فشل العملية", "error");
                        @else
                        swal("ًWrong!", "The Operation Failed", "error");
                        @endif
                    }
                },
                fail: function(xhrerrorThrown){
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                    swal("خطأ!", "فشل العملية", "error");
                    @else
                    swal("ًWrong!", "The Operation Failed", "error");
                    @endif
                }

            });
            } else {
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                swal("ألغاء", "تم ألغاء العملية", "error");
                @else
                swal("Cancelled", "Order has been Cancelled", "error");
                @endif
            }
        });
    })
</script>
