@extends('layouts.cpanel_layout.master')
@section('title')
{{trans('word.Work Days')}}
@endsection

@section('css')
<!--alerts CSS -->
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{trans('word.Work Days')}}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('word.Work Days')}}</li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <button id="delete" class="btn pull-right hidden-sm-down btn-danger m-l-10">{{trans('word.Delete 0 row')}}</button>
            <button data-toggle="modal" data-target=".bs-example-modal-lg" class="btn pull-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> {{trans('word.Create')}}</button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    @if($errors->any())
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i>warning!</h4>
        <ul>
          @foreach($errors->all() as $error)
          <li style="font-weight: 40"><h4 style="font-weight:40">{{$error}}</h4></li>
          @endforeach
        </ul>
      </div>
    @endif

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                        {{$Restaurant->ar_title}}
                    @else
                        {{$Restaurant->en_title}}
                    @endif
                    </h4>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('word.Day')}}</th>
                                    <th>{{trans('word.Action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @inject('Availblehours','App\Availblehours')
                                <?php
                                    $dt = Carbon\Carbon::now();
                                    $count=1;
                                ?>
                              @foreach($Days as $Day)
                                <tr data-id="{{$Day->id}}">
                                    <td>{{$count}}</td>
                                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                                    <td>{{$Day->ar_title}}</td>
                                    @else
                                    <td>{{$Day->title}}</td>
                                    @endif
                                    <td class="text-nowrap">
                                        <a class=" btn btn-primary" href="{{url('times/'.$Restaurant->id.'/'.$Day->id)}}"><i class="fa fa-clock-o"></i></a>
                                    </td>
                                  </tr>
                                  <?php $count++; ?>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

<!-- insert modal content -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h4 class="modal-title m-b-0 text-white" id="myLargeModalLabel">{{trans('word.Work Days')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-block">
                                <form action="{{url('storeDaysToKitchen')}}" method="POST">
                                    @csrf
                                    <div class="form-body">
                                        </div>
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                <label class="control-label">{{trans('word.Day')}}</label>
                                                <input name="kitchen_id" type="hidden" value="{{$Restaurant->id}}">
                                                <select class="form-control custom-select" name="day_id" required data-validation-required-message="{{trans('word.This field is required')}}">
                                                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                    <option value="0">السبت</option>
                                                    <option value="1">الأحد</option>
                                                    <option value="2">الأثنين</option>
                                                    <option value="3">الثلاثاء</option>
                                                    <option value="4">الأربعاء</option>
                                                    <option value="5">الخميس</option>
                                                    <option value="6">الجمعة</option>
                                                    @else
                                                    <option value="0">Saturday</option>
                                                    <option value="1">Sunday</option>
                                                    <option value="2">Monday</option>
                                                    <option value="3">Tuesday</option>
                                                    <option value="4">Wednesday</option>
                                                    <option value="5">Thursday</option>
                                                    <option value="6">Friday</option>                                                    
                                                    @endif
                                                </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{trans('word.Save')}}</button>
                                        <button type="button" class="btn btn-inverse" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection

@section('js')
<!-- This is data table -->
<script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
    
    <script>

        @if(isset($message) == "success")
        $(".bs-example-modal-lg").modal('show')
        @endif

            var table = $('#myTable').DataTable({
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
                    }
                @endif
            });
    $('#myTable tbody').on( 'click', 'tr', function () {
        if (event.ctrlKey) {
            $(this).toggleClass('selected');
            @if( LaravelLocalization::getCurrentLocale() == "ar")
            $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
            @else
            $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
            @endif
        }else{
            var isselected = false 
            var numSelected= table.rows('.selected').data().length
            if($(this).hasClass('selected') && numSelected==1){
                isselected = true
            }
            $('#myTable tbody tr').removeClass('selected');
            if(!isselected){
                $(this).toggleClass('selected');
            }
            @if( LaravelLocalization::getCurrentLocale() == "ar")
            $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
            @else
            $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
            @endif
        }
    });
 


$("body").on("click", "#delete", function () {
  
    var dataList=[];
    $("#myTable .selected").each(function(index) {
        dataList.push($(this).closest('tr').data('id'))
    })
    if(dataList.length >0){
        
        swal({
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                title: "هل انت متأكد؟",   
            @else
                title: "Are you sure?",   
            @endif 
            text: "",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                confirmButtonText: "نعم, انا متأكد!",   
                cancelButtonText: "لأ",   
            @else
                confirmButtonText: "Yes, Sure it!",   
                cancelButtonText: "No",   
            @endif 
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){
            if (isConfirm) {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url:'{{url("DeleteKitchenDays")}}',
                type:"post",
                data:{'id':dataList,_token: CSRF_TOKEN},
                success: function (data) {
                    if(data.message == "Success")
                    {
                        $("#myTable .selected").hide();
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        $('#delete').text('حذف 0 سجل');
                        @else
                        $('#delete').text('Delete 0 row');
                        @endif
                        swal("Deleted!", "Your Record has been deleted.", "success"); 
                    }else{
                        swal("Sorry", "Your imaginary file is safe :)", "error"); 
                    }
                },
                fail: function(xhrerrorThrown){
                    swal("Sorry", "Your imaginary file is safe :)", "error"); 
                }
            
            });
                  
            } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");   
            }
        });
    }
});
   
        

   
</script>
<?php
$message=session()->get("message");
?>
@if( session()->has("message"))
  @if( $message == "Success")
    <script>
    swal("Successfully!", "Your Operation has been Successfully.", "success"); 
    </script>
  @elseif ( $message == "Failed")
  <script>
        swal("Sorry", "Your imaginary file is safe :)", "error"); 
    </script>
  @endif
@endif


@endsection
