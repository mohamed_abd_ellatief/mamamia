@extends('layouts.cpanel_layout.master')
@section('title')
{{trans('word.Rules')}}
@endsection

@section('css')
<!--alerts CSS -->
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                    {{$Restaurant->ar_title}}
                    @else
                    {{$Restaurant->en_title}}
                @endif
            </h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('word.Rules')}}</li>
            </ol>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    @if($errors->any())
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i>warning!</h4>
        <ul>
          @foreach($errors->all() as $error)
          <li style="font-weight: 40"><h4 style="font-weight:40">{{$error}}</h4></li>
          @endforeach
        </ul>
      </div>
    @endif

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <!-- Nav tabs -->
                        <div class="vtabs">
                            <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#Rules" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">{{trans('word.Kitchen Rule')}}</span> </a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#RuleUser" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">{{trans('word.User Points')}}</span></a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="Rules" role="tabpanel">
                                    <div id="accordion2" class="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="card card-outline-info">
                                                    <div class="card-block">
                                                        <form action="{{url('insertUpdateRule')}}" method="POST" novalidate>
                                                            @csrf
                                                            <div class="form-body">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label">{{trans('word.Money')}}</label>
                                                                            <div class="controls">
                                                                                <div class="input-group"> <span class="input-group-addon">$</span>
                                                                                <input type="number" name="rule" class="form-control" @if($Rule) value="{{$Rule->rule}}" @endif required data-validation-required-message="{{trans('word.This field is required')}}"> <span class="input-group-addon">.00</span> </div>
                                                                            </div>
                                                                            <input type="hidden" name="kitchen_id" class="form-control" @if($Restaurant) value="{{$Restaurant->id}}" @endif>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label">{{trans('word.Points')}}</label>
                                                                            <div class="controls">
                                                                                <input type="text" name="points" class="form-control" @if($Rule) value="{{$Rule->points}}" @endif required data-validation-required-message="{{trans('word.This field is required')}}" data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="{{trans('word.No Characters Allowed, Only Numbers')}}">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label class="control-label">{{trans('word.Discount')}}</label>
                                                                            <div class="controls">
                                                                                <div class="input-group"> <span class="input-group-addon">$</span>
                                                                                <input type="number" name="discount" class="form-control" @if($Rule) value="{{$Rule->discount}}" @endif required data-validation-required-message="{{trans('word.This field is required')}}"> <span class="input-group-addon">.00</span> </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--/span-->
                                                                </div>
                                                                <!--/row-->
                                                            </div>
                                                            <div class="form-actions">
                                                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i>{{trans('word.Save')}}</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="RuleUser" role="tabpanel">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>{{trans('word.Name')}}</th>
                                                <th>{{trans('word.Mobile')}}</th>
                                                <th>{{trans('word.Points')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($Points as $Point)
                                            <tr>
                                                <td>{{$Point->name}}</td>
                                                <td>{{$Point->mobilenumber}}</td>
                                                <td>{{$Point->points}}</td>
                                            </tr>
                                            @endforeach 
                                        </tbody>
                                    </table> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

@endsection

@section('js')
<!-- This is data table -->
<script src="{{asset('cpanel/en/js/validation.js')}}"></script>
<script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
    
    <script>
            var table = $('#myTable').DataTable({
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                    "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
                    }
                @endif
            });


        ! function(window, document, $) {
            "use strict";
            $("input,select").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);

    
</script>

<?php
$message=session()->get("message");
?>
@if( session()->has("message"))
  @if( $message == "Success")
    <script>
        swal({   
            @if( LaravelLocalization::getCurrentLocale() == "ar")
            title: "نجحت!",   
            text: "تمت العملية بنجاح.", 
            @else
            title: "Succeeded!",   
            text: "Operation completed successfully.", 
            @endif
            type:"success" , 
            timer: 1000,   
            showConfirmButton: false 
        });
    </script>
  @elseif ( $message == "Failed")
    <script>
        swal({   
            @if( LaravelLocalization::getCurrentLocale() == "ar")
            title: "فشل",   
            text: "عفوا فشلت العملية :(", 
            @else
            title: "Sorry",   
            text: "Oops, the operation failed :(", 
            @endif
            type:"error" , 
            timer: 2000,   
            showConfirmButton: false 
        }); 
    </script>
  @endif
@endif


@endsection
