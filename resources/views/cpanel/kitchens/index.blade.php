@extends('layouts.cpanel_layout.master')
@section('title')
{{trans('word.Kitchen')}}
@endsection

@section('css')
<!--alerts CSS -->

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
<style>
    /* Always set the map height explicitly to define the size of the div
        * element that contains the map. */
    #map {
        height: 100%;
        margin-bottom: 250px;
    }
    #editmap {
        height: 100%;
        margin-bottom: 250px;
    }
    #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
    }
    #infowindow-content .title {
        font-weight: bold;
    }
    #infowindow-content {
        display: none;
    }
    /* #map #infowindow-content {
        display: inline;
    } */
    .pac-container{
        z-index: 1050;
    }
    .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }
    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }
    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }
    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }
    #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
    }
    #target {
        width: 345px;
    }
</style>
@endsection

@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{trans('word.Kitchen')}}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('word.Kitchen')}}</li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <button id="delete" class="btn pull-right hidden-sm-down btn-danger m-l-10">{{trans('word.Delete 0 row')}}</button>
            <button data-toggle="modal" data-target=".bs-example-modal-lg" class="btn pull-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> {{trans('word.Create')}}</button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    @if($errors->any())
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i>warning!</h4>
        <ul>
          @foreach($errors->all() as $error)
          <li style="font-weight: 40"><h4 style="font-weight:40">{{$error}}</h4></li>
          @endforeach
        </ul>
      </div>
    @endif

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('word.Title')}}</th>
                                    <th>{{trans('word.Description')}}</th>
                                    <th>{{trans('word.Status')}}</th>
                                    <th>{{trans('word.image')}}</th>
                                    <th>{{trans('word.Tags')}}</th>
                                    <th>{{trans('word.Category')}}</th>
                                    <th>{{trans('word.Mobile')}}</th>
                                    <th>{{trans('word.Action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($Kitchens as $Kitchen)
                                <tr id="{{$Kitchen->id}}">
                                    <td>{{$Kitchen->id}}</td>
                                    <td>
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                        {{$Kitchen->ar_title}}
                                        @else
                                        {{$Kitchen->en_title}}
                                        @endif
                                    </td>
                                    <td>
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                        {{str_limit($Kitchen->ar_description,20)}}
                                        @else
                                        {{str_limit($Kitchen->en_description,20)}}
                                        @endif
                                    </td>
                                    <td>
                                        <div class="switchery-demo m-b-30" data-id="{{$Kitchen->id}}">
                                            <input type="checkbox" name="status" @if($Kitchen->status == "on") checked @endif class="js-switch UpdateStatus" data-color="#009efb" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="image-popups">
                                            {{-- <img width="150px" src="{{asset('cpanel/upload/kitchen/'.$Kitchen->image)}}"/> --}}
                                            <a href="{{asset('cpanel/upload/kitchen/'.$Kitchen->image)}}" data-effect="mfp-newspaper">
                                                {{-- <img src="../assets/images/big/img2.jpg" class="img-responsive" /> --}}
                                                <img style="width:100px" src="{{asset('cpanel/upload/kitchen/'.$Kitchen->image)}}" class="img-responsive"/>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        @if(\App\Kitchencategory::find($Kitchen->kitchencategory_id))
                                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                                            {{\App\Kitchencategory::find($Kitchen->kitchencategory_id)->ar_title}}
                                            @else
                                            {{\App\Kitchencategory::find($Kitchen->kitchencategory_id)->title}}
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        {{-- {{\DB::table('kitchen_kitchentag')->where('kitchen_id',$Kitchen->id)->count()}} --}}
                                        @if(\DB::table('kitchen_kitchentag')->where('kitchen_id',$Kitchen->id)->count()>0)
                                            @foreach(\DB::table('kitchen_kitchentag')->where('kitchen_id',$Kitchen->id)->get() as $tagsId)
                                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                                            {{\DB::table('kitchentags')->find($tagsId->kitchentag_id)->ar_title}}<br>
                                            @else
                                            {{\DB::table('kitchentags')->find($tagsId->kitchentag_id)->title}}<br>
                                            @endif
                                            @endforeach
                                        @endif
                                    </td>
                                    <td>{{$Kitchen->phone}}</td>
                                    <td class="text-nowrap">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-list"></i>
                                            </button>
                                            <div class="dropdown-menu animated rubberBand">
                                                <a class="edit-kitchen dropdown-item" style="width:80%" href="#" data-id="{{$Kitchen->id}}" data-original-title="Edit">{{trans('word.Update Kitchen')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('meals/'.$Kitchen->id)}}" data-original-title="Meals">{{trans('word.Meals')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('orders/'.$Kitchen->id)}}" data-original-title="Orders">{{trans('word.Orders')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('invoices/'.$Kitchen->id)}}" data-original-title="Invoices">{{trans('word.Invoices')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('archives/'.$Kitchen->id)}}" data-original-title="Archives">{{trans('word.Archives')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('banquets/'.$Kitchen->id)}}" data-original-title="Banquets">{{trans('word.Banquets')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('invoicesbanquets/'.$Kitchen->id)}}" data-original-title="Invoices Banquets">{{trans('word.Invoices Banquets')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('archivesbanquets/'.$Kitchen->id)}}" data-original-title="Archives Banquets">{{trans('word.Archives Banquets')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('days/'.$Kitchen->id)}}" data-original-title="days">{{trans('word.Work Days')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('rules/'.$Kitchen->id)}}" data-original-title="Rules">{{trans('word.Rules')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('images_kitchen/'.$Kitchen->id)}}" data-original-title="Image">{{trans('word.images')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('cashiers/'.$Kitchen->id)}}" data-original-title="cashiers">{{trans('word.Cashier')}}</a>
                                            </div>
                                        </div>
                                    </td>
                                  </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

<!-- insert modal content -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h3 class="modal-title m-b-0 text-white" id="myLargeModalLabel">{{trans('word.New Kitchen')}}</h3>
                <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-block">
                                <form action="{{url('Store_Kitchen')}}" method="post" enctype="multipart/form-data" novalidate>
                                    @csrf
                                    <div class="form-body">
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                        <div class="row p-t-20">
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Arabic Title')}}</label>
                                                    <div class="controls">
                                                        <input type="text" id="ar_title" name="ar_title" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">
                                                    </div>    
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.English Title')}}</label>
                                                    <div class="controls">
                                                        <input type="text" id="en_title" name="en_title" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @else
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.English Title')}}</label>
                                                    <div class="controls">
                                                        <input type="text" id="en_title" name="en_title" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Arabic Title')}}</label>
                                                    <div class="controls">
                                                        <input type="text" id="ar_title" name="ar_title" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">
                                                    </div>    
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        @endif
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Arabic Description')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                                    <textarea type="text" id="ar_description" class="form-control" name="ar_description"></textarea>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.English Description')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                                    <textarea type="text" id="en_description" name="en_description" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        @else
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.English Description')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                                    <textarea type="text" id="en_description" name="en_description" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Arabic Description')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                                    <textarea type="text" id="ar_description" class="form-control" name="ar_description" ></textarea>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        @endif
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Category')}}</label>
                                                    <div class="controls">
                                                    <select class="form-control custom-select" name="category" required data-validation-required-message="{{trans('word.This field is required')}}">
                                                      @foreach($Categories as $Category)
                                                      @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                        <option value="{{$Category->id}}">{{$Category->ar_title}}</option>
                                                        @else
                                                        <option value="{{$Category->id}}">{{$Category->title}}</option>
                                                        @endif
                                                      @endforeach
                                                    </select> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Tag')}}</label>
                                                    <div class="controls">
                                                        @foreach($Tags as $Tag)
                                                        <div class="form-check">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="tags[]" value="{{$Tag->id}}" class="custom-control-input" data-validation-minchecked-minchecked="1" data-validation-minchecked-message="{{trans('word.Choose at least one')}}">
                                                                <span class="custom-control-indicator"></span>
                                                                <span class="custom-control-description">
                                                                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                                    {{$Tag->ar_title}}
                                                                    @else
                                                                    {{$Tag->title}}
                                                                    @endif
                                                                </span>
                                                            </label>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                       
                                        <div class="row p-t-20">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Minimun Charge')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                                    <div class="controls">
                                                        <div class="input-group"> <span class="input-group-addon">{{trans('word.SR')}}</span>
                                                            <input type="number" id="minicharge" class="form-control" name="minicharge" step="0.01" min="0" oninput="validity.valid||(value='');" >
                                                        <span class="input-group-addon">.00</span> </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Delivery Price per kilo')}}</label>
                                                    <div class="controls">
                                                        <div class="input-group"> <span class="input-group-addon">{{trans('word.SR')}}</span>
                                                            <input type="number" id="deliverypriceperkilo" name="deliverypriceperkilo" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}" step="0.01" min="0" oninput="validity.valid||(value='');">
                                                        <span class="input-group-addon">.00</span> </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.delivery max bill')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                                    <div class="controls">
                                                        <div class="input-group"> <span class="input-group-addon">{{trans('word.SR')}}</span>
                                                            <input type="number" id="delivermaxbill" name="delivermaxbill" class="form-control" step="0.01" min="0" oninput="validity.valid||(value='');">
                                                        <span class="input-group-addon">.00</span> </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <hr>
                                        <div class="row">
                                          <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>{{trans('word.Address')}}</label>
                                                    <div class="controls">
                                                        <div class="input-group">
                                                            <input id="SearchAddress" placeholder="Enter a location" size="50" type="text" class="form-control" name="address" required data-validation-required-message="{{trans('word.This field is required')}}">
                                                            <div id="getlocation" class="btn input-group-addon"><i class=" ti-location-pin"></i></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="lat" class="form-control" name="lat">
                                                <input type="hidden" id="lng" class="form-control" name="lng">
                                            </div>
                                            <div class="col-md-12">
                                                <div id="map"></div>
                                            </div>
                                          <!--/span-->
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label>{{trans('word.Mobile')}}</label>
                                                    <div class="controls">
                                                            <input type="text" name="phone" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}" data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="{{trans('word.No Characters Allowed, Only Numbers')}}" data-validation-regex-regex="(05)[0-9]{8}" data-validation-regex-message="{{trans('word.Invalid Mobile Number')}}">
                                                    </div>
                                                    <div class="form-control-feedback"><small>{{trans('word.phone number must not less than 10')}}</small></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{trans('word.another Mobile')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                                    <div class="controls">
                                                        <input type="text" name="phone2" class="form-control" data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="{{trans('word.No Characters Allowed, Only Numbers')}}" data-validation-regex-regex="(05)[0-9]{8}" data-validation-regex-message="{{trans('word.Invalid Mobile Number')}}">
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Master image Kitchen')}}</label>
                                                    <div class="controls">
                                                        <input type="file" name="image" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">
                                                    </div>
                                                    <div class="form-control-feedback"><small>{{trans('word.image must be type ( jpg,jpeg,png,gif )')}}</small></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.images')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                                    <input type="file" name="images[]" multiple class="form-control">
                                                    <div class="form-control-feedback"><small>{{trans('word.image must be type ( jpg,jpeg,png,gif )')}}</small></div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{trans('word.Save')}}</button>
                                        <button type="button" class="btn btn-inverse" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.modal -->
<div class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h3 class="modal-title text-white" id="myLargeModalLabel">{{trans('word.Kitchen')}}</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection

@section('js')
    {{-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIWl0KLlaJV31fhfzuAul2sJA_u2gQuVA&libraries=places"></script> --}}
    <!-- This is data table -->
    <script src="{{asset('cpanel/en/js/validation.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjpAEh4sxHppRuwnYEr6QmegGIQ-1wuNo&libraries=places&callback=initAutocomplete&v=3.exp&sensor=false" async defer></script>
    <script>
      
        jQuery( document ).ready( function( $ ){
            $( "#getlocation" ).click( function(e) {
                e.preventDefault();
                navigator.geolocation.getCurrentPosition(
                    function( position ){ // success cb
                        var lat = position.coords.latitude;
                        var lng = position.coords.longitude;
                        var google_map_position = new google.maps.LatLng( lat, lng );
                        var google_maps_geocoder = new google.maps.Geocoder();
                        google_maps_geocoder.geocode(
                            { 'latLng': google_map_position },
                            function( results, status ) {
                                $('#lat').val(lat)
                                $('#lng').val(lng)
                                $('#SearchAddress').val(results[0].formatted_address)
                            }
                        );
                        
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 17,
                            center: new google.maps.LatLng(lat, lng),
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });

                        var myMarker = new google.maps.Marker({
                            position: new google.maps.LatLng(lat, lng),
                            draggable: true
                        });

                        google.maps.event.addListener(myMarker, 'dragend', function (evt) {
                            var latt=evt.latLng.lat().toFixed(8);
                            var lngg=evt.latLng.lng().toFixed(8);
                            var addresss = new google.maps.LatLng( latt, lngg );
                            var newgeocoder = new google.maps.Geocoder();
                            newgeocoder.geocode(
                                { 'latLng': addresss },
                                function( result, statu ) {
                                    $('#lat').val(latt)
                                    $('#lng').val(lngg)
                                    $('#SearchAddress').val(result[0].formatted_address)
                                }
                            );
                        });
                        map.setCenter(myMarker.position)
                        myMarker.setMap(map)

                });
            });
        });
        


    function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 24.7241502, lng: 47.3830349},
          zoom: 17,
        });
       

        // Create the search box and link it to the UI element.
        var input = document.getElementById('SearchAddress');
        var searchBox = new google.maps.places.SearchBox(input);
        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': places[0].formatted_address}, function(results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $('#lat').val(latitude)
                $('#lng').val(longitude)
                var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 17,
                            center: new google.maps.LatLng(latitude, longitude),
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });

                        var myMarker = new google.maps.Marker({
                            position: new google.maps.LatLng(latitude, longitude),
                            draggable: true
                        });
                        google.maps.event.addListener(myMarker, 'dragend', function (evt) {
                            var latt=evt.latLng.lat().toFixed(8);
                            var lngg=evt.latLng.lng().toFixed(8);
                            var addresss = new google.maps.LatLng( latt, lngg );
                            var newgeocoder = new google.maps.Geocoder();
                            newgeocoder.geocode(
                                { 'latLng': addresss },
                                function( result, statu ) {
                                    $('#lat').val(latt)
                                    $('#lng').val(lngg)
                                    $('#SearchAddress').val(result[0].formatted_address)
                                }
                            );
                        });
                        map.setCenter(myMarker.position)
                        myMarker.setMap(map)
                } 
            }); 
            

          if (places.length == 0) {
            return;
          }
     
    
        });
    }
    
        //DataTable
        var table = $('#myTable').DataTable({
            dom: 'Bfrtip',
            paging:false,
            buttons: [
                { extend: 'copy', text: '{{trans('word.copy')}}' },
                { extend: 'excel', text: '{{trans('word.excel')}}' },
                { extend: 'pdf', text: '{{trans('word.pdf')}}' },
                { extend: 'print', text: '{{trans('word.print')}}' },
            ],
            @if( LaravelLocalization::getCurrentLocale() == "ar")
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
                }
            @endif
        });
        //End DataTable

            //Select Row From Table
            $('#myTable tbody').on( 'click', 'tr', function () {
                if (event.ctrlKey) {
                    $(this).toggleClass('selected');
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                    $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
                    @else
                    $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
                    @endif
                }else{
                    var isselected = false 
                    var numSelected= table.rows('.selected').data().length
                    if($(this).hasClass('selected') && numSelected==1){
                        isselected = true
                    }
                    $('#myTable tbody tr').removeClass('selected');
                    if(!isselected){
                        $(this).toggleClass('selected');
                    }
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                    $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
                    @else
                    $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
                    @endif
                }
            });
            //End Select Row From Table

            $('.image-popups').magnificPopup({
                delegate: 'a',
                type: 'image',
                removalDelay: 500, //delay removal by X to allow out-animation
                callbacks: {
                beforeOpen: function() {
                    // just a hack that adds mfp-anim class to markup 
                    this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                    this.st.mainClass = this.st.el.attr('data-effect');
                }
                },
                closeOnContentClick: true,
                midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
            });


            //Delete Row
            $("body").on("click", "#delete", function () {
            
                var dataList=[];
                $("#myTable .selected").each(function(index) {
                    dataList.push($(this).find('td:first').text())
                })
                if(dataList.length >0){
                    swal({
                        title: "{{trans('word.Are you sure?')}}",   
                        text: "",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{trans('word.Yes, Sure it!')}}",   
                        cancelButtonText: "{{trans('word.No')}}",
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){
                        if (isConfirm) {
                            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'); 
                            $.ajax({
                            url:'{{url("Delete_Kitchen")}}',
                            type:"post",
                            data:{'id':dataList,_token: CSRF_TOKEN},
                            dataType:"JSON",
                            success: function (data) {
                                if(data.message == "Success")
                                {
                                    $("#myTable .selected").hide();
                                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                                    $('#delete').text('حذف 0 سجل');
                                    @else
                                    $('#delete').text('Delete 0 row');
                                    @endif
                                    swal("{{trans('word.Deleted')}}", "{{trans('word.Message_Delete')}}", "success"); 
                                }else{
                                    swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error"); 
                                }
                            },
                            fail: function(xhrerrorThrown){
                                swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error"); 
                            }
                        });
                        } else {
                            swal("{{trans('word.Cancelled')}}", "{{trans('word.Message_Cancelled_Delete')}}", "error");   
                        }
                    });
                }
            });
            //End Delete Row


   
        ! function(window, document, $) {
            "use strict";
            $("input,select").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);

        $(".switchery-demo").click(function(){
            var id=$(this).data('id')
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "POST",
                url: "{{url('UpdateStatusKitchen')}}",
                data: {"id":id,_token:CSRF_TOKEN},
                success: function (data) {
                }
            })
        })

    $(".edit-kitchen").click(function(){
      var id=$(this).data('id')
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
        type: "GET",
         url: "{{url('Edit_Kitchen')}}",
         data: {"id":id},
         success: function (data) {
              $(".bs-edit-modal-lg .modal-body").html(data)
              $(".bs-edit-modal-lg").modal('show')
              $(".bs-edit-modal-lg").on('hidden.bs.modal',function (e){
                //   $('.bs-edit-modal-lg').empty();
                  $('.bs-edit-modal-lg').hide();
              })
          }
      })
    })

    jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
    });
</script>
<?php
$message=session()->get("message");
?>
@if( session()->has("message"))
    @if( $message == "Success")
        <script>
            swal({   
                title: "{{trans('word.Succeeded')}}",   
                text: "{{trans('word.Operation completed successfully')}}", 
                type:"success" , 
                timer: 1000,   
                showConfirmButton: false 
            });
        </script>
    @elseif ( $message == "Failed")
        <script>
            swal({
                title: "{{trans('word.Sorry')}}",   
                text: "{{trans('word.the operation failed')}}",
                type:"error" , 
                timer: 2000,   
                showConfirmButton: false 
            }); 
        </script>
    @endif
@endif


@endsection
