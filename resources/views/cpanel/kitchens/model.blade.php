
<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-block">
                <form action="{{url('Update_Kitchen')}}" method="POST" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="form-body">
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{trans('word.English Title')}}</label>
                                    <div class="controls">
                                    <input type="text" id="en_title" name="en_title" class="form-control" value="{{$kitchen->en_title}}" required data-validation-required-message="{{trans('word.This field is required')}}">
                                    </div>
                                    <input type="hidden" id="id" name="id" value="{{$kitchen->id}}" class="form-control" required>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{trans('word.Arabic Title')}}</label>
                                    <div class="controls">
                                    <input type="text" id="ar_title" name="ar_title" value="{{$kitchen->ar_title}}" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{trans('word.English Description')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                    <textarea type="text" id="en_description" name="en_description" class="form-control">{{$kitchen->en_description}}</textarea>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{trans('word.Arabic Description')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                    <textarea type="text" id="ar_description" class="form-control" name="ar_description">{{$kitchen->ar_description}}</textarea>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{trans('word.Category')}}</label>
                                    <div class="controls">
                                    <select class="form-control custom-select" name="category" required data-validation-required-message="{{trans('word.This field is required')}}">
                                        @if( LaravelLocalization::getCurrentLocale() == "ar"))
                                            @foreach($Categories as $Category)
                                                @if($kitchen->kitchencategory_id == $Category->id )
                                                    <option selected value="{{$Category->id}}">{{$Category->ar_title}}</option>
                                                @else
                                                    <option value="{{$Category->id}}">{{$Category->ar_title}}</option>
                                                @endif
                                            @endforeach
                                        @else
                                            @foreach($Categories as $Category)
                                                @if($kitchen->kitchencategory_id == $Category->id )
                                                    <option selected value="{{$Category->id}}">{{$Category->title}}</option>
                                                @else
                                                    <option value="{{$Category->id}}">{{$Category->title}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label class="control-label">
                                    {{trans('word.Tag')}}
                                </label>
                                @if( LaravelLocalization::getCurrentLocale() == "ar")
                                    @foreach($Tags as $Tag)
                                    <div class="form-check">
                                        <label class="custom-control custom-checkbox">
                                            <input id="tags" @if(in_array($Tag->id,$array)) checked @endif type="checkbox" name="tags[]" value="{{$Tag->id}}" placeholder="{{$Tag->ar_title}}" class="custom-control-input">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">{{$Tag->ar_title}}</span>
                                        </label>
                                    </div>
                                    @endforeach
                                @else
                                    @foreach($Tags as $Tag)
                                    <div class="form-check">
                                        <label class="custom-control custom-checkbox">
                                            <input id="tags" @if(in_array($Tag->id,$array)) checked @endif type="checkbox" name="tags[]" value="{{$Tag->id}}" placeholder="{{$Tag->title}}" class="custom-control-input">
                                            <span class="custom-control-indicator"></span>
                                            <span class="custom-control-description">{{$Tag->title}}</span>
                                        </label>
                                    </div>
                                    @endforeach
                                @endif
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        
                        <div class="row p-t-20">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{trans('word.Minimun Charge')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                    <div class="controls">
                                        <div class="input-group"> 
                                            <span class="input-group-addon">{{trans('word.SR')}}</span>
                                            <input type="number" id="minicharge" value="{{$kitchen->minicharge}}" class="form-control" name="minicharge" step="0.01" min="0" oninput="validity.valid||(value='');">
                                            <span class="input-group-addon">.00</span> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{trans('word.Delivery Price per kilo')}}</label>
                                    <div class="controls">
                                        <div class="input-group"> 
                                            <span class="input-group-addon">{{trans('word.SR')}}</span>
                                            <input type="number" id="deliverypriceperkilo" value="{{$kitchen->deliverypriceperkilo}}" name="deliverypriceperkilo" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}" step="0.01" min="0" oninput="validity.valid||(value='');">
                                            <span class="input-group-addon">.00</span> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label">{{trans('word.delivery max bill')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                    <div class="controls">
                                        <div class="input-group"> 
                                            <span class="input-group-addon">{{trans('word.SR')}}</span>
                                            <input type="number" id="delivermaxbill" name="delivermaxbill" value="{{$kitchen->delivermaxbill}}" class="form-control" step="0.01" min="0" oninput="validity.valid||(value='');">
                                            <span class="input-group-addon">.00</span> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                        <!--/row-->
                        <h3 class="box-title m-t-30">{{trans('word.Address')}}</h3>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>{{trans('word.Address')}}</label>
                                    <div class="controls">
                                        <div class="input-group">
                                            <input id="editSearchAddress" value="{{$kitchen->address}}" placeholder="Enter a location" size="50" type="text" class="form-control" name="address" required data-validation-required-message="{{trans('word.This field is required')}}">
                                            <div id="editgetlocation" class="btn input-group-addon"><i class=" ti-location-pin"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="editlat" class="form-control" value="{{$kitchen->lat}}" name="lat">
                                <input type="hidden" id="editlng" class="form-control" value="{{$kitchen->lng}}" name="lng">
                            </div>
                            <div class="col-md-12">
                                <div id="editmap"></div>
                            </div>
                            <!--/span-->
                        </div>
                        <div class="row">
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <label>{{trans('word.Mobile')}}</label>
                                    <div class="controls">
                                    <input type="text" id="phone" name="phone" value="{{$kitchen->phone}}" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}" data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="{{trans('word.No Characters Allowed, Only Numbers')}}" data-validation-regex-regex="(05)[0-9]{8}" data-validation-regex-message="{{trans('word.Invalid Mobile Number')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>{{trans('word.another Mobile')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                    <input type="text" id="phone2" name="phone2" value="{{$kitchen->phone2}}" class="form-control" data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="{{trans('word.No Characters Allowed, Only Numbers')}}" data-validation-regex-regex="(05)[0-9]{8}" data-validation-regex-message="{{trans('word.Invalid Mobile Number')}}">
                                </div>
                            </div>
                        </div>
                        <div class="row p-t-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">{{trans('word.Master image Kitchen')}}</label>
                                    <input type="file" name="image" class="form-control">
                                </div>
                            </div>
                            <!--/span-->
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{trans('word.Save')}}</button>
                        <button type="button" class="btn btn-inverse" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('cpanel/en/js/validation.js')}}"></script>
{{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9lejt5buMBaloCEE5XIqA1sH9dvp14Rc&libraries=places&callback=initAutocomplete&v=3.exp&sensor=false" async defer></script> --}}
    <script>
        jQuery( document ).ready( function( $ ){
        var map = new google.maps.Map(document.getElementById('editmap'), {
            zoom: 17,
            center: new google.maps.LatLng({{$kitchen->lat}}, {{$kitchen->lng}}),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var myMarker = new google.maps.Marker({
            position: new google.maps.LatLng({{$kitchen->lat}}, {{$kitchen->lng}}),
            draggable: true
        });

        google.maps.event.addListener(myMarker, 'dragend', function (evt) {
            var latt=evt.latLng.lat().toFixed(8);
            var lngg=evt.latLng.lng().toFixed(8);
            var addresss = new google.maps.LatLng( latt, lngg );
            var newgeocoder = new google.maps.Geocoder();
            newgeocoder.geocode(
                { 'latLng': addresss },
                function( result, statu ) {
                    $('#editlat').val(latt)
                    $('#editlng').val(lngg)
                    $('#editSearchAddress').val(result[0].formatted_address)
                }
            );
        });
        map.setCenter(myMarker.position)
        myMarker.setMap(map)
        // Create the search box and link it to the UI element.
        var input = document.getElementById('editSearchAddress');
        var searchBox = new google.maps.places.SearchBox(input);
        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { 'address': places[0].formatted_address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    $('#editlat').val(latitude)
                    $('#editlng').val(longitude)
                    var map = new google.maps.Map(document.getElementById('editmap'), {
                        zoom: 17,
                        center: new google.maps.LatLng(latitude, longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                    var myMarker = new google.maps.Marker({
                        position: new google.maps.LatLng(latitude, longitude),
                        draggable: true
                    });
                    google.maps.event.addListener(myMarker, 'dragend', function (evt) {
                        var latt=evt.latLng.lat().toFixed(8);
                        var lngg=evt.latLng.lng().toFixed(8);
                        var addresss = new google.maps.LatLng( latt, lngg );
                        var newgeocoder = new google.maps.Geocoder();
                        newgeocoder.geocode(
                            { 'latLng': addresss },
                            function( result, statu ) {
                                $('#editlat').val(latt)
                                $('#editlng').val(lngg)
                                $('#editSearchAddress').val(result[0].formatted_address)
                            }
                        );
                    });
                    map.setCenter(myMarker.position)
                    myMarker.setMap(map)
                } 
            });
            if (places.length == 0) {
            return;
            }
        });

            $( "#editgetlocation" ).click( function(e) {
                e.preventDefault();
                navigator.geolocation.getCurrentPosition(
                    function( position ){ // success cb
                        var lat = position.coords.latitude;
                        var lng = position.coords.longitude;
                        var google_map_position = new google.maps.LatLng( lat, lng );
                        var google_maps_geocoder = new google.maps.Geocoder();
                        google_maps_geocoder.geocode(
                            { 'latLng': google_map_position },
                            function( results, status ) {
                                $('#editlat').val(lat)
                                $('#editlng').val(lng)
                                $('#editSearchAddress').val(results[0].formatted_address)
                            }
                        );
                        var map = new google.maps.Map(document.getElementById('editmap'), {
                            zoom: 17,
                            center: new google.maps.LatLng(lat, lng),
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });
                        var myMarker = new google.maps.Marker({
                            position: new google.maps.LatLng(lat, lng),
                            draggable: true
                        });
                        google.maps.event.addListener(myMarker, 'dragend', function (evt) {
                            var latt=evt.latLng.lat().toFixed(8);
                            var lngg=evt.latLng.lng().toFixed(8);
                            var addresss = new google.maps.LatLng( latt, lngg );
                            var newgeocoder = new google.maps.Geocoder();
                            newgeocoder.geocode(
                                { 'latLng': addresss },
                                function( result, statu ) {
                                    $('#editlat').val(latt)
                                    $('#editlng').val(lngg)
                                    $('#editSearchAddress').val(result[0].formatted_address)
                                }
                            );
                        });
                        map.setCenter(myMarker.position)
                        myMarker.setMap(map)
                });
            });
        });
    

    ! function(window, document, $) {
            "use strict";
            $("input,select").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);
</script>