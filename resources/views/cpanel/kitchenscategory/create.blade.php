@extends('layouts.cpanel_layout.cpanel_layout')
@section('title')
Add New Kitchen Category
@endsection
@section('moduleoptions')
  <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
    <li class="nav-item active">
      <a href="{{ route('kitchenscategory.create') }}" class="nav-link">
        <i class="mdi mdi-plus"></i>Add New Kitchen Category
      </a>
      </li>
      <li class="nav-item active">
        <a href="{{ route('kitchenscategory.index') }}" class="nav-link">
          <i class="mdi mdi-arrow-left"></i>Back To list All Categories
        </a>
        </li>
      {{-- <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
        </li> --}}
      </ul>
    @endsection
@section('content')
  @if (\Session::has('success'))
    <div class="col-12">
    <div class="row purchace-popup alert alert-success">
        <strong>Success!</strong>{{ session()->get('success') }}

          <i class="mdi mdi-close popup-dismiss"></i>
      </div>
    </div>
  @endif

      <div class="row">
          <div class="col-12 grid-margin">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Add New Kitchens Category</h4>
                <form class="form-sample"  action="{{route('kitchenscategory.store')}}" method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}

                  <p class="card-description">
                    Kitchen Category info
                  </p>
                  <div class="row">
                          <div class="col-md-6">
                            <div class="form-group row">
                              <label class="col-sm-3 col-form-label"> Title </label>
                              <div class="col-sm-9">
                                <input class="form-control" type="text" name="title">
                              </div>
                            </div>
                          </div>

                            <div class="col-md-6">
                              <div class="form-group row">
                                <div class="col-sm-6" style="width:50%;margin:auto">
                                  <label for="file-upload" class="custom-file-upload">
                                    <i class="mdi mdi-cloud-upload"></i>  Upload  Images
                                  </label>
                                  <input id="file-upload" type="file" name="image" multiple accept="image/"/>
                                </div>
                              </div>
                            </div>
                  </div>

                  <button type="submit"  class="btn btn-success mr-2">Submit</button>

                </form>
              </div>
            </div
          </div>
      </div>
@endsection
