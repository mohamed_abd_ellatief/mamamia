@extends('layouts.cpanel_layout.cpanel_layout')
@section('title')
All Kitchens Tags
@endsection
@section('moduleoptions')
  <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
    <li class="nav-item active">
      <a href="{{ route('kitchenstag.create') }}" class="nav-link">
        <i class="mdi mdi-plus"></i>Add New Kitchen Tag
      </a>
      </li>
      {{-- <li class="nav-item">
        <a href="#" class="nav-link">
          <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>
        </li> --}}
      </ul>
    @endsection
@section('content')

  @if (\Session::has('success'))
    <div class="col-12">
    <div class="row purchace-popup alert alert-success">
        <strong>Success!</strong>{{ session()->get('success') }}

          <i class="mdi mdi-close popup-dismiss"></i>
      </div>
    </div>
  @endif




      <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Kitchen Tags Table</h4>
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                      <tr>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>


                    @foreach ($allkitchenstags as $onekitchentag)

                      <tr>


                        <td class="py-1">{{ $onekitchentag->title }}</td>
                        <td class="py-1"><img src="{{ url('resources/assets/cpanel/dist/img/kitchen/kitchentag',$onekitchentag->image) }}" alt="image" /></td>

                        <td>
                                <a type="button" class="btn btn-icons btn-rounded btn-primary" href="{{ route('kitchenstag.edit',$onekitchentag->id) }}" style="color:#fff">
                                  <i class="mdi mdi-update"></i>
                                </a>

                              <form method="post" action="{{ route('kitchenstag.destroy',$onekitchentag->id) }}" style="display:inline">
                                {{ csrf_field () }}
                                {{method_field('Delete')}}
                                <button class="btn btn-icons btn-rounded btn-danger"  href=""style="color:#fff">
                                   <i class="mdi mdi-delete-empty"></i>
                                </button>
                              </form>
                    </td>
                              </tr>
                    @endforeach
                  </tbody>
                </table>

                {{ $allkitchenstags->links() }}
              </div>
            </div>
          </div>
        </div>

      </div>
@endsection
