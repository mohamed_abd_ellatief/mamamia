@inject('Notifications','App\Notification')
@if($Notifications->data() != false)
  @foreach($Notifications->data() as $Notification)
    @if(LaravelLocalization::getCurrentLocale() == 'ar')
    <a href="#" class="Ordered" data-type="{{$Notification->type_order}}" data-id="{{$Notification->order_id}}">
        <div class="mail-contnet">
            <h5></h5> 
            <span class="mail-desc">{{$Notification->ar_description}}</span> 
            <span class="time">{{$Notification->ar_ago}}</span> 
        </div>
    </a>
    @else
    <a href="#" class="Ordered" data-type="{{$Notification->type_order}}" data-id="{{$Notification->order_id}}">
        <div class="mail-contnet">
            <h5></h5> 
            <span class="mail-desc">{{$Notification->en_description}}</span> 
            <span class="time">{{$Notification->en_ago}}</span> 
        </div>
    </a>
    @endif
  @endforeach
@endif

    <script>
    $(".Ordered").click(function(){
        var id=$(this).data('id')
        var type_order =$(this).data('type')
        //   var CSRF_TOKEN = {{ csrf_field() }};
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        if(type_order == "meal"){
            $.ajax({
                type: "POST",
                url: "{{url(LaravelLocalization::getCurrentLocale().'/OrderDetails')}}",
                data: {"id":id,_token:CSRF_TOKEN},
                success: function (data) {

                    $(".bs-OrderMaster-modal-lg .modal-body").html(data)
                    $(".bs-OrderMaster-modal-lg").modal('show')
                    
                    $(".bs-OrderMaster-modal-lg").on('hidden.bs.modal',function (e){
                    //   $('.OrderMaster').empty();
                        $('.bs-OrderMaster-modal-lg').hide();
                    })
                }
            })
        }else if(type_order == "banquet"){
            $.ajax({
                type: "POST",
                url: "{{url(LaravelLocalization::getCurrentLocale().'/BanquetDetails')}}",
                data: {"id":id,_token:CSRF_TOKEN},
                success: function (data) {

                    $(".bs-OrderMaster-modal-lg .modal-body").html(data)
                    $(".bs-OrderMaster-modal-lg").modal('show')
                    
                    $(".bs-OrderMaster-modal-lg").on('hidden.bs.modal',function (e){
                    //   $('.OrderMaster').empty();
                        $('.bs-OrderMaster-modal-lg').hide();
                    })
                }
            })   
        }
    })
    </script>

    
