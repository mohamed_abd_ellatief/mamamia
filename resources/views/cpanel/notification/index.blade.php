@extends('layouts.cpanel_layout.master')
    @section('title')
        {{trans('word.Orders')}}
    @endsection

@section('css')
<!--alerts CSS -->
<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">{{trans('word.Orders')}}</h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('word.Orders')}}</li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <button id="delete" class="btn pull-right hidden-sm-down btn-danger m-l-10">Delete 0 row</button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    @if($errors->any())
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i>warning!</h4>
        <ul>
          @foreach($errors->all() as $error)
          <li style="font-weight: 40"><h4 style="font-weight:40">{{$error}}</h4></li>
          @endforeach
        </ul>
      </div>
    @endif

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">{{trans('word.Orders')}}</h4>
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped display" style="width:100%">
                            <thead>
                                <tr>
                                    <th># </th>
                                    <th>{{trans('word.Order Name')}} </th>
                                    <th>{{trans('word.Kitchen Name')}} </th>
                                    <th>{{trans('word.type')}} </th>
                                    <th>{{trans('word.Price')}} </th>
                                    <th>{{trans('word.Status')}} </th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($Orders as $Order)
                                <tr id="{{$Order->id}}">
                                    <td>{{$Order->id}}</td>
                                    <td>{{$Order->title}}</td>
                                    <td>{{$Order->kitchen_title}}</td>
                                    <td>
                                        @if($Order->type == "banquet")
                                        <h3>{{trans('word.client banquet')}}</h3>
                                        @else
                                        <h3>{{trans('word.meal')}}</h3>
                                        @endif
                                    </td>
                                    <td>{{$Order->totalprice}} SR</td>
                                    <td>
                                        @if($Order->type == "meal")
                                        <a class="btn btn-success text-white status" data-id="{{$Order->id}}" data-status="1">{{trans('word.accept')}}</a>
                                        @else
                                        <button class="btn btn-success text-white" disabled>{{trans('word.send')}}</button>
                                        @endif
                                        <a class="btn btn-danger text-white status" data-id="{{$Order->id}}" data-status="-1">{{trans('word.reject')}}</a>
                                        @if($Order->type == "meal")
                                        <a data-toggle="modal" data-target=".bs-Edit-modal-lg" data-id="{{$Order->id}}" class="btn btn-info text-white edit" href="#" data-toggle="tooltip" data-original-title="view">
                                            {{trans('word.view')}}
                                        </a>
                                        @else
                                        <a data-toggle="modal" data-target=".bs-Edit-modal-lg" data-id="{{$Order->id}}" class="btn btn-info text-white editBanquet" href="#" data-toggle="tooltip" data-original-title="view">
                                            {{trans('word.view')}}
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

<!-- Edit modal content -->
<div class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h4 class="modal-title m-b-0 text-white" id="myLargeModalLabel">{{trans('word.Meal')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

@endsection

@section('js')

<!-- This is data table -->
<script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
    
    <script>
  
    $('#myTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
                { extend: 'copy', text: '{{trans('word.copy')}}' },
                { extend: 'excel', text: '{{trans('word.excel')}}' },
                { extend: 'pdf', text: '{{trans('word.pdf')}}' },
                { extend: 'print', text: '{{trans('word.print')}}' },
            ],
        @if( LaravelLocalization::getCurrentLocale() == "ar")
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
            }
        @endif
        
    });


    $(".edit").click(function(){
      var id=$(this).data('id')
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
        type: "POST",
        url: "{{url(LaravelLocalization::getCurrentLocale().'/OrderDetails')}}",
        data: {"id":id,_token:CSRF_TOKEN},
        success: function (data) {
            $(".bs-edit-modal-lg .modal-body").html(data)
            $(".bs-edit-modal-lg").modal('show')
            $(".bs-edit-modal-lg").on('hidden.bs.modal',function (e){
                $('.bs-edit-modal-lg').hide();
            })
        }
      })
    })

    $(".editBanquet").click(function(){
      var id=$(this).data('id')
    //   var CSRF_TOKEN = {{ csrf_field() }};
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
        type: "POST",
        url: "{{url(LaravelLocalization::getCurrentLocale().'/BanquetDetails')}}",
        data: {"id":id,_token:CSRF_TOKEN},
        success: function (data) {
            $(".bs-edit-modal-lg .modal-body").html(data)
            $(".bs-edit-modal-lg").modal('show')
            $(".bs-edit-modal-lg").on('hidden.bs.modal',function (e){
            //   $('.bs-edit-modal-lg').empty();
                $('.bs-edit-modal-lg').hide();
            })
        }
      })
    })
    
   

    $(".status").click(function(){
      var id=$(this).data('id')
      var status=$(this).data('status')
      var tr=$(this).parent().parent()
      console.log(tr[0])
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      swal({
            title: "Are you sure?",   
            text: "",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, Sure it!",   
            cancelButtonText: "No, cancel please!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){
            if (isConfirm) {
                $.ajax({
                url:'{{url("updateStatus")}}',
                type:"post",
                data:{'id':id,'status':status,_token: CSRF_TOKEN},
                dataType:"JSON",
                success: function (data) {
                    if(data.message == "accepted")
                    {
                        $(tr[0]).hide()
                        // $(tr[0]).text(null)
                        // $(tr[0]).append("<div class='label label-table label-primary'>accepted</div>")
                        swal("accepted!", "Order has been accepted.", "success"); 
                    }else if(data.message == "refusal")
                    {
                        $(tr[0]).hide()
                        // $(tr[0]).text(null)
                        // $(tr[0]).append("<div class='label label-table label-danger'>refusal</div>")
                        swal("refusal!", "Order has been refusal.", "success"); 
                    }else{
                        swal("Sorry", "Your imaginary file is safe :)", "error"); 
                    }
                },
                fail: function(xhrerrorThrown){
                    swal("Sorry", "Your imaginary file is safe :)", "error"); 
                }
            
            });
                  
            } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");   
            }
        });
    })


    jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
    });
</script>
<script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>


@endsection
