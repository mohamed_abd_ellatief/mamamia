@extends('layouts.cpanel_layout.master')
@section('title')
{{trans('word.Meal Availability')}}
@endsection

@section('css')
<!--alerts CSS -->
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">
                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                        {{$Meal->ar_title}}
                        @else
                        {{$Meal->en_title}}
                    @endif
            </h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('word.Meal Availability')}}</li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <button id="delete" class="btn pull-right hidden-sm-down btn-danger m-l-10">{{trans('word.Delete 0 row')}}</button>
            <button data-toggle="modal" data-target=".time-meal-modal" class="btn pull-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> {{trans('word.Create')}}</button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    @if($errors->any())
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i>warning!</h4>
        <ul>
          @foreach($errors->all() as $error)
          <li style="font-weight: 40"><h4 style="font-weight:40">{{$error}}</h4></li>
          @endforeach
        </ul>
      </div>
    @endif

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('word.From')}}</th>
                                    <th>{{trans('word.To')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $dt = Carbon\Carbon::now();
                                    $count=1;
                                ?>
                              @foreach($Times as $Time)
                                <tr data-id="{{$Time->id}}">
                                    <td>{{$count}}</td>
                                    {{-- <td>{{date('s:i',$Availblehours->getAvailblehours($Restaurant->id,$Day->id,'kitchen')->from)}}</td> --}}
                                    <td>{{$Time->from}}</td>
                                    <td>{{$Time->to}}</td>
                                  </tr>
                                  <?php $count++; ?>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

<!-- insert modal content -->
<div class="modal fade time-meal-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h4 class="modal-title m-b-0 text-white" id="myLargeModalLabel">{{trans('word.Meal Availability')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-block">
                                <form action="{{url('storeTimeToKitchen')}}" method="POST" novalidate>
                                    @csrf
                                    <div class="form-body">
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.From')}}</label>
                                                    <input name="attribute_id" type="hidden" value="{{$Meal->id}}">
                                                    <input name="type" type="hidden" value="meal">
                                                    <div class="controls">
                                                        <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                                            <input name="from" type="text" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}" >
                                                            <span class="input-group-addon"> <span class="fa fa-clock-o"></span> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.To')}}</label>
                                                    <div class="controls">
                                                        <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true">
                                                            <input name="to" type="text" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}" >
                                                            <span class="input-group-addon"> <span class="fa fa-clock-o"></span> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{trans('word.Save')}}</button>
                                        <button type="button" class="btn btn-inverse" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection

@section('js')
<script src="{{asset('cpanel/en/js/validation.js')}}"></script>
<!-- This is data table -->
<script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<!-- Clock Plugin JavaScript -->
<script src="{{asset('cpanel/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>
    <!-- Sweet-Alert  -->
<script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
    
    <script>

        @if(isset($message) == "success")
        $(".time-meal-modal").modal('show')
        @endif

        var table = $('#myTable').DataTable({
            searching: false,
            paging: false,
            "info": false,
            @if( LaravelLocalization::getCurrentLocale() == "ar")
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
            }
            @endif
        });

        $('.clockpicker').clockpicker({
            donetext: 'Done',
        })


        $('#myTable tbody').on( 'click', 'tr', function () {
            if (event.ctrlKey) {
                $(this).toggleClass('selected');
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
                @else
                $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
                @endif
            
            }else{
                var isselected = false 
                if($(this).hasClass('selected')){
                    isselected = true
                }
                $('#myTable tbody tr').removeClass('selected');
                if(!isselected){
                    $(this).toggleClass('selected');
                }
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
                @else
                $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
                @endif
            }
        });
 
        $("body").on("click", "#delete", function () {
            
            var dataList=[];
            $("#myTable .selected").each(function(index) {
                dataList.push($(this).closest('tr').data('id'))
            })
            if(dataList.length >0){
        swal({
            title: "{{trans('word.Are you sure?')}}",   
            text: "",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "{{trans('word.Yes, Sure it!')}}",   
            cancelButtonText: "{{trans('word.No')}}",    
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'); 
                $.ajax({
                url:'{{url("DeleteKitchenTimes")}}',
                type:"post",
                data:{'id':dataList,_token: CSRF_TOKEN},
                dataType:"JSON",
                success: function (data) {
                    if(data.message == "Success")
                    {
                        $("#myTable .selected").hide();
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        $('#delete').text('حذف 0 سجل');
                        @else
                        $('#delete').text('Delete 0 row');
                        @endif
                        swal("{{trans('word.Deleted')}}", "{{trans('word.Message_Delete')}}", "success");
                    }else{
                        swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error"); 
                    }
                },
                fail: function(xhrerrorThrown){
                    swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error"); 
                }
            
            });
                  
            } else {     
                swal("{{trans('word.Cancelled')}}", "{{trans('word.Message_Cancelled_Delete')}}", "error");
            } 
        });       
    }

        });   

        ! function(window, document, $) {
            "use strict";
            $("input,select,textarea").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);

        jQuery(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });
        });
    </script>

<?php
$message=session()->get("message");
?>
@if( session()->has("message"))
  @if( $message == "Success")
    <script>
        swal({   
            title: "{{trans('word.Succeeded')}}",   
            text: "{{trans('word.Operation completed successfully')}}", 
            type:"success" , 
            timer: 1000,   
            showConfirmButton: false 
        });
    </script>
  @elseif ( $message == "Failed")
    <script>
        swal({
            title: "{{trans('word.Sorry')}}",   
            text: "{{trans('word.the operation failed')}}",
            type:"error" , 
            timer: 2000,   
            showConfirmButton: false 
        }); 
    </script>
  @endif
@endif


@endsection