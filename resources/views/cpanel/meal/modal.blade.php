<div class="row">
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-block">
                    <form action="{{url('Update_Meal')}}" method="POST" enctype="multipart/form-data" novalidate>
                        @csrf
                        <div class="form-body">
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('word.English Title')}}</label>
                                        <input type="hidden" name="id" value="{{$Meal->id}}" class="form-control">
                                        <div class="controls">
                                        <input type="text" id="en_title" name="en_title" value="{{$Meal->en_title}}" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('word.Arabic Title')}}</label>
                                        <div class="controls">
                                        <input type="text" id="ar_title" name="ar_title" value="{{$Meal->ar_title}}" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row p-t-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('word.English Description')}}</label>
                                        <div class="controls">
                                        <textarea type="text" id="en_description" name="en_description" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">{{$Meal->en_description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('word.Arabic Description')}}</label>
                                        <div class="controls">
                                        <textarea type="text" id="ar_description" class="form-control" name="ar_description" required data-validation-required-message="{{trans('word.This field is required')}}">{{$Meal->ar_description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('word.Tag')}}</label>
                                        <select class="form-control custom-select edit_tags_meals" name="tag" required data-validation-required-message="{{trans('word.This field is required')}}">
                                            @if( LaravelLocalization::getCurrentLocale() == "ar")
                                            @foreach($Tags as $Tag)
                                                @if($Tag->id == $Meal->kitchentag_id)
                                                    <option selected value="{{$Tag->id}}">{{$Tag->ar_title}}</option>
                                                @else
                                                    <option value="{{$Tag->id}}">{{$Tag->ar_title}}</option>
                                                @endif
                                            @endforeach
                                            @else
                                            @foreach($Tags as $Tag)
                                                @if($Tag->id == $Meal->kitchentag_id)
                                                    <option selected value="{{$Tag->id}}">{{$Tag->title}}</option>
                                                @else
                                                    <option value="{{$Tag->id}}">{{$Tag->title}}</option>
                                                @endif
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('word.Component')}}  <small class="text-danger">({{trans('word.optional')}})</small></label>
                                        <div class="controls">
                                        <input type="text" id="component" value="{{$Meal->component}}" class="form-control" name="component">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/row-->

                            <div class="row">
                                <div class="col-md-6 edit_meal_category">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('word.Meals Category')}}</label>
                                        <div class="form-check">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" id="select_all_edit_meal_category" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{trans('word.Select All')}}</span>
                                            </label>
                                        </div>
                                        <div class="controls">
                                            @foreach($CategoriesMeals as $CategoryMeal)
                                            <div class="form-check">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="Eventcategory[]" value="{{$CategoryMeal->id}}" @if(in_array($CategoryMeal->id,$array)) checked @endif class="custom-control-input select_all_edit_meal_category" data-validation-minchecked-minchecked="1" data-validation-minchecked-message="{{trans('word.Choose at least one')}}">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">
                                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                        {{$CategoryMeal->ar_title}}
                                                        @else
                                                        {{$CategoryMeal->en_title}}
                                                        @endif
                                                    </span>
                                                </label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 edit_banquet_category">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('word.Banquets Category')}}</label>
                                        <div class="form-check">
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" id="select_all_edit_banquet_category" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">{{trans('word.Select All')}}</span>
                                            </label>
                                        </div>
                                        <div class="controls">
                                            @foreach($CategoriesBanquets as $CategoryBanquet)
                                            <div class="form-check">
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="Eventcategory[]" @if(in_array($CategoryBanquet->id,$array)) checked @endif value="{{$CategoryBanquet->id}}" class="custom-control-input select_all_edit_banquet_category" data-validation-minchecked-minchecked="1" data-validation-minchecked-message="{{trans('word.Choose at least one')}}">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">
                                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                        {{$CategoryBanquet->ar_title}}
                                                        @else
                                                        {{$CategoryBanquet->en_title}}
                                                        @endif
                                                    </span>
                                                </label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row p-t-20">
                                <!--/span-->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('word.Price')}}</label>
                                        <div class="controls">
                                        <input type="number" name="price" value="{{$Meal->price}}" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}" step="0.01" min="0" oninput="validity.valid||(value='');">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('word.Preparation time')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                        <div class="controls">
                                        <input type="number" name="neededtime" value="{{$Meal->neededtime}}" class="form-control"  min="0" oninput="validity.valid||(value='');">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('word.Customer Number')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                        <input type="number" name="customer_num" value="{{$Meal->customer_num}}" class="form-control" min="0" oninput="validity.valid||(value='');">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <div class="row p-t-20">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('word.Master image Meal')}}</label>
                                        <input type="file" name="image" class="form-control">
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{trans('word.Save')}}</button>
                            <button type="button" class="btn btn-inverse" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<script src="{{asset('cpanel/en/js/validation.js')}}"></script>
<script>

    if($(".edit_tags_meals").val() == '1'){
        $('.edit_meal_category').addClass('hide');
        $("#select_all_edit_meal_category").prop('checked', false);
        $(".select_all_edit_meal_category").prop('checked', false);
    }else{
        $('.edit_banquet_category').addClass('hide');
        $("#select_all_edit_banquet_category").prop('checked', false);
        $(".select_all_edit_banquet_category").prop('checked', false);
    }


    $(".edit_tags_meals").on('change', function() {
        if ($(this).val() == '1'){
            //
            $('.edit_banquet_category').removeClass('hide');
            $('.edit_meal_category').addClass('hide');
        } else {
            //
            $('.edit_banquet_category').addClass('hide');
            $('.edit_meal_category').removeClass('hide');
        }
    });


    //select all checkboxes
    $("#select_all_edit_meal_category").change(function(){  //"select all" change
        $(".select_all_edit_meal_category").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
    });

    //".checkbox" change
    $('.select_all_edit_meal_category').change(function(){
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if(false == $(this).prop("checked")){ //if this item is unchecked
            $("#select_all_edit_meal_category").prop('checked', false); //change "select all" checked status to false
        }
        //check "select all" if all checkbox items are checked
        if ($('.select_all_edit_meal_category:checked').length == $('.select_all_edit_meal_category').length ){
            $("#select_all_edit_meal_category").prop('checked', true);
        }
    });
        if ($('.select_all_edit_meal_category:checked').length == $('.select_all_edit_meal_category').length ){

            $("#select_all_edit_meal_category").prop('checked', true);
        }


    //select all checkboxes
    $("#select_all_edit_banquet_category").change(function(){  //"select all" change
        $(".select_all_edit_banquet_category").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
    });

    //".checkbox" change
    $('.select_all_edit_banquet_category').change(function(){
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if(false == $(this).prop("checked")){ //if this item is unchecked
            $("#select_all_edit_banquet_category").prop('checked', false); //change "select all" checked status to false
        }
        //check "select all" if all checkbox items are checked
        if ($('.select_all_edit_banquet_category:checked').length == $('.select_all_edit_banquet_category').length ){
            $("#select_all_edit_banquet_category").prop('checked', true);
        }
    });
        //check "select all" if all checkbox items are checked
        if ($('.select_all_edit_banquet_category:checked').length == $('.select_all_edit_banquet_category').length ){
            $("#select_all_edit_banquet_category").prop('checked', true);
        }


    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation()
    }(window, document, jQuery);

</script>
