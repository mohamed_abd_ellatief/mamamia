@extends('layouts.cpanel_layout.master')
@section('title')
{{trans('word.Images Meal')}}
@endsection

@section('css')
<!--alerts CSS -->

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">

<link href="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />

@endsection

@section('content')
<div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-6 col-8 align-self-center">
                <h3 class="text-themecolor m-b-0 m-t-0">( @if( LaravelLocalization::getCurrentLocale() == "ar") {{$Meal->ar_title}} @else {{$Meal->en_title}} @endif ) {{trans('word.Images Meal')}}</h3>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                    <li class="breadcrumb-item active">{{trans('word.Images Meal')}}</li>
                </ol>
            </div>
            <div class="col-md-6 col-4 align-self-center">
                <button data-toggle="modal" data-target=".bs-example-modal-lg" class="btn pull-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> {{trans('word.Create')}}</button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
                            <div class="row el-element-overlay">
                            @foreach($Images as $Image)
                                    <div class="col-lg-3 col-md-6">
                                        <div class="card">
                                            <div class="el-card-item">
                                                <div class="el-card-avatar el-overlay-1"> <img src="{{asset('cpanel/upload/meal/'.$Image->image)}}" alt="user" />
                                                    <div class="el-overlay">
                                                        <ul class="el-info">
                                                            <li><a class="btn default btn-outline image-popup-vertical-fit" href="{{asset('cpanel/upload/meal/'.$Image->image)}}"><i class="icon-magnifier"></i></a></li>
                                                            <li><a class="btn default btn-outline delete" data-id="{{$Image->id}}" href="javascript:void(0);"><i class="icon-close"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>

<!-- insert modal content -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h4 class="modal-title m-b-0 text-white" id="myLargeModalLabel">{{trans('word.Images Meal')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-block">
                                <form action="{{url('StoreImagesMeal')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-body">
                                        <h3 class="card-title">{{trans('word.Add Images Meal')}}</h3>
                                        <hr>
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.images')}}</label>
                                                    <input type="file" name="images[]" multiple required>
                                                    <input type="hidden" name="meal_id" value="{{$Meal->id}}">
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{trans('word.Save')}}</button>
                                        <button type="button" class="btn btn-inverse" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection

@section('js')
<!-- This is data table -->
<script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<!-- start - This is for export functionality only -->

    <!-- Magnific popup JavaScript -->
    <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js')}}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
    
    <script>

   
$("body").on("click", ".delete", function () {
  
    var id=$(this).data('id')
    var div = $(this).parent().parent().parent().parent().parent().parent().parent()
    // console.log(id)
        swal({
            title: "Are you sure?",   
            text: "You will not be able to recover this imaginary file!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel plx!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){
            if (isConfirm) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content'); 
                $.ajax({
                url:'{{url("Delete_ImagesMeal")}}',
                type:"POST",
                data:{'id':id,_token: CSRF_TOKEN},
                dataType:"JSON",
                success: function (data) {
                    if(data.message == "Success")
                    {
                        div.hide();
                        swal("Deleted!", "Your Record has been deleted.", "success"); 
                    }else{
                        swal("Sorry", "Your imaginary file is safe :)", "error"); 
                    }
                },
                fail: function(xhrerrorThrown){
                    swal("Sorry", "Your imaginary file is safe :)", "error"); 
                }
            
            });
                  
            } else {
                swal("Cancelled", "Your imaginary file is safe :)", "error");   
            }
        });
});

   
       

    

    jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
    });
</script>
<?php
$message=session()->get("message");
?>
@if( session()->has("message"))
  @if( $message == "Success")
    <script>
    swal("Successfully!", "Operation completed successfully.", "success"); 
    </script>
  @elseif ( $message == "Failed")
    <script>
      swal("Sorry", "Oops, the operation failed :(", "error");  
    </script>
  @endif
@endif
<script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>


@endsection
