@extends('layouts.cpanel_layout.master')
@section('title')
{{trans('word.Meals')}}
@endsection

@section('css')
<!--alerts CSS -->

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css">

<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">
                @if( LaravelLocalization::getCurrentLocale() == "ar")
                {{$Restaurant->ar_title}}
                @else
                {{$Restaurant->en_title}}
                @endif
            </h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">{{trans('word.Home')}}</a></li>
                <li class="breadcrumb-item active">{{trans('word.Meals')}}</li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <button id="delete" class="btn pull-right hidden-sm-down btn-danger m-l-10">{{trans('word.Delete 0 row')}}</button>
            <button data-toggle="modal" data-target=".bs-example-modal-lg" class="btn pull-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> {{trans('word.Create')}}</button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->

    @if($errors->any())
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i>warning!</h4>
        <ul>
          @foreach($errors->all() as $error)
          <li style="font-weight: 40"><h4 style="font-weight:40">{{$error}}</h4></li>
          @endforeach
        </ul>
      </div>
    @endif

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('word.Title')}}</th>
                                    <th>{{trans('word.Description')}}</th>
                                    <th>{{trans('word.Status')}}</th>
                                    <th>{{trans('word.image')}}</th>
                                    <th>{{trans('word.Tags')}}</th>
                                    <th>{{trans('word.Price')}}</th>
                                    <th>{{trans('word.Meals Category')}}</th>
									                                    <th>@if(Request::segment(1) == 'ar') الاضافات @else Additions  @endif</th>
                                    @if(Auth::user()->category == 'vendor')
                                    <th>{{trans('word.Action')}}</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($Meals as $Meal)
                                <tr id="{{$Meal->id}}">
                                    <td>{{$Meal->id}}</td>
                                    <td>
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                        {{$Meal->ar_title}}
                                        @else
                                        {{$Meal->en_title}}
                                        @endif
                                    </td>
                                    <td>
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                        {{str_limit($Meal->ar_description,20)}}
                                        @else
                                        {{str_limit($Meal->en_description,20)}}
                                        @endif
                                    </td>
                                    <td>
                                        <div class="switchery-demo m-b-30" data-id="{{$Meal->id}}">
                                            <input type="checkbox" name="status" @if($Meal->status == "on") checked @endif class="js-switch UpdateStatus" data-color="#009efb" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="image-popups">
                                            {{-- <img width="150px" src="{{asset('cpanel/upload/kitchen/'.$Kitchen->image)}}"/> --}}
                                            <a href="{{asset('cpanel/upload/meal/'.$Meal->image)}}" data-effect="mfp-newspaper">
                                                {{-- <img src="../assets/images/big/img2.jpg" class="img-responsive" /> --}}
                                                <img style="width:100px" src="{{asset('cpanel/upload/meal/'.$Meal->image)}}" class="img-responsive"/>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                        {{\App\Kitchentag::find($Meal->kitchentag_id)->ar_title}}
                                        @else
                                        {{\App\Kitchentag::find($Meal->kitchentag_id)->title}}
                                        @endif
                                    </td>
                                    <td>{{$Meal->price}}</td>
                                    <td>
                                        @inject('Categories','App\Eventcategory')
                                        @if($Categories->getCategories($Meal->id))
                                        @foreach($Categories->getCategories($Meal->id) as $Category )
                                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                                        <p>{{$Category->ar_title}}</p>
                                        @else
                                        <p>{{$Category->en_title}}</p>
                                        @endif
                                        @endforeach
                                        @endif
                                    </td>
									    <td>
                                            <a class="btn btn-success" href="/Additions/{{$Meal->id}}"  >@if(Request::segment(1) == 'ar') الاضافات @else Additions  @endif</a>
                                        </td>
                                    @if(Auth::user()->category == 'vendor')
                                    <td class="text-nowrap">
                                        {{-- <a data-toggle="modal" data-target=".bs-Edit-modal-lg" class="edit btn btn-primary" href="#" data-id="{{$Meal->id}}" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-info" href="{{url('timesmeal/'.$Meal->id.'/meal')}}" data-original-title="Edit"><i class="fa fa-clock-o"></i></a> --}}
                                        <div class="btn-group action">
                                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-list"></i>
                                            </button>
                                            <div class="dropdown-menu animated rubberBand">
                                                {{-- <a class="dropdown-item" href="#">Action</a> --}}
                                                <a class="edit-kitchen dropdown-item edit" style="width:80%" href="#" data-id="{{$Meal->id}}" data-original-title="Edit">{{trans('word.Update Meal')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('timesmeal/'.$Meal->id)}}" data-original-title="Time">{{trans('word.Time')}}</a>
                                                <a class="dropdown-item" style="width:80%" href="{{url('images_meal/'.$Meal->id)}}" data-original-title="Image">{{trans('word.images')}}</a>
                                            </div>
                                        </div>
                                    </td>
                                    @endif
                                  </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

<!-- insert modal content -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h4 class="modal-title m-b-0 text-white" id="myLargeModalLabel">{{trans('word.Meals')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-block">
                                <form action="{{url('Store_Meal')}}" method="POST" enctype="multipart/form-data" novalidate>
                                    @csrf
                                    <div class="form-body">
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.English Title')}}</label>
                                                    <input type="hidden" name="Restaurant_id" value="{{$Restaurant->id}}">
                                                    <div class="controls">
                                                    <input type="text" id="en_title" name="en_title" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Arabic Title')}}</label>
                                                    <div class="controls">
                                                    <input type="text" id="ar_title" name="ar_title" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.English Description')}}</label>
                                                    <div class="controls">
                                                    <textarea type="text" id="en_description" name="en_description" class="form-control"required data-validation-required-message="{{trans('word.This field is required')}}"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Arabic Description')}}</label>
                                                    <div class="controls">
                                                    <textarea type="text" id="ar_description" class="form-control" name="ar_description"required data-validation-required-message="{{trans('word.This field is required')}}"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Tag')}}</label>
                                                    <select class="form-control custom-select tags_meals" name="tag" required data-validation-required-message="{{trans('word.This field is required')}}">
                                                        @foreach($Tags as $Tag)
                                                            <option value="{{$Tag->id}}" @if($Tag->id == 3) selected @endif>
                                                                @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                                    {{$Tag->ar_title}}
                                                                    @else
                                                                    {{$Tag->title}}
                                                                @endif
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Component')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                                    <div class="controls">
                                                    <input type="text" id="component" class="form-control" name="component">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 meal_category">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Meals Category')}}</label>
                                                    <div class="form-check">
                                                        <label class="custom-control custom-checkbox">
                                                            <input type="checkbox" checked id="select_all_meal_category" class="custom-control-input">
                                                            <span class="custom-control-indicator"></span>
                                                            <span class="custom-control-description">{{trans('word.Select All')}}</span>
                                                        </label>
                                                    </div>
                                                    <div class="controls">
                                                    @foreach($CategoriesMeals as $CategoryMeal)
                                                        <div class="form-check">
                                                            <label class="custom-control custom-checkbox">
                                                                <input type="checkbox" name="Eventcategory[]" checked value="{{$CategoryMeal->id}}" class="custom-control-input select_all_meal_category" data-validation-minchecked-minchecked="1" data-validation-minchecked-message="{{trans('word.Choose at least one')}}" >
                                                                <span class="custom-control-indicator"></span>
                                                                <span class="custom-control-description">
                                                                    @if( LaravelLocalization::getCurrentLocale() == "ar") {{$CategoryMeal->ar_title}} @else {{$CategoryMeal->en_title}} @endif
                                                                </span>
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 banquet_category">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Banquets Category')}}</label>
                                                    <div class="form-check">
                                                        <label class="custom-control custom-checkbox">
                                                            <input type="checkbox" id="select_all_banquet_category" class="custom-control-input">
                                                            <span class="custom-control-indicator"></span>
                                                            <span class="custom-control-description">{{trans('word.Select All')}}</span>
                                                        </label>
                                                    </div>
                                                    <div class="controls">
                                                    @foreach($CategoriesBanquets as $CategoryBanquet)
                                                      <div class="form-check">
                                                          <label class="custom-control custom-checkbox">
                                                              <input type="checkbox" name="Eventcategory[]"  value="{{$CategoryBanquet->id}}" class="custom-control-input select_all_banquet_category" data-validation-minchecked-minchecked="1" data-validation-minchecked-message="{{trans('word.Choose at least one')}}">
                                                              <span class="custom-control-indicator"></span>
                                                              <span class="custom-control-description">
                                                                    @if( LaravelLocalization::getCurrentLocale() == "ar")
                                                                    {{$CategoryBanquet->ar_title}}
                                                                    @else
                                                                    {{$CategoryBanquet->en_title}}
                                                                    @endif
                                                                </span>
                                                          </label>
                                                      </div>
                                                    @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/row-->

                                        <div class="row p-t-20">
                                            <!--/span-->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Price')}}</label>
                                                    <div class="controls">
                                                    <input type="number" name="price" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}"  step="0.01" min="0" oninput="validity.valid||(value='');">
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Preparation time')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                                    <input type="number" name="neededtime" class="form-control" min="0" oninput="validity.valid||(value='');">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Customer Number')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                                    <input type="number" name="customer_num" class="form-control" min="0" oninput="validity.valid||(value='');">
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.Master image Meal')}}</label>
                                                    <div class="controls">
                                                    <input type="file" name="image" class="form-control" required data-validation-required-message="{{trans('word.This field is required')}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">{{trans('word.images')}} <small class="text-danger">({{trans('word.optional')}})</small></label>
                                                    <div class="controls">
                                                    <input type="file" name="images[]" multiple class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{trans('word.Save')}}</button>
                                        <button type="button" class="btn btn-inverse" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Edit modal content -->
<div class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h4 class="modal-title m-b-0 text-white" id="myLargeModalLabel">{{trans('word.Edit Meal')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection

@section('js')
<script src="{{asset('cpanel/en/js/validation.js')}}"></script>
<!-- This is data table -->
<script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- Sweet-Alert  -->
<script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>
<script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>

    <script>
        var table = $('#myTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print',
        ],
        @if( LaravelLocalization::getCurrentLocale() == "ar")
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
            }
        @endif

    });

    if($(".tags_meals").val() == '1'){
        $('.meal_category').addClass('hide');
    }else{
        $('.banquet_category').addClass('hide');
    }


    $(".tags_meals").on('change', function() {
        if ($(this).val() == '1'){
            $('.banquet_category').removeClass('hide');
            $('.meal_category').addClass('hide');
            //
            $("#select_all_banquet_category").prop('checked', true);
            $(".select_all_banquet_category").prop('checked', true);
            $("#select_all_meal_category").prop('checked', false);
            $(".select_all_meal_category").prop('checked', false);
        } else {
            $("#select_all_banquet_category").prop('checked', false);
            $(".select_all_banquet_category").prop('checked', false);
            $("#select_all_meal_category").prop('checked', true);
            $(".select_all_meal_category").prop('checked', true);

            $('.banquet_category').addClass('hide');
            $('.meal_category').removeClass('hide');
        }
    });


    //select all checkboxes
    $("#select_all_meal_category").change(function(){  //"select all" change
        $(".select_all_meal_category").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
    });

    //".checkbox" change
    $('.select_all_meal_category').change(function(){
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if(false == $(this).prop("checked")){ //if this item is unchecked
            $("#select_all_meal_category").prop('checked', false); //change "select all" checked status to false
        }
        //check "select all" if all checkbox items are checked
        if ($('.select_all_meal_category:checked').length == $('.select_all_meal_category').length ){
            $("#select_all_meal_category").prop('checked', true);
        }
    });


    //select all checkboxes
    $("#select_all_banquet_category").change(function(){  //"select all" change
        $(".select_all_banquet_category").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
    });

    //".checkbox" change
    $('.select_all_banquet_category').change(function(){
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if(false == $(this).prop("checked")){ //if this item is unchecked
            $("#select_all_banquet_category").prop('checked', false); //change "select all" checked status to false
        }
        //check "select all" if all checkbox items are checked
        if ($('.select_all_banquet_category:checked').length == $('.select_all_banquet_category').length ){
            $("#select_all_banquet_category").prop('checked', true);
        }
    });

        $('.image-popups').magnificPopup({
            delegate: 'a',
            type: 'image',
            removalDelay: 500, //delay removal by X to allow out-animation
            callbacks: {
            beforeOpen: function() {
                // just a hack that adds mfp-anim class to markup
                this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                this.st.mainClass = this.st.el.attr('data-effect');
            }
            },
            closeOnContentClick: true,
            midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
        });

    $('#myTable tbody').on( 'click', 'tr', function (e) {
        if (e.ctrlKey) {
            $(this).toggleClass('selected');
            @if( LaravelLocalization::getCurrentLocale() == "ar")
            $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
            @else
            $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
            @endif
        }else{
            var isselected = false
            if($(this).hasClass('selected')){
                isselected = true
            }
            $('#myTable tbody tr').removeClass('selected');
            if(!isselected){
                $(this).toggleClass('selected');
            }
            @if( LaravelLocalization::getCurrentLocale() == "ar")
            $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
            @else
            $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
            @endif
        }
    });

    $('.image-popups').magnificPopup({
        delegate: 'a',
        type: 'image',
        removalDelay: 500, //delay removal by X to allow out-animation
        callbacks: {
            beforeOpen: function() {
                // just a hack that adds mfp-anim class to markup
                this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                this.st.mainClass = this.st.el.attr('data-effect');
            }
        },
        closeOnContentClick: true,
        midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });

$("body").on("click", "#delete", function () {
    var dataList=[];
    $("#myTable .selected").each(function(index) {
        dataList.push($(this).find('td:first').text())
    })
    if(dataList.length >0){
        swal({
            title: "{{trans('word.Are you sure?')}}",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "{{trans('word.Yes, Sure it!')}}",
            cancelButtonText: "{{trans('word.No')}}",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){
            if (isConfirm) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                url:'{{url("Delete_Meal")}}',
                type:"post",
                data:{'id':dataList,_token: CSRF_TOKEN},
                dataType:"JSON",
                success: function (data) {
                    if(data.message == "Success")
                    {
                        $("#myTable .selected").hide();
                        @if( LaravelLocalization::getCurrentLocale() == "ar")
                        $('#delete').text('حذف 0 سجل');
                        @else
                        $('#delete').text('Delete 0 row');
                        @endif
                        swal("{{trans('word.Deleted')}}", "{{trans('word.Message_Delete')}}", "success");
                    }else{
                        swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error");
                    }
                },
                fail: function(xhrerrorThrown){
                    swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error");
                }
            });
            } else {
                swal("{{trans('word.Cancelled')}}", "{{trans('word.Message_Cancelled_Delete')}}", "error");
            }
        });
    }
});

    $(".edit").click(function(){
        var id=$(this).data('id')
        $.ajax({
            type: "GET",
            url: "{{url('Edit_Meal')}}",
            data: {"id":id},
            success: function (data) {
                $(".bs-edit-modal-lg .modal-body").html(data)
                $(".bs-edit-modal-lg").modal('show')
                $(".bs-edit-modal-lg").on('hidden.bs.modal',function (e){
                    //   $('.bs-edit-modal-lg').empty();
                    $('.bs-edit-modal-lg').hide();
                })
            }
        })
    })

    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation()
    }(window, document, jQuery);

    $(".switchery-demo").click(function(){
      var id=$(this).data('id')
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
        type: "POST",
         url: "{{url('UpdateStatusMeal')}}",
         data: {"id":id,_token:CSRF_TOKEN},
         success: function (data) {
          }
      })
    })


    jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
    });
</script>
<?php
$message=session()->get("message");
?>
@if( session()->has("message"))
  @if( $message == "Success")
    <script>
        swal({
            title: "{{trans('word.Succeeded')}}",
            text: "{{trans('word.Operation completed successfully')}}",
            type:"success" ,
            timer: 1000,
            showConfirmButton: false
        });
    </script>
  @elseif ( $message == "Failed")
    <script>
        swal({
            title: "{{trans('word.Sorry')}}",
            text: "{{trans('word.the operation failed')}}",
            type:"error" ,
            timer: 2000,
            showConfirmButton: false
        });
    </script>
  @endif
@endif


@endsection
