<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-block">
                <form action="{{url('Update_Additions')}}" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>{{trans('word.ar_title')}}</h5>
                                    <input type="text" name="ar_title" value="{{$Additions->ar_title}}" class="form-control">
                                    <input type="hidden" name="id" value="{{$Additions->id}}">
                                    <div class="form-control-feedback"><small>{{trans('word.optional')}}</small></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <h5>{{trans('word.en_title')}}</h5>
                                    <input type="text" name="en_title" value="{{$Additions->en_title}}" class="form-control">
                                    <div class="form-control-feedback"><small>{{trans('word.optional')}}</small></div>
                                </div>
                            </div>
                        </div>
                                        
                                        <div class="row">
  <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>@if(Request::segment(1) == 'ar') السعر   @else price @endif</h5>
                                                    <input type="text" name="price" class="form-control" required value="{{$Additions->price}}">
                                                </div>
                                            </div>
                                             <div class="col-md-6">
                                <div class="form-group">
                                    <h5>@if(Request::segment(1) == 'ar')  التفعيل  @else Confirm  @endif</h5>
                                    <select type="text" name="confirm" class="form-control" required>
                                      @if(Request::segment(1) == 'ar')
                                      @if($Additions->confirm == 1)
                                      <option selected value='1' >   مفعل  </option>
                                      <option value='0' >  غير مفعل   </option>
                                      @else
                                      <option  value='1' >   مفعل  </option>
                                      <option   selected  value='0' >  غير مفعل   </option>
                                      @endif
                                      @else
                                      @if($Additions->confirm == 1)
                                      <option selected value='1' >   active  </option>
                                      <option value='0' >  inActive   </option>
                                      @else
                                      <option value='1' >   active  </option>
                                      <option selected value='0' >  inActive   </option>
                                      @endif
                                      @endif
                                    </select>
                                </div>
                            </div>
                                          
                                           
                                        </div>
                        

                        
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> {{trans('word.Save')}}</button>
                        <button type="button" class="btn btn-inverse" data-dismiss="modal">{{trans('word.Cancel')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{asset('cpanel/assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="{{asset('cpanel/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('cpanel/en/js/validation.js')}}"></script>
    <script>
    ! function(window, document, $) {
            "use strict";
            $("input,select").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);

        jQuery('#date-range').datepicker({
                toggleActive: true,
        });

        $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>
