@extends('layouts.cpanel_layout.master')
@section('title')
@if(Request::segment(1) == 'ar')
الاضافات 
@else
Additions
@endif
@endsection

@section('css')
<!--alerts CSS -->

<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/magnific-popup.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset('cpanel/assets/plugins/dropify/dist/css/dropify.min.css')}}">
<!-- Date picker plugins css -->
<link href="{{asset('cpanel/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="text-themecolor m-b-0 m-t-0">
                    @if(Request::segment(1) == 'ar')
                    الاضافات 
                    @else
                    Additions
                    @endif
            </h3>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">
                  @if(Request::segment(1) == 'ar')
                الصفحة الرئيسية
                @else
                Home
                @endif</a></li>
                <li class="breadcrumb-item active">
                        @if(Request::segment(1) == 'ar')
                        الاضافات 
                        @else
                        Additions
                        @endif
                </li>
            </ol>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <button id="delete" class="btn pull-right hidden-sm-down btn-danger m-l-10"> @if(Request::segment(1) == 'ar')  حذف @else Delete @endif</button>
            <button data-toggle="modal" data-target=".bs-example-modal-lg" class="btn pull-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> @if(Request::segment(1) == 'ar')  اضافة @else Create @endif</button>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    @if(session()->has('message'))
    <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4>نجاح <i class="icon fa fa-check"></i></h4>
    {{ session()->get('message') }}
  </div>
@endif
@if(session()->has('message_delete'))
    <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4>نجاح <i class="icon fa fa-check"></i></h4>
    {{ session()->get('message_delete') }}
  </div>
@endif
@if(session()->has('error_message'))
<div class="alert alert-danger alert-dismissible">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<h4>عفوا <i class="icon fa fa-close"></i></h4>
{{ session()->get('error_message') }}
</div>
@endif
    @if($errors->any())
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i>warning!</h4>
        <ul>
          @foreach($errors->all() as $error)
          <li style="font-weight: 40"><h4 style="font-weight:40">{{$error}}</h4></li>
          @endforeach
        </ul>
      </div>
    @endif

    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered display" style="width:100%">
                            <thead>
                                <tr>
                                        <th>#</th>

                                    <th>@if(Request::segment(1) == 'ar') الاسم   @else  title @endif</th>
                                    <th>@if(Request::segment(1) == 'ar') السعر    @else price  @endif</th>
                                    <th>@if(Request::segment(1) == 'ar') التفعيل   @else  confirm  @endif</th>
                                    <th>@if(Request::segment(1) == 'ar') تم الانشاء بواسطة    @else  created by  @endif</th>
                                    <th>@if(Request::segment(1) == 'ar') تم التعديل بواسطة    @else  updated by  @endif</th>
                                    <th>@if(Request::segment(1) == 'ar')  تعديل   @else Edit @endif</th>
                                </tr>
                            </thead>
                            <tbody>
                              @foreach($Additions as $Cat)
                                <tr id="{{$Cat->id}}">
                                    <th>{{$Cat->id}}</th>
                                    <td>
                                        @if( Request::segment(1) == "ar")
                                        {{$Cat->ar_title}}
                                        @else
                                        {{$Cat->en_title}}
                                        @endif
                                    </td>
                                    <td>
                                            {{$Cat->price}}
                                        </td>
                                    <td>
                                      @if(Request::segment(1) == 'ar')
                                      @if($Cat->confirm == 1)
                                        مفعل
                                      @else
                                        غير مفعل
                                      @endif
                                      @else
                                      @if($Cat->confirm == 1)
                                        Active
                                      @else
                                        inActive
                                      @endif
                                      @endif
                                    </td>
                                    @inject('user','App\User')
                                    <td>
                                      @if($user->find($Cat->created_by))

                                      {{$user->find($Cat->created_by)->name}}

                                      @endif
                                    </td>
                                    <td>
                                      @if($user->find($Cat->updated_by))

                                      {{$user->find($Cat->updated_by)->name}}

                                      @endif
                                    </td>
                                    <td>
                                        <a class="edit-Advert btn btn-info" href="#" data-id="{{$Cat->id}}" data-original-title="Edit">@if(Request::segment(1) == 'ar')  تعديل   @else Edit @endif</a>
                                    </td>
                                  </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>

<!-- insert modal content -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h3 class="modal-title m-b-0 text-white" id="myLargeModalLabel">@if(Request::segment(1) == 'ar') اضافة جديده   @else Create  @endif</h3>
                <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-outline-info">
                            <div class="card-block">
                                <form action="/Store_Additions" method="post" enctype="multipart/form-data" novalidate>
                                    @csrf
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>@if(Request::segment(1) == 'ar')  الاسم باللغة العربية  @else Arabic Title  @endif</h5>
                                                    <input type="text" name="ar_title" class="form-control" required>
													   <input type="hidden" name="Meal_id" value="{{$id}}" class="form-control" required>

                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>@if(Request::segment(1) == 'ar') الاسم باللغة الانجليزية   @else English Title @endif</h5>
                                                    <input type="text" name="en_title" class="form-control" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
  <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>@if(Request::segment(1) == 'ar') السعر   @else price @endif</h5>
                                                    <input type="text" name="price" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h5>@if(Request::segment(1) == 'ar')  التفعيل  @else Confirm  @endif</h5>
                                                    <select type="text" name="confirm" class="form-control" required>
                                                      @if(Request::segment(1) == 'ar')
                                                      <option value='1' >   مفعل  </option>
                                                      <option value='0' >  غير مفعل   </option>
                                                      @else
                                                      <option value='1' >   active  </option>
                                                      <option value='0' >  inActive   </option>
                                                      @endif
                                                    </select>
                                                </div>
                                            </div>
                                          
                                           
                                        </div>

                                       
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> @if(Request::segment(1) == 'ar')  حفظ @else Save @endif</button>
                                        <button type="button" class="btn btn-inverse" data-dismiss="modal">@if(Request::segment(1) == 'ar')  اللغاء @else Cancel @endif</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- /.modal -->
<div class="modal fade bs-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h3 class="modal-title text-white" id="myLargeModalLabel">{{trans('word.Edit Advertisement')}}</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection

@section('js')
    <!-- This is data table -->
    <script src="{{asset('cpanel/en/js/validation.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('cpanel/assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js')}}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{asset('cpanel/assets/plugins/switchery/dist/switchery.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/sweetalert/sweetalert.min.js')}}"></script>
    <!-- jQuery file upload -->
    <script src="{{asset('cpanel/assets/plugins/dropify/dist/js/dropify.min.js')}}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{asset('cpanel/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

    <script>



        //DataTable
        var table = $('#myTable').DataTable({
            dom: 'Bfrtip',
            "ordering":false,
            @if( Request::segment(1) == "ar")
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
                }
            @endif
        });
        //End DataTable

            //Select Row From Table
            $('#myTable tbody').on( 'click', 'tr', function () {
                if (event.ctrlKey) {
                    $(this).toggleClass('selected');
                    @if( Request::segment(1) == "ar")
                    $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
                    @else
                    $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
                    @endif
                }else{
                    var isselected = false
                    var numSelected= table.rows('.selected').data().length
                    if($(this).hasClass('selected') && numSelected==1){
                        isselected = true
                    }
                    $('#myTable tbody tr').removeClass('selected');
                    if(!isselected){
                        $(this).toggleClass('selected');
                    }
                    @if( Request::segment(1) == "ar")
                    $('#delete').text('حذف '+ table.rows('.selected').data().length +' سجل');
                    @else
                    $('#delete').text('Delete '+ table.rows('.selected').data().length +' row');
                    @endif
                }
            });
            //End Select Row From Table

            $('.image-popups').magnificPopup({
                delegate: 'a',
                type: 'image',
                removalDelay: 500, //delay removal by X to allow out-animation
                callbacks: {
                beforeOpen: function() {
                    // just a hack that adds mfp-anim class to markup
                    this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
                    this.st.mainClass = this.st.el.attr('data-effect');
                }
                },
                closeOnContentClick: true,
                midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
            });


            jQuery('#date-range').datepicker({
                toggleActive: true,
            });

            //Delete Row
            $("body").on("click", "#delete", function () {

                var dataList=[];
                $("#myTable .selected").each(function(index) {
                    dataList.push($(this).find('td:first').text())
                })
                if(dataList.length >0){
                    swal({
                        title: "{{trans('word.Are you sure?')}}",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "{{trans('word.Yes, Sure it!')}}",
                        cancelButtonText: "{{trans('word.No')}}",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    }, function(isConfirm){
                        console.log(dataList)

                        if (isConfirm) {
                            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                            $.ajax({
                            url:'{{url("Delete_Additions")}}',
                            type:"GET",
                            data:{'id':dataList},
                            dataType:"JSON",
                            success: function (data) {
                                console.log(data)
                                if(data.message == "Success")
                                {
                                    $("#myTable .selected").hide();
                                    @if( Request::segment(1) == "ar")
                                    $('#delete').text('حذف 0 سجل');
                                    @else
                                    $('#delete').text('Delete 0 row');
                                    @endif
                                    swal("{{trans('word.Deleted')}}", "{{trans('word.Message_Delete')}}", "success");
                                }else{
                                    swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error");
                                }
                            },
                            fail: function(xhrerrorThrown){
                                swal("{{trans('word.Sorry')}}", "{{trans('word.Message_Fail_Delete')}}", "error");
                            }
                        });
                        } else {
                            swal("{{trans('word.Cancelled')}}", "{{trans('word.Message_Cancelled_Delete')}}", "error");
                        }
                    });
                }
            });
            //End Delete Row

    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });


        ! function(window, document, $) {
            "use strict";
            $("input,select").not("[type=submit]").jqBootstrapValidation()
        }(window, document, jQuery);

        $(".switchery-demo").click(function(){
            var id=$(this).data('id')
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: "get",
                url: "{{url('UpdateStatusAdvert')}}",
                data: {"id":id,_token:CSRF_TOKEN},
                success: function (data) {
                }
            })
        })

    $(".edit-Advert").click(function(){
      var id=$(this).data('id')
      var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
        type: "GET",
         url: "{{url('Edit_Additions')}}",
         data: {"id":id},
         success: function (data) {
              $(".bs-edit-modal-lg .modal-body").html(data)
              $(".bs-edit-modal-lg").modal('show')
              $(".bs-edit-modal-lg").on('hidden.bs.modal',function (e){
                //   $('.bs-edit-modal-lg').empty();
                  $('.bs-edit-modal-lg').hide();
              })
          }
      })
    })

    jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
    });
</script>
<?php
$message=session()->get("message");
?>
@if( session()->has("message"))
    @if( $message == "Success")
        <script>
            swal({
                title: "{{trans('word.Succeeded')}}",
                text: "{{trans('word.Operation completed successfully')}}",
                type:"success" ,
                timer: 1000,
                showConfirmButton: false
            });
        </script>
    @elseif ( $message == "Failed")
        <script>
            swal({
                title: "{{trans('word.Sorry')}}",
                text: "{{trans('word.the operation failed')}}",
                type:"error" ,
                timer: 2000,
                showConfirmButton: false
            });
        </script>
    @endif
@endif


@endsection
