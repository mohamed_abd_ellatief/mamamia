<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="@if(session()->get('locale') == 'ar') {{'rtl'}} @else {{ 'ltr'}} @endif">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">`
    <meta name="author" content="">
    <!-- Favicon icon -->
    @inject('Setting','App\Mainsettings')
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('cpanel/upload/mainSetting/'.$Setting->find(1)->logo)}}">
    <title>{{$Setting->find(1)->title}} || @yield('title')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('cpanel/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    @if( session()->get('locale') == "ar")
    <style>
        body{
            direction: rtl;
        }
        .dropdown-menu {
            position: absolute;
            top: 100%;
            left: 0;
            z-index: 1000;
            display: none;
            float: left;
            min-width: 10rem;
            padding: .5rem 0;
            margin: .125rem 0 0;
            font-size: 1rem;
            color: #292b2c;
            text-align: left;
            list-style: none;
            background-color: #fff;
            -webkit-background-clip: padding-box;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, .15);
            border-radius: .25rem
        }
    </style>
    <link href="{{asset('cpanel/assets/plugins/bootstrap-rtl-master/dist/css/custom-bootstrap-rtl.css')}}" rel="stylesheet">
    @else
    <style>
        .dropdown-menu {
            position: absolute;
            top: 100%;
            right: 0;
            z-index: 1000;
            display: none;
            float: left;
            min-width: 10rem;
            padding: .5rem 0;
            margin: .125rem 0 0;
            font-size: 1rem;
            color: #292b2c;
            text-align: left;
            list-style: none;
            background-color: #fff;
            -webkit-background-clip: padding-box;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, .15);
            border-radius: .25rem
        }
    </style>
    @endif
    <link href="{{asset('cpanel/en/css/animate.min.css')}}" rel="stylesheet">

    @yield('css')
    <!-- Custom CSS -->
    @if( session()->get('locale') == "ar")
    <link href="{{asset('cpanel/ar/css/style.css')}}" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{asset('cpanel/ar/css/colors/blue.css')}}" id="theme" rel="stylesheet">
    @else
    <link href="{{asset('cpanel/en/css/style.css')}}" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{asset('cpanel/en/css/colors/blue.css')}}" id="theme" rel="stylesheet">
    @endif
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-85622565-1', 'auto');
    ga('send', 'pageview');
    </script>





</head>

<body class="fix-header fix-sidebar card-no-border">

    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                  <!-- Logo -->
                    <!-- ============================================================== -->
                    <div class="navbar-header" style="background-color: initial">
                            {{-- <a class="navbar-brand" href="{{url('DashBoard')}}">
                                <!-- Logo icon -->
                                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                                        <!-- Dark Logo icon -->
                                        <img src="{{asset('cpanel/assets/images/logo-text.png')}}" alt="homepage" class="dark-logo" />
                                        <!-- Light Logo icon -->

                                <!--End Logo icon -->
                                <!-- Logo text -->
                            </a> --}}
                            <a class="navbar-brand" href="{{url('/')}}">
                                <!-- Logo icon -->
                                <b>
                                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                                    <!-- Dark Logo icon -->
                                    <img src="{{asset('cpanel/upload/mainSetting/'.$Setting->find(1)->logo)}}" alt="homepage" class="dark-logo" />
                                    <!-- Light Logo icon -->
                                    <img src="{{asset('cpanel/upload/mainSetting/'.$Setting->find(1)->logo)}}" alt="homepage" class="light-logo" />
                                </b>
                                <!--End Logo icon -->
                                <!-- Logo text -->
                                <span>
                                    <!-- dark Logo text -->
                                    <img src="{{asset('cpanel/upload/mainSetting/'.$Setting->find(1)->image)}}" alt="homepage" class="dark-logo" />
                                    <!-- Light Logo text -->
                                    <img src="{{asset('cpanel/upload/mainSetting/'.$Setting->find(1)->image)}}" class="light-logo" alt="homepage" />
                                </span>
                            </a>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End Logo -->
                    <!-- ============================================================== -->

                <div class="navbar-collapse">

                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0 ">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>


                        <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        @if(Auth::user()->category == 'vendor' || Auth::user()->category == 'admin')
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bell-o"></i>
                                <div class="notify" id="notfic">
                                    @inject('Notifications','App\Notification')
                                    @if($Notifications->data() == true)
                                    <span class="label label-danger">{{count($Notifications->data())}}</span>
                                    @endif
                                </div>
                            </a>
                            <div class="dropdown-menu mailbox animated bounceInDown" style="left:0">
                                <ul>
                                    <li>
                                        <div class="drop-title">{{__('word.Notifications')}}</div>
                                    </li>
                                    <li>
                                        <div class="message-center" id="NotifiyData">
                                            <!-- Message -->
                                            @if($Notifications->data() == true)
                                            @foreach($Notifications->data() as $Notification)
                                            @if(LaravelLocalization::getCurrentLocale() == 'ar')
                                            <a href="@if(Auth::user()->category == 'admin')/Vendors @else # @endif" class="Ordered" data-type="{{$Notification->type_order}}" data-id="{{$Notification->order_id}}">
                                                <div class="mail-contnet">
                                                    <h5></h5>
                                                    <span class="mail-desc">{{$Notification->ar_description}}</span>
                                                    <span class="time">{{$Notification->ar_ago}}</span>
                                                </div>
                                            </a>
                                            @else
                                            <a href="@if(Auth::user()->category == 'admin')/Vendors @else # @endif" class="Ordered" data-type="{{$Notification->type_order}}" data-id="{{$Notification->order_id}}">
                                                <div class="mail-contnet">
                                                    <h5></h5>
                                                    <span class="mail-desc">{{$Notification->en_description}}</span>
                                                    <span class="time">{{$Notification->en_ago}}</span>
                                                </div>
                                            </a>
                                            @endif
                                            @endforeach
                                            @endif
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="@if(Auth::user()->category == 'admin' ) /Vendors @elseif(Auth::user()->category == 'vendor') {{url('allOrders')}} @endif"> <strong>{{__('word.Check all notifications')}}</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        @endif
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->

                    </ul>

                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="/" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{asset('cpanel/upload/user/'.Auth::user()->image)}}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="{{asset('cpanel/upload/user/'.Auth::user()->image)}}" alt="user"></div>
                                            <div class="u-text">
                                                <h4>{{Auth::user()->name}}</h4>
                                                <p class="text-muted">{{Auth::user()->email}}</p></div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                            
                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off"></i> {{__('word.Logout')}}</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;" >
                                    @csrf
                                    </form>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            @if(session()->get('locale') == "en")
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flag-icon flag-icon-us"></i>
                            </a>
                            @else
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="flag-icon flag-icon-sa"></i>
                            </a>
                            @endif
                            <div class="dropdown-menu  dropdown-menu-right animated bounceInDown">

                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <a class="dropdown-item" rel="alternate" hreflang="{{ $localeCode }}" href="lang/{{ $localeCode }}">
                                            <i class="flag-icon flag-icon-{{ $properties['flag'] }}"></i> {{ $properties['title'] }}
                                        </a>
                                @endforeach
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="{{asset('cpanel/upload/user/'.Auth::user()->image)}}" alt="user" /> </div>
                    <!-- User profile text-->
                    <div class="profile-text"> <a href="#" class="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">{{Auth::user()->name}} <span class="caret"></span></a>
                        <div class="dropdown-menu animated flipInY">
                            @if(Auth::user()->category == "vendor")
                                <a href="{{ url("Profile") }}" class="dropdown-item"><i class="ti-user"></i> {{__('word.My Profile')}}</a>
                            @else
                                <a href="{{ url("ProfileCashier") }}" class="dropdown-item"><i class="ti-user"></i> {{__('word.My Profile')}}</a>
                            @endif
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="fa fa-power-off"></i> {{__('word.Logout')}}</a>
                        </div>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        @if(Auth::user()->category == "vendor")
                        <li><a class="" href="{{url('DashBoard')}}" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">{{__('word.Main')}}</span></a></li>
                        <li><a class="" href="{{url('/kitchens')}}" aria-expanded="false"><i class="mdi mdi-bowl"></i><span class="hide-menu">{{__('word.Kitchen')}}</span></a></li>
                        @elseif(Auth::user()->category == "cashier")
                        <li><a class="" href="{{url('orders_cashier')}}" aria-expanded="false"><i class="mdi mdi-bowl"></i><span class="hide-menu">{{__('word.Orders')}}</span></a></li>
                        <li><a class="" href="{{url('invoice_cashier')}}" aria-expanded="false"><i class="mdi mdi-bowl"></i><span class="hide-menu">{{__('word.Invoices')}}</span></a></li>
                        <li><a class="" href="{{url('archive_cashier')}}" aria-expanded="false"><i class="mdi mdi-bowl"></i><span class="hide-menu">{{__('word.Archives')}}</span></a></li>
                        <li><a class="" href="{{url('banquet_cashier')}}" aria-expanded="false"><i class="mdi mdi-bowl"></i><span class="hide-menu">{{__('word.Banquets')}}</span></a></li>
                        <li><a class="" href="{{url('invoiceBanquet_cashier')}}" aria-expanded="false"><i class="mdi mdi-bowl"></i><span class="hide-menu">{{__('word.Invoices Banquets')}}</span></a></li>
                        <li><a class="" href="{{url('archiveBanquet_cashier')}}" aria-expanded="false"><i class="mdi mdi-bowl"></i><span class="hide-menu">{{__('word.Archives Banquets')}}</span></a></li>
                        {{-- <li><a class="" href="{{url('invoiceBanquet_cashier')}}" aria-expanded="false"><i class="mdi mdi-bowl"></i><span class="hide-menu">{{__('word.Orders')}}</span></a></li> --}}
                        @elseif(Auth::user()->category == "admin")
                        <li><a class="" href="{{url('/')}}" aria-expanded="false"><i class="mdi mdi-chart-areaspline"></i><span class="hide-menu">{{__('word.Main')}}</span></a></li>
						                        <li><a class="" href="{{url('/Users')}}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">@if(Request::segment(1) =='ar') العملاء @else Users @endif</span></a></li>




                           


  <li><a class="has-arrow" id="all_drivers" href="{{url('/all_drivers')}}" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">
@if(Request::segment(1) =='ar') السائقين @else السائقين @endif
                        </a>
                          <ul>
                            <li><a class="" id="all_drivers" href="{{url('/all_drivers')}}" aria-expanded="false"><i class="mdi mdi-panorama"></i><span class="hide-menu">@if(Request::segment(1) =='ar') السائثين @else كل السائقين  @endif</span></a></li>

                        <li><a class="" id="all_drivers" href="{{url('/driver_income')}}" aria-expanded="false"><i class="mdi mdi-panorama"></i><span class="hide-menu">@if(Request::segment(1) =='ar') السائثين @else دخل السائقين  @endif</span></a></li>

                        <li><a class="" id="driver_order" href="{{url('/driver_order')}}" aria-expanded="false"><i class="mdi mdi-panorama"></i><span class="hide-menu">@if(Request::segment(1) =='ar') السائثين @else طلبات السائقين  @endif</span></a></li>

                       
                        
                      </ul>
                    </li>



                                                     
                                                       
                                                     

                        <li><a class="" href="{{url('/Vendors')}}" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">{{__('word.Vendors')}}</span></a></li>
                        <li><a class="" href="{{url('/Advertisement')}}" aria-expanded="false"><i class="mdi mdi-panorama"></i><span class="hide-menu">{{__('word.Advertisement')}}</span></a></li>
                        <li><a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">
                          @if(Request::segment(1) == 'ar')  الايردات  @else Revenue  @endif
                        </a>
                          <ul>
                        <li><a class="" href="{{url('/Profit')}}" aria-expanded="false"><i class="mdi mdi-panorama"></i><span class="hide-menu">@if(Request::segment(1) == 'ar')  اجمالي الايرادات   @else All Revenue  @endif</span></a></li>
                        <li><a class="" href="{{url('/ProfitYear')}}" aria-expanded="false"><i class="mdi mdi-panorama"></i><span class="hide-menu">@if(Request::segment(1) == 'ar')  الايرادات السنوية    @else  Revenue In Year  @endif</span></a></li>
                        <li><a class="" href="{{url('/ProfitMonth')}}" aria-expanded="false"><i class="mdi mdi-panorama"></i><span class="hide-menu">@if(Request::segment(1) == 'ar') الايرادات في الشهر   @else  Revenue In Month @endif</span></a></li>
                        
                      </ul>
                    </li>
                    <li><a class="" href="{{url('/Income')}}" aria-expanded="false"><i class="mdi mdi-panorama"></i><span class="hide-menu">@if(Request::segment(1) == 'ar') الفواتير المحصلة   @else Invoices received
                            Invoices received
                            @endif</span></a></li>

                        <li><a class="" href="{{url('/Setting')}}" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">{{__('word.Setting')}}</span></a></li>
                        <li><a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">
                          @if(LaravelLocalization::getCurrentLocale() == 'ar')
                      الطلبات

                      @else
                      Orders
                      @endif

                    </a>
                      <ul aria-expanded="false" class="collapse">
                           <li><a class="has-arrow" href="/All_Orders" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">
                             كل الطلبات
                           </a>
                         </li>
                           <li><a class="has-arrow" href="/AllOrders/0" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">
                             الطلبات النشطة
                           </a>
                         </li>

                         <li><a class="has-arrow" href="/AllOrders/1" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">
                           الطلبات المنتهية
                         </a>
                       </li>
                       <li><a class="has-arrow" href="/AllOrders/-1" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">
                         الطلبات المرفوضة
                        </a>
                     </li>
                       <li><a class="has-arrow" href="/AllOrders/-2" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">
                         الطلبات الملغاء
                        </a>
                     </li>
                      </ul>
                    </li>

                          <li><a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">
                                @if(LaravelLocalization::getCurrentLocale() == 'ar')
                            البائعين
                            @else
                            Vendors
                            @endif
                           </a>
                           @inject('Vendors','App\User')
                           @inject('Kitchens','App\Kitchen')

                           <ul aria-expanded="false" class="collapse">
                               @foreach($Vendors->where('category','vendor')->where('status',1)->get() as $vendor)
                                <li><a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">
                                   {{$vendor->name}}
                                </a>
                                             <ul aria-expanded="false" class="collapse">
                                                 @foreach($Kitchens->where('user_id',$vendor->id)->get() as $kitchen)
                                                    <li><a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">
                                                        @if(Request::segment(1) == 'ar')
                                                        {{$kitchen->ar_title}}
                                                        @else
                                                        {{$kitchen->en_title}}
                                                        @endif
                                                    </a>
                                                    <ul aria-expanded="false" class="collapse">
                                                            <li><a href="{{url('Meals/'.$kitchen->id)}}">{{__('word.Meals')}}</a></li>
                                                            <li><a href="{{url('Orders/'.$kitchen->id)}}">{{__('word.Orders')}}</a></li>
                                                            <li><a href="{{url('Invoices/'.$kitchen->id)}}">{{__('word.Invoices')}}</a></li>
                                                            <li><a href="{{url('Archives/'.$kitchen->id)}}">{{__('word.Archives')}}</a></li>
                                                            <li><a href="{{url('Banquets/'.$kitchen->id)}}">{{__('word.Banquets')}}</a></li>
                                                            <li><a href="{{url('Invoicesbanquets/'.$kitchen->id)}}">{{__('word.Invoices Banquets')}}</a></li>
                                                            <li><a href="{{url('Archivesbanquets/'.$kitchen->id)}}">{{__('word.Archives Banquets')}}</a></li>
                                                            <li><a href="{{url('Days/'.$kitchen->id)}}">{{__('word.Work Days')}}</a></li>
                                                            <li><a href="{{url('Rules/'.$kitchen->id)}}">{{__('word.Rules')}}</a></li>
                                                            <li><a href="{{url('Images_kitchen/'.$kitchen->id)}}">{{__('word.images')}}</a></li>
                                                            <li><a href="{{url('Cashiers/'.$kitchen->id)}}">{{__('word.Cashier')}}</a></li>
                                                        </ul>
                                                </li>
                                                @endforeach
                                             </ul>
                                    </li>
                                @endforeach
                           </ul>
                        </li>
                        @endif
                        @inject('Kitchens','App\Kitchen')
                        @if(count($Kitchens->Kitchens())>0)
                        @if(LaravelLocalization::getCurrentLocale() == 'ar')
                        <li class="nav-small-cap">المطاعم</li>
                        @else
                        <li class="nav-small-cap">KITCHENS</li>
                        @endif
                        @foreach($Kitchens->Kitchens() as $Kitchen)
                        <li>
                            @if(LaravelLocalization::getCurrentLocale() == 'ar')
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">{{$Kitchen->ar_title}}</span></a>
                            @else
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">{{$Kitchen->en_title}}</span></a>
                            @endif
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{url('meals/'.$Kitchen->id)}}">{{__('word.Meals')}}</a></li>
                                <li><a href="{{url('orders/'.$Kitchen->id)}}">{{__('word.Orders')}}</a></li>
                                <li><a href="{{url('invoices/'.$Kitchen->id)}}">{{__('word.Invoices')}}</a></li>
                                <li><a href="{{url('archives/'.$Kitchen->id)}}">{{__('word.Archives')}}</a></li>
                                <li><a href="{{url('banquets/'.$Kitchen->id)}}">{{__('word.Banquets')}}</a></li>
                                <li><a href="{{url('invoicesbanquets/'.$Kitchen->id)}}">{{__('word.Invoices Banquets')}}</a></li>
                                <li><a href="{{url('archivesbanquets/'.$Kitchen->id)}}">{{__('word.Archives Banquets')}}</a></li>
                                <li><a href="{{url('days/'.$Kitchen->id)}}">{{__('word.Work Days')}}</a></li>
                                <li><a href="{{url('rules/'.$Kitchen->id)}}">{{__('word.Rules')}}</a></li>
                                <li><a href="{{url('images_kitchen/'.$Kitchen->id)}}">{{__('word.images')}}</a></li>
                                <li><a href="{{url('cashiers/'.$Kitchen->id)}}">{{__('word.Cashier')}}</a></li>
                            </ul>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->

              @yield('content')
           {{--    @if( session()->get('locale') == "en")
              {{ "mohamed "}}
              @else 
              {{ " محمد"}}
              @endif --}}
              <footer class="footer">
                © 2018 MamaMia by Invetects.com
            </footer>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->


<!-- Edit modal content -->
<div class="modal fade bs-OrderMaster-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content card card-outline-info">
            <div class="modal-header card-header">
                <h4 class="modal-title m-b-0 text-white" id="myLargeModalLabel">{{__('word.Order Info')}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{asset('cpanel/assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('cpanel/assets/plugins/bootstrap/js/tether.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    @if( session()->get('locale') == "ar")
    <script src="{{asset('cpanel/ar/js/jquery.slimscroll.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('cpanel/ar/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('cpanel/ar/js/sidebarmenu.js')}}"></script>
    @else
    <script src="{{asset('cpanel/en/js/jquery.slimscroll.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('cpanel/en/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('cpanel/en/js/sidebarmenu.js')}}"></script>
    @endif
    <!--stickey kit -->
    <script src="{{asset('cpanel/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>
    @if( session()->get('locale') == "ar")
    <script src="{{asset('cpanel/ar/js/custom.min.js')}}"></script>
    @else
    <script src="{{asset('cpanel/en/js/custom.min.js')}}"></script>
    @endif
    <!-- ============================================================== -->
    @yield('js')

    <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = false;

        var pusher = new Pusher('1f67708ec361e19834e9', {
        cluster: 'mt1',
        forceTLS: true
        });

        var channel = pusher.subscribe('my-channel');
        channel.bind('my-event', function(data) {
            var user_id = JSON.stringify(data.user_id)
            var count = `<span class="label label-danger">`+JSON.stringify(data.count)+`</span>`;
            if(user_id == {{Auth::user()->id}}){
                if(JSON.stringify(data.count) == 0){
                $('#notfic').html(null);
                }else{
                $('#notfic').html(count);
                }
                $( "#NotifiyData" ).load( '{{url("NotificationLoad")}}' );
            }
        });

    $(".Ordered").click(function(){
        var id=$(this).data('id')
        var type_order =$(this).data('type')
        //   var CSRF_TOKEN = {{ csrf_field() }};
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        if(type_order == "meal"){
            $.ajax({
                type: "POST",
                url: "{{url(LaravelLocalization::getCurrentLocale().'/OrderDetails')}}",
                data: {"id":id,_token:CSRF_TOKEN},
                success: function (data) {

                    $(".bs-OrderMaster-modal-lg .modal-body").html(data)
                    $(".bs-OrderMaster-modal-lg").modal('show')

                    $(".bs-OrderMaster-modal-lg").on('hidden.bs.modal',function (e){
                        $('.bs-OrderMaster-modal-lg').hide();
                    })
                }
            })
        }else if(type_order == "banquet"){
            $.ajax({
                type: "POST",
                url: "{{url(LaravelLocalization::getCurrentLocale().'/BanquetDetails')}}",
                data: {"id":id,_token:CSRF_TOKEN},
                success: function (data) {

                    $(".bs-OrderMaster-modal-lg .modal-body").html(data)
                    $(".bs-OrderMaster-modal-lg").modal('show')

                    $(".bs-OrderMaster-modal-lg").on('hidden.bs.modal',function (e){
                        $('.bs-OrderMaster-modal-lg').hide();
                    })
                }
            })
        }
    })


    </script>
    <script>
		        $(".notfic-demo").click(function(){
            $.ajax({
                type: "get",
                url: "{{url('NotificationCountEdit')}}",
				success: function (data) {
                    var alert=``;
                    if(data == 1){
                        alert=`<p class="label label-success">{{__('word.Activted')}}</p>`;
                    } else {
                        alert=`<p class="label label-danger">{{__('word.No Activted')}}</p>`;
                    }
                }
            })
        })


	</script>
    <?php
    $messag=session()->get("message");
    ?>
    @if( session()->has("message"))
      @if( $messag == "accepted")
        <script>
        swal("Successfully!", "Order has been successfully accepted.", "success");
        </script>
      @elseif( $messag == "sent")
        <script>
        swal("Successfully!", "Order has been successfully Sent.", "success");
        </script>
      @elseif ( $messag == "rejected")
        <script>
        swal("Rejected", "Order has been rejected. :(", "error");
        </script>
      @endif
    @endif
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{asset('cpanel/assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>
</body>

</html>
