<!DOCTYPE html>
<html lang="en" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('cpanel/assets/images/favicon.png')}}">
    <title>MamaMia Login</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('cpanel/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    @if( LaravelLocalization::getCurrentLocale() == "ar")
    <link href="{{asset('cpanel/assets/plugins/bootstrap-rtl-master/dist/css/custom-bootstrap-rtl.css')}}" rel="stylesheet">
    @endif
    <!-- Custom CSS -->
    @if( LaravelLocalization::getCurrentLocale() == "ar")
    <link href="{{asset('cpanel/ar/css/style.css')}}" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{asset('cpanel/ar/css/colors/blue.css')}}" id="theme" rel="stylesheet">
    @else
    <link href="{{asset('cpanel/en/css/style.css')}}" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="{{asset('cpanel/en/css/colors/blue.css')}}" id="theme" rel="stylesheet">
    @endif
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
            
    </div>

    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
  @yield('content');
  <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{asset('cpanel/assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('cpanel/assets/plugins/bootstrap/js/tether.min.js')}}"></script>
    <script src="{{asset('cpanel/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    
    <script src="{{asset('cpanel/en/js/jquery.slimscroll.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('cpanel/en/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('cpanel/en/js/sidebarmenu.js')}}"></script>
    <!--stickey kit -->
    <script src="{{asset('cpanel/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('cpanel/en/js/custom.min.js')}}"></script>
    @yield('js')
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="{{asset('cpanel/assets/plugins/styleswitcher/jQuery.style.switcher.js')}}"></script>
</body>

</html>