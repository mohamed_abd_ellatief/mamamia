<li class="nav-item">
  <a class="nav-link" data-toggle="collapse" href="#users" aria-expanded="false" aria-controls="users">
    <i class="menu-icon mdi mdi-account-circle"></i>
    <span class="menu-title">Users</span>
    <i class="menu-arrow"></i>
  </a>
  <div class="collapse" id="users">
    <ul class="nav flex-column sub-menu">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('vendors.index') }}">View Vendors</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('users.index') }}">View Users</a>
      </li>
    </ul>
  </div>
</li>



<li class="nav-item">
  <a class="nav-link" data-toggle="collapse" href="#kitchens" aria-expanded="false" aria-controls="kitchens">
    <i class="menu-icon mdi mdi-food"></i>
    <span class="menu-title">Kitchens</span>
    <i class="menu-arrow"></i>
  </a>
  <div class="collapse" id="kitchens">
    <ul class="nav flex-column sub-menu">

      <li class="nav-item">
        <a class="nav-link" href="{{ route('kitchens.index') }}">View All Kitchens</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ route('kitchenscategory.index') }}">View All Kitchens Categories</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ route('kitchenstag.index') }}">View All Kitchens Tags</a>
      </li>

    </ul>
  </div>
</li>
<li class="nav-item">
  <a class="nav-link" data-toggle="collapse" href="#meals" aria-expanded="false" aria-controls="meals">
    <i class="menu-icon mdi mdi-food"></i>
    <span class="menu-title">Meals</span>
    <i class="menu-arrow"></i>
  </a>
  <div class="collapse" id="meals">
    <ul class="nav flex-column sub-menu">

      <li class="nav-item">
        <a class="nav-link" href="{{ route('meals.index') }}">View All Meals</a>
      </li>

      {{-- <li class="nav-item">
        <a class="nav-link" href="{{ route('kitchenscategory.index') }}">View All Kitchens Categories</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="{{ route('kitchenstag.index') }}">View All Kitchens Tags</a>
      </li> --}}

    </ul>
  </div>
</li>

{{-- <li class="nav-item">
  <a class="nav-link" data-toggle="collapse" href="#services" aria-expanded="false" aria-controls="services">
    <i class="menu-icon mdi mdi-headset"></i>
    <span class="menu-title">Services</span>
    <i class="menu-arrow"></i>
  </a>
  <div class="collapse" id="services">
    <ul class="nav flex-column sub-menu">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('servicecategory.index') }}">Services Categories</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('servicetags.index') }}">Services Tags</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('services.index') }}">View All Services</a>
      </li>
    </ul>
  </div>
</li> --}}
