<center>
<div class="row">
<div class="col-lg-12">
<div class="card card-outline-info">
<div class="card-block">

<div class="row">
<div class="col-md-12">
<div class="form-group row">
<h3 class="card-title">{{trans('word.Component')}}</h3>
<div class="col-md-12">
<table class="table color-table inverse-table">
<thead>
<tr>
<th>#</th>
<th>{{trans('word.Title')}}</th>
<th>{{trans('word.Quantity')}} × {{trans('word.Price')}}</th>
<th>{{trans('word.total price')}}</th>
</tr>
</thead>
</table>
</div>
</div>
</div>
</div>
<!--/row-->
<hr>
<div class="row">
<div class="col-md-6">
<div class="form-group row">
<label class="control-label  col-md-5 text-bold text-info">رقم الطلب</label>
<div class="col-md-7">
<p class="form-control-static"> {{$order->id}} </p>
</div>
</div>
</div>
<!--/span-->
<div class="col-md-6">
<div class="form-group row">
<label class="control-label col-md-5 text-bold text-info">{{trans('word.Status')}}</label>
<div class="col-md-7">
@if($order->status == 1)
<p><span class="badge badge-pill badge-success">{{trans('word.accepted')}}</span></p>
@elseif($order->status == -1)
<p><span class="badge badge-pill badge-danger">{{trans('word.rejected')}}</span></p>
@elseif($order->status == 0)
<p><span class="badge badge-pill badge-info">{{trans('word.new')}}</span></p>
@elseif($order->status == -2)
<p><span class="badge badge-pill badge-danger">@if(Request::segment(1) == 'ar') ملغاء @else Cancelled @endif</span></p>
@endif
</div>
</div>
</div>
<!--/span-->
</div>
<hr>
<div class="row">
<div class="col-md-6">
<div class="form-group row">
<label class="control-label col-md-5 text-bold text-info">{{trans('word.Customer Name')}} </label>
<p class="form-control-static col-md-7"> {{$user->name}}</p>
</div>
</div>
<div class="col-md-6">
<div class="form-group row">
<label class="control-label col-md-5 text-bold text-info">{{trans('word.Mobile')}} </label>
@if(\App\User::find($order->user_id))
<p class="form-control-static col-md-7"> {{\App\User::find($order->user_id)->mobilenumber}}</p>
@else
<p class="form-control-static col-md-7">تم حذف بيانات العميل</p>
@endif
</div>
</div>
</div>
<hr>
<div class="row">
<div class="col-md-6">
<div class="form-group row">
<label class="control-label col-md-5 text-bold text-info">تاريخ الطلب  </label>
<p class="form-control-static col-md-7"> {{$order->created_at->toDateString()}}</p>
</div>
</div>
<div class="col-md-6">
<div class="form-group row">
<label class="control-label col-md-5 text-bold text-info">وقت الطلب  </label>
<p class="form-control-static col-md-7"> {{$order->created_at->format("h:i A")}}</p>
</div>
</div>
</div>
<hr>
<div class="row">
<div class="col-md-12">
<div class="form-group row">
العنوان 
<p class="form-control-static text-center col-md-12"> {{$order->userplace}}</p>
</div>
</div>
</div>
<hr>
<div class="row">
<div class="col-md-12">
<div class="form-group row">
<label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Preparation')}}: </label>

</div>
</div>
</div>
<hr>

<div class="row">
<div class="col-md-12">
<div class="form-group row">
<label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Kitchen Name')}}: </label>
<p class="form-control-static text-center col-md-12">
@inject('Kitchen_Name','App\Kitchen')
@if( LaravelLocalization::getCurrentLocale() == "ar")
{{$kitchen->ar_title}}
@else
{{$kitchen->en_title}}
@endif
</p>
</div>
</div>
</div>
<hr>
<div class="row">
<div class="col-md-12">
<div class="form-group row">
<label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.distance to user')}}: </label>
<p class="form-control-static text-center col-md-12">
{{$order->distancetouser}} {{trans('word.km')}}
</p>
</div>
</div>
</div>
<hr>
<div class="row">
<div class="col-md-12">
<div class="form-group row">
<label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.delivery total price')}}: </label>
<p class="form-control-static text-center col-md-12">
{{$order->deliverytotalprice}}
</p>
</div>
</div>
</div>
<hr>
<div class="row">
<div class="col-md-12">
<div class="form-group row">
<label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Vat')}}: </label>
<p class="form-control-static text-center col-md-12">
{{$order->priceVat}}
</p>
</div>
</div>
</div>
<hr>
<div class="row">
<div class="col-md-6">
<div class="form-group row">
<label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Discount Price')}}: </label>
<p class="form-control-static text-center col-md-12">
{{$order->discountpoint}} {{trans('word.SR')}}
</p>
</div>
</div>
<div class="col-md-6">
<div class="form-group row">
<label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.Discount Points')}}: </label>
<p class="form-control-static text-center col-md-12">
{{$order->usepoint}}
</p>
</div>
</div>
</div>
<hr>
<div class="row">
<div class="col-md-12">
<div class="form-group row">
<label class="control-label text-center col-md-12 text-bold text-info">{{trans('word.total price')}}: </label>
<p class="form-control-static text-center col-md-12">
{{$order->totalprice}}
</p>
</div>
</div>
</div>
<hr>


</div>
</div>
</div>


</div>
</div>
</div>
</div>
</center>