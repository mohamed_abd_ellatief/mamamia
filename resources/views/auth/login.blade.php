@extends('layouts.cpanel_layout.login_layout')

@section('content')
<section id="wrapper">
<div class="login-register" style="background-image:url({{url('cpanel/assets/images/background/rawpixel-754031-unsplash.jpg')}});">
    
    <div class="login-box card">
        <div class="card-block">
            <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('login') }}">
              @csrf
                <h3 class="box-title m-b-20">{{trans('word.Sign In')}}</h3>
                <div class="form-group ">
                    <label class="form-label">{{trans('word.Mobile')}}</label>
                    <div class="col-xs-12">
                        <input class="form-control" type="text" name="mobilenumber" required=""> 
                        {{-- @if ($errors->has('mobilenumber'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('mobilenumber') }}</strong>
                            </span>
                        @endif --}}
                    </div>
                </div>
                <div class="form-group">
                        <label class="form-label">{{trans('word.Password')}}</label>
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="password" required=""> 
                        {{-- @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif --}}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <a href="javascript:void(0)" id="to-recover" class="text-dark">
                            <i class="fa fa-lock m-r-5"></i> {{trans('word.Forgot Password?')}}
                        </a> 
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{trans('word.Log In')}}</button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>{{trans("word.Don't have an account?")}} <a href="{{url('register')}}" class="text-info m-l-5"><b>{{trans('word.Sign Up')}}</b></a></p>
                    </div>
                </div>
            </form>
            <form class="form-horizontal" id="recoverform" method="POST" action="/password/reset">
				@csrf
                <div class="form-group ">
                    <div class="col-xs-12">
                        <h3>{{trans('word.Recover Password')}}</h3>
                        <p class="text-muted">{{trans('word.Enter your Mobile and instructions will be sent to you!')}} </p>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="form-label">{{trans('word.Mobile')}}</label>
                    <div class="col-xs-12">
                        <input class="form-control" type="text" required="" name="phone"> 
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{trans('word.Reset')}}</button>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
    
</section>
@endsection
