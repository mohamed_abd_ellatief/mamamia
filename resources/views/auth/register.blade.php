@extends('layouts.cpanel_layout.login_layout')

@section('content')

<section id="wrapper">
    <div class="login-register" style="background-image:url({{url('cpanel/assets/images/background/rawpixel-754031-unsplash.jpg')}});">        
        <div class="register-box card">
        <div class="card-block">
            <form class="form-horizontal form-material" id="loginform" method="POST" action="/CreateUser" >
                @csrf
                <h3 class="box-title m-b-20">{{trans('word.Sign Up')}}</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label class="form-label">{{trans('word.Name')}}</label>
                                <div class="controls">
                                    <input class="form-control" name="name" type="text" required data-validation-required-message="{{trans('word.This field is required')}}">
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <div class="col-xs-12">
                                <label class="form-label">{{trans('word.Email')}}</label>
                                <input class="form-control" name="email" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="form-label">{{trans('word.Mobile')}}</label>
                        <div class="controls">
                            <input class="form-control" name="mobilenumber" type="text" required data-validation-required-message="{{trans('word.This field is required')}}" data-validation-containsnumber-regex="(\d)+" data-validation-containsnumber-message="{{trans('word.No Characters Allowed, Only Numbers')}}" data-validation-regex-regex="(05)[0-9]{8}" data-validation-regex-message="{{trans('word.Invalid Mobile Number')}}">
                        </div>
                        @if ($errors->has('mobilenumber'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('mobilenumber') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label class="form-label">{{trans('word.Password')}}</label>
                                <div class="controls">
                                    <input class="form-control" name="password" type="password" minlength="4" required data-validation-required-message="{{trans('word.This field is required')}}" data-validation-minlength-message="{{trans('word.Too short: Minimum of "4" characters')}}">
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label class="form-label">{{trans('word.Confirm Password')}}</label>
                                <div class="controls">
                                    <input class="form-control" type="password"  name="password_confirmation" required data-validation-match-match="password" data-validation-match-message="{{trans('word.Password Must match')}}" data-validation-required-message="{{trans('word.This field is required')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="checkbox checkbox-success p-t-0 p-l-10">
                                    <div class="controls">
                                        <input id="checkbox-signup" type="checkbox" name="terms" checked required data-validation-required-message="{{trans('word.Conditions must be approved')}}">
                                        <label for="checkbox-signup"> {{trans('word.I agree to all')}} <a href="#" data-toggle="modal" data-target="#exampleModalLong">{{trans('word.Terms')}}</a></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">{{trans('word.Sign Up')}}</button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>{{trans('word.Already have an account?')}} <a href="{{url('login')}}" class="text-info m-l-5"><b>{{trans('word.Sign In')}}</b></a></p>
                    </div>
                </div>
            </form>
            
        </div>
      </div>
    </div>
    
  </section>

  <!-- Modal -->
  <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @if(Request::segment(1) == 'ar')
            <h2 style="text-align: center;text-decoration: underline;" class="text-danger">الأحكام والشروط</h2>
            <br>
            <p>يرجى قراءة هذه الشروط والأحكام بعناية قبل استخدام تطبيق ماميا  الذي نقوم بتشغيله من قبلنا
            <p>يعنى  وصولك إلى الخدمة واستخدامك لها على موافقتك لهذه الشروط والامتثال لها. تنطبق هذه الشروط على جميع الزوار والمستخدمين وغيرهم ممن يدخلون إلى الخدمة أو يستخدمونها .</p>
            <br>
            <p>بالوصول الى الخدمه او استخدامها ، فإنك توافق على الالتزام بهذه الشروط. إذا كنت لا توافق على أي جزء من الشروط ، فلا يجوز لك الوصول إلى الخدمة.</p>
            <br>
            <h3>المشتريات</h3>
            <br>
            <p>إذا كنت ترغب في شراء أي منتج أو خدمة يتم توفيرها من خلال الخدمة، قد يُطلب منك تقديم معلومات معينة متعلقة بمشترياتك بما في ذلك ، على سبيل المثال لا الحصر ...</p>
            <br>
            <h3>الاشتراكات</h3>
            <br>
            <p>تتم محاسبة بعض أجزاء الخدمة على أساس الاشتراك </p>
            <br>
            <h3>المحتوي</h3>
            <br>
            <p><p>تتيح لك خدمتنا إمكانية نشر ، أو ربط ، أو تخزين ، أو مشاركة ، أو توفير معلومات معينة ، أو نصوص ، أو رسوم ، أو مقاطع فيديو ، أو مواد أخرى. سوف تصبح انت المسئوول عن ذللك.</p>
            <br>
            <br>
            <hr>
            <br>
            <br>
            <h3>روابط لمواقع ويب أخرى</h3>
            <br>
            <p>قد تحتوي خدمتنا على روابط إلى مواقع ويب أو خدمات تابعة لجهات خارجية لا تمتلكهااو تتحكم فيها شركتنا</p>
            <br>
            <p>شركه تقنيات الابتكارلا تتحمل أي مسؤولية عن المحتوى أو سياسات الخصوصية أو الممارسات الخاصة بأي مواقع ويب أو خددمات خارجيه ولا نتحمل مسئوليه اى ضرر بشكل مباشر او غير مباشر الناتجه من مواقع او خدمات خارجيه .</p></p>
            <br>
            <br>
            <h3>التغييرات</h3>
            <br>
            <p>نحن نحتفظ بالحق ، وفقًا لتقديرنا الخاص ، في تعديل هذه الشروط أو استبدالها في أي وقت.</p>
            <br>
            <h3>اتصل بنا</h3>
            <br>
            <p>إذا كان لديك أي أسئلة حول هذه الشروط ، يرجى الاتصال بنا</p>
            <br>
            <br>
            <br>
            <p>شركة تقنيات الإبتكار للإستشارات التقنية والتحولات الرقميه</p>
             <p>www.invetechs.com</p>
            <br>
            <p>طريق صلاح الدين الأيوبي ، العمارة 7644-مكتب رقم 14 ، الرياض</p>
            <br>
            <h5>الهاتف</h5>
            <p> 0114880122</p>
            <br>
            <h5>البريد الإلكتروني:-</h5>
            <p>info@invetechs.com </p>
            
            @else
            <h2 style="text-align: center;text-decoration: underline;" class="text-danger">Terms and Conditions</h2>
<br>
<br>
<br>
<p>Please read these Terms and Conditions carefully before using the MamaMia application operated by us</p>
<p>Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</p>
<br>
<p>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>
<br>
<h3>Purchases</h3>
<br>
<p>If you wish to purchase any product or service made available through the Service ("Purchase"), you may be asked to supply certain information relevant to your Purchase including, without limitation …</p>
<br>
<h3>Subscriptions</h3>
<br>
<p>Some parts of the Service are billed on a subscription basis ("Subscription(s)"). You will be billed in advance on a recurring...</p>
<br>
<h3>Content</h3>
<br>
<p>Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material ("Content"). You are responsible for that .</p>
<br>
<h3>Links to Other Web Sites</h3>
<br>
<p>Our Service may contain links to third-party web sites or services that are not owned or controlled by Invetechs.</p>
<br>
<p>Invetechs has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree Invetechs shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.</p>
<br>
<br>
<h3>Changes</h3>
<br>
<p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time.</p>
<br>
<hr>
<br>
<h3>Contact Us</h3>
<br>
<p>If you have any questions about these Terms, please contact us.</p>
<br>
<br>
<p>Technology Innovation Company </p>
<p>www.invetechs.com</p>
<br>
<p>Salah Al-Din Al-Ayoubi Road ,Building 7644 ,Riyadh </p>
<br>
<h5>Telephone </h5>
<p>0114880122</p>
<br>
<h5>Email </h5>
<p> info@invetechs.com</p>
            @endif
        </div>
        <div class="modal-footer ">
          <button type="button" class="btn btn-info align-middle" data-dismiss="modal">{{trans('word.Agree')}}</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
<script src="{{asset('cpanel/en/js/validation.js')}}"></script>
<script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation()
    }(window, document, jQuery);
    
    
</script>
@endsection
