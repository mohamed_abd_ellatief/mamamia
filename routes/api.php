	<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/********************DriverApi's *****************************/
Route::post('driver_login', 'DriverController@login');
Route::post('driver_registration', 'DriverController@registration');
Route::post('driver_foroget_passworod', 'DriverController@foroget_passworod');
Route::post('driver_change_passworod', 'DriverController@change_passworod');
Route::post('driver_verify_verificationcode_forget', 'DriverController@verify_verificationcode_forget');
Route::post('driver_verify_verificationcode', 'DriverController@verify_verificationcode');
Route::group(['middleware' => ['ApiAuth']], function () {
    //
Route::post('nearst_order', 'DriverController@nearst_order');
Route::post('reject_order', 'DriverController@reject_order');
Route::post('accept_order', 'DriverController@accept_order');
Route::post('update_driver_location', 'DriverController@update_driver_location');
Route::post('Driver_Order', 'DriverController@Driver_Order');
Route::post('Driver_Orderdetails', 'DriverController@Driver_Orderdetails');
Route::post('Driver_Ready', 'DriverController@Driver_Ready');
Route::post('Driver_Deliver', 'DriverController@Driver_Deliver');
Route::post('Driver_history', 'DriverController@Driver_history');
Route::post('Driver_profile', 'DriverController@Driver_profile');
Route::post('Driver_Update_profile', 'DriverController@Driver_Update_profile');
Route::get('terms', 'AdminController\SettingController@terms');
Route::post('order_invoice', 'DriverController@order_invoice');
Route::post('Driver_income', 'DriverController@Driver_income');
Route::post('Driver_summary', 'DriverController@Driver_summary');
Route::get('order_status', 'DriverController@order_status');

});
Route::get('order_invoice_view', 'DriverController@order_invoice_view');


/********************AuthenticationApi *****************************/
Route::post('login', 'ApiController\AuthController@login');
Route::post('registration', 'ApiController\AuthController@registration');
Route::post('foroget_passworod', 'ApiController\AuthController@foroget_passworod');
Route::post('change_passworod', 'ApiController\AuthController@change_passworod');
Route::post('verify_verificationcode_forget', 'ApiController\AuthController@verify_verificationcode_forget');
Route::post('verify_verificationcode', 'ApiController\AuthController@verify_verificationcode');


Route::post('Orderdetails', 'ApiController\VendorControllers\OrderController@Orderdetails');
Route::group(['middleware' => 'auth:api'], function ($router) {

});

  Route::post('UpdateProfile', 'ApiController\AuthController@UpdateProfile');
  /******************** Vendor Api*****************************/
  Route::post('Getvendorprofile', 'ApiController\VendorController@Getvendorprofile');


  /******************** Kitchen Api*****************************/
    Route::post('Selectonvendorkitchens', 'ApiController\VendorControllers\KitchenController@Selectonvendorkitchens');
    Route::post('SelectonecKitchendata', 'ApiController\VendorControllers\KitchenController@SelectonecKitchendata');
    Route::post('GetTime', 'ApiController\VendorControllers\KitchenController@GetTime');
    Route::post('ChangeKitchenopenstatus', 'ApiController\VendorControllers\KitchenController@ChangeKitchenopenstatus');
    Route::post('checkuseractivation', 'ApiController\VendorControllers\KitchenController@checkuseractivation');
    Route::get('selectalldays', 'ApiController\VendorControllers\KitchenController@Selectalldays');
    Route::get('SelectallKitchensCategories', 'ApiController\VendorControllers\KitchenController@SelectallKitchensCategories');
    Route::get('SelectallKitchensTags', 'ApiController\VendorControllers\KitchenController@SelectallKitchensTags');
    Route::post('search', 'ApiController\VendorControllers\KitchenController@search');
    Route::get('Selectalltimescategories', 'ApiController\VendorControllers\KitchenController@Selectalltimescategories');


    /******************** Meals Api*****************************/
    Route::get('Selectalleventscategories', 'ApiController\VendorControllers\MealsController@Selectalleventscategories');
    Route::get('Selecteveryday', 'ApiController\VendorControllers\MealsController@Selecteveryday');
    Route::post('Selectonekitchenmeals', 'ApiController\VendorControllers\MealsController@Selectonekitchenmeals');
    Route::post('Selectonecmealdata', 'ApiController\VendorControllers\MealsController@Selectonecmealdata');
    Route::post('Changemealopenstatus', 'ApiController\VendorControllers\MealsController@Changemealopenstatus');


  /******************** Orders Api*****************************/
  Route::post('NewOrdersByVender', 'ApiController\VendorControllers\OrderController@NewOrdersByVender');
  Route::post('ArchivesByVender', 'ApiController\VendorControllers\OrderController@ArchivesByVender');
  Route::post('NewArchivesByVender', 'ApiController\VendorControllers\OrderController@NewArchivesByVender');
  Route::post('InvoiceByVender', 'ApiController\VendorControllers\OrderController@InvoiceByVender');
  Route::post('BanquetsByVender', 'ApiController\VendorControllers\OrderController@BanquetsByVender');
  Route::post('acceptorder', 'ApiController\VendorControllers\OrderController@acceptorder');
  Route::post('ignororder', 'ApiController\VendorControllers\OrderController@ignororder');
  Route::post('OrderAccept', 'ApiController\VendorControllers\OrderController@OrderAccept');


  /******************** Points Api*****************************/
  Route::post('Selectonevendorrules', 'ApiController\VendorControllers\PointsController@Selectonevendorrules');
  Route::post('Selectuserspoints', 'ApiController\VendorControllers\PointsController@Selectuserspoints');



  // //////////////////////// Api User ///////////////////////////

  ////////////////////////////Profile///////////////////////////
  Route::post('UpdateProfileUser', 'ApiController\UserController\ProfileController@update');


  ////////////////////////////Kitchen///////////////////////////
  Route::post('getKitchens', 'ApiController\UserController\KitchenController@Kitchens');
  Route::post('RestaurantDetails', 'ApiController\UserController\KitchenController@RestaurantDetails');

  /////////////////////////Meals//////////////////////////////
  Route::post('MealDetails', 'ApiController\UserController\MealController@MealDetails');
  Route::post('getMeals', 'ApiController\UserController\MealController@Meals');
  // Route::post('MealSearch', 'ApiController\UserController\MealController@MealSearch');


  ///////////////////////////Addresses\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  Route::post('getAddressesByUser', 'ApiController\UserController\AddressesController@getAddressesByUser');
  Route::post('insertAddressesByUser', 'ApiController\UserController\AddressesController@store');
  Route::post('updateAddressesByUser', 'ApiController\UserController\AddressesController@update');
  Route::post('deleteAddressesByUser', 'ApiController\UserController\AddressesController@delete');


  // /////////////////////////////Review////////////////////////
  Route::post('getReview', 'ApiController\UserController\ReviewController@getReview');
  Route::post('storeReview', 'ApiController\UserController\ReviewController@store');

  /////////////////////////////////Rules//////////////////////////////////
  Route::post('RulesByKitchen', 'ApiController\UserController\RuleController@RulesByKitchen');
  Route::post('PointsByUser', 'ApiController\UserController\RuleController@PointsByUser');


  /////////////////////////////////Orders/////////////////////////////
  Route::post('OrdersByUser', 'ApiController\UserController\OrderController@OrdersByUser');
  Route::post('InvoiceByUser', 'ApiController\UserController\OrderController@InvoiceByUser');
  Route::post('Store_banquetUser', 'ApiController\UserController\OrderController@Store_banquetUser');
  Route::post('OffersBanquetByUser', 'ApiController\UserController\OrderController@OffersBanquetByUser');
  Route::post('OrdersBanquetByUser', 'ApiController\UserController\OrderController@OrdersBanquetByUser');
  Route::post('AcceptOfferBanquetByUser', 'ApiController\UserController\OrderController@AcceptOfferBanquetByUser');
  Route::post('CancelOffersBanquetByUser', 'ApiController\UserController\OrderController@CancelOffersBanquetByUser');
  Route::post('RefusedOfferBanquetByUser', 'ApiController\UserController\OrderController@RefusedOfferBanquetByUser');
  Route::post('OrderDetailsCancelled', 'ApiController\UserController\OrderController@OrderDetailsCancelled');


  ////////////////////////////Advertisement////////////////////////////////////
  Route::post('Advertisement', 'ApiController\UserController\AdvertisementController@Advertisement');


  ////////////////////////////Notifications////////////////////////////////////
  Route::post('NotificationsByVendor', 'ApiController\UserController\OrderController@NotificationsByVendor');
  Route::post('NotificationByUser', 'ApiController\VendorControllers\NotificationController@NotificationByUser');
  Route::post('convertViewsNotificationUser', 'ApiController\UserController\NotificationController@convertViewsNotificationUser');
  Route::post('convertViewsNotificationVendor', 'ApiController\VendorControllers\NotificationController@convertViewsNotificationVendor');


  //////////////////////////////Cart/////////////////////////////////////////////
  Route::post('PointsUser', 'ApiController\UserController\CartController@PointsUser');
  Route::post('OrderCart', 'ApiController\UserController\CartController@OrderCart');
