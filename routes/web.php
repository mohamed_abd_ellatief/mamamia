<?php
if (!session()->has('locale')) {
            session()->put('locale', 'ar');
        }
        
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Artisan::call('php artisan route:cache');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::post('/CreateUser','AdminController\RegisterController@store');

Route::prefix(LaravelLocalization::setLocale())->group(function () {
  Auth::routes();
});

  Route::get('/lang/{locale}','HomeController@lang');

// Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name('logout');

Route::group([
  // 'middleware' => ,
	'middleware' => ['auth','admin' ]
], function(){
  Route::get('Profit','AdminController\ProfitController@index');
  Route::get('ProfitYear','AdminController\ProfitController@ProfitYear');
  Route::get('ProfitMonth','AdminController\ProfitController@ProfitMonth');

  Route::get('Income','AdminController\incomeTaxesController@index');
	
  Route::get('/Users', 'AdminController\UserController@index');
  Route::get('/Delete_Vendor/{dataList}', 'AdminController\VendorController@delete');

  Route::get('/','AdminController\HomeController@index');
  Route::get('Advertisement','AdminController\AdvertisementController@index');
  Route::get('Edit_Advert','AdminController\AdvertisementController@edit');
  Route::get('Edit_Vendor','AdminController\VendorController@edit');
  Route::get('UpdateStatusAdvert','AdminController\AdvertisementController@UpdateStatusAdvert');
  Route::get('Setting','AdminController\SettingController@index');
  Route::get('Vendors','AdminController\VendorController@index');  
  Route::get('Vendor/{id}','AdminController\VendorController@Vendor');

  Route::get("/ProfileCashier","ProfileController@profile");


  Route::get('Meals/{id}','CpanelController\CpanelMealsController@index');
  Route::get('AllOrders/{id}','CpanelController\CpanelOrderController@AllOrders');

  /************************Order Routes*******************************************/
  Route::get('Orders/{id}','CpanelController\CpanelOrderController@index');
  Route::get('Invoices/{id}','CpanelController\CpanelOrderController@invoices');
  Route::get('Archives/{id}','CpanelController\CpanelOrderController@archives');
  Route::get('Cashiers/{id}','CpanelController\CashierController@index');
  Route::get('Edit_Cashier','CpanelController\CashierController@edit');


    /************************Banquet Routes*******************************************/
    Route::get('Banquets/{id}','CpanelController\CpanelBanquetController@index');
    Route::get('Invoicesbanquets/{id}','CpanelController\CpanelBanquetController@invoices');
    Route::get('Archivesbanquets/{id}','CpanelController\CpanelBanquetController@archives');

    /************************Days Routes*******************************************/
    Route::get('Days/{id}','CpanelController\CpanelDayController@index');

    Route::get("/Rules/{id}","CpanelController\CpanelRuleController@index");
 Route::get('Images_kitchen/{id}','CpanelController\CpanelKitchensControllers\CpanelkitchensController@images');
    //driver routes
Route::get('all_drivers','AdminDriverController@index');
Route::post('driver_active','AdminDriverController@driver_active');
Route::post('driver_dis_active','AdminDriverController@driver_dis_active');
Route::get('DeleteDriver/{id}','AdminDriverController@DeleteDriver');
Route::post('Delete_multi_Driver','AdminDriverController@Delete_multi_Driver');
Route::get('DeleteDriverIncome/{id}','AdminDriverController@DeleteDriverIncome');
Route::post('Delete_multi_DriverIncome','AdminDriverController@Delete_multi_DriverIncome');
Route::get('add_form_driver','AdminDriverController@add_driver_form');
Route::post('add_driver_submit','AdminDriverController@add_driver_submit');


Route::get('add_form_driverIncome','AdminDriverController@add_driver_formIncome');
Route::post('add_driver_submitIncome','AdminDriverController@add_driver_submitIncome');


Route::get('edit/{id}','AdminDriverController@edit');
Route::post('update_driver_submit/{id}','AdminDriverController@update_driver_submit');
Route::get('driver_place/{lat}/{lng}','AdminDriverController@driver_place');
Route::get('driver_income','AdminDriverController@driver_income');
Route::post('pay','AdminDriverController@pay');
Route::get('driver_order','AdminDriverController@driver_order');
Route::get('All_Orders','AdminDriverController@all_driver_order');


});
Route::get('ganntChartAdmin','CpanelController\ChartController@ganntChartAdmin');

Route::post('UpdateStatusVendor','AdminController\VendorController@UpdateStatusVendor');
Route::post('Store_Vendor','AdminController\VendorController@store');
Route::post('Update_Vendor','AdminController\VendorController@update');

// Route::post('UpdateStatusVendor','AdminController\VendorController@UpdateStatusVendor');
Route::post('Store_User','AdminController\UserController@store');
// Route::post('Update_Vendor','AdminController\VendorController@update');

Route::post('Store_Advert','AdminController\AdvertisementController@store');
Route::post('Delete_Advert','AdminController\AdvertisementController@delete');
Route::post('Update_Advert','AdminController\AdvertisementController@update');
Route::post('UpdateSetting','AdminController\SettingController@update');

// income Taxes Routes
Route::post('StoreIncome','AdminController\incomeTaxesController@store');
// Additions route
Route::post('Store_Additions','CpanelController\AdditionsController@store');
Route::get('Delete_Additions','CpanelController\AdditionsController@delete');
Route::post('Update_Additions','CpanelController\AdditionsController@update');
Route::get('Edit_Additions','CpanelController\AdditionsController@edit');


Route::group([
  // 'middleware' => ,
  'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath','auth:web','cashier' ]
], function(){

  Route::get('orders_cashier','CashierController\OrderController@index');
  Route::get('invoice_cashier','CashierController\OrderController@invoice');
  Route::get('archive_cashier','CashierController\OrderController@archive');
  Route::get('banquet_cashier','CashierController\BanquetController@index');
  Route::get('invoiceBanquet_cashier','CashierController\BanquetController@invoice');
  Route::get('archiveBanquet_cashier','CashierController\BanquetController@archive');
  Route::get("/ProfileCashier","ProfileController@profile");


});

Route::group([
  // 'middleware' => ,
  'prefix' => LaravelLocalization::setLocale(),
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath','auth:web','vendor' ]
], function(){
Route::get('Additions/{id}','CpanelController\AdditionsController@index');


  // Route::get('/home', 'HomeController@index')->name('home');

  Route::get('DashBoard','CpanelController\CpanelHomeController@index');
  /************************Vendor Routes*******************************************/
  Route::resource('vendors','CpanelController\CpanelUsersControllers\CpanelVendorController');
  Route::get('activatevendor/{id}','CpanelController\CpanelUsersControllers\CpanelVendorController@activatevendor')->name('vendors.activate');
  Route::get('disactivatevendor/{id}','CpanelController\CpanelUsersControllers\CpanelVendorController@disactivatevendor')->name('vendors.disactivate');

  Route::resource('users','CpanelController\CpanelUsersControllers\CpanelUsersController');


  /************************Kitchens Routes*******************************************/
  Route::get('kitchens','CpanelController\CpanelKitchensControllers\CpanelkitchensController@index');
  Route::get('Edit_Kitchen','CpanelController\CpanelKitchensControllers\CpanelkitchensController@edit');
  Route::get('images_kitchen/{id}','CpanelController\CpanelKitchensControllers\CpanelkitchensController@images');


  Route::resource('kitchensimage','CpanelController\CpanelKitchensControllers\CpanelKitchensImagesController');
  Route::resource('kitchenscategory','CpanelController\CpanelKitchensControllers\CpanelKitchensCategoryController');
  Route::resource('kitchenstag','CpanelController\CpanelKitchensControllers\CpanelServictagsController');
  /************************Meals Routes*******************************************/
  Route::get('meals/{id}','CpanelController\CpanelMealsController@index');

  Route::get('Edit_Meal','CpanelController\CpanelMealsController@edit');


  Route::get('images_meal/{id}','CpanelController\CpanelMealsController@images');



  // Route::resource('mealimages','CpanelController\CpanelMealsimagesController');

  Route::get('auto-complete-city', 'AutoCompleteController@index')->name('auto-complete-city');



  /************************Order Routes*******************************************/
  Route::get('orders/{id}','CpanelController\CpanelOrderController@index');
  Route::get('invoices/{id}','CpanelController\CpanelOrderController@invoices');
  Route::get('archives/{id}','CpanelController\CpanelOrderController@archives');
  Route::get('cashiers/{id}','CpanelController\CashierController@index');
  Route::get('Edit_Cashier','CpanelController\CashierController@edit');


  /************************Banquet Routes*******************************************/
  Route::get('banquets/{id}','CpanelController\CpanelBanquetController@index');
  Route::get('invoicesbanquets/{id}','CpanelController\CpanelBanquetController@invoices');
  Route::get('archivesbanquets/{id}','CpanelController\CpanelBanquetController@archives');

  /************************Profile Routes*******************************************/



  /************************Days Routes*******************************************/
  Route::get('days/{id}','CpanelController\CpanelDayController@index');
  /************************Time Routes*******************************************/
  Route::get('times/{kitchen_id}/{day_id}','CpanelController\CpanelTimeController@index');
  Route::get('timesmeal/{meal_id}','CpanelController\CpanelTimeController@timesmeal');
  /************************Profile Routes*******************************************/
  Route::get("/Profile","ProfileController@profile");


  // Rules Controller
  Route::get("/rules/{id}","CpanelController\CpanelRuleController@index");

  // Notification
  Route::get('NotificationCount','CpanelController\CpanelNotificationController@NotificationCount');
  Route::get('NotificationData','CpanelController\CpanelNotificationController@NotificationData');
  Route::get('allOrders','CpanelController\CpanelNotificationController@index');
  Route::get('SaveNotifaction','CpanelController\CpanelNotificationController@SaveNotifaction');








// ////////////////////////////////////DashBoard Chart////////////////////////////////////
Route::get('ganntChart','CpanelController\ChartController@ganntChart');
Route::get('filter_Kitchen','CpanelController\ChartController@Kitchen');
Route::get('UsersTable','CpanelController\ChartController@UsersTable');
Route::get('ReviewsTable','CpanelController\ChartController@ReviewsTable');
Route::get('MostOffers','CpanelController\ChartController@most_offer');
Route::get('most_offerr','CpanelController\ChartController@most_offerr');
Route::get('hotDays','CpanelController\ChartController@hotDays');





  Route::get('convert2english/{string}','CpanelController\CpanelOrderController@convert2english');



  // Route::get('cashier_orders','CpanelController\CpanelOrderController@cashier_orders');






});
// Kitchen
Route::post('Store_Kitchen','CpanelController\CpanelKitchensControllers\CpanelkitchensController@store');
Route::post('Delete_Kitchen','CpanelController\CpanelKitchensControllers\CpanelkitchensController@delete');

Route::post('Update_Kitchen','CpanelController\CpanelKitchensControllers\CpanelkitchensController@update');
Route::post('UpdateStatusKitchen','CpanelController\CpanelKitchensControllers\CpanelkitchensController@UpdateStatus');

Route::post('Store_ImagesKitchen','CpanelController\CpanelKitchensControllers\CpanelkitchensController@Store_imagesKitchen');
Route::post('Delete_ImagesKitchen','CpanelController\CpanelKitchensControllers\CpanelkitchensController@Delete_images');


Route::post('Store_Meal','CpanelController\CpanelMealsController@store');
Route::post('Update_Meal','CpanelController\CpanelMealsController@update');
Route::post('Delete_Meal','CpanelController\CpanelMealsController@delete');
Route::post('UpdateStatusMeal','CpanelController\CpanelMealsController@UpdateStatus');
Route::post('UpdateOrder','CpanelController\CpanelOrderController@UpdateOrder');
Route::post('UpdateBanquet','CpanelController\CpanelBanquetController@UpdateOrder');
Route::post('acceptOrder2','CpanelController\CpanelOrderController@acceptOrder2');
Route::post('rejectOrder2','CpanelController\CpanelOrderController@rejectOrder2');
Route::post('acceptOrder','CpanelController\CpanelOrderController@acceptOrder');
Route::post('rejectOrder','CpanelController\CpanelOrderController@rejectOrder');
Route::post('rejectBanquet','CpanelController\CpanelBanquetController@rejectOrder');
Route::post('storeDaysToKitchen','CpanelController\CpanelDayController@storeDaysToKitchen');
Route::post('DeleteKitchenDays','CpanelController\CpanelDayController@DeleteKitchenDays');
Route::post('{lang}/OrderDetails','CpanelController\CpanelOrderController@OrderDetails');
Route::post('{lang}/BanquetDetails','CpanelController\CpanelBanquetController@BanquetDetails');
Route::post('{lang}/receipt','CpanelController\CpanelOrderController@receipt');

Route::post('storeTimeToKitchen','CpanelController\CpanelTimeController@storeTimeToKitchen');
Route::post('DeleteKitchenTimes','CpanelController\CpanelTimeController@DeleteKitchenTimes');

Route::post("/UpdateProfile","ProfileController@update");
Route::post('insertUpdateRule','CpanelController\CpanelRuleController@insertUpdateRule');

Route::post('Delete_ImagesMeal','CpanelController\CpanelMealsController@Delete_images');
Route::post('StoreImagesMeal','CpanelController\CpanelMealsController@StoreimagesMeal');
// //////////////////////////////Cashier////////////////////////////////
Route::post('Store_Cashier','CpanelController\CashierController@store');
Route::post('Update_Cashier','CpanelController\CashierController@update');
Route::post('UpdateStatusCashier','CpanelController\CashierController@UpdateStatusCashier');
Route::post('UpdateKitchenCashier','CpanelController\CashierController@UpdateKitchenCashier');

// Time Meal

Route::post('storeTimeToMeal','CpanelController\CashierController@store');












    // Test Notifiy
    Route::get('Notifiy','NotifiyController@Notifiy');
    Route::get('NotificationLoad', function () {
      return view('cpanel.notification.Data');
  });
